//
//  Localizations.h
//  U3AN
//
//  Created by satheesh on 12/1/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#ifndef localize
#define localize(key) [[Localizations sharedInstance] localizedStringForKey:key]
#endif

#import <Foundation/Foundation.h>

@interface Localizations : NSObject

@property (nonatomic, retain) NSBundle* fallbackBundle;
@property (nonatomic, retain) NSBundle* preferredBundle;

@property (nonatomic, copy) NSString* fallbackLanguage;
@property (nonatomic, copy) NSString* preferredLanguage;

-(NSString*) localizedStringForKey:(NSString*)key;

-(NSString*) pathForFilename:(NSString*)filename type:(NSString*)type;

+(Localizations *)sharedInstance;

-(void) setPreferred:(NSString*)preferred fallback:(NSString*)fallback;

@end
