//
//  FAQViewController.m
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "FAQViewController.h"

@interface FAQViewController ()

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self loadPageDetails];
}


-(void)updateViewHeading
{
    self.faqWebView.layer.cornerRadius = 2.0;
    self.faqWebView.layer.masksToBounds = YES;
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)loadPageDetails
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getPageDetailsWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     
                     PageDetails *pageItem = [ApplicationDelegate.mapper getPageDetailFromDictionary:responseDictionary];
                     NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.faqWebView.frame.size.width-10))];
                     
                     
                     NSString *formattedHTMLText = [NSString stringWithFormat:@"<html> \n"
                                                    "<head> \n"
                                                    "<style type=\"text/css\"> \n"
                                                    "body {font-family: \"%@\"; font-size: %@;} b {font-weight:bolder; font-size:%@; }\n"
                                                    "</style> \n"
                                                    "</head> \n"
                                                    "<body><div style=\"width: %@; word-wrap: break-word\">%@</div></body> \n"
                                                    "</html>", @"BREESERIF-REGULAR", [NSNumber numberWithInt:11],[NSNumber numberWithInt:11],widthVal, pageItem.page_Data];
                     [self.faqWebView loadHTMLString:[NSString stringWithFormat:@"<div style=\"width: %@; word-wrap: break-word; color: #ffffff;\">%@</div>",widthVal,formattedHTMLText] baseURL:nil];
                    // [self.faqWebView loadHTMLString:pageItem.page_Data baseURL:nil];
                 }
             }
             [ApplicationDelegate removeProgressHUD];
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
    
}



-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:@"faq" forKey:@"PageName"];
    
    
    return postDic;
}


@end
