//
//  FeedBackViewController.m
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "FeedBackViewController.h"
#define MAXLENGTH 25
@interface FeedBackViewController ()
{
    NSString *design_Feedback,*usability_Feedback,*ordering_Feedback;
    
    CustomerAddressDetails *selectedAddressItem;
}

@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
    [self.designGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.designGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.designVGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.designVGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.designExellentBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.designExellentBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    
    [self.usabilityGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.usabilityGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.usabilityVGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.usabilityVGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.usabilityExellentBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.usabilityExellentBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    
    [self.orderingGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.orderingGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.orderingVGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.orderingVGoodBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.orderingExellentBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.orderingExellentBttn setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self setupUI];
}


-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.feedBackScrollView.layer.cornerRadius = 3.0;
    self.feedBackScrollView.layer.masksToBounds = YES;
    self.feedBackScrollView.contentOffset = CGPointZero;
    
    [self.giveFeedbackLbl setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    self.descriptionLbl.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
    self.nameLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.emailLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.commentLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.qnLbl1.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.qnLbl2.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.qnLbl3.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    
    self.nameTxtField.text = @"";
    self.emailTxtField.text = @"";
    self.commentTxtField.text = @"";
    
    [self websiteDesignRadioBttnAction:self.designGoodBttn];
    [self usabilityRadioBttnAction:self.usabilityGoodBttn];
    [self orderingRadioBttnAction:self.orderingGoodBttn];
    
    if ([ApplicationDelegate isLoggedIn])
    {
        [self getCustomerAddresses];
    }
    
    self.feedBackScrollView.contentSize = CGSizeMake(self.feedBackScrollView.frame.size.width,  self.submitBttn.frame.origin.y+self.submitBttn.frame.size.height+50);
}

- (IBAction)backgroundTapAction:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)websiteDesignRadioBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (sender.tag==11) {
        self.designGoodBttn.selected = YES;
        self.designVGoodBttn.selected = NO;
        self.designExellentBttn.selected = NO;
        design_Feedback=@"Good";
        
    }
    else if (sender.tag==12)
    {
        self.designGoodBttn.selected = NO;
        self.designVGoodBttn.selected = YES;
        self.designExellentBttn.selected = NO;
        design_Feedback=@"VeryGood";
        
    }
    else if (sender.tag==13)
    {
        self.designGoodBttn.selected = NO;
        self.designVGoodBttn.selected = NO;
        self.designExellentBttn.selected = YES;
        design_Feedback=@"Exellent";
        
    }
    
}
- (IBAction)usabilityRadioBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (sender.tag==21) {
        self.usabilityGoodBttn.selected = YES;
        self.usabilityVGoodBttn.selected = NO;
        self.usabilityExellentBttn.selected = NO;
        usability_Feedback=@"Good";
        
    }
    else if (sender.tag==22)
    {
        self.usabilityGoodBttn.selected = NO;
        self.usabilityVGoodBttn.selected = YES;
        self.usabilityExellentBttn.selected = NO;
        usability_Feedback=@"VeryGood";
        
    }
    else if (sender.tag==23)
    {
        self.usabilityGoodBttn.selected = NO;
        self.usabilityVGoodBttn.selected = NO;
        self.usabilityExellentBttn.selected = YES;
        usability_Feedback=@"Exellent";
        
    }
    
}

- (IBAction)orderingRadioBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (sender.tag==31) {
        self.orderingGoodBttn.selected = YES;
        self.orderingVGoodBttn.selected = NO;
        self.orderingExellentBttn.selected = NO;
        ordering_Feedback=@"Good";
        
    }
    else if (sender.tag==32)
    {
        self.orderingGoodBttn.selected = NO;
        self.orderingVGoodBttn.selected = YES;
        self.orderingExellentBttn.selected = NO;
        ordering_Feedback=@"VeryGood";
        
    }
    else if (sender.tag==33)
    {
        self.orderingGoodBttn.selected = NO;
        self.orderingVGoodBttn.selected = NO;
        self.orderingExellentBttn.selected = YES;
        ordering_Feedback=@"Exellent";
        
    }
    
}
- (IBAction)submittFeedbackBttnAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    if (self.emailTxtField.text.length>0 && self.nameTxtField.text.length>0) {
        
        
            if ( ![ApplicationDelegate isEmailValidWithEmailID:self.emailTxtField.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                self.emailTxtField.text=@"";
                
            }
            else
            {
                [self postFeedback];

            }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
    }
}



-(void)postFeedback
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine postFeedbackWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             [ApplicationDelegate removeProgressHUD];
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                 {
                     [ApplicationDelegate showAlertWithMessage:@"Your feedback submitted successfully" title:nil];
                     [self setupUI];
                 }
                 else
                 {
                     [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                 }
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }

}


-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:self.nameTxtField.text forKey:@"Name"];
    [postDic setObject:self.emailTxtField.text forKey:@"Email"];
    [postDic setObject:self.commentTxtField.text forKey:@"Comments"];
    [postDic setObject:design_Feedback forKey:@"DesignFeedback"];
    [postDic setObject:usability_Feedback forKey:@"UsabilityFeedback"];
    [postDic setObject:ordering_Feedback forKey:@"OrderProcess"];
    
    return postDic;
}

#pragma mark - TEXT FIELD DELEGATES

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGFloat yFact;
    

            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 0; // ToolBar Inclusived case
            }
            else
            {
                yFact = 80; // ToolBar Inclusived case
            }
    if (textField.superview.frame.origin.y>yFact)
    {
        [self.feedBackScrollView setContentOffset:CGPointMake(self.feedBackScrollView.contentOffset.x, (textField.superview.frame.origin.y-yFact))];
    }
    else
    {
        [self.feedBackScrollView setContentOffset:CGPointZero animated:YES];
    }
    
}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//
//    //[self.feedBackScrollView setContentOffset:CGPointZero animated:YES];
//
//    if (textField == self.emailTxtField)
//    {
//        if (textField.text.length!=0)
//        {
//            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
//            {
//                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
//                self.emailTxtField.text=@"";
//                
//            }
//        }
//    }
//    [textField resignFirstResponder];
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   // [self.feedBackScrollView setContentOffset:CGPointZero animated:YES];
    
    if (textField == self.emailTxtField)
    {
        if (textField.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                self.emailTxtField.text=@"";
                
            }
        }
    }
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
#pragma mark - GET CUSTOMER ADDRESS

-(void)getCustomerAddresses
{
    
    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    if (custPostData.count>0)
    {
        __block BOOL dataFound = NO;
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             if (!dataFound)
             {
                 dataFound = YES;
                 
                 NSMutableArray *addressArray = [[NSMutableArray alloc] init];
                 
                 if ([ApplicationDelegate isValid:responseDictionary])
                 {
                     if (responseDictionary.count>0)
                     {
                         if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                         {
                             
                             if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                             {
                                 
                                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
                                 {
                                     addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
                                 }
                                 if (addressArray.count>0)
                                 {
                                     if ([ApplicationDelegate isValid:[addressArray objectAtIndex:0]])
                                     {
                                         selectedAddressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:0]];
                                         
                                         self.nameTxtField.text = selectedAddressItem.firstName;
                                         self.emailTxtField.text = selectedAddressItem.email;
                                         
                                     }
                                     
                                 }
                                 
                             }
                         }
                     }
                 }
                 
                 [ApplicationDelegate removeProgressHUD];
             }
             
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}

-(void)xibLoading
{
    NSString *nibName = @"FeedBackViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
