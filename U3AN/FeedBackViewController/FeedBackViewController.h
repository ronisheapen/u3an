//
//  FeedBackViewController.h
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedBackViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *feedBackScrollView;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (strong, nonatomic) IBOutlet UILabel *giveFeedbackLbl;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *emailLbl;
@property (strong, nonatomic) IBOutlet UILabel *commentLbl;
@property (strong, nonatomic) IBOutlet UIButton *designGoodBttn;
@property (strong, nonatomic) IBOutlet UIButton *designVGoodBttn;
@property (strong, nonatomic) IBOutlet UIButton *designExellentBttn;
@property (strong, nonatomic) IBOutlet UIButton *usabilityGoodBttn;
@property (strong, nonatomic) IBOutlet UIButton *usabilityVGoodBttn;
@property (strong, nonatomic) IBOutlet UIButton *usabilityExellentBttn;
@property (strong, nonatomic) IBOutlet UIButton *orderingGoodBttn;
@property (strong, nonatomic) IBOutlet UIButton *orderingVGoodBttn;
@property (strong, nonatomic) IBOutlet UIButton *orderingExellentBttn;
@property (strong, nonatomic) IBOutlet UILabel *qnLbl1;
@property (strong, nonatomic) IBOutlet UILabel *qnLbl2;
@property (strong, nonatomic) IBOutlet UILabel *qnLbl3;
@property (strong, nonatomic) IBOutlet UIButton *submitBttn;
@property (strong, nonatomic) IBOutlet UITextField *nameTxtField;
@property (strong, nonatomic) IBOutlet UITextField *emailTxtField;
@property (strong, nonatomic) IBOutlet UITextField *commentTxtField;


- (IBAction)backgroundTapAction:(id)sender;

- (IBAction)websiteDesignRadioBttnAction:(UIButton *)sender;
- (IBAction)usabilityRadioBttnAction:(UIButton *)sender;
- (IBAction)orderingRadioBttnAction:(UIButton *)sender;
- (IBAction)submittFeedbackBttnAction:(UIButton *)sender;

@end
