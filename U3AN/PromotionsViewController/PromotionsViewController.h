//
//  PromotionsViewController.h
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PromotionCell.h"

@interface PromotionsViewController : UIViewController<promotionItemDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *promoScrollView;

@end
