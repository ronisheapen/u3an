//
//  PromotionCell.h
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllRestaurantPromotion.h"

@protocol promotionItemDelegate;

@interface PromotionCell : UIView
@property(weak,nonatomic)id<promotionItemDelegate> delegatePromotionItem;

@property (strong, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (strong, nonatomic) IBOutlet UILabel *restaurantName;
@property (strong, nonatomic) IBOutlet UILabel *restaurantStatus;
//@property (strong, nonatomic)AllRestaurantPromotion *restaurantPromoObj;
@property(strong,nonatomic)Restaurant *promoRestaurantObj;
@end
@protocol promotionItemDelegate <NSObject>
- (void) promotionItemDidClicked:(PromotionCell *) restaurantPromotionItem;
@end