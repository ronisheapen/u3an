//
//  PromotionsViewController.m
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "PromotionsViewController.h"
#import "PromotionDetailVC.h"

@interface PromotionsViewController ()

@end

@implementation PromotionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self getRestaurantPromotionDetails];
}


-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)getRestaurantPromotionDetails
{
    NSMutableArray *restaurantPromoListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getAllRestaurantPromotionsWithDataDictionary:postData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Restaurant *promoItem = [ApplicationDelegate.mapper getRestaurantListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             if (!([promoItem.restaurant_Status caseInsensitiveCompare:@"hidden"]==NSOrderedSame))
                             {
                                 
                                 [restaurantPromoListArray addObject:promoItem];
                             }
                         }
                     }
                     
                 }
             }
             if (restaurantPromoListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareRestaurantPromotionListScroll_Grid_WithArray:restaurantPromoListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:kCountryId forKey:@"countryId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

#pragma mark - RESTAURANTS IN SCROLLVIEW METHODS

-(void)prepareRestaurantPromotionListScroll_Grid_WithArray:(NSMutableArray *)promoListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.promoScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.promoScrollView.backgroundColor = [UIColor clearColor];
    
    [self.promoScrollView setContentOffset:CGPointZero];
    //self.restaurant_ScrollView.contentSize = CGSizeMake((restaurantListArray.count*self.restaurant_ScrollView.frame.size.width), 0);
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    PromotionCell *vw = [[PromotionCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.promoScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<promoListArray.count; i++)
    {
        int colIndex = i%colCount;
        PromotionCell *promoCell = [[PromotionCell alloc] init];
        
        promoCell.layer.cornerRadius = 2.0;
        promoCell.layer.masksToBounds = YES;
        promoCell.restaurantImageView.layer.cornerRadius = 2.0;
        promoCell.restaurantImageView.layer.masksToBounds = YES;
        
        Restaurant *promoItem = [promoListArray objectAtIndex:i];
        promoCell.promoRestaurantObj=promoItem;
        promoCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  promoCell.frame.size.width, promoCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:promoItem.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            
            promoCell.restaurantImageView.image = responseImage;
            [promoCell.restaurantImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        promoCell.delegatePromotionItem=self;
        promoCell.restaurantName.text=promoItem.restaurant_Name;
        [promoCell.restaurantName setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        promoCell.restaurantStatus.text=promoItem.restaurant_Status;
        [promoCell.restaurantStatus setFont:[UIFont fontWithName:@"Tahoma-Bold" size:13.0f]];
        if (![promoItem.restaurant_Status isEqualToString:[NSString stringWithFormat: @"OPEN"]])
        {
            promoCell.restaurantStatus.textColor=[UIColor redColor];
        }
        else
        {
            // restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
        }
        [self.promoScrollView addSubview:promoCell];
        
        if (colIndex == (colCount - 1))
        {
            yVal = promoCell.frame.origin.y + promoCell.frame.size.height;
        }
        
        scrollHeight = promoCell.frame.origin.y + promoCell.frame.size.height;
    }
    
    self.promoScrollView.contentSize = CGSizeMake(self.promoScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
}

#pragma mark -Restaurant Delegate Method

-(void) promotionItemDidClicked:(PromotionCell *)restaurantPromotionItem
{
    if ([restaurantPromotionItem.restaurantStatus.text isEqualToString:[NSString stringWithFormat: @"OPEN"]]) {
        
        
        PromotionDetailVC *promoDetailVC = [[PromotionDetailVC alloc] initWithNibName:@"PromotionDetailVC" bundle:nil]
        ;
        promoDetailVC.view.backgroundColor = [UIColor clearColor];
        promoDetailVC.promoRestaurantObj=restaurantPromotionItem.promoRestaurantObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[promoDetailVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:promoDetailVC animated:NO];
        }
    }}


@end
