//
//  PromotionDetailVC.m
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "PromotionDetailVC.h"
#import "AllPromotionDetail.h"
#import "RestaurantDetailsVC.h"

@interface PromotionDetailVC ()

@end

@implementation PromotionDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self getRestaurantPromotionDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
    
    self.promotionalItemSubView.layer.cornerRadius = 3.0;
    self.promotionalItemSubView.layer.masksToBounds = YES;
    self.restaurantNameLbl.text=[NSString stringWithFormat:@"Promotional Items From %@",self.promoRestaurantObj.restaurant_Name];
    self.restaurantDescriptionTxt.text=self.promoRestaurantObj.restaurant_Name;
    [self.restaurantNameLbl setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f]];
    //[self.restaurant_Name_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f]];
}

-(void)getRestaurantPromotionDetails
{
    NSMutableArray *restaurantPromoListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];

        [ApplicationDelegate.engine getRestaurantsByPromotionsWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             NSMutableArray *tempListArray = [[NSMutableArray alloc] init];
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"RestaurantDescription"]])
                 {
                     self.restaurantDescriptionTxt.text=[responseDictionary objectForKey:@"RestaurantDescription"];
                 }
                 
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"PromotionItemsDetails"]])
                 {
                     tempListArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"PromotionItemsDetails"]];
                 }
                 if (tempListArray.count>0)
                 {
                     for (int i=0; i<tempListArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[tempListArray objectAtIndex:i]])
                         {
                             AllPromotionDetail *promoItem = [ApplicationDelegate.mapper getPromotionDetailFromDictionary:[tempListArray objectAtIndex:i]];
                             
                             [restaurantPromoListArray addObject:promoItem];
                         }
                     }
                     
                 }
             }
             if (restaurantPromoListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareRestaurantPromotionListScroll_Grid_WithArray:restaurantPromoListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:self.promoRestaurantObj.restaurant_Id forKey:@"restId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    
    return postDic;
}

#pragma mark - RESTAURANTS IN SCROLLVIEW METHODS

-(void)prepareRestaurantPromotionListScroll_Grid_WithArray:(NSMutableArray *)promoListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.promotionDetailScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.promotionDetailScrollView.backgroundColor = [UIColor clearColor];
    self.promotionalItemSubView.backgroundColor=[UIColor whiteColor];
    [self.promotionDetailScrollView setContentOffset:CGPointZero];
    //self.restaurant_ScrollView.contentSize = CGSizeMake((restaurantListArray.count*self.restaurant_ScrollView.frame.size.width), 0);
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    PromotionDetailCell *vw = [[PromotionDetailCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.promotionDetailScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<promoListArray.count; i++)
    {
        int colIndex = i%colCount;
        PromotionDetailCell *promoCell = [[PromotionDetailCell alloc] init];
        
        promoCell.layer.cornerRadius = 2.0;
        promoCell.layer.masksToBounds = YES;
        promoCell.cuisineImageView.layer.cornerRadius = 2.0;
        promoCell.cuisineImageView.layer.masksToBounds = YES;
        
        AllPromotionDetail *promoItem = [promoListArray objectAtIndex:i];
        // restaurantCell.restaurantObj=restaurantItem;
        promoCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  promoCell.frame.size.width, promoCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:promoItem.thumbnail] completionHandler:^(UIImage *responseImage) {
            
            promoCell.cuisineImageView.image = responseImage;
            [promoCell.cuisineImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        promoCell.delegatePromotionDetailItem=self;
        promoCell.cuisineName.text=promoItem.name;
        [promoCell.cuisineName setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        promoCell.cuisinePriceLbl.text=[NSString stringWithFormat:@"KD %@",promoItem.price];
        [promoCell.cuisinePriceLbl setFont:[UIFont fontWithName:@"Tahoma" size:11.0f]];

        [self.promotionDetailScrollView addSubview:promoCell];
        
        if (colIndex == (colCount - 1))
        {
            yVal = promoCell.frame.origin.y + promoCell.frame.size.height;
        }
        
        scrollHeight = promoCell.frame.origin.y + promoCell.frame.size.height;
    }
    
    self.promotionDetailScrollView.contentSize = CGSizeMake(self.promotionDetailScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
}


#pragma mark -PromotionDetailButton Delegate Method

-(void)promotionDetailItemDidClicked:(PromotionDetailCell *)restaurantPromotionItem
{
    if ([self.promoRestaurantObj.restaurant_Status isEqualToString:[NSString stringWithFormat: @"OPEN"]]) {
        
        
        RestaurantDetailsVC *restaurantDetailVC = [[RestaurantDetailsVC alloc] initWithNibName:@"RestaurantDetailsVC" bundle:nil]
        ;
        restaurantDetailVC.view.backgroundColor = [UIColor clearColor];
        restaurantDetailVC.restaurantObj=self.promoRestaurantObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantDetailVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantDetailVC animated:NO];
        }
    }
}

-(void)xibLoading
{
    NSString *nibName = @"PromotionDetailVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
