//
//  PromotionDetailCell.h
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol promotionDetailItemDelegate;

@interface PromotionDetailCell : UIView
@property(weak,nonatomic)id<promotionDetailItemDelegate> delegatePromotionDetailItem;
@property (strong, nonatomic) IBOutlet UIImageView *cuisineImageView;
@property (strong, nonatomic) IBOutlet UILabel *cuisineName;
@property (strong, nonatomic) IBOutlet UILabel *cuisinePriceLbl;
@property (strong, nonatomic)Restaurant *restaurantPromoObj;

@end

@protocol promotionDetailItemDelegate <NSObject>
- (void) promotionDetailItemDidClicked:(PromotionDetailCell *) restaurantPromotionItem;
@end