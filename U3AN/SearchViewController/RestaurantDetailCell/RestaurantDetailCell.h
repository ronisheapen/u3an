//
//  RestaurantDetailCell.h
//  U3AN
//
//  Created by Vipin on 12/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantDetail.h"

@protocol restaurantDetailItemDelegate;

@interface RestaurantDetailCell : UIView
@property(weak,nonatomic)id<restaurantDetailItemDelegate> delegateRestaurantDetailItem;
@property (weak, nonatomic) IBOutlet UIImageView *restaurant_ImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurant_NameLabel;
@property (weak, nonatomic) IBOutlet UIView *rating_View;
@property (weak, nonatomic) IBOutlet UILabel *price_Label;
@property (weak, nonatomic) IBOutlet UILabel *restaurant_StatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *workingHour_Label;
@property (weak, nonatomic) IBOutlet UILabel *deliveryTime_Label;
@property (weak, nonatomic) IBOutlet UIImageView *knet_ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *visa_ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cash_ImageView;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;

- (IBAction)detailButton_Action:(UIButton *)sender;

@end

@protocol restaurantDetailItemDelegate <NSObject>
- (void) detailButtonDidClicked:(RestaurantDetailCell *) delegateRestaurantDetailItem;

@end