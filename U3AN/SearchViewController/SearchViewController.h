//
//  SearchViewController.h
//  U3AN
//
//  Created by Vipin on 12/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantDetailCell.h"
#import "RestaurantDetail.h"
#import "DropDownWithHeaderSelection.h"
#import "DropDownView.h"
#import "Areas.h"
#import "Cuisine.h"

@interface SearchViewController : UIViewController<ListSelectionProtocol,ListWithHeaderSelectionProtocol,restaurantDetailItemDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *restaurantsScrollView;
@property(nonatomic,retain)NSString *cuisine_Id;
@property(nonatomic,retain)NSString *area_Id;
@property(nonatomic,retain)NSMutableArray *restaurantObjtArray;
@property(nonatomic,retain)NSMutableArray *areaListArray;
@property(nonatomic,retain)NSMutableArray *cityNameArray;
@property(nonatomic,retain)NSMutableArray *cuisineListArray;
@property(nonatomic,retain)NSMutableArray *areaObjArray;
@property(nonatomic,retain)NSMutableArray *cuisineObjArray;
@property (weak, nonatomic) IBOutlet UILabel *filterType_label;
@property (weak, nonatomic) IBOutlet UILabel *area_Label;
@property (weak, nonatomic) IBOutlet UILabel *cuisine_Label;
@property (weak, nonatomic) IBOutlet UITextField *quick_RestaurantName_TxtBox;
@property (weak, nonatomic) IBOutlet UIView *advance_Search_View;
@property (weak, nonatomic) IBOutlet UIScrollView *searchScrollView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) DropDownWithHeaderSelection *areaDropDownObj;
@property (strong, nonatomic) DropDownView *cuisineDropDownObj;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet UIButton *changeAreaButton;
@property (weak, nonatomic) IBOutlet UIButton *changeCuisineButton;
@property (strong, nonatomic)Areas *areaObj;
@property (strong, nonatomic)Cuisine *cuisineObj;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;

- (IBAction)goButtonAction:(UIButton *)sender;
- (IBAction)cancelButtonAction:(UIButton *)sender;
- (IBAction)sortBy_ButtonAction:(UIButton *)sender;
- (IBAction)changeAreaButtonAction:(UIButton *)sender;
- (IBAction)changeCuisineButtonAction:(UIButton *)sender;

@end
