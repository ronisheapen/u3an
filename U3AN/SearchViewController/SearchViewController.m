//
//  SearchViewController.m
//  U3AN
//
//  Created by Vipin on 12/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SearchViewController.h"
#import "RestaurantMenuListVC.h"

#define MAXLENGTH 25

@interface SearchViewController ()

@end

@implementation SearchViewController
NSArray* sortTypeArray, *sortTypeForPostArray ;
NSString *sortType;
bool fromSortBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
    [self loadDefaultValues];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.advance_Search_View.hidden=YES;
    [self loadRestaurantScrollView];
    [self updateViewHeading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadDefaultValues
{
    self.searchScrollView.contentSize = CGSizeMake(self.searchScrollView.frame.size.width, self.cancelButton.frame.origin.y+self.cancelButton.frame.size.height+100);
    sortTypeArray = [NSArray arrayWithObjects: @"Default", @"Minimum Charge", @"Newly Added Restaurant", @"Ratings",nil];
    sortTypeForPostArray = [NSArray arrayWithObjects: @"", @"min", @"new", @"rating",nil];
    self.advance_Search_View.layer.cornerRadius = 3.0;
    self.advance_Search_View.layer.masksToBounds = YES;
    self.filterType_label.text= @"Default";
    sortType=@"";
}
-(void)updateViewHeading
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    if (ApplicationDelegate.subHomeNav.viewControllers.count==1)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=NO;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=NO;
    [HomeTabViewController sharedViewController].headerView.hidden=NO;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;

}


-(void)loadRestaurantScrollView
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.restaurantsScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.restaurantsScrollView.backgroundColor = [UIColor clearColor];
    
    [self.restaurantsScrollView setContentOffset:CGPointZero];
    
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    RestaurantDetailCell *vw = [[RestaurantDetailCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.restaurantsScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<self.restaurantObjtArray.count; i++)
    {
        int colIndex = i%colCount;
        RestaurantDetailCell *restaurantCell = [[RestaurantDetailCell alloc] init];
        
        restaurantCell.layer.cornerRadius = 2.0;
        restaurantCell.layer.masksToBounds = YES;
        restaurantCell.restaurant_ImageView.layer.cornerRadius = 2.0;
        restaurantCell.restaurant_ImageView.layer.masksToBounds = YES;
        RestaurantDetail *restaurantDetailItem = [self.restaurantObjtArray objectAtIndex:i];
         restaurantCell.restaurantDetailObj=restaurantDetailItem;
        restaurantCell.restaurant_NameLabel.text=restaurantDetailItem.restaurant_Name;
        restaurantCell.delegateRestaurantDetailItem=self;
        [restaurantCell.restaurant_NameLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        //
        restaurantCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  restaurantCell.frame.size.width, restaurantCell.frame.size.height);
        //
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:restaurantDetailItem.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            restaurantCell.restaurant_ImageView.image = responseImage;
            [restaurantCell.restaurant_ImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            restaurantCell.restaurant_ImageView.image =[UIImage imageNamed:@"Default_Food.png"];
            
        }];
        int rating_Count= [restaurantDetailItem.rating integerValue];
        for (int k=0,gap=5; k<5; k++,gap=gap+5) {
            UIImageView *star =[[UIImageView alloc] initWithFrame:CGRectMake((k*18)+gap,0,18,18)];
            star.image=[UIImage imageNamed:@"star_unsel.png"];
            [restaurantCell.rating_View addSubview:star];
            if (k<rating_Count) {
                star.image=[UIImage imageNamed:@"star_sel.png"];
            }
            
        }
        
        restaurantCell.price_Label.text=[NSString stringWithFormat:@"RANGE :-KD %@",restaurantDetailItem.min_Amount];
        //         NSLog(@"Font families: %@", [UIFont fontNamesForFamilyName:@"Tahoma"]);
        [restaurantCell.price_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        restaurantCell.restaurant_StatusLabel.text=restaurantDetailItem.restaurant_Status;
        [restaurantCell.restaurant_StatusLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        if (![restaurantDetailItem.restaurant_Status isEqualToString:[NSString stringWithFormat: @"OPEN"]])
        {
            restaurantCell.restaurant_StatusLabel.textColor=[UIColor redColor];
        }
        else
        {
            // restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
        }
        restaurantCell.workingHour_Label.text=restaurantDetailItem.working_Hours;
        [restaurantCell.workingHour_Label setFont:[UIFont fontWithName:@"Tahoma" size:10.0f]];
        restaurantCell.deliveryTime_Label.text=restaurantDetailItem.delivery_Time;
        [restaurantCell.deliveryTime_Label setFont:[UIFont fontWithName:@"Tahoma" size:10.0f]];
        if ([restaurantDetailItem.accepts_CC isEqualToString:@"1"] ) {
            restaurantCell.visa_ImageView.hidden=NO;
        }
        else{
            restaurantCell.visa_ImageView.hidden=YES;
        }
        if ([restaurantDetailItem.accepts_Cash isEqualToString:@"1"] )
        {
            restaurantCell.cash_ImageView.hidden=NO;
        }
        else
        {
            restaurantCell.cash_ImageView.hidden=YES;
        }
        if ([restaurantDetailItem.accepts_KNET isEqualToString:@"1"] ) {
            restaurantCell.knet_ImageView.hidden=NO;
        }
        else{
            restaurantCell.knet_ImageView.hidden=YES;
        }

        restaurantCell.cash_ImageView.hidden=NO;
        if (restaurantCell.visa_ImageView.hidden)
        {
            if (!restaurantCell.knet_ImageView.hidden) {
                
                restaurantCell.knet_ImageView.frame = CGRectMake(restaurantCell.visa_ImageView.frame.origin.x, restaurantCell.knet_ImageView.frame.origin.y, restaurantCell.knet_ImageView.frame.size.width, restaurantCell.knet_ImageView.frame.size.height);
            }
        }
        
        if (restaurantCell.visa_ImageView.hidden&&restaurantCell.knet_ImageView.hidden)
        {
            restaurantCell.cash_ImageView.frame = CGRectMake(restaurantCell.visa_ImageView.frame.origin.x, restaurantCell.cash_ImageView.frame.origin.y, restaurantCell.cash_ImageView.frame.size.width, restaurantCell.cash_ImageView.frame.size.height);
        }
        
        [self.restaurantsScrollView addSubview:restaurantCell];
        if (colIndex == (colCount - 1))
        {
            yVal = restaurantCell.frame.origin.y + restaurantCell.frame.size.height;
        }
        
        scrollHeight = restaurantCell.frame.origin.y + restaurantCell.frame.size.height;
    }
    
    //   self.mostSellingDishesCellScrollView.contentSize = CGSizeMake(self.mostSellingDishesCellScrollView.frame.size.width,  (scrollHeight + offsetValue));
    self.restaurantsScrollView.contentSize = CGSizeMake(self.restaurantsScrollView.frame.size.width,  (scrollHeight + offsetValue+30));
    
}


- (IBAction)goButtonAction:(id)sender {
    [self getRestaurantDetails];
}

- (IBAction)cancelButtonAction:(UIButton *)sender {
    self.advance_Search_View.hidden=YES;
}

- (IBAction)sortBy_ButtonAction:(UIButton *)sender {
    [self showfilterTypeList];
    fromSortBtn=YES;
}

- (IBAction)changeAreaButtonAction:(UIButton *)sender {
    if (self.areaListArray.count!=0) {
        [self showAreaList];
    }
}

- (IBAction)changeCuisineButtonAction:(UIButton *)sender {
    [self showCuisineList];
    fromSortBtn=NO;
}

#pragma mark - GET Restaurant Details by Advanced Search 

-(void)getRestaurantDetails
{
    self.restaurantObjtArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *searchPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getSearchPostData]];
    if (searchPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getRestaurantListByAdvancedSearchWithDataDictionary:searchPostData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             self.restaurantDetailObj = [ApplicationDelegate.mapper getRestaurantDetailsFromDictionary:[responseArray objectAtIndex:i]];
                             
                             if (!([self.restaurantDetailObj.restaurant_Status caseInsensitiveCompare:@"hidden"]==NSOrderedSame))
                             {
                                 [self.restaurantObjtArray addObject:self.restaurantDetailObj];
                             }
                             
                         }
                     }
                     
                 }
             }
             [ApplicationDelegate removeProgressHUD];
             
             if (self.restaurantObjtArray.count!=0)
             {
                 self.advance_Search_View.hidden=YES;
                 [self loadRestaurantScrollView];
             }
             else
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getSearchPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:sortType forKey:@"FilterBy"];
    if ([ApplicationDelegate isValid:self.area_Id])
    {
        [postDic setObject:self.area_Id forKey:@"areaId"];
    }
    else
    {
        [postDic setObject:@"" forKey:@"areaId"];
    }
    if ([ApplicationDelegate isValid:self.cuisine_Id])
    {
        [postDic setObject:self.cuisine_Id forKey:@"cuisineId"];
    }
    else
    {
        [postDic setObject:@"" forKey:@"cuisineId"];
    }
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.quick_RestaurantName_TxtBox.text forKey:@"restName"];
    
    
    return postDic;
}


-(void)showfilterTypeList
{
    self.sortButton.selected = !self.sortButton.selected;
    if (self.sortButton.selected)
    {
        if (self.cuisineDropDownObj.view.superview) {
            [self.cuisineDropDownObj.view removeFromSuperview];
        }
        
        self.cuisineDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.cuisineDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.cuisineDropDownObj.view.frame = CGRectMake((self.sortButton.superview.frame.origin.x),(self.sortButton.superview.frame.origin.y+self.sortButton.superview.frame.size.height), (self.sortButton.superview.frame.size.width), 0);
        
        self.cuisineDropDownObj.dropDownDelegate = self;
        self.cuisineDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:sortTypeArray];
        
        self.cuisineDropDownObj.view.layer.borderWidth = 0.1;
        self.cuisineDropDownObj.view.layer.shadowOpacity = 1.0;
        self.cuisineDropDownObj.view.layer.shadowRadius = 5.0;
        self.cuisineDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.cuisineDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.cuisineDropDownObj.textLabelColor = [UIColor blackColor];
        self.cuisineDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (sortTypeArray.count>0)
        {
            [self.sortButton.superview.superview addSubview:self.cuisineDropDownObj.view];
        }
        else
        {
            self.sortButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)+18.0f;
        }
        //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.cuisineDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.cuisineDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x+2,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.cuisineDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.cuisineDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cuisineDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
}


-(void)showAreaList
{
    self.changeAreaButton.selected = !self.changeAreaButton.selected;
    if (self.changeAreaButton.selected)
    {
        if (self.areaDropDownObj.view.superview) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
        self.areaDropDownObj= [[DropDownWithHeaderSelection alloc] initWithNibName:@"DropDownWithHeaderSelection" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.areaDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.areaDropDownObj.view.frame = CGRectMake((self.changeAreaButton.superview.frame.origin.x),(self.changeAreaButton.superview.frame.origin.y+self.changeAreaButton.superview.frame.size.height), (self.changeAreaButton.superview.frame.size.width), 0);
        
        self.areaDropDownObj.headerDropDownDelegate = self;
        self.areaDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:self.areaListArray];
        self.areaDropDownObj.headerDataArray = [[NSMutableArray alloc] initWithArray:self.cityNameArray];
        self.areaDropDownObj.headerLabelFont = [UIFont fontWithName:@"Verdana" size:14.0];
        self.areaDropDownObj.view.layer.borderWidth = 0.1;
        self.areaDropDownObj.view.layer.shadowOpacity = 1.0;
        self.areaDropDownObj.view.layer.shadowRadius = 5.0;
        self.areaDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.areaSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.areaDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.areaDropDownObj.textLabelColor = [UIColor blackColor];
        self.areaDropDownObj.view.backgroundColor = [UIColor whiteColor];
        self.areaDropDownObj.headerTextLabelColor = [UIColor redColor];
        if (self.areaListArray.count>0)
        {
            [self.changeAreaButton.superview.superview addSubview:self.areaDropDownObj.view];
        }
        else
        {
            self.changeAreaButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)+18.0f;
        }
        //        [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.areaDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.areaDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x+2,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.areaDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.areaDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.areaDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
    
}

-(void)showCuisineList
{
    self.changeCuisineButton.selected = !self.changeCuisineButton.selected;
    if (self.changeCuisineButton.selected)
    {
        if (self.cuisineDropDownObj.view.superview) {
            [self.cuisineDropDownObj.view removeFromSuperview];
        }
        
        self.cuisineDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.cuisineDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.cuisineDropDownObj.view.frame = CGRectMake((self.changeCuisineButton.superview.frame.origin.x),(self.changeCuisineButton.superview.frame.origin.y+self.changeCuisineButton.superview.frame.size.height), (self.changeCuisineButton.superview.frame.size.width), 0);
        
        self.cuisineDropDownObj.dropDownDelegate = self;
        self.cuisineDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:self.cuisineListArray];
        
        self.cuisineDropDownObj.view.layer.borderWidth = 0.1;
        self.cuisineDropDownObj.view.layer.shadowOpacity = 1.0;
        self.cuisineDropDownObj.view.layer.shadowRadius = 5.0;
        self.cuisineDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.cuisineDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.cuisineDropDownObj.textLabelColor = [UIColor blackColor];
        self.cuisineDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (self.cuisineListArray.count>0)
        {
            [self.changeCuisineButton.superview.superview addSubview:self.cuisineDropDownObj.view];
        }
        else
        {
            self.changeCuisineButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)+18.0f;
        }
        //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.cuisineDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.cuisineDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x+2,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.cuisineDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.cuisineDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cuisineDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
}


#pragma mark - DROP-DOWN List Delegate Method


-(void)headerSelectList:(int)selectedIndex :(NSInteger)section{
    ////////// ReEnabling the UserInteraction of all SubViews/////////
    //    for (UIView *vw in self.view.subviews) {
    //        [vw setUserInteractionEnabled:YES];
    //    }
    ///////////////////
    
    [UIView animateWithDuration:0.4f animations:^{
        self.areaDropDownObj.view.frame =
        CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                   self.areaDropDownObj.view.frame.origin.y,
                   self.areaDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.changeAreaButton.selected = NO;
    self.area_Label.text =[[self.areaListArray objectAtIndex:section]objectAtIndex:selectedIndex];
    self.areaObj=[self.areaObjArray objectAtIndex:section];
    self.area_Id=[[self.areaObj.area_List objectAtIndex:selectedIndex]objectForKey:@"AreaId"];
    ApplicationDelegate.selected_AreaID=self.area_Id;
    
}

-(void)selectList:(int)selectedIndex
{
    if (!fromSortBtn) {
        [UIView animateWithDuration:0.4f animations:^{
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cuisineDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.changeCuisineButton.selected = NO;
        self.cuisine_Label.text =[self.cuisineListArray objectAtIndex:selectedIndex];
        self.cuisineObj=[self.cuisineObjArray objectAtIndex:selectedIndex];
        self.cuisine_Id=self.cuisineObj.cuisine_Id;
    }
    else
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cuisineDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.sortButton.selected = NO;
        self.filterType_label.text =[sortTypeArray objectAtIndex:selectedIndex];
        sortType=[sortTypeForPostArray objectAtIndex:selectedIndex];
      //  self.cuisineObj=[self.cuisineObjArray objectAtIndex:selectedIndex];
    }
    }


#pragma mark - DETAIL CELL DELEGATES

-(void)detailButtonDidClicked:(RestaurantDetailCell *)delegateRestaurantDetailItem
{
    
    if ([delegateRestaurantDetailItem.restaurant_StatusLabel.text isEqualToString:[NSString stringWithFormat: @"OPEN"]]) {
        
        
    if ([ApplicationDelegate isValid:delegateRestaurantDetailItem.restaurantDetailObj])
    {
        RestaurantMenuListVC *restaurantMenuListVC = [[RestaurantMenuListVC alloc] initWithNibName:@"RestaurantMenuListVC" bundle:nil];
        
        restaurantMenuListVC.restaurantDetailObj = delegateRestaurantDetailItem.restaurantDetailObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[RestaurantMenuListVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantMenuListVC animated:NO];
        }
    }
        
        
    }
}

#pragma mark - TEXT FIELD DELEGATES
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   // [self.contentScroll setContentOffset:CGPointZero animated:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //    UITouch *touch = [touches anyObject];
    //    CGPoint location = [touch locationInView:[touch view]];
    [self.view  endEditing:YES];
    // [self.registerView endEditing:YES];
}


-(void)xibLoading
{
    NSString *nibName = @"SearchViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
