//
//  PaymentOptionCell.m
//  U3AN
//
//  Created by Vipin on 16/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "PaymentOptionCell.h"

@implementation PaymentOptionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"PaymentOptionCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (PaymentOptionCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

- (IBAction)deliverTimeRadioAction:(UIButton *)sender
{
    self.deliverAtTxtValue.text = @"";
    
    if (sender.tag==1)
    {
        self.deliverNowButton.selected = NO;
        self.deliverAtButton.selected = YES;
    }
    else
    {
        self.deliverNowButton.selected = YES;
        self.deliverAtButton.selected = NO;
    }
    [self.delegatepaymentOptionCell deliveryTimeRadioBttnDidClicked:self];
    
}
- (IBAction)deliveryDropdownBttnAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    [self.delegatepaymentOptionCell deliveryAtTimeDropDownBttnDidClicked:self];
}

@end
