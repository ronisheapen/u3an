//
//  CuisineListItemCell.h
//  U3AN
//
//  Created by Vipin on 16/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CuisineListItemCellDelegate;

@interface CuisineListItemCell : UIView

@property(weak,nonatomic)id<CuisineListItemCellDelegate> delegateCuisineItemCell;

@property (strong, nonatomic)CartDetails *cartDetailObj;

@property (strong, nonatomic) IBOutlet UILabel *cuisine_NameLbl;
@property (strong, nonatomic) IBOutlet UILabel *quantityLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalLbl;
@property (strong, nonatomic) IBOutlet UILabel *special_RequestLbl;

@property (strong, nonatomic) IBOutlet UITextField *specialRequestTxtField;

- (IBAction)removeBttnAction:(UIButton *)sender;

@end

@protocol CuisineListItemCellDelegate <NSObject>
- (void) removeCuisineItemButtonDidClicked:(CuisineListItemCell *) cuisineItemCell;
@end
