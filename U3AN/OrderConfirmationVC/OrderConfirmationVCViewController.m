//
//  OrderConfirmationVCViewController.m
//  U3AN
//
//  Created by Vipin on 11/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "OrderConfirmationVCViewController.h"
#import "OrderConfirmationResultVC.h"
#define MAXLENGTH 25

@interface OrderConfirmationVCViewController ()
{
    BOOL fromAddAddress;
    NSInteger numberFieldVal;
    NSString *phoneStr, * addressType;
    NSString *deliveryInfoRadioOptionString;
    NSMutableArray *areaListArray,*cityListArray,*areaObjArray,*addressPlaceArray, *timeArray;
    NSString *selectedAreaId;
    CustomerAddressDetails *selectedAddressItem;
    
    BOOL confirmOrderSummaryCashFlag, confirmOrderSummaryKnetFlag, confirmOrderSummaryVisaFlag, confirmOrderSummaryU3anCreditFlag;
}
@end

@implementation OrderConfirmationVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    
    self.orderConfirmScrollView.layer.borderColor = [UIColor redColor].CGColor;
    self.orderConfirmScrollView.layer.borderWidth = 2;
    
    self.orderSumView.layer.borderColor = [UIColor redColor].CGColor;
    self.orderSumView.layer.borderWidth = 2;
    
    self.confOrderVew.layer.borderColor = [UIColor redColor].CGColor;
    self.confOrderVew.layer.borderWidth = 2;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    self.confirmYourOrderView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.orderSummaryView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.orderConfirmScrollView setContentOffset:CGPointZero];
    self.orderSummaryBackButton.superview.center = CGPointMake(self.orderSummaryView.center.x, self.orderSummaryBackButton.superview.center.y);
    self.confirmYourOrderView.hidden = YES;
    self.orderSummaryView.hidden = YES;
    self.orderConfirmScrollView.superview.hidden=NO;
    deliveryInfoRadioOptionString = @"";
    numberFieldVal = 0;
    phoneStr = @"";
    
    confirmOrderSummaryCashFlag = NO;
    confirmOrderSummaryKnetFlag = NO;
    confirmOrderSummaryVisaFlag = NO;
    
    confirmOrderSummaryU3anCreditFlag = NO;
    
    [self toolbarForNumberPadSetup];
    
    CGFloat yFact = 0.0f;
    
    if (ApplicationDelegate.isLoggedIn)
    {
        self.addressWithGoView.hidden = NO;
        self.personalInfoView.hidden = YES;
        
        yFact = self.addressWithGoView.frame.origin.y+self.addressWithGoView.frame.size.height + 10;
        fromAddAddress=NO;
        [self getCustomerAddresses];

    }
    else
    {
        self.addressWithGoView.hidden = YES;
        self.personalInfoView.hidden = NO;
        
        
        yFact = self.personalInfoView.frame.origin.y+self.personalInfoView.frame.size.height + 10;
        [self updateTextValuesAndView:0];
    }
    
    self.otherInfoView.frame = CGRectMake(self.otherInfoView.frame.origin.x, yFact, self.otherInfoView.frame.size.width, self.otherInfoView.frame.size.height);
    
    self.orderConfirmScrollView.contentSize = CGSizeMake(0, (self.otherInfoView.frame.origin.y+self.otherInfoView.frame.size.height+10));
    
    [self setupUI];
    [self getAreaDetails];
    
    
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}
-(void)setupUI
{
    [(UIView *)[self.view.subviews objectAtIndex:0] layer].cornerRadius = 3.0;
    [(UIView *)[self.view.subviews objectAtIndex:0] layer].masksToBounds = YES;
    
    self.deliveryHeaderLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f];
    self.villaHouseNameLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    self.buildingNameLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    self.officeNameLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    
    [self.villaHouseRadioButton setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.villaHouseRadioButton setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.buildingRadioButton setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.buildingRadioButton setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    [self.officeRadioButton setBackgroundImage:[UIImage imageNamed:@"radio_btn.png"] forState:UIControlStateNormal];
    [self.officeRadioButton setBackgroundImage:[UIImage imageNamed:@"radio_btn_sel.png"] forState:UIControlStateSelected];
    
    self.areaNameLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    self.areaValueLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    
    
    NSMutableParagraphStyle *paragraphStyles = [[NSMutableParagraphStyle alloc] init];
    paragraphStyles.alignment = NSTextAlignmentJustified;      //justified text
    paragraphStyles.firstLineHeadIndent = 1.0;                //must have a value to make it work
    NSDictionary *attributes = @{NSParagraphStyleAttributeName: paragraphStyles};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: self.tipsTextLabel.text attributes: attributes];
    self.tipsTextLabel.attributedText = attributedString;
    
    
    self.tipsTextLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    
    
    self.deliveryLocationTextLabel.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    
    self.adressNameLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    self.adressDropDownValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.addnewAddressButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    
    self.firstNameTextLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.firstNameValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.lastNameTextLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.lastNameValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.emailTextLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.emailValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.mobileTextLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.mobileValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    
    self.blockNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.blockValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.streetNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.streetValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.juddaNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.juddaValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.buildingNumberNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.buildingNumValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.floorNumberNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.floorNumValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.apartmentNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.apartmentValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.extraDirNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.extraDirValueField.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    self.saveAndContinueButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    self.nextButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    
    self.orderSummaryHeaderLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f];
    self.orderSummaryBackButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.orderSummaryNextButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    
    self.confirmYourOrderHeaderLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f];
    self.confirmOrderPaymentsLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    
    self.subTotalTextLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.subTotalValueLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.deliveryChargesTextLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.deliveryChargesValueLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.discountTextLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.discountValueLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.grandTotalTextLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    self.grandTotalValueLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    self.payByCashTextLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    self.payByCashValueLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    
    self.confirmOrderDetailsContainerView.layer.cornerRadius = 3.0;
    self.confirmOrderDetailsContainerView.layer.masksToBounds = YES;
    
    [(UIView *)[self.confirmOrderDetailsContainerView.subviews objectAtIndex:0] layer].cornerRadius = 3.0;
    [(UIView *)[self.confirmOrderDetailsContainerView.subviews objectAtIndex:0] layer].masksToBounds = YES;
    
    self.confirmOrderButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    
}

-(void)updateTextValuesAndView:(NSInteger)selectedIndex
{
    if (ApplicationDelegate.isLoggedIn) {
        
        self.blockValueField.userInteractionEnabled=NO;
        self.streetValueField.userInteractionEnabled=NO;
        self.juddaValueField.userInteractionEnabled=NO;
        self.buildingNumValueField.userInteractionEnabled=NO;
        self.floorNumValueField.userInteractionEnabled=NO;
        self.apartmentValueField.userInteractionEnabled=NO;
        self.extraDirValueField.userInteractionEnabled=NO;
        self.villaHouseRadioButton.userInteractionEnabled=NO;
        self.buildingRadioButton.userInteractionEnabled=NO;
        self.officeRadioButton.userInteractionEnabled=NO;
        self.areaDropDownButton.userInteractionEnabled=NO;
        self.cancelButton.hidden=YES;
        self.saveAndContinueButton.hidden=YES;
        self.nextButton.hidden=NO;
        [self populateTxtValues:selectedIndex];
        
    }
    else
    {
        
        [self clearAndEnableTextFieldsForAddingNewAddress];
        
    }
}

-(void)populateTxtValues:(NSInteger)selectedIndex
{
    selectedAddressItem =[self.custAddressListArray objectAtIndex:selectedIndex];
    self.blockValueField.text=selectedAddressItem.block;
    self.streetValueField.text=selectedAddressItem.street;
    self.juddaValueField.text=selectedAddressItem.judda;
    self.buildingNumValueField.text=selectedAddressItem.buildingNo;
    self.floorNumValueField.text=selectedAddressItem.floor;
    self.apartmentValueField.text=selectedAddressItem.type;
    self.extraDirValueField.text=selectedAddressItem.extraDirections;
    self.areaDropDownValueField.text=selectedAddressItem.areaName;
    self.adressDropDownValueField.text=selectedAddressItem.areaName;
    self.areaValueLabel.text = selectedAddressItem.areaName;
    selectedAreaId=selectedAddressItem.areaID;
    ApplicationDelegate.selected_AreaID=selectedAreaId;
    
    self.villaHouseRadioButton.selected = NO;
    self.buildingRadioButton.selected = NO;
    self.officeRadioButton.selected = NO;
    
    if ([selectedAddressItem.type caseInsensitiveCompare:@"villa"]==NSOrderedSame)
    {
        [self deliveryInfoOptionSelection:self.villaHouseRadioButton];
    }
    if ([selectedAddressItem.type caseInsensitiveCompare:@"building"]==NSOrderedSame)
    {
        [self deliveryInfoOptionSelection:self.buildingRadioButton];
    }
    if ([selectedAddressItem.type caseInsensitiveCompare:@"office"]==NSOrderedSame)
    {
       [self deliveryInfoOptionSelection:self.officeRadioButton];
    }
    
}
-(void)clearAndEnableTextFieldsForAddingNewAddress
{
   
        self.blockValueField.userInteractionEnabled=YES;
        self.streetValueField.userInteractionEnabled=YES;
        self.juddaValueField.userInteractionEnabled=YES;
        self.buildingNumValueField.userInteractionEnabled=YES;
        self.floorNumValueField.userInteractionEnabled=YES;
        self.apartmentValueField.userInteractionEnabled=YES;
        self.extraDirValueField.userInteractionEnabled=YES;
        self.villaHouseRadioButton.userInteractionEnabled=YES;
        self.buildingRadioButton.userInteractionEnabled=YES;
        self.officeRadioButton.userInteractionEnabled=YES;
        self.areaDropDownButton.userInteractionEnabled=YES;
    if (ApplicationDelegate.isLoggedIn) {
        self.cancelButton.hidden=NO;
        self.saveAndContinueButton.hidden=NO;
        self.nextButton.hidden=YES;
    }
    else
    {
        self.cancelButton.hidden=YES;
        self.saveAndContinueButton.hidden=YES;
        self.nextButton.hidden=NO;
    }
    
        [self clearTxtValues];
        
    }

-(void)clearTxtValues
{
    /*
    self.villaHouseRadioButton.selected = YES;
    self.officeRadioButton.selected = NO;
    self.buildingRadioButton.selected = NO;
    deliveryInfoRadioOptionString = @"0";
    */
    
    [self deliveryInfoOptionSelection:self.villaHouseRadioButton];
    
    self.firstNameValueField.text = @"";
    self.lastNameValueField.text = @"";
    self.emailValueField.text = @"";
    self.mobileValueField.text = @"";
    self.blockValueField.text=@"";
    self.streetValueField.text=@"";
    self.juddaValueField.text=@"";
    self.buildingNumValueField.text=@"";
    self.floorNumValueField.text=@"";
    self.apartmentValueField.text=@"";
    self.extraDirValueField.text=@"";
    
}
#pragma mark - GET AREA DETAILS


-(void)getAreaDetails
{
    areaListArray=[[NSMutableArray alloc] init];
    areaObjArray=[[NSMutableArray alloc] init];
    cityListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getAreaPostData]];
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        __block BOOL success = NO;
        
        [ApplicationDelegate.engine getAreasWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             if (!success)
             {
                 success = YES;
                 if ([ApplicationDelegate isValid:responseArray])
                 {
                     if (responseArray.count>0)
                     {
                         for (int i=0; i<responseArray.count; i++)
                         {
                             if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                             {
                                 Areas *areaItem = [ApplicationDelegate.mapper getAreaListFromDictionary:[responseArray objectAtIndex:i]];
                                 
                                 [cityListArray addObject:areaItem.city_Name];
                                 [areaObjArray addObject:areaItem];
                                 if(areaItem.area_List.count!=0)
                                 {
                                     NSMutableArray *areaItemListArray=[[NSMutableArray alloc] init];
                                     
                                     for (int j=0; j<areaItem.area_List.count; j++)
                                     {
                                         if(![ApplicationDelegate isValid:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]] )
                                         {
                                             [areaItemListArray addObject:@""];
                                         }
                                         else
                                         {
                                             [areaItemListArray addObject:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]];
                                         }
                                         
                                     }
                                     //     NSLog(@"AREA_ITEM_LIST%@",areaItemListArray);
                                     [areaListArray addObject:areaItemListArray];
                                     //   NSLog(@"AREA_LIST%@",areaListArray);
                                 }
                                 
                             }
                             
                         }
                         NSLog(@"AREA_LIST%@",areaListArray);
                         
                         
                     }
                 }
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getAreaPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:kCountryId forKey:@"countryId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    return postDic;
}

-(void)showAreaList
{
    self.areaDropDownButton.selected = !self.areaDropDownButton.selected;
    if (self.areaDropDownButton.selected)
    {
        if (self.areaDropDownObj.view.superview) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
        self.areaDropDownObj= [[DropDownWithHeaderSelection alloc] initWithNibName:@"DropDownWithHeaderSelection" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.areaDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.areaDropDownObj.view.frame = CGRectMake((self.areaDropDownButton.superview.frame.origin.x),(self.areaDropDownButton.superview.frame.origin.y+self.areaDropDownButton.superview.frame.size.height), (self.areaDropDownButton.superview.frame.size.width), 0);
        
        self.areaDropDownObj.headerDropDownDelegate = self;
        self.areaDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:areaListArray];
        self.areaDropDownObj.headerDataArray = [[NSMutableArray alloc] initWithArray:cityListArray];
        self.areaDropDownObj.headerLabelFont = [UIFont fontWithName:@"Verdana" size:14.0];
        self.areaDropDownObj.view.layer.borderWidth = 0.1;
        self.areaDropDownObj.view.layer.shadowOpacity = 1.0;
        self.areaDropDownObj.view.layer.shadowRadius = 5.0;
        self.areaDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.areaSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.areaDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.areaDropDownObj.textLabelColor = [UIColor blackColor];
        self.areaDropDownObj.view.backgroundColor = [UIColor whiteColor];
        self.areaDropDownObj.headerTextLabelColor = [UIColor redColor];
        if (cityListArray.count>0)
        {
            [self.areaDropDownButton.superview.superview addSubview:self.areaDropDownObj.view];
        }
        else
        {
            self.areaDropDownButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)+18.0f;
        }
        //        [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.areaDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.areaDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x+2,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.areaDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.areaDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.areaDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
    
}

#pragma mark - GET CUSTOMER ADDRESS

-(void)getCustomerAddresses
{
    self.custAddressListArray= [[NSMutableArray alloc] init];
    addressPlaceArray = [[NSMutableArray alloc] init];
    
    confirmOrderSummaryU3anCreditFlag = NO;
    
    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    if (custPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             NSMutableArray *addressArray = [[NSMutableArray alloc] init];
             
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"U3ANCredit"]])
                             {
                                 if ([[responseDictionary objectForKey:@"U3ANCredit"] length]>0)
                                 {
                                     /*
                                     if ([ApplicationDelegate isLoggedIn])
                                     {
                                         confirmOrderSummaryU3anCreditFlag = YES;
                                     }
                                     */
                                     if ([[responseDictionary objectForKey:@"U3ANCredit"] floatValue]>0.0f)
                                     {
                                         if ([ApplicationDelegate isLoggedIn])
                                         {
                                             confirmOrderSummaryU3anCreditFlag = YES;
                                         }
                                     }
                                    
                                 }
                                 
                             }
                             
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
                             {
                                 addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
                             }
                             if (addressArray.count>0)
                             {
                                 for (int i=0; i<addressArray.count; i++)
                                 {
                                     if ([ApplicationDelegate isValid:[addressArray objectAtIndex:i]])
                                     {
                                         CustomerAddressDetails *addressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:i]];
                                         
                                         [self.custAddressListArray addObject:addressItem];
                                         [addressPlaceArray addObject:addressItem.areaName];
                                     }
                                     
                                 }
                                 if (!fromAddAddress)
                                 {
                                     fromAddAddress=NO;
                                     [self updateTextValuesAndView:0];

                                 }
                                 
                             }
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}


-(void)showAddressList
{
    self.adressDropDownButton.selected = !self.adressDropDownButton.selected;
    if (self.adressDropDownButton.selected)
    {
        if (self.addressDropDownObj.view.superview) {
            [self.addressDropDownObj.view removeFromSuperview];
        }
        
        self.addressDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.addressDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.addressDropDownObj.view.frame = CGRectMake((self.adressDropDownButton.superview.frame.origin.x),(self.adressDropDownButton.superview.frame.origin.y+self.adressDropDownButton.superview.frame.size.height), (self.adressDropDownButton.superview.frame.size.width), 0);
        
        self.addressDropDownObj.dropDownDelegate = self;
        self.addressDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:addressPlaceArray];
        
        self.addressDropDownObj.view.layer.borderWidth = 0.1;
        self.addressDropDownObj.view.layer.shadowOpacity = 1.0;
        self.addressDropDownObj.view.layer.shadowRadius = 5.0;
        self.addressDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.addressDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.addressDropDownObj.textLabelColor = [UIColor blackColor];
        self.addressDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (addressPlaceArray.count>0)
        {
            [self.adressDropDownButton.superview.superview addSubview:self.addressDropDownObj.view];
        }
        else
        {
            self.adressDropDownButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.addressDropDownObj.cellHeight*self.addressDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.addressDropDownObj.cellHeight*self.addressDropDownObj.dataArray.count)+18.0f;
        }
        //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.addressDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.addressDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.addressDropDownObj.view.frame =
            CGRectMake(self.addressDropDownObj.view.frame.origin.x+2,
                       self.addressDropDownObj.view.frame.origin.y,
                       self.addressDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.addressDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.addressDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.addressDropDownObj.view.frame =
            CGRectMake(self.addressDropDownObj.view.frame.origin.x,
                       self.addressDropDownObj.view.frame.origin.y,
                       self.addressDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.addressDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
}


#pragma mark - DROP-DOWN List Delegate Method


-(void)headerSelectList:(int)selectedIndex :(NSInteger)section{
    ////////// ReEnabling the UserInteraction of all SubViews/////////
    //    for (UIView *vw in self.view.subviews) {
    //        [vw setUserInteractionEnabled:YES];
    //    }
    ///////////////////
    
    [UIView animateWithDuration:0.4f animations:^{
        self.areaDropDownObj.view.frame =
        CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                   self.areaDropDownObj.view.frame.origin.y,
                   self.areaDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.areaDropDownButton.selected = NO;
    self.areaDropDownValueField.text =[[areaListArray objectAtIndex:section]objectAtIndex:selectedIndex];
    self.areaValueLabel.text = [[areaListArray objectAtIndex:section]objectAtIndex:selectedIndex];
    self.areaObj=[areaObjArray objectAtIndex:section];
    selectedAreaId=[[self.areaObj.area_List objectAtIndex:selectedIndex]objectForKey:@"AreaId"];
    ApplicationDelegate.selected_AreaID=selectedAreaId;
    
}

-(void)selectList:(int)selectedIndex
{
    if ([self.addressDropDownObj.view superview])
    {
        [UIView animateWithDuration:0.4f animations:^{
            self.addressDropDownObj.view.frame =
            CGRectMake(self.addressDropDownObj.view.frame.origin.x,
                       self.addressDropDownObj.view.frame.origin.y,
                       self.addressDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.addressDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.adressDropDownButton.selected = NO;
        self.adressDropDownValueField.text =[addressPlaceArray objectAtIndex:selectedIndex];
        [self updateTextValuesAndView:selectedIndex];
        //self.cuisineObj=[cuisineObjArray objectAtIndex:selectedIndex];
    }
    else if([self.orderSummaryDeliveryAtDropDownObj.view superview])
    {
        PaymentOptionCell *payment;
        
        BOOL success = NO;
        
        for (id sub in self.orderSummaryScroll.subviews)
        {
            if ([sub isKindOfClass:[PaymentOptionCell class]])
            {
                PaymentOptionCell *payCell = (PaymentOptionCell *)sub;
                
                if (payCell.tag==self.orderSummaryDeliveryAtDropDownObj.view.tag)
                {
                    payment = payCell;
                    success = YES;
                    break;
                }
            }
            
        }
        
        [UIView animateWithDuration:0.4f animations:^{
            self.orderSummaryDeliveryAtDropDownObj.view.frame =
            CGRectMake(self.orderSummaryDeliveryAtDropDownObj.view.frame.origin.x,
                       self.orderSummaryDeliveryAtDropDownObj.view.frame.origin.y,
                       self.orderSummaryDeliveryAtDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.orderSummaryDeliveryAtDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        if (success)
        {
            payment.deliveryAtDropDownButton.selected = NO;
            payment.deliverAtTxtValue.text =[timeArray objectAtIndex:selectedIndex];
        }

    }
    else
    {
        
    }
}


#pragma mark - TEXT FIELD DELEGATES

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   // [self.view endEditing:YES];
    //[self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
    
    if (textField == self.emailValueField)
    {
        if (textField.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                self.emailValueField.text=@"";
            }
        }
        
    }
    
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGFloat yFact;
    
    if (!self.orderConfirmScrollView.superview.hidden)
    {
        [self.orderConfirmScrollView setContentOffset:CGPointZero animated:NO];
        //textFieldFlag = YES;
        
        if (textField==self.mobileValueField)
        {
            numberFieldVal = 1;
            phoneStr = self.mobileValueField.text;
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 0; // ToolBar Inclusived case
            }
            else
            {
                yFact = 80; // ToolBar Inclusived case
            }
        }
        else
        {
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 45;
            }
            else
            {
                yFact = 130;
            }
        }
        
        if ((textField==self.floorNumValueField)||(textField==self.apartmentValueField))
        {
            if ((textField.superview.superview.superview.frame.origin.y+textField.superview.superview.frame.origin.y+textField.superview.frame.origin.y)>yFact)
            {
                [self.orderConfirmScrollView setContentOffset:CGPointMake(self.orderConfirmScrollView.contentOffset.x, (textField.superview.superview.superview.frame.origin.y+textField.superview.superview.frame.origin.y+textField.superview.frame.origin.y-yFact))];
            }
            else
            {
                [self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
            }
            
        }
        else if ((textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y)>yFact)
        {
            [self.orderConfirmScrollView setContentOffset:CGPointMake(self.orderConfirmScrollView.contentOffset.x, (textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y-yFact))];
        }
        else
        {
            [self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
        }
    }
    else if ([self.orderSummaryView superview])
    {
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 45;
        }
        else
        {
            yFact = 130;
        }
        
        if ((textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y+textField.superview.superview.superview.frame.origin.y+textField.superview.superview.superview.superview.frame.origin.y)>yFact)
        {
            [self.orderSummaryScroll setContentOffset:CGPointMake(self.orderSummaryScroll.contentOffset.x, (textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y+textField.superview.superview.superview.frame.origin.y+textField.superview.superview.superview.superview.frame.origin.y-yFact))];
        }
        else
        {
            [self.orderSummaryScroll setContentOffset:CGPointZero animated:YES];
        }
    }
    else if ([self.confirmYourOrderView superview])
    {
        
    }
    else
    {
        
    }
    

    
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if (textField==self.mobileValueField)
    {
        if (oldLength>14)
        {
            return newLength <= 14 || returnKey;
        }
    }
    
    
    return newLength <= MAXLENGTH || returnKey;
}
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    //[self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
//    //textFieldFlag = NO;
//    //Email
//    if (textField == self.emailValueField)
//    {
//        if (textField.text.length!=0)
//        {
//            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
//            {
//                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
//                self.emailValueField.text=@"";
//            }
//        }
//        
//    }
//}
#pragma mark - NUMBER PAD METHODS
#pragma mark -
-(void)toolbarForNumberPadSetup
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, ApplicationDelegate.window.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    
    self.mobileValueField.inputAccessoryView = numberToolbar;

}
-(void)cancelNumberPad{
    
    if (numberFieldVal==1)
    {
        self.mobileValueField.text=phoneStr;
    }
    
    [self.mobileValueField resignFirstResponder];

}

-(void)doneWithNumberPad
{
    if (numberFieldVal==1)
    {
        [self.mobileValueField resignFirstResponder];
        if (!((self.mobileValueField.text.length>=6)&&(self.mobileValueField.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
            self.mobileValueField.text=phoneStr;
        }
    }
    
    [self toolbarForNumberPadSetup];
}

#pragma mark - TEXT VIEW DELEGATES

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //textFieldFlag = YES;
    CGFloat yFact;
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        yFact = 20;
    }
    else
    {
        yFact = 80;
    }
    
    if (!self.orderConfirmScrollView.superview.hidden)
    {
        [self.orderConfirmScrollView setContentOffset:CGPointZero animated:NO];
        
        if (textView==self.extraDirValueField)
        {
            if ((textView.superview.superview.superview.frame.origin.y+textView.superview.superview.frame.origin.y+textView.superview.frame.origin.y)>yFact)
            {
                [self.orderConfirmScrollView setContentOffset:CGPointMake(self.orderConfirmScrollView.contentOffset.x, (textView.superview.superview.superview.frame.origin.y+textView.superview.superview.frame.origin.y+textView.superview.frame.origin.y-yFact))];
            }
            else
            {
                [self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
            }
        }
        else if ((textView.superview.frame.origin.y+textView.superview.superview.frame.origin.y)>yFact)
        {
            [self.orderConfirmScrollView setContentOffset:CGPointMake(self.orderConfirmScrollView.contentOffset.x, (textView.superview.frame.origin.y+textView.superview.superview.frame.origin.y-yFact))];
        }
        else
        {
            [self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
        }
        
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    //textFieldFlag = NO;
    numberFieldVal=0;
    //[self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - 

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    
    [self.view endEditing:YES];
    
}

- (IBAction)tapOutsideTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - --------------DELIVERY INFO METHODS------------
#pragma mark -

- (IBAction)deliveryInfoOptionSelection:(UIButton *)sender
{
    deliveryInfoRadioOptionString = @"";
    
    self.villaHouseRadioButton.selected = NO;
    self.buildingRadioButton.selected = NO;
    self.officeRadioButton.selected = NO;
    
    sender.selected = YES;
    
    switch (sender.tag)
    {
        case 1:
        {
            // Villa House
            deliveryInfoRadioOptionString = @"0";
            self.villaHouseRadioButton.selected=YES;
        }
            break;
        case 2:
        {
            // Buiding
            deliveryInfoRadioOptionString = @"1";
            self.buildingRadioButton.selected=YES;
        }
            break;
        case 3:
        {
            // Office
            deliveryInfoRadioOptionString = @"2";
            self.officeRadioButton.selected=YES;
        }
            break;
        default:
            break;
    }
    
    
    if (!self.orderConfirmScrollView.superview.hidden)
    {
        CGFloat yFact = 171.0f;
        self.villaOptionalView.hidden = YES;
        
        if (![deliveryInfoRadioOptionString isEqualToString:@"0"])
        {
            self.villaOptionalView.hidden = NO;
            yFact = 257.0f;
        }
        
        self.extraDirView.frame = CGRectMake(self.extraDirView.frame.origin.x, yFact, self.extraDirView.frame.size.width, self.extraDirView.frame.size.height);
        
//        self.cancelButton.superview.frame = CGRectMake(self.cancelButton.superview.frame.origin.x, (yFact+self.extraDirView.frame.size.height+35), self.cancelButton.superview.frame.size.width, self.extraDirView.frame.size.height);
        
        self.nextButton.frame = CGRectMake(self.nextButton.frame.origin.x, (yFact+self.extraDirView.frame.size.height+35), self.nextButton.frame.size.width, self.nextButton.frame.size.height);
        
    }
    
    
}

- (IBAction)selectAddressButtonAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (addressPlaceArray.count!=0) {
        [self showAddressList];
    }
    else
    {
        [self getCustomerAddresses];
        [self showAddressList];
    }
}

- (IBAction)AddAddressBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    [self clearAndEnableTextFieldsForAddingNewAddress];
}
- (IBAction)areaSelectionBttnAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    if (areaListArray.count!=0) {
        [self showAreaList];
    }
    else
    {
        [self getAreaDetails];
        [self showAreaList];
    }
}

- (IBAction)saveAndContinueAction:(id)sender
{
    if (self.blockValueField.text.length>0 && self.streetValueField.text.length>0&&self.buildingNumValueField.text.length>0&&self.areaDropDownValueField.text.length>0)
                {
                    [self addAddressDetails];
                }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
    }
}

- (IBAction)cancelAction:(id)sender
{
    [self.view endEditing:YES];
    [self updateTextValuesAndView:0];
}

- (IBAction)nextButtonAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    if (ApplicationDelegate.isLoggedIn)
    {
        if (self.blockValueField.text.length>0 && self.streetValueField.text.length>0&&self.buildingNumValueField.text.length>0&&self.areaDropDownValueField.text.length>0)
        {
            self.orderConfirmScrollView.superview.hidden=YES;
            self.confirmYourOrderView.hidden = YES;
            self.orderSummaryView.hidden = NO;
            self.orderSummaryScroll.contentOffset = CGPointZero;
            
            if ([self.orderSummaryView superview])
            {
                [self.orderSummaryView removeFromSuperview];
            }
            
            [(UIView *)[self.orderSummaryView.subviews objectAtIndex:0] layer].cornerRadius = 3.0;
            [(UIView *)[self.orderSummaryView.subviews objectAtIndex:0] layer].masksToBounds = YES;
            
            self.orderSummaryView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:self.orderSummaryView];
            
             [self getCartInfoList];
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
        }
    }
    else
    {
        if (self.firstNameValueField.text.length>0 && self.lastNameValueField.text.length>0 && self.emailValueField.text.length>0 && self.mobileValueField.text.length>0 && self.blockValueField.text.length>0 && self.streetValueField.text.length>0&&self.buildingNumValueField.text.length>0&&self.areaDropDownValueField.text.length>0)
        {
            if (self.emailValueField.text.length!=0)
            {
                if ( ![ApplicationDelegate isEmailValidWithEmailID:self.emailValueField.text])
                {
                    [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                    //self.email_txtBox.text=@"";
                }
                else if (!((self.mobileValueField.text.length>=6)&&(self.mobileValueField.text.length<=15)))
                {
                    [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:@"Mobile"];
                    self.mobileValueField.text=phoneStr;
                }
                else
                {
                    
                    self.orderConfirmScrollView.superview.hidden=YES;
                    self.confirmYourOrderView.hidden = YES;
                    self.orderSummaryView.hidden = NO;
                    self.orderSummaryScroll.contentOffset = CGPointZero;
                    
                    if ([self.orderSummaryView superview])
                    {
                        [self.orderSummaryView removeFromSuperview];
                    }
                    
                    [(UIView *)[self.orderSummaryView.subviews objectAtIndex:0] layer].cornerRadius = 3.0;
                    [(UIView *)[self.orderSummaryView.subviews objectAtIndex:0] layer].masksToBounds = YES;
                    self.orderSummaryView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                    [self.view addSubview:self.orderSummaryView];
                    
                    [self getTempCartInfoList];
                }
            }
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Please check that Area is selected and all mandatory fields are filled" title:kMESSAGE];
        }
    }
    

}


-(void)addAddressDetails
{
    
    NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:[self getAddressPostData]];
    if (addressData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine addAddressDetailsWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]] )
                     {
                         if ([[responseDictionary objectForKey:@"Status"] isEqualToString:[NSString stringWithFormat: @"Success"]])
                         {
                             fromAddAddress=YES;
                             
                             [self getCustomerAddresses];
                             if (self.blockValueField.text.length>0 && self.streetValueField.text.length>0&&self.buildingNumValueField.text.length>0&&self.areaDropDownValueField.text.length>0)
                             {
                                 [self getCartInfoList];
                                 self.orderConfirmScrollView.superview.hidden=YES;
                                 self.confirmYourOrderView.hidden = YES;
                                 self.orderSummaryView.hidden = NO;
                                 self.orderSummaryScroll.contentOffset = CGPointZero;
                                 
                                 if ([self.orderSummaryView superview])
                                 {
                                     [self.orderSummaryView removeFromSuperview];
                                 }
                                 
                                 [(UIView *)[self.orderSummaryView.subviews objectAtIndex:0] layer].cornerRadius = 3.0;
                                 [(UIView *)[self.orderSummaryView.subviews objectAtIndex:0] layer].masksToBounds = YES;
                                 self.orderSummaryView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                                 [self.view addSubview:self.orderSummaryView];
                             }
                             else
                             {
                                 [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
                             }
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                     
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
    
}


-(NSMutableDictionary *)getAddressPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:selectedAreaId forKey:@"areaId"];
    [postDic setObject:self.blockValueField.text forKey:@"block"];
    [postDic setObject:self.buildingNumValueField.text forKey:@"buildingNo"];
    [postDic setObject:self.extraDirValueField.text forKey:@"extraDirections"];
    [postDic setObject:self.floorNumValueField.text forKey:@"floor"];
    [postDic setObject:@"" forKey:@"isPrimary"];
    [postDic setObject:self.juddaValueField.text forKey:@"judda"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"profileName"];
    [postDic setObject:self.streetValueField.text forKey:@"street"];
    [postDic setObject:@"" forKey:@"suite"];
    [postDic setObject:deliveryInfoRadioOptionString forKey:@"type"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}

#pragma mark -
#pragma mark - --------------ORDER SUMMARY METHODS------------
#pragma mark -


-(void)getCartInfoList
{
    self.orderSummaryNextButton.enabled=NO;
    NSMutableArray *cartListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getCartPostData]];
    if (userData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.orderSummaryView];
        [ApplicationDelegate.engine getCartInfoWithDataDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             self.cartInfoItem = [ApplicationDelegate.mapper getCartInfoListFromDictionary:responseDictionary];
                             
                             
                             if (self.cartInfoItem.cart_InfoList_Array.count>0)
                             {
                                 for (int i=0; i<self.cartInfoItem.cart_InfoList_Array.count; i++)
                                 {
                                     if ([ApplicationDelegate isValid:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]])
                                     {
                                         RestaurantCart *restaurantCartItem = [ApplicationDelegate.mapper getRestaurantCartListFromDictionary:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]];
                                         
                                         [cartListArray addObject:restaurantCartItem];
                                     }
                                     
                                 }
                                 
                             }
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
                 
             }
             
             if (cartListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
                 
                 for (UIView *vw in self.orderSummaryScroll.subviews)
                 {
                     [vw removeFromSuperview];
                 }
                 self.orderSummaryNextButton.enabled=NO;
             }
             else{
                 self.orderSummaryNextButton.enabled=YES;
                 [self prepareCartListScrollWithListArray:cartListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getCartPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}
-(void)getTempCartInfoList
{
    self.orderSummaryNextButton.enabled = NO;
    NSMutableArray *cartListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getTempUserPostData]];
    if (userData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getTempCartInfoWithDataDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             self.cartInfoItem = [ApplicationDelegate.mapper getCartInfoListFromDictionary:responseDictionary];
                             
                             
                             if (self.cartInfoItem.cart_InfoList_Array.count>0)
                             {
                                 for (int i=0; i<self.cartInfoItem.cart_InfoList_Array.count; i++)
                                 {
                                     if ([ApplicationDelegate isValid:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]])
                                     {
                                         RestaurantCart *restaurantCartItem = [ApplicationDelegate.mapper getRestaurantCartListFromDictionary:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]];
                                         
                                         [cartListArray addObject:restaurantCartItem];
                                     }
                                     
                                 }
                                 
                             }
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
             }
             
             
             if (cartListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
                 
                 for (UIView *vw in self.orderSummaryScroll.subviews)
                 {
                     [vw removeFromSuperview];
                 }
                 self.orderSummaryNextButton.enabled=NO;
             }
             else{
                 self.orderSummaryNextButton.enabled = YES;
                 [self prepareCartListScrollWithListArray:cartListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getTempUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:ApplicationDelegate.currentDeviceId forKey:@"userId"];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

-(NSMutableDictionary *)getCartPostDataWithCuisineItem:(CuisineListItemCell *)cuisinelistItem
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:cuisinelistItem.cartDetailObj.shopping_CartId forKey:@"ShoppingCartId"];
    if (ApplicationDelegate.isLoggedIn) {
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"Username"];
    }
    else
    {
        [postDic setObject:ApplicationDelegate.currentDeviceId forKey:@"Username"];
    }
    
    
    return postDic;
}

-(void)prepareCartListScrollWithListArray:(NSMutableArray *)listArray
{
    confirmOrderSummaryCashFlag = NO;
    confirmOrderSummaryKnetFlag = NO;
    confirmOrderSummaryVisaFlag = NO;
    
    for (UIView *vw in self.orderSummaryScroll.subviews)
    {
        [vw removeFromSuperview];
    }
    
    CGFloat yOffset = 0.0f;
    //CGFloat total_Order_Amount = 0.0f;
    
    if ([ApplicationDelegate isValid:listArray])
    {
        for (int i=0; i<listArray.count; i++)
        {
            
            RestaurantCart *restCartObj = [listArray objectAtIndex:i];
            if (restCartObj.restaurant_Id.length>0)
            {
                PaymentOptionCell *paymentOptionCell = [[PaymentOptionCell alloc] init];
                
                paymentOptionCell.restaurant_NameLbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
                paymentOptionCell.paymentOptionsNameLabel.font=[UIFont fontWithName:@"Tahoma" size:13.0f];
                paymentOptionCell.deliveryNowNameLabel.font=[UIFont fontWithName:@"Tahoma" size:13.0f];
                paymentOptionCell.deliveryAtNameLabel.font=[UIFont fontWithName:@"Tahoma" size:13.0f];
                paymentOptionCell.deliverAtTxtValue.font=[UIFont fontWithName:@"Tahoma" size:13.0f];
                paymentOptionCell.menuLbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                paymentOptionCell.itemNameLbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                paymentOptionCell.qtyLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                paymentOptionCell.priceLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                paymentOptionCell.totalLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                paymentOptionCell.deliverNowButton.selected=YES;
                paymentOptionCell.restaurantCartObject = restCartObj;
                
                paymentOptionCell.visaImgView.hidden = YES;
                paymentOptionCell.cashImgView.hidden = NO;
                paymentOptionCell.creditImgView.hidden = YES;
                paymentOptionCell.kNetImgView.hidden = YES;
                
                CGFloat xPos = paymentOptionCell.cashImgView.frame.origin.x;
                
//                if (restCartObj.accepts_Cash.length>0)
//                {
//                    if ([restCartObj.accepts_Cash boolValue])
//                    {
//                        paymentOptionCell.cashImgView.frame = CGRectMake(xPos, paymentOptionCell.cashImgView.frame.origin.y, 35, 25);
//                        paymentOptionCell.cashImgView.hidden = NO;
//                        confirmOrderSummaryCashFlag = YES;
//                        xPos = xPos + 44.0f;
//                    }
//                }
                
                paymentOptionCell.cashImgView.frame = CGRectMake(xPos, paymentOptionCell.cashImgView.frame.origin.y, 35, 25);
                paymentOptionCell.cashImgView.hidden = NO;
                confirmOrderSummaryCashFlag = YES;
                xPos = xPos + 44.0f;
                
                if (restCartObj.accepts_CC.length>0)
                {
                    if ([restCartObj.accepts_CC boolValue])
                    {
                        paymentOptionCell.visaImgView.frame = CGRectMake(xPos, paymentOptionCell.visaImgView.frame.origin.y, 35, 25);
                        paymentOptionCell.visaImgView.hidden = NO;
                        confirmOrderSummaryVisaFlag = YES;
                        xPos = xPos + 44.0f;
                    }
                }
                
                if (restCartObj.accepts_KNET.length>0)
                {
                    if ([restCartObj.accepts_KNET boolValue])
                    {
                        paymentOptionCell.kNetImgView.frame = CGRectMake(xPos, paymentOptionCell.kNetImgView.frame.origin.y, 35, 25);
                        paymentOptionCell.kNetImgView.hidden = NO;
                        confirmOrderSummaryKnetFlag = YES;
                        xPos = xPos + 44.0f;
                    }
                }
                
                paymentOptionCell.frame = CGRectMake(0, (yOffset + 5), self.orderSummaryScroll.frame.size.width, paymentOptionCell.frame.size.height);
                paymentOptionCell.tag = i;
                paymentOptionCell.delegatepaymentOptionCell=self;
                if (restCartObj.restaurant_Thumbnail.length>0)
                {
                    [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:restCartObj.restaurant_Thumbnail] completionHandler:^(UIImage *responseImage) {
                        
                        if ([ApplicationDelegate isValid:responseImage])
                        {
                            [ApplicationDelegate loadImageWithAnimationInImageView:paymentOptionCell.restaurantImageView withImage:responseImage];
                        }
                        
                    } errorHandler:^(NSError *error) {
                        
                    }];
                }
                paymentOptionCell.restaurant_NameLbl.text = restCartObj.restaurant_Name;

                [self.orderSummaryScroll addSubview:paymentOptionCell];
                
                yOffset = paymentOptionCell.frame.origin.y + paymentOptionCell.frame.size.height;
                
                UIView *listContainerOuterView  = [[UIView alloc] initWithFrame:CGRectMake(0, (yOffset+5), self.orderSummaryScroll.frame.size.width, ((restCartObj.cart_InfoArray.count*135)+165+2))];
                listContainerOuterView.backgroundColor = [UIColor colorWithRed:(119.0/255.0) green:(119/255.0) blue:(119.0/255.0) alpha:1.0];
                
                UIView *listContainerInnerView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, (listContainerOuterView.frame.size.width-2), (listContainerOuterView.frame.size.height-2))];
                
                listContainerInnerView.backgroundColor = [UIColor colorWithRed:(230.0/255.0) green:(230/255.0) blue:(230.0/255.0) alpha:1.0];
                
                listContainerOuterView.layer.cornerRadius = 2.0;
                listContainerOuterView.layer.masksToBounds = YES;
                
                listContainerInnerView.layer.cornerRadius = 2.0;
                listContainerInnerView.layer.masksToBounds = YES;
                
                [listContainerOuterView addSubview:listContainerInnerView];
                
                CGFloat listY_Fact = 0.0f;
                for (int j=0; j<restCartObj.cart_InfoArray.count; j++)
                {
                    CartDetails *cartDetailItem = [ApplicationDelegate.mapper getCartDetailFromDictionary:[restCartObj.cart_InfoArray objectAtIndex:j]];
                    CuisineListItemCell *listCell = [[CuisineListItemCell alloc] init];
                    //listCell.cartDetailObj=cartDetailItem;
                    listCell.frame = CGRectMake(0, listY_Fact, listContainerInnerView.frame.size.width, listCell.frame.size.height);
                //    listCell.delegateCartListItem=self;
                    //  listCell.orderItemLabel.text=cartDetailItem.item_Name;
                    // listCell.orderItemLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                    
                    listCell.cartDetailObj = cartDetailItem;
                    listCell.delegateCuisineItemCell=self;
                    
                    listCell.cuisine_NameLbl.text=cartDetailItem.item_Name;
                    //  order_Amount = [cartDetailItem.item_Price floatValue]+order_Amount;
                    listCell.priceLbl.text =cartDetailItem.item_Price;
                    listCell.quantityLbl.font=[UIFont fontWithName:@"Tahoma" size:11.0f];
                    listCell.cuisine_NameLbl.font=[UIFont fontWithName:@"Tahoma" size:12.0f];
                    listCell.priceLbl.font=[UIFont fontWithName:@"Tahoma" size:11.0f];
                    listCell.totalLbl.font=[UIFont fontWithName:@"Tahoma" size:11.0f];
                    listCell.special_RequestLbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                    listCell.specialRequestTxtField.font=[UIFont fontWithName:@"Tahoma" size:13.0f];
                    listCell.specialRequestTxtField.delegate = self;
                    listCell.quantityLbl.text = cartDetailItem.quantity;
                    listCell.totalLbl.text = [NSString stringWithFormat:@"KD %.2f",(([cartDetailItem.quantity floatValue])*([cartDetailItem.item_Price floatValue]))];
                    listCell.tag = (i*1000)+j;
                    [ApplicationDelegate loadMarqueeLabelWithText:listCell.cuisine_NameLbl.text Font:listCell.cuisine_NameLbl.font InPlaceOfLabel:listCell.cuisine_NameLbl];
                    [listContainerInnerView addSubview:listCell];
                    
                    listY_Fact = ((j+1)*listCell.frame.size.height);
                }
                
                GeneralRequestCell *generalCell = [[GeneralRequestCell alloc] init];
                generalCell.frame = CGRectMake(0, listY_Fact, listContainerInnerView.frame.size.width, generalCell.frame.size.height);
                generalCell.tag = i;
                
                
                //generalCell.restaurant_NameLbl.text = restCartObj.restaurant_Name;
                generalCell.subTotalNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.subTotalValueLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.deliveryChargesNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.deliveryChargesValueLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.discountNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.discountValueLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.restaurantTotalNameLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.restaurantTotalValueLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                
                generalCell.generalRequestNameLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                generalCell.generalRequestTxtField.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                generalCell.generalRequestTxtField.delegate = self;
                generalCell.subTotalValueLabel.text = [NSString stringWithFormat:@"KD %@",restCartObj.subtotal];
                generalCell.deliveryChargesValueLabel.text = [NSString stringWithFormat:@"KD %@",restCartObj.delivery_Charges];
                generalCell.discountValueLabel.text = [NSString stringWithFormat:@"KD %@",restCartObj.discount];
                generalCell.restaurantTotalValueLabel.text = [NSString stringWithFormat:@"KD %@",restCartObj.restaurant_Total];
                
                [listContainerInnerView addSubview:generalCell];
                [self.orderSummaryScroll addSubview:listContainerOuterView];
                yOffset = listContainerOuterView.frame.origin.y + listContainerOuterView.frame.size.height;
            }

        }
    }
    self.orderSummaryScroll.contentSize = CGSizeMake(0, yOffset+100);
}
-(void)showDeliveryTimeList
{
    self.adressDropDownButton.selected = !self.adressDropDownButton.selected;
    if (self.adressDropDownButton.selected)
    {
        if (self.addressDropDownObj.view.superview) {
            [self.addressDropDownObj.view removeFromSuperview];
        }
        
        self.addressDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.addressDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.addressDropDownObj.view.frame = CGRectMake((self.adressDropDownButton.superview.frame.origin.x),(self.adressDropDownButton.superview.frame.origin.y+self.adressDropDownButton.superview.frame.size.height), (self.adressDropDownButton.superview.frame.size.width), 0);
        
        self.addressDropDownObj.dropDownDelegate = self;
        self.addressDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:addressPlaceArray];
        
        self.addressDropDownObj.view.layer.borderWidth = 0.1;
        self.addressDropDownObj.view.layer.shadowOpacity = 1.0;
        self.addressDropDownObj.view.layer.shadowRadius = 5.0;
        self.addressDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.addressDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.addressDropDownObj.textLabelColor = [UIColor blackColor];
        self.addressDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (addressPlaceArray.count>0)
        {
            [self.adressDropDownButton.superview.superview addSubview:self.addressDropDownObj.view];
        }
        else
        {
            self.adressDropDownButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.addressDropDownObj.cellHeight*self.addressDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.addressDropDownObj.cellHeight*self.addressDropDownObj.dataArray.count)+18.0f;
        }
        //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.addressDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.addressDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.addressDropDownObj.view.frame =
            CGRectMake(self.addressDropDownObj.view.frame.origin.x+2,
                       self.addressDropDownObj.view.frame.origin.y,
                       self.addressDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.addressDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.addressDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.addressDropDownObj.view.frame =
            CGRectMake(self.addressDropDownObj.view.frame.origin.x,
                       self.addressDropDownObj.view.frame.origin.y,
                       self.addressDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.addressDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
}
-(NSString *)getCurrentDeliveryTimeString
{
    NSString *timeStr = @"";
    
    NSDateFormatter *sourceDateFormatter = [[NSDateFormatter alloc] init];
    sourceDateFormatter.dateFormat = @"EEEE HH:mm a";
    //[sourceDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    timeStr = [sourceDateFormatter stringFromDate:[NSDate date]];
    
    return timeStr;
}
- (IBAction)orderSummaryBackButtonAction:(id)sender
{
    [self.view endEditing:YES];
    
    self.orderConfirmScrollView.superview.hidden=NO;
    self.confirmYourOrderView.hidden = YES;
    
    [self.orderConfirmScrollView setContentOffset:CGPointZero];
    
    if ([self.orderSummaryView superview])
    {
        [self.orderSummaryView removeFromSuperview];
    }
}

- (IBAction)orderSummaryNextButtonAction:(id)sender
{
    [self.view endEditing:YES];
    
    self.orderConfirmScrollView.superview.hidden=YES;
    self.orderSummaryView.hidden=YES;
    self.confirmYourOrderView.hidden = NO;
    if ([self.confirmYourOrderView superview])
    {
        [self.confirmYourOrderView removeFromSuperview];
    }
    [(UIView *)[self.confirmYourOrderView.subviews objectAtIndex:0] layer].cornerRadius = 3.0;
    [(UIView *)[self.confirmYourOrderView.subviews objectAtIndex:0] layer].masksToBounds = YES;
    
    self.cashRadioOptionButton.superview.hidden = YES;
    self.visaRadioOptionButton.superview.hidden = YES;
    self.knetRadioOptionButton.superview.hidden = YES;
    self.u3anCreditOptionButton.superview.hidden = YES;
    
    CGFloat xPos = self.cashRadioOptionButton.superview.frame.origin.x;
    
//    if (confirmOrderSummaryCashFlag)
//    {
//        self.cashRadioOptionButton.superview.frame = CGRectMake(xPos, self.cashRadioOptionButton.superview.frame.origin.y, 65, 27);
//        self.cashRadioOptionButton.superview.hidden = NO;
//        xPos = xPos + 67.0f;
//    }
    
    self.cashRadioOptionButton.superview.frame = CGRectMake(xPos, self.cashRadioOptionButton.superview.frame.origin.y, 65, 27);
    self.cashRadioOptionButton.superview.hidden = NO;
    xPos = xPos + 67.0f;
    
    if (confirmOrderSummaryVisaFlag)
    {
        self.visaRadioOptionButton.superview.frame = CGRectMake(xPos, self.visaRadioOptionButton.superview.frame.origin.y, 65, 27);
        self.visaRadioOptionButton.superview.hidden = NO;
        xPos = xPos + 67.0f;
    }
    
    if (confirmOrderSummaryKnetFlag)
    {
        self.knetRadioOptionButton.superview.frame = CGRectMake(xPos, self.knetRadioOptionButton.superview.frame.origin.y, 65, 27);
        self.knetRadioOptionButton.superview.hidden = NO;
        xPos = xPos + 67.0f;
    }
    
    if ([ApplicationDelegate isLoggedIn])
    {
        if (confirmOrderSummaryU3anCreditFlag)
        {
            self.u3anCreditOptionButton.superview.frame = CGRectMake(xPos, self.u3anCreditOptionButton.superview.frame.origin.y, 65, 27);
            self.u3anCreditOptionButton.superview.hidden = NO;
            xPos = xPos + 67.0f;
        }
    }
    
    self.subTotalValueLabel.text = [NSString stringWithFormat:@"KD %.2f",[self.cartInfoItem.subtotal_All floatValue]];
    self.deliveryChargesValueLabel.text = [NSString stringWithFormat:@"KD %.2f",[self.cartInfoItem.deliveryCharges_All floatValue]];
    self.discountValueLabel.text = [NSString stringWithFormat:@"KD %.2f",[self.cartInfoItem.discount_All floatValue]];
    self.grandTotalValueLabel.text = [NSString stringWithFormat:@"KD %.2f",[self.cartInfoItem.grandTotal_All floatValue]];
    self.payByCashValueLabel.text = [NSString stringWithFormat:@"KD %.2f",[self.cartInfoItem.payByCash_All floatValue]];
    self.confirmYourOrderScroll.contentSize= CGSizeMake(self.confirmYourOrderScroll.frame.size.width, 260);
    self.confirmYourOrderView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.confirmYourOrderView];

}
#pragma mark -
#pragma mark - --------------CONFIRM ORDER METHODS------------
#pragma mark -

- (IBAction)confirmOrderBackButtonAction:(id)sender
{
    [self.view endEditing:YES];
    
    self.orderConfirmScrollView.superview.hidden=YES;
    self.orderSummaryView.hidden = NO;
    
    [self.orderSummaryScroll setContentOffset:CGPointZero];
    
    if ([self.confirmYourOrderView superview])
    {
        [self.confirmYourOrderView removeFromSuperview];
    }
}

- (IBAction)paymentRadioOptionSelection:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    self.cashRadioOptionButton.selected = NO;
    self.knetRadioOptionButton.selected = NO;
    self.visaRadioOptionButton.selected = NO;
    self.u3anCreditOptionButton.selected = NO;
    
    sender.selected = YES;
}

- (IBAction)confirmOrderButtonAction:(id)sender
{
    [self.view endEditing:YES];
    
    if ((self.cashRadioOptionButton.selected)||(self.knetRadioOptionButton.selected)||(self.visaRadioOptionButton.selected)||(self.u3anCreditOptionButton.selected))
    {
        if ([[self getConfirmOrderPostData] count]>0)
        {
            //NSLog(@"%@",[self getConfirmOrderPostData]);
            
            __block BOOL dataFound = NO;
            
            [ApplicationDelegate.engine sendPlaceOrderRequestWithPostData:[self getConfirmOrderPostData] CompletionHandler:^(NSMutableDictionary *responseDictionary)
             {
                 if (!dataFound)
                 {
                     dataFound = YES;
                     
                     //NSLog(@"%@",responseDictionary);
                     
                     if ([ApplicationDelegate isValid:responseDictionary])
                     {
                         if (responseDictionary.count>0)
                         {
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                             {
                                 
                                 if ([[responseDictionary objectForKey:@"status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                                 {
                                     [ApplicationDelegate clearGuestUserSelection];
                                     
                                     self.confirmOrderButton.enabled=NO;
                                     
                                     OrderConfirmationResultVC *resVC = [[OrderConfirmationResultVC alloc] initWithNibName:@"OrderConfirmationResultVC" bundle:nil]
                                     ;
                                     resVC.view.backgroundColor = [UIColor clearColor];
                                     resVC.urlString=[responseDictionary objectForKey:@"Value"];
                                     
                                     if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[resVC class]])
                                     {
                                         [ApplicationDelegate.subHomeNav pushViewController:resVC animated:NO];
                                     }
                                     
                                 }
                                 else
                                 {
                                     [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                                 }
                             }
                         }
                     }
                 }
                
            } errorHandler:^(NSError *error) {
                
            }];
            
            
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Unable to proceed. Please verify your Orders." title:kMESSAGE];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please select your payment mode to proceed further" title:kMESSAGE];
    }
}

-(NSMutableDictionary *)getConfirmOrderPostData
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    
    BOOL success = NO;
    
    NSMutableArray *orderXMLArray = [[NSMutableArray alloc] init];
    NSString *paymentDeliveryTime = @"";
    
    for (id sub in self.orderSummaryScroll.subviews)
    {
        if ([sub isKindOfClass:[PaymentOptionCell class]])
        {
            PaymentOptionCell *paymentCell = (PaymentOptionCell *)sub;
            
            if (paymentCell.deliverNowButton.selected)
            {
                paymentDeliveryTime = [self getCurrentDeliveryTimeString];
            }
            else
            {
                paymentDeliveryTime = paymentCell.deliverAtTxtValue.text;
            }
        }
        else
        {
            UIView *containerVw = [[(UIView *)sub subviews] objectAtIndex:0];
            
            NSString *generalReqVal = @"";
            
            if (containerVw.subviews.count>0)
            {
                if ([[containerVw.subviews lastObject] isKindOfClass:[GeneralRequestCell class]])
                {
                    GeneralRequestCell *genCell = (GeneralRequestCell *)[containerVw.subviews lastObject];
                    
                    generalReqVal = genCell.generalRequestTxtField.text;
                }
                
                for (id item in containerVw.subviews)
                {
                    if ([item isKindOfClass:[CuisineListItemCell class]])
                    {
                        CuisineListItemCell *cuisineCell = (CuisineListItemCell *)item;
                        
                        NSMutableDictionary *itemDic = [[NSMutableDictionary alloc] init];
                        
                        [itemDic setObject:[NSNumber numberWithLongLong:[cuisineCell.cartDetailObj.area_Id longLongValue]] forKey:@"AreaId"];
                        [itemDic setObject:paymentDeliveryTime forKey:@"Deliverytime"];
                        [itemDic setObject:generalReqVal forKey:@"GeneralRequest"];
                        [itemDic setObject:cuisineCell.cartDetailObj.item_ChoiceId forKey:@"ItemChoiceIds"];
                        [itemDic setObject:cuisineCell.cartDetailObj.quantity forKey:@"ItemChoiceQty"];
                        [itemDic setObject:[NSNumber numberWithLongLong:[cuisineCell.cartDetailObj.item_Id longLongValue]] forKey:@"ItemId"];
                        [itemDic setObject:cuisineCell.cartDetailObj.item_Price forKey:@"Price"];
                        [itemDic setObject:[NSNumber numberWithLongLong:[cuisineCell.cartDetailObj.quantity longLongValue]] forKey:@"Quantity"];
                        [itemDic setObject:[NSNumber numberWithLongLong:[cuisineCell.cartDetailObj.restaurant_Id longLongValue]] forKey:@"RestaurantId"];
                        [itemDic setObject:cuisineCell.specialRequestTxtField.text forKey:@"SpecialRequest"];
                        
                        //NSLog(@"ITM = %@",itemDic);
                        
                        [orderXMLArray addObject:itemDic];
                    }
                }
                
            }
            
        }
        
        
    }
    
    if ([self.cartInfoItem.grandTotal_All floatValue]>0)
    {
        success = YES;
    }
    
    if (success)
    {
        [postData setObject:[self getPaymentMode] forKey:@"PaymentMode"];
        
        [postData setObject:@"0" forKey:@"payByCash"];
        [postData setObject:@"0" forKey:@"payByCC"];
        [postData setObject:@"0" forKey:@"payByKNET"];
        [postData setObject:@"0" forKey:@"payByU3an"];
        
        if ([[self getPaymentMode] integerValue]==1)
        {
            [postData setObject:self.cartInfoItem.payByCash_All forKey:@"payByCash"];
        }
        else if([[self getPaymentMode] integerValue]==2)
        {
            [postData setObject:self.cartInfoItem.grandTotal_All forKey:@"payByKNET"];
        }
        else if([[self getPaymentMode] integerValue]==3)
        {
            [postData setObject:self.cartInfoItem.grandTotal_All forKey:@"payByCC"];
        }
        else if([[self getPaymentMode] integerValue]==4)
        {
            [postData setObject:self.cartInfoItem.grandTotal_All forKey:@"payByU3an"];
        }
        else
        {
            
        }
        
        if (ApplicationDelegate.isLoggedIn)
        {
            [postData setObject:ApplicationDelegate.logged_User_Name forKey:@"username"];
            [postData setObject:ApplicationDelegate.authn_Key forKey:@"authKey"];
            [postData setObject:selectedAddressItem.firstName forKey:@"firstname"];
            [postData setObject:selectedAddressItem.lastName forKey:@"lastname"];
            [postData setObject:selectedAddressItem.email forKey:@"email"];
            [postData setObject:selectedAddressItem.mobile forKey:@"mobile"];
            [postData setObject:selectedAddressItem.company forKey:@"company"];
            [postData setObject:selectedAddressItem.housePhone forKey:@"housephone"];
            [postData setObject:selectedAddressItem.workPhone forKey:@"workphone"];


        }
        else
        {
            [postData setObject:ApplicationDelegate.currentDeviceId forKey:@"username"];
            [postData setObject:@"" forKey:@"authKey"];
            [postData setObject:self.firstNameValueField.text forKey:@"firstname"];
            [postData setObject:self.lastNameValueField.text forKey:@"lastname"];
            [postData setObject:self.emailValueField.text forKey:@"email"];
            [postData setObject:self.mobileValueField.text forKey:@"mobile"];
            [postData setObject:@"" forKey:@"company"];
            [postData setObject:@"" forKey:@"housephone"];
            [postData setObject:@"" forKey:@"workphone"];


        }
        
        [postData setObject:@"Mobile Application" forKey:@"orderSource"];
        //[postData setObject:kLocal forKey:@"locale"];
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [postData setObject:k_AR_Local forKey:@"locale"];
        }
        else
        {
            [postData setObject:kLocal forKey:@"locale"];
        }
        [postData setObject:deliveryInfoRadioOptionString forKey:@"addressType"];
        [postData setObject:self.apartmentValueField.text forKey:@"apartment"];
        [postData setObject:selectedAreaId forKey:@"areaId"];
        [postData setObject:self.blockValueField.text forKey:@"block"];
        [postData setObject:self.buildingNumValueField.text forKey:@"buildingNo"];
        [postData setObject:self.extraDirValueField.text forKey:@"extraDirections"];
        [postData setObject:self.floorNumValueField.text forKey:@"floor"];
        [postData setObject:self.juddaValueField.text forKey:@"judda"];
        [postData setObject:self.streetValueField.text forKey:@"street"];
        [postData setObject:orderXMLArray forKey:@"orderXML"];
        
    }
    
    
    return postData;
}
-(NSString *)getPaymentMode
{
    NSString *paymentMode = @"0";
    if (self.cashRadioOptionButton.selected)
    {
        paymentMode = @"1";
    }
    if (self.knetRadioOptionButton.selected)
    {
        paymentMode = @"2";
    }
    if (self.visaRadioOptionButton.selected)
    {
        paymentMode = @"3";
    }
    if (self.u3anCreditOptionButton.selected)
    {
        paymentMode = @"4";
    }
    
    return paymentMode;
}
#pragma mark - --------------PAYMENT DELEGATES------------


-(void)deliveryAtTimeDropDownBttnDidClicked:(PaymentOptionCell *)paymentOptionCellItem
{
    [self.view endEditing:YES];
    
    if (paymentOptionCellItem.deliverAtButton.selected)
    {
        if (paymentOptionCellItem.deliveryAtDropDownButton.selected)
        {
            // NSLog(@"TIME ARRAY>>>>%@",paymentOptionCellItem.restaurantCartObject.delivery_TimingArray);
            
            timeArray = [[NSMutableArray alloc] init];
            
            if ([ApplicationDelegate isValid:paymentOptionCellItem.restaurantCartObject.delivery_TimingArray])
            {
                if (paymentOptionCellItem.restaurantCartObject.delivery_TimingArray.count>0)
                {
                    for (NSMutableDictionary *dic in paymentOptionCellItem.restaurantCartObject.delivery_TimingArray)
                    {
                        if ([ApplicationDelegate isValid:dic])
                        {
                            if ([ApplicationDelegate isValid:[dic objectForKey:@"DelTime"]])
                            {
                                [timeArray addObject:[dic objectForKey:@"DelTime"]];
                            }
                        }
                    }
                }
            }
            
            //---IF DELEIVERY TIME IS EMPTY ADD CURRENT TIME TO THE LIST
            
            if (timeArray.count==0)
            {
                [timeArray addObject:[self getCurrentDeliveryTimeString]];
            }
            
            if (self.orderSummaryDeliveryAtDropDownObj.view.superview)
            {
                [self.orderSummaryDeliveryAtDropDownObj.view removeFromSuperview];
            }
            
            self.orderSummaryDeliveryAtDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
            
            //////////// Customize the dropDown view Frame as your requirement
            
            self.orderSummaryDeliveryAtDropDownObj.cellHeight = 35.0f;
            
            self.orderSummaryDeliveryAtDropDownObj.view.frame = CGRectMake((paymentOptionCellItem.deliveryAtDropDownButton.superview.superview.frame.origin.x+paymentOptionCellItem.deliveryAtDropDownButton.superview.frame.origin.x),(paymentOptionCellItem.deliveryAtDropDownButton.superview.superview.frame.origin.y+paymentOptionCellItem.deliveryAtDropDownButton.superview.frame.origin.y+paymentOptionCellItem.deliveryAtDropDownButton.superview.frame.size.height), (paymentOptionCellItem.deliveryAtDropDownButton.superview.frame.size.width), 0);
            
            self.orderSummaryDeliveryAtDropDownObj.dropDownDelegate = self;
            self.orderSummaryDeliveryAtDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:timeArray];
            
            self.orderSummaryDeliveryAtDropDownObj.view.layer.borderWidth = 0.1;
            self.orderSummaryDeliveryAtDropDownObj.view.layer.shadowOpacity = 1.0;
            self.orderSummaryDeliveryAtDropDownObj.view.layer.shadowRadius = 5.0;
            self.orderSummaryDeliveryAtDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
            
            ///////Disabling Other part of the view except the DropDown View 	Container////////
            //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
            //            if (vw!=self.self.cuisineSelectionButton) {
            //                [vw setUserInteractionEnabled:NO];
            //            }
            //        }
            //////////////
            self.orderSummaryDeliveryAtDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
            self.orderSummaryDeliveryAtDropDownObj.textLabelColor = [UIColor blackColor];
            self.orderSummaryDeliveryAtDropDownObj.view.backgroundColor = [UIColor whiteColor];
            
            self.orderSummaryDeliveryAtDropDownObj.view.tag = paymentOptionCellItem.tag;
            
            if (timeArray.count>0)
            {
                [paymentOptionCellItem.superview addSubview:self.orderSummaryDeliveryAtDropDownObj.view];
            }
            else
            {
                paymentOptionCellItem.deliveryAtDropDownButton.selected = NO;
            }
            
            /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
            CGFloat tableHT = 120.0f;
            
            if ((self.orderSummaryDeliveryAtDropDownObj.cellHeight*self.orderSummaryDeliveryAtDropDownObj.dataArray.count)<tableHT)
            {
                tableHT = (self.orderSummaryDeliveryAtDropDownObj.cellHeight*self.orderSummaryDeliveryAtDropDownObj.dataArray.count)+10.0f;
            }
            //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
            
            [UIView animateWithDuration:0.09f animations:^{
                self.orderSummaryDeliveryAtDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
                self.orderSummaryDeliveryAtDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
                self.orderSummaryDeliveryAtDropDownObj.view.frame =
                CGRectMake(self.orderSummaryDeliveryAtDropDownObj.view.frame.origin.x+2,
                           self.orderSummaryDeliveryAtDropDownObj.view.frame.origin.y,
                           self.orderSummaryDeliveryAtDropDownObj.view.frame.size.width-4,
                           tableHT);
            }];
            
            
            
        }
        else
        {
            self.orderSummaryDeliveryAtDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
            self.orderSummaryDeliveryAtDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
            
            ////////// ReEnabling the UserInteraction of all SubViews/////////
            for (UIView *vw in self.view.subviews) {
                [vw setUserInteractionEnabled:YES];
            }
            ///////////////////
            
            [UIView animateWithDuration:0.4f animations:^{
                self.orderSummaryDeliveryAtDropDownObj.view.frame =
                CGRectMake(self.orderSummaryDeliveryAtDropDownObj.view.frame.origin.x,
                           self.orderSummaryDeliveryAtDropDownObj.view.frame.origin.y,
                           self.orderSummaryDeliveryAtDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.orderSummaryDeliveryAtDropDownObj.view removeFromSuperview];
                }
                
            }];
        }
    }
      
}

-(void)deliveryTimeRadioBttnDidClicked:(PaymentOptionCell *)paymentOptionCellItem
{
    [self.view endEditing:YES];
    
    paymentOptionCellItem.deliveryAtDropDownButton.userInteractionEnabled = NO;
    
    if (paymentOptionCellItem.deliverAtButton.selected)
    {
        paymentOptionCellItem.deliveryAtDropDownButton.userInteractionEnabled = YES;
        paymentOptionCellItem.deliveryAtDropDownButton.selected = NO;
    }
    else
    {
        if (self.orderSummaryDeliveryAtDropDownObj.view.superview)
        {
            [self.orderSummaryDeliveryAtDropDownObj.view removeFromSuperview];
        }
    }
}

#pragma mark - --------------CUISINE LIST ITEM DELEGATES------------

- (void) removeCuisineItemButtonDidClicked:(CuisineListItemCell *) cuisineItemCell
{
    [self.view endEditing:YES];
    
    NSMutableDictionary *cartPostDic = [[NSMutableDictionary alloc] initWithDictionary:[self getCartPostDataWithCuisineItem:cuisineItemCell]];
    
    if (cartPostDic.count>0)
    {
        __block BOOL dataFound = NO;
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        if (ApplicationDelegate.isLoggedIn) {
        
        [ApplicationDelegate.engine removeFromCartWithDataDictionary:cartPostDic CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             if (!dataFound)
             {
                 dataFound = YES;
                 
                 if ([ApplicationDelegate isValid:responseDictionary])
                 {
                     //NSLog(@"%@",responseDictionary);
                     
                     if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                     {
                         if ([self.orderSummaryView superview])
                         {
                             //self.orderConfirmScrollView.hidden=YES;
                             
                             [self getCartInfoList];
                         }
                         
                         
                     }
                 }
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             
         }];
        }
        else
        {
            [ApplicationDelegate.engine removeFromTempCartWithDataDictionary:cartPostDic CompletionHandler:^(NSMutableDictionary *responseDictionary)
             {
                 if (!dataFound)
                 {
                     dataFound = YES;
                     
                     if ([ApplicationDelegate isValid:responseDictionary])
                     {
                         //NSLog(@"%@",responseDictionary);
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             if ([self.orderSummaryView superview])
                             {
                                 self.orderConfirmScrollView.hidden=YES;
                                 
                                 [self getTempCartInfoList];
                             }
                             
                             
                         }
                     }
                     [ApplicationDelegate removeProgressHUD];
                 }
                 
             } errorHandler:^(NSError *error) {
                 
                 [ApplicationDelegate removeProgressHUD];
                 
             }];
        }
        
    }
}


-(void)xibLoading
{
    NSString *nibName = @"OrderConfirmationVCViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
