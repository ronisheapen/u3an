//
//  OrderConfirmationResultVC.h
//  U3AN
//
//  Created by Vipin on 10/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderConfirmationResultVC : UIViewController

@property (strong, nonatomic) NSString *urlString;
@property (strong, nonatomic) IBOutlet UIWebView *resultWebView;

@end
