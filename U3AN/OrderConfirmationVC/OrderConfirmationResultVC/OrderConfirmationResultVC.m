//
//  OrderConfirmationResultVC.m
//  U3AN
//
//  Created by Vipin on 10/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "OrderConfirmationResultVC.h"

@interface OrderConfirmationResultVC ()

@end

@implementation OrderConfirmationResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self setupUI];
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
  //  [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.resultWebView.layer.cornerRadius = 3.0;
    self.resultWebView.layer.masksToBounds = YES;
    self.resultWebView.scalesPageToFit = YES;
    [ApplicationDelegate addProgressHUDToView:self.resultWebView];
    [self.resultWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    
}

#pragma mark - UIWebViewDelegate Protocol Methods

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [ApplicationDelegate addProgressHUDToView:self.resultWebView];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [ApplicationDelegate removeProgressHUD];
}

@end
