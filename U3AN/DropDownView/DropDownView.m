//
//  DropDownView.m
//  DropDown
//
//  Created by Ratheesh Kumar R on 28/01/14.
//  Copyright (c) 2014 Ratheesh. All rights reserved.
//

#import "DropDownView.h"

@interface DropDownView ()

@end

@implementation DropDownView
@synthesize dropDownDelegate,dataArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITABLEVIEW METHODS

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Code comes here..
    }
    cell.textLabel.text =[NSString stringWithFormat:@"%@",[dataArray objectAtIndex:indexPath.row]];
//    cell.textLabel.textColor = [UIColor grayColor];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundView.backgroundColor = [UIColor clearColor];

    cell.backgroundColor = [UIColor clearColor];
//    cell.backgroundColor = [UIColor colorWithRed:(245.0f/255.0f) green:(245.0f/255.0f) blue:(245.0f/255.0f) alpha:1.0];
    
    //    UIImageView * separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 267, 1)];
    //    separatorLineView.backgroundColor = [UIColor clearColor];
    //    separatorLineView.image = [UIImage imageNamed:@"line_black.png"];
    //    [cell.contentView addSubview:separatorLineView];
    
//    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    [cell.textLabel setFont:self.textLabelFont];
    [cell.textLabel setTextColor:self.textLabelColor];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        cell.textLabel.textAlignment = NSTextAlignmentRight;
    }
    return cell;
}

//-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
//    [headerView setBackgroundColor:[UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.75]];
//    //headerView.backgroundColor = [UIColor clearColor];
//
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)];
//    label.text = [self tableView:self.menuTableView titleForHeaderInSection:section];
//    label.font = [UIFont fontWithName:@"Arial" size:14.0];
//    label.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
//    label.backgroundColor = [UIColor clearColor];
//    [headerView addSubview:label];
//
//    return headerView;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellHeight;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.dropDownDelegate selectList:indexPath.row];
}

@end
