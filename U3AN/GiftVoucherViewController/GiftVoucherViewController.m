//
//  GiftVoucherViewController.m
//  U3AN
//
//  Created by Vipin on 19/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "GiftVoucherViewController.h"
#import "GiftVoucher.h"
#import "GiftVoucherDetailVC.h"

@interface GiftVoucherViewController ()

@end

@implementation GiftVoucherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self setupUI];
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.giftVoucherTitleView.layer.cornerRadius = 3.0;
    self.giftVoucherTitleView.layer.masksToBounds = YES;
    self.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f];
    self.descriptionLabel.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
    [self getGiftVoucherDetails];
}

-(void)getGiftVoucherDetails
{
    NSMutableArray *giftVoucherListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getGiftVouchersWithDataDictionary:postData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             GiftVoucher *giftVoucherItem = [ApplicationDelegate.mapper getGiftVoucherDetailsFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [giftVoucherListArray addObject:giftVoucherItem];
                         }
                     }
                     
                 }
             }
             if (giftVoucherListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareGiftVoucherListScroll_Grid_WithArray:giftVoucherListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

#pragma mark - RESTAURANTS IN SCROLLVIEW METHODS

-(void)prepareGiftVoucherListScroll_Grid_WithArray:(NSMutableArray *)voucherListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.giftVoucherScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.giftVoucherScrollView.backgroundColor = [UIColor clearColor];
    
    [self.giftVoucherScrollView setContentOffset:CGPointZero];
    //self.restaurant_ScrollView.contentSize = CGSizeMake((restaurantListArray.count*self.restaurant_ScrollView.frame.size.width), 0);
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    GiftVoucherCell *vw = [[GiftVoucherCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.giftVoucherScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<voucherListArray.count; i++)
    {
        int colIndex = i%colCount;
        GiftVoucherCell *giftVoucherCell = [[GiftVoucherCell alloc] init];
        
        giftVoucherCell.layer.cornerRadius = 2.0;
        giftVoucherCell.layer.masksToBounds = YES;
        giftVoucherCell.giftVoucherImageView.layer.cornerRadius = 2.0;
        giftVoucherCell.giftVoucherImageView.layer.masksToBounds = YES;
        
        GiftVoucher *voucherItem = [voucherListArray objectAtIndex:i];
        giftVoucherCell.giftVoucherObj=voucherItem;
        giftVoucherCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  giftVoucherCell.frame.size.width, giftVoucherCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:voucherItem.voucher_Image] completionHandler:^(UIImage *responseImage) {
            
            giftVoucherCell.giftVoucherImageView.image = responseImage;
            [giftVoucherCell.giftVoucherImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        giftVoucherCell.delegategiftVoucherItem=self;
        
        [self.giftVoucherScrollView addSubview:giftVoucherCell];
        
        if (colIndex == (colCount - 1))
        {
            yVal = giftVoucherCell.frame.origin.y + giftVoucherCell.frame.size.height;
        }
        
        scrollHeight = giftVoucherCell.frame.origin.y + giftVoucherCell.frame.size.height;
    }
    
    self.giftVoucherScrollView.contentSize = CGSizeMake(self.giftVoucherScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
}

#pragma mark -Gift Voucher Delegate Method

-(void) giftVoucherItemDidClicked:(GiftVoucherCell *)giftVoucherCellItem
{
    
        GiftVoucherDetailVC *giftVoucherDetailVC = [[GiftVoucherDetailVC alloc] initWithNibName:@"GiftVoucherDetailVC" bundle:nil]
        ;
        giftVoucherDetailVC.view.backgroundColor = [UIColor clearColor];
        giftVoucherDetailVC.giftVoucherObj=giftVoucherCellItem.giftVoucherObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[giftVoucherDetailVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:giftVoucherDetailVC animated:NO];
        }
}

-(void)xibLoading
{
    NSString *nibName = @"GiftVoucherViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
