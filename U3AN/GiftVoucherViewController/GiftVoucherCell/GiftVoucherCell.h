//
//  GiftVoucherCell.h
//  U3AN
//
//  Created by Vipin on 19/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol giftVoucherItemDelegate;

@interface GiftVoucherCell : UIView
@property(weak,nonatomic)id<giftVoucherItemDelegate> delegategiftVoucherItem;
@property (strong, nonatomic) IBOutlet UIImageView *giftVoucherImageView;
@property(strong,nonatomic)GiftVoucher *giftVoucherObj;

@end

@protocol giftVoucherItemDelegate <NSObject>
- (void) giftVoucherItemDidClicked:(GiftVoucherCell *) giftVoucherCellItem;
@end