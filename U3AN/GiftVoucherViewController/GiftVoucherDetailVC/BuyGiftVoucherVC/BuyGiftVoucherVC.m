//
//  BuyGiftVoucherVC.m
//  U3AN
//
//  Created by Vipin on 20/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "BuyGiftVoucherVC.h"

@interface BuyGiftVoucherVC ()

@end

@implementation BuyGiftVoucherVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self setupUI];
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.kNETWebView.layer.cornerRadius = 3.0;
    self.kNETWebView.layer.masksToBounds = YES;
    self.kNETWebView.scalesPageToFit = YES;
    [ApplicationDelegate addProgressHUDToView:self.kNETWebView];
[self.kNETWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    
}

#pragma mark - UIWebViewDelegate Protocol Methods

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
[ApplicationDelegate addProgressHUDToView:self.kNETWebView];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [ApplicationDelegate removeProgressHUD];
}
@end
