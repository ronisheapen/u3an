//
//  GiftVoucherDetailVC.h
//  U3AN
//
//  Created by Vipin on 19/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftVoucher.h"

@interface GiftVoucherDetailVC : UIViewController
@property (strong,nonatomic)GiftVoucher *giftVoucherObj;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *couponNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *emailLbl;
@property (strong, nonatomic) IBOutlet UILabel *policyLbl;
@property (strong, nonatomic) IBOutlet UIButton *submitBttn;
@property (strong, nonatomic) IBOutlet UIButton *policyTickBttn;
@property (strong, nonatomic) IBOutlet UITextField *couponNameText;
@property (strong, nonatomic) IBOutlet UITextField *emailText;

- (IBAction)backgroundTapAction:(id)sender;

- (IBAction)policyTickBttnAction:(UIButton *)sender;
- (IBAction)submitBttnAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *giftVoucherScrollView;
@property (strong, nonatomic) IBOutlet UIView *formView;

@end
