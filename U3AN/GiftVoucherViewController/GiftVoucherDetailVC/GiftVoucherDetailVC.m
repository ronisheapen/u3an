//
//  GiftVoucherDetailVC.m
//  U3AN
//
//  Created by Vipin on 19/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "GiftVoucherDetailVC.h"
#import "BuyGiftVoucherVC.h"

#define MAXLENGTH 25

@interface GiftVoucherDetailVC ()

@end

@implementation GiftVoucherDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
    [self.policyTickBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.policyTickBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    self.submitBttn.enabled=NO;
    self.giftVoucherScrollView.contentSize = CGSizeMake(self.giftVoucherScrollView.frame.size.width,  self.submitBttn.superview.frame.origin.y+self.submitBttn.superview.frame.size.height+10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self setupUI];
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.giftVoucherScrollView.layer.cornerRadius = 3.0;
    self.giftVoucherScrollView.layer.masksToBounds = YES;
    self.formView.layer.cornerRadius = 3.0;
    self.formView.layer.masksToBounds = YES;
    self.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:16.0f];
    self.policyLbl.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
    self.couponNameLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.emailLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.couponNameText.text = @"";
    self.emailText.text = @"";
    self.policyTickBttn.selected = NO;
    self.submitBttn.enabled = NO;
}


- (IBAction)backgroundTapAction:(id)sender
{
    [self.view endEditing:YES];
    [self.giftVoucherScrollView setContentOffset:CGPointZero];
}

- (IBAction)policyTickBttnAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    self.policyTickBttn.selected = !self.policyTickBttn.selected;
    
    if (self.policyTickBttn.selected==YES) {
        self.submitBttn.enabled=YES;
    }
    else
    {
        self.submitBttn.enabled=NO;
    }
}

- (IBAction)submitBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if ((self.couponNameText.text.length>0)&&(self.emailText.text.length>0))
    {
        if ([ApplicationDelegate isEmailValidWithEmailID:self.emailText.text])
        {
            [self buyGiftVoucher];
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
            self.emailText.text=@"";
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please enter your Coupon Name and Email" title:@""];
    }
    
}

-(void)buyGiftVoucher
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine buyGiftVoucherWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"status"]])
                 {
                     if ([[responseDictionary objectForKey:@"status"] isEqualToString:@"success"])
                     {
                         
                             BuyGiftVoucherVC *buyGiftVoucherVC = [[BuyGiftVoucherVC alloc] initWithNibName:@"BuyGiftVoucherVC" bundle:nil]
                             ;
                             buyGiftVoucherVC.view.backgroundColor = [UIColor clearColor];
                             buyGiftVoucherVC.urlString=[responseDictionary objectForKey:@"Url"];
                             
                             if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[buyGiftVoucherVC class]])
                             {
                                 [ApplicationDelegate.subHomeNav pushViewController:buyGiftVoucherVC animated:NO];
                             }
                         
                     }
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}


-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:self.couponNameText.text forKey:@"CouponName"];
    [postDic setObject:self.emailText.text forKey:@"Email"];
    [postDic setObject:self.giftVoucherObj.voucher_Amount forKey:@"amount"];
    [postDic setObject:self.giftVoucherObj.voucher_Id forKey:@"giftVoucherId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    return postDic;
}


#pragma mark - TEXT FIELD DELEGATES

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGFloat yFact;
    
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        yFact = 0; // ToolBar Inclusived case
    }
    else
    {
        yFact = 80; // ToolBar Inclusived case
    }
    if (textField.superview.frame.origin.y>yFact)
    {
        [self.giftVoucherScrollView setContentOffset:CGPointMake(self.giftVoucherScrollView.contentOffset.x, (textField.superview.frame.origin.y-yFact))];
    }
    else
    {
        [self.giftVoucherScrollView setContentOffset:CGPointZero animated:YES];
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    //[self.giftVoucherScrollView setContentOffset:CGPointZero animated:YES];
    
    if (textField == self.emailText)
    {
        if (textField.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                self.emailText.text=@"";
                
            }
        }
    }
    [textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   // [self.giftVoucherScrollView setContentOffset:CGPointZero animated:YES];
    
    if (textField == self.emailText)
    {
        if (textField.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                self.emailText.text=@"";
                
            }
        }
    }
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}

-(void)xibLoading
{
    NSString *nibName = @"GiftVoucherDetailVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
