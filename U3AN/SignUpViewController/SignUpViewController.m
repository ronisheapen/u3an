//
//  SignUpViewController.m
//  U3AN
//
//  Created by Vipin on 18/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SignUpViewController.h"
#import "LoginViewController.h"
#import "LaunchingViewController.h"

#define MAXLENGTH 25

@interface SignUpViewController ()
{
    NSString *mobileStr,*housePhoneStr, *houseNoStr;
    NSInteger numberFieldVal; // 1 = Mobile Num, 2 = House Phone, 3 = House Number
    NSMutableArray *areaListArray,*cityListArray,*areaObjArray,*occupationArray,*howUHearArray,*genderArray;
    NSString *selectedAreaId,*adderss_Type;
    bool fromOccupationBtn, keepPostedSMS,latest_Promos,fromGenderBtn;
}
@end


@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    self.account_Information_Label.text = localize(@"Account Information");
    self.email_Label.text = localize(@"Email*:");
    self.password_Label.text = localize(@"Password*:");
    self.confirm_Password_Label.text = localize(@"Confirm Password*:");
    self.first_Name_Label.text = localize(@"First Name*:");
    self.last_Name_Label.text = localize(@"Last Name*:");
    self.mobile_Label.text = localize(@"Mobile*:");
    self.house_Phone_Label.text = localize(@"House Phone*:");
    
    self.delivery_Information_Label.text = localize(@"Delivery Information");
    self.building_Label.text = localize(@"Building");
    self.villa_House_Label.text = localize(@"Villa House");
    self.office_Label.text = localize(@"Office");
    self.area_Label.text = localize(@"Area*:");
    self.profile_Name_Label.text = localize(@"Profile Name*:");
    self.block_Label.text = localize(@"Block*:");
    self.judda_Label.text = localize(@"Jidda (Optional):");
    self.street_Label.text = localize(@"Street*:");
    self.house_No_Label.text = localize(@"House No*:");
    self.floor_Label.text = localize(@"Floor No*:");
    self.apartment_Office_Label.text = localize(@"Apartment/Office*:");
    self.direction_Label.text = localize(@"Direction (Optional):");
    
    self.personal_Information_Label.text = localize(@"Personal Information");
    self.gender_Label.text = localize(@"Gender*:");
    self.occupation_Label.text = localize(@"Occupation*:");
    self.hear_About_Label.text = localize(@"How did you hear about us?:");
    self.referredBy_Label.text = localize(@"Referred by friend? Enter his/ her Email:");
    self.latest_Promos_Label.text = localize(@"Get latest promos and offers by u3an Newsletter");
    self.keep_Posted_SMS_Label.text = localize(@"Keep me posted by u3an SMS");
    self.privacy_Policy_Label.text = localize(@"I have read and understand the Privacy & Policy and agree to the Terms of Use");
    
    // Do any additional setup after loading the view from its nib.
    [self.villa_Button setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.villa_Button setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    [self.building_Button setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.building_Button setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    [self.office_Button setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.office_Button setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    
    [self.latest_Promos_Bttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.latest_Promos_Bttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    [self.keep_Posted_SMS_Bttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.keep_Posted_SMS_Bttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    [self.privacy_Policy_Bttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.privacy_Policy_Bttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    adderss_Type=@"0";
    self.submit_Bttn.enabled=NO;
    self.villa_Button.selected = YES;
    keepPostedSMS=NO;
    latest_Promos=NO;
    [self hideExtraFieldView];
    self.account_Info_Scroll_View.contentSize = CGSizeMake(self.account_Info_Scroll_View.frame.size.width,  self.accNxtButton.superview.frame.origin.y+self.accNxtButton.superview.frame.size.height+130);
    [self clearAccountInfoEntries];
    
    numberFieldVal = 0;
    [self toolbarForNumberPadSetup];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (!ApplicationDelegate.isLoggedIn)
    {
        [self updateViewHeading];
        [self setUpUI];
        numberFieldVal = 0;
        [self toolbarForNumberPadSetup];
    }
    else
    {
        [self loadHomeView];
    }
    
}


-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setUpUI
{
    // SetUp Account Info View
    
    self.account_Info_View.layer.cornerRadius = 2.0;
    self.account_Info_View.layer.masksToBounds = YES;
    [self.account_Information_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.email_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.password_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.confirm_Password_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.first_Name_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.last_Name_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.mobile_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.house_Phone_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    
    // SetUp Delivery Info View
    
//    self.account_Info_View.layer.cornerRadius = 2.0;
//    self.account_Info_View.layer.masksToBounds = YES;
    [self.delivery_Information_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.area_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.profile_Name_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.block_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.judda_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.street_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.house_No_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.floor_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.apartment_Office_Label setFont:[UIFont fontWithName:@"Tahoma" size:13.0f]];
    [self.direction_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    
     // SetUp Personal Info View
    
    [self.personal_Information_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.gender_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.occupation_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.hear_About_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.referredBy_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    
    [self.latest_Promos_Label setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];
    [self.keep_Posted_SMS_Label setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];
    [self.privacy_Policy_Label setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];


}

#pragma mark - BUTTON ACTIONS

- (IBAction)account_Info_NextBttn_Action:(id)sender
{
    [self.view endEditing:YES];
    
    if (self.email_txtBox.text.length>0 && self.password_txtBox.text.length>0&&self.confm_PassWrd_txtBox.text.length>0&&self.first_Name_txtBox.text.length>0&&self.last_Name_txtBox.text.length>0&&self.mobile_txtBox.text.length>0&&self.house_Phone_txtBox.text.length>0) {

        if ( ![ApplicationDelegate isEmailValidWithEmailID:self.email_txtBox.text])
        {
            [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
            //self.email_txtBox.text=@"";
        }

        else  if (![self.password_txtBox.text isEqualToString:self.confm_PassWrd_txtBox.text])
        {
            
            [ApplicationDelegate showAlertWithMessage:@"Password Mismatch" title:kMESSAGE];
            self.password_txtBox.text=@"";
            self.confm_PassWrd_txtBox.text=@"";
            
        }
        else if(self.password_txtBox.text.length<7)
        {
            [ApplicationDelegate showAlertWithMessage:@"Password will be min 7 characters" title:kMESSAGE];
            self.password_txtBox.text=@"";
            self.confm_PassWrd_txtBox.text=@"";
        }
        else if (!((self.mobile_txtBox.text.length>=6)&&(self.mobile_txtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:@"Mobile"];
           
        }
        else if (!((self.house_Phone_txtBox.text.length>=6)&&(self.house_Phone_txtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:@"House Phone"];
           
        }
        else
        {
    
            self.account_Info_Scroll_View.hidden=YES;
            self.personal_Info_View.hidden = YES;
            
            self.delivery_Info_View.frame = CGRectMake(0, 0, self.account_Info_View.frame.size.width, self.account_Info_View.frame.size.height);
            
            [self clearDeliveryInfoEntries];
            
            if (![self.delivery_Info_View superview])
            {
                [self.account_Info_View addSubview:self.delivery_Info_View];
            }
            self.delivery_Info_View.hidden = NO;
            self.delivery_Info_Scroll_View.contentSize = CGSizeMake(self.delivery_Info_Scroll_View.frame.size.width,self.delivery_SubView2.frame.origin.y+self.delivery_SubView2.frame.size.height+130);
            
            [self getAreaDetails];
        
        }
        
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
    }
}

- (IBAction)delivery_Info_NextBttn_Action:(id)sender {
    
    [self.view endEditing:YES];
    
    if (!self.villa_Button.selected) {

        if (self.area_TxtBox.text.length>0 && self.profile_Name_TxtBox.text.length>0&&self.block_TxtBox.text.length>0&&self.street_TxtBox.text.length>0&&self.house_No_TxtBox.text.length>0&&self.floor_TxtBox.text.length>0&&self.apartment_Office_TxtBox.text.length>0)
        {
            if (!((self.house_No_TxtBox.text.length>=6)&&(self.house_No_TxtBox.text.length<=15)))
            {
                [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:@"House Number"];
                
            }
            else
            {
                self.account_Info_Scroll_View.hidden = YES;
                self.delivery_Info_View.hidden=YES;
                
                self.personal_Info_View.frame = CGRectMake(0, 0, self.account_Info_View.frame.size.width, self.account_Info_View.frame.size.height);
                
                [self clearPersonalInfoEntries];
                
                if (![self.personal_Info_View superview])
                {
                    [self.account_Info_View addSubview:self.personal_Info_View];
                }
                self.personal_Info_View.hidden = NO;
                
                self.personal_Info_Scroll_View.contentSize = CGSizeMake(self.personal_Info_Scroll_View.frame.size.width, self.submit_Bttn.frame.origin.y+self.submit_Bttn.frame.size.height+130);
            }
            
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
        }
    }
    else
    {
        
        if (self.area_TxtBox.text.length>0 && self.profile_Name_TxtBox.text.length>0&&self.block_TxtBox.text.length>0&&self.street_TxtBox.text.length>0&&self.house_No_TxtBox.text.length>0)
        {
            if (!((self.house_No_TxtBox.text.length>=6)&&(self.house_No_TxtBox.text.length<=15)))
            {
                [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:@"House Number"];
                
            }
            else
            {
                self.account_Info_Scroll_View.hidden = YES;
                self.delivery_Info_View.hidden=YES;
                
                self.personal_Info_View.frame = CGRectMake(0, 0, self.account_Info_View.frame.size.width, self.account_Info_View.frame.size.height);
                
                [self clearPersonalInfoEntries];
                
                if (![self.personal_Info_View superview])
                {
                    [self.account_Info_View addSubview:self.personal_Info_View];
                }
                self.personal_Info_View.hidden = NO;
                
                self.personal_Info_Scroll_View.contentSize = CGSizeMake(self.personal_Info_Scroll_View.frame.size.width, self.submit_Bttn.frame.origin.y+self.submit_Bttn.frame.size.height+130);
            }
            
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
        }
    }
}

- (IBAction)keep_Posted_SMS_Bttn_Action:(id)sender {
    
    [self.view endEditing:YES];
    
    self.keep_Posted_SMS_Bttn.selected = !self.keep_Posted_SMS_Bttn.selected;
    
    if (self.keep_Posted_SMS_Bttn.selected==YES) {
        keepPostedSMS=YES;
    }
    else{
        keepPostedSMS=NO;
    }
}

- (IBAction)privacy_Policy_Bttn_Action:(id)sender {
    
    [self.view endEditing:YES];
    
    self.privacy_Policy_Bttn.selected = !self.privacy_Policy_Bttn.selected;
    
    if (self.privacy_Policy_Bttn.selected==YES) {
        self.submit_Bttn.enabled=YES;
    }
    else
    {
        self.submit_Bttn.enabled=NO;
    }
}

- (IBAction)backgroundTapAction:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)genderSelectionBttnAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    fromOccupationBtn=NO;
    fromGenderBtn=YES;
    genderArray = [[NSMutableArray alloc] initWithObjects:@"Male",@"Female", nil];
    
    self.genderSelection_Bttn.selected = !self.genderSelection_Bttn.selected;
    if (self.genderSelection_Bttn.selected)
    {
        if (self.signUpDropDownObj.view.superview) {
            [self.signUpDropDownObj.view removeFromSuperview];
        }
        
        self.signUpDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.signUpDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.signUpDropDownObj.view.frame = CGRectMake((self.genderSelection_Bttn.superview.frame.origin.x),(self.genderSelection_Bttn.superview.frame.origin.y+self.genderSelection_Bttn.superview.frame.size.height), (self.genderSelection_Bttn.superview.frame.size.width), 0);
        
        self.signUpDropDownObj.dropDownDelegate = self;
        self.signUpDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:genderArray];
        
        self.signUpDropDownObj.view.layer.borderWidth = 0.1;
        self.signUpDropDownObj.view.layer.shadowOpacity = 1.0;
        self.signUpDropDownObj.view.layer.shadowRadius = 5.0;
        self.signUpDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.occupationSelectionBttn.superview) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.signUpDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.signUpDropDownObj.textLabelColor = [UIColor grayColor];
        self.signUpDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (genderArray.count>0)
        {
            [self.genderSelection_Bttn.superview.superview addSubview:self.signUpDropDownObj.view];
        }
        else
        {
            self.genderSelection_Bttn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 140.0f;
        
        if ((self.signUpDropDownObj.cellHeight*self.signUpDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.signUpDropDownObj.cellHeight*self.signUpDropDownObj.dataArray.count)+10.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.signUpDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.signUpDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x+2,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.signUpDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.signUpDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.signUpDropDownObj.view removeFromSuperview];
            }
            
        }];
    }

}

- (IBAction)latest_Promos_Bttn_Action:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    self.latest_Promos_Bttn.selected = !self.latest_Promos_Bttn.selected;
    if (self.latest_Promos_Bttn.selected==YES) {
        latest_Promos=YES;
    }
    else{
        latest_Promos=NO;
    }
}

- (IBAction)submit_Bttn_Action:(id)sender
{
    [self.view endEditing:YES];
    
    if (self.gender_TxtBox.text.length>0 && self.occupation_TxtBox.text.length>0)
    {
        if (self.referred_By_TxtBox.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:self.referred_By_TxtBox.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                //self.email_txtBox.text=@"";
            }
            else
            {
                [self registerUser];
            }
        }
        else
        {
            [self registerUser];
        }
        
        
            
        
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
    }
}

- (IBAction)occupation_SelectionBttnAction:(id)sender
{
    [self.view endEditing:YES];
    
    fromOccupationBtn=YES;
    fromGenderBtn=NO;
    occupationArray = [[NSMutableArray alloc] initWithObjects:@"Other",@"Professional Services",@"Self-employed/Owner", nil];
    
    self.occupationSelectionBttn.selected = !self.occupationSelectionBttn.selected;
    if (self.occupationSelectionBttn.selected)
    {
        if (self.signUpDropDownObj.view.superview) {
            [self.signUpDropDownObj.view removeFromSuperview];
        }
        
        self.signUpDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.signUpDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.signUpDropDownObj.view.frame = CGRectMake((self.occupationSelectionBttn.superview.frame.origin.x),(self.occupationSelectionBttn.superview.frame.origin.y+self.occupationSelectionBttn.superview.frame.size.height), (self.occupationSelectionBttn.superview.frame.size.width), 0);
        
        self.signUpDropDownObj.dropDownDelegate = self;
        self.signUpDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:occupationArray];
        
        self.signUpDropDownObj.view.layer.borderWidth = 0.1;
        self.signUpDropDownObj.view.layer.shadowOpacity = 1.0;
        self.signUpDropDownObj.view.layer.shadowRadius = 5.0;
        self.signUpDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
//        for (UIView *vw in self.view.subviews) {
//            if (vw!=self.self.occupationSelectionBttn.superview) {
//                [vw setUserInteractionEnabled:NO];
//            }
//        }
        //////////////
        self.signUpDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.signUpDropDownObj.textLabelColor = [UIColor grayColor];
        self.signUpDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (occupationArray.count>0)
        {
            [self.occupationSelectionBttn.superview.superview addSubview:self.signUpDropDownObj.view];
        }
        else
        {
            self.occupationSelectionBttn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 140.0f;
        
        if ((self.signUpDropDownObj.cellHeight*self.signUpDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.signUpDropDownObj.cellHeight*self.signUpDropDownObj.dataArray.count)+10.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.signUpDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.signUpDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x+2,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.signUpDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.signUpDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.signUpDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    

}

- (IBAction)how_U_HearAbout_BttnAction:(id)sender
{
    [self.view endEditing:YES];
    
    fromOccupationBtn=NO;
    fromGenderBtn=NO;
    howUHearArray = [[NSMutableArray alloc] initWithObjects:@"Other", nil];
    
    self.howUHearAboutSelectionBttn.selected = !self.howUHearAboutSelectionBttn.selected;
    if (self.howUHearAboutSelectionBttn.selected)
    {
        if (self.signUpDropDownObj.view.superview) {
            [self.signUpDropDownObj.view removeFromSuperview];
        }
        
        self.signUpDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.signUpDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.signUpDropDownObj.view.frame = CGRectMake((self.howUHearAboutSelectionBttn.superview.frame.origin.x),(self.howUHearAboutSelectionBttn.superview.frame.origin.y+self.howUHearAboutSelectionBttn.superview.frame.size.height), (self.howUHearAboutSelectionBttn.superview.frame.size.width), 0);
        
        self.signUpDropDownObj.dropDownDelegate = self;
        self.signUpDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:howUHearArray];
        
        self.signUpDropDownObj.view.layer.borderWidth = 0.1;
        self.signUpDropDownObj.view.layer.shadowOpacity = 1.0;
        self.signUpDropDownObj.view.layer.shadowRadius = 5.0;
        self.signUpDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.occupationSelectionBttn.superview) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.signUpDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.signUpDropDownObj.textLabelColor = [UIColor grayColor];
        self.signUpDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (howUHearArray.count>0)
        {
            [self.howUHearAboutSelectionBttn.superview.superview addSubview:self.signUpDropDownObj.view];
        }
        else
        {
            self.howUHearAboutSelectionBttn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 140.0f;
        
        if ((self.signUpDropDownObj.cellHeight*self.signUpDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.signUpDropDownObj.cellHeight*self.signUpDropDownObj.dataArray.count)+10.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.signUpDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.signUpDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x+2,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.signUpDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.signUpDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.signUpDropDownObj.view removeFromSuperview];
            }
            
        }];
    }

}

- (IBAction)personalViewBackBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    self.account_Info_Scroll_View.hidden=YES;
    self.delivery_Info_View.hidden = NO;
    
    [self.delivery_Info_Scroll_View setContentOffset:CGPointZero];
    
    if ([self.personal_Info_View superview])
    {
        [self.personal_Info_View removeFromSuperview];
    }
}

- (IBAction)radioBttn_Action:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (sender.tag==1)
    {
        [self clearTextFieldsUponAddressTypeSelection];
        self.villa_Button.selected = YES;
        self.building_Button.selected = NO;
        self.office_Button.selected = NO;
        adderss_Type=@"0";
        [self hideExtraFieldView];
        //self.delivery_Info_Scroll_View.contentSize = CGSizeMake(self.delivery_Info_Scroll_View.frame.size.width,self.delivery_Nxt_Bttn.superview.superview. frame.origin.x+self.delivery_Nxt_Bttn.superview.superview.frame.size.height+130);
        
    }
    else if (sender.tag==2)
    {
        [self clearTextFieldsUponAddressTypeSelection];
        self.villa_Button.selected = NO;
        self.building_Button.selected = YES;
        self.office_Button.selected = NO;
        adderss_Type=@"1";
        [self showExtraFieldView];
        //self.delivery_Info_Scroll_View.contentSize = CGSizeMake(self.delivery_Info_Scroll_View.frame.size.width,self.delivery_Nxt_Bttn.superview.superview. frame.origin.x+self.delivery_Nxt_Bttn.superview.superview.frame.size.height+self.delivery_SubView1.frame.size.height+130);
    }
    else if (sender.tag==3)
    {
        [self clearTextFieldsUponAddressTypeSelection];
        self.villa_Button.selected = NO;
        self.building_Button.selected = NO;
        self.office_Button.selected = YES;
        adderss_Type=@"2";
        [self showExtraFieldView];
        //self.delivery_Info_Scroll_View.contentSize = CGSizeMake(self.delivery_Info_Scroll_View.frame.size.width,self.delivery_Nxt_Bttn.superview.superview. frame.origin.x+self.delivery_Nxt_Bttn.superview.superview.frame.size.height+self.delivery_SubView1.frame.size.height+130);
    }
    self.delivery_Info_Scroll_View.contentSize = CGSizeMake(self.delivery_Info_Scroll_View.frame.size.width,self.delivery_SubView2.frame.origin.y+self.delivery_SubView2.frame.size.height+130);
}


-(void)clearTextFieldsUponAddressTypeSelection
{
    self.profile_Name_TxtBox.text=@"";
    self.block_TxtBox.text=@"";
    self.judda_TxtBox.text=@"";
    self.area_TxtBox.text=@"";
    self.street_TxtBox.text=@"";
    self.house_No_TxtBox.text=@"";
    self.floor_TxtBox.text=@"";
    self.apartment_Office_TxtBox.text=@"";
    self.direction_TxtBox.text=@"";
    
}
-(void)clearAccountInfoEntries
{
    numberFieldVal = 0;
    mobileStr = @"";
    housePhoneStr = @"";
    
    self.email_txtBox.text = @"";
    self.password_txtBox.text = @"";
    self.confm_PassWrd_txtBox.text = @"";
    self.first_Name_txtBox.text = @"";
    self.last_Name_txtBox.text = @"";
    self.mobile_txtBox.text = @"";
    self.house_Phone_txtBox.text = @"";
    
    [self.account_Info_Scroll_View setContentOffset:CGPointZero];
}
-(void)clearDeliveryInfoEntries
{
    [self radioBttn_Action:self.villa_Button];
    
    numberFieldVal = 0;
    houseNoStr = @"";
    
    self.area_TxtBox.text = @"";
    self.areaSelectionButton.selected = NO;
    
    self.profile_Name_TxtBox.text = @"";
    self.block_TxtBox.text = @"";
    self.judda_TxtBox.text = @"";
    self.street_TxtBox.text = @"";
    self.house_No_TxtBox.text = @"";
    self.floor_TxtBox.text = @"";
    self.apartment_Office_TxtBox.text = @"";
    self.direction_TxtBox.text = @"";
}
-(void)clearPersonalInfoEntries
{
    self.gender_TxtBox.text = @"";
    self.genderSelection_Bttn.selected = NO;
    self.occupation_TxtBox.text = @"";
    self.occupationSelectionBttn.selected = NO;
    self.hear_About_TxtBox.text = @"";
    self.howUHearAboutSelectionBttn.selected = NO;
    self.referred_By_TxtBox.text = @"";
    self.latest_Promos_Bttn.selected = NO;
    latest_Promos = NO;
    self.keep_Posted_SMS_Bttn.selected = NO;
    keepPostedSMS = NO;
    self.privacy_Policy_Bttn.selected = NO;
    self.submit_Bttn.enabled=NO;
    
}
- (IBAction)area_SelectionBttn_Action:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (areaListArray.count!=0) {
        [self showAreaList];
    }
    else
    {
        [self getAreaDetails];
       // [self showAreaList];
    }
    
}

- (IBAction)deliverViewBckBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    self.account_Info_Scroll_View.hidden=NO;
    
    [self.account_Info_Scroll_View setContentOffset:CGPointZero];
    
    if ([self.delivery_Info_View superview])
    {
        [self.delivery_Info_View removeFromSuperview];
    }
    if ([self.personal_Info_View superview])
    {
        [self.personal_Info_View removeFromSuperview];
    }
}

#pragma mark - Show And Hide Extra Field


-(void)showExtraFieldView
{
    self.delivery_SubView1.hidden=NO;
    self.delivery_SubView2.frame = CGRectMake(self.delivery_SubView1.frame.origin.x, (self.delivery_SubView1.frame.origin.y+self.delivery_SubView1.frame.size.height), self.delivery_SubView2.frame.size.width, self.delivery_SubView2.frame.size.height);
    
}

-(void)hideExtraFieldView
{
    self.delivery_SubView1.hidden=YES;
    self.delivery_SubView2.frame = CGRectMake(self.delivery_SubView1.frame.origin.x, (self.delivery_SubView1.frame.origin.y), self.delivery_SubView2.frame.size.width, self.delivery_SubView2.frame.size.height);
}

#pragma mark - REGISTER USER

-(void)registerUser
{
    NSMutableDictionary *registerData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserSignUpPostData]];
    if (registerData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine registerUserWithDataDictionary:registerData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]] )
                     {
                         if ([[responseDictionary objectForKey:@"Status"] isEqualToString:[NSString stringWithFormat: @"Success"]])
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                             
                             [self clearPostDictData];
                             
                             LoginViewController *loginVc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                             loginVc.view.backgroundColor = [UIColor clearColor];
                             
                             if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[loginVc class]])
                             {
                                 [ApplicationDelegate.subHomeNav pushViewController:loginVc animated:NO];
                             }
                             

                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:@"User registartion failed. Please try again" title:nil];
                         }
                     }
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:@"User registartion failed. Please try again" title:nil];
                     }
                 }
                 else
                 {
                     [ApplicationDelegate showAlertWithMessage:@"User registartion failed. Please try again" title:nil];
                 }
             }
             else
             {
                 [ApplicationDelegate showAlertWithMessage:@"User registartion failed. Please try again" title:nil];
             }
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }

    
}

-(NSMutableDictionary *)getUserSignUpPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:kCountryId forKey:@"countryId"];
    [postDic setObject:self.email_txtBox.text forKey:@"email"];
    [postDic setObject:self.password_txtBox.text forKey:@"password"];
    [postDic setObject:self.first_Name_txtBox.text forKey:@"firstName"];
    [postDic setObject:self.last_Name_txtBox.text forKey:@"lastName"];
    [postDic setObject:self.mobile_txtBox.text forKey:@"mobile"];
    [postDic setObject:self.house_Phone_txtBox.text forKey:@"housePhone"];
    [postDic setObject:selectedAreaId forKey:@"AreaId"];
    [postDic setObject:self.profile_Name_TxtBox.text forKey:@"ProfileName"];
    [postDic setObject:self.block_TxtBox.text forKey:@"Block"];
    [postDic setObject:self.judda_TxtBox.text forKey:@"Judda"];
    [postDic setObject:adderss_Type forKey:@"AddressType"];
    [postDic setObject:self.street_TxtBox.text forKey:@"Street"];
    [postDic setObject:self.house_No_TxtBox.text forKey:@"HouseNumber"];
    [postDic setObject:self.floor_TxtBox.text forKey:@"floor"];
    [postDic setObject:self.apartment_Office_TxtBox.text forKey:@"office"];
    [postDic setObject:self.direction_TxtBox.text forKey:@"Direction"];
    [postDic setObject:self.gender_TxtBox.text forKey:@"gender"];
    [postDic setObject:self.occupation_TxtBox.text forKey:@"occupation"];
    [postDic setObject:self.hear_About_TxtBox.text forKey:@"howDidYouHear"];
    [postDic setObject:self.referred_By_TxtBox.text forKey:@"referredBy"];
    [postDic setObject:[NSNumber numberWithBool:latest_Promos]
                forKey:@"subscribedToNewsletter"];
    [postDic setObject:[NSNumber numberWithBool:keepPostedSMS]
                forKey:@"subscribedToSMS"];

    return postDic;
}

-(void)clearPostDictData
{
    self.email_txtBox.text=@"";
    self.password_txtBox.text=@"";
    self.confm_PassWrd_txtBox.text=@"";
    self.first_Name_txtBox.text=@"";
    self.last_Name_txtBox.text=@"";
    self.mobile_txtBox.text=@"";
    self.house_Phone_txtBox.text=@"";
    selectedAreaId=@"";
    self.profile_Name_TxtBox.text=@"";
    self.block_TxtBox.text=@"";
    self.judda_TxtBox.text=@"";
    self.area_TxtBox.text=@"";
    adderss_Type=@"0";
    self.street_TxtBox.text=@"";
    self.house_No_TxtBox.text=@"";
    self.floor_TxtBox.text=@"";
    self.apartment_Office_TxtBox.text=@"";
    self.direction_TxtBox.text=@"";
    self.gender_TxtBox.text=@"";
    self.occupation_TxtBox.text=@"";
    self.hear_About_TxtBox.text=@"";
    self.referred_By_TxtBox.text=@"";
    latest_Promos=NO;
    keepPostedSMS=NO;
    self.submit_Bttn.enabled=NO;
    self.villa_Button.selected = YES;
    self.latest_Promos_Bttn.selected=NO;
    self.keep_Posted_SMS_Bttn.selected=NO;
    self.privacy_Policy_Bttn.selected=NO;
    
    
}

#pragma mark - GET AREA LIST

-(void)getAreaDetails
{
    areaListArray=[[NSMutableArray alloc] init];
    areaObjArray=[[NSMutableArray alloc] init];
    cityListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getAreaPostData]];
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        __block BOOL success = NO;
        
        [ApplicationDelegate.engine getAreasWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             if (!success)
             {
                 success = YES;
                 if ([ApplicationDelegate isValid:responseArray])
                 {
                     if (responseArray.count>0)
                     {
                         for (int i=0; i<responseArray.count; i++)
                         {
                             if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                             {
                                 Areas *areaItem = [ApplicationDelegate.mapper getAreaListFromDictionary:[responseArray objectAtIndex:i]];
                                 
                                 [cityListArray addObject:areaItem.city_Name];
                                 [areaObjArray addObject:areaItem];
                                 if(areaItem.area_List.count!=0)
                                 {
                                     NSMutableArray *areaItemListArray=[[NSMutableArray alloc] init];
                                     
                                     for (int j=0; j<areaItem.area_List.count; j++)
                                     {
                                         if(![ApplicationDelegate isValid:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]] )
                                         {
                                             [areaItemListArray addObject:@""];
                                         }
                                         else
                                         {
                                             [areaItemListArray addObject:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]];
                                         }
                                         
                                     }
                                     //     NSLog(@"AREA_ITEM_LIST%@",areaItemListArray);
                                     [areaListArray addObject:areaItemListArray];
                                     //   NSLog(@"AREA_LIST%@",areaListArray);
                                 }
                                 
                             }
                             
                         }
                         NSLog(@"AREA_LIST%@",areaListArray);
                         
                         
                     }
                 }
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getAreaPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:kCountryId forKey:@"countryId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    return postDic;
}

#pragma mark - SHOW AREA LIST

-(void)showAreaList
{
    self.areaSelectionButton.selected = !self.areaSelectionButton.selected;
    if (self.areaSelectionButton.selected)
    {
        if (self.areaDropDownObj.view.superview) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
        self.areaDropDownObj= [[DropDownWithHeaderSelection alloc] initWithNibName:@"DropDownWithHeaderSelection" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.areaDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.areaDropDownObj.view.frame = CGRectMake((self.areaSelectionButton.superview.frame.origin.x),(self.areaSelectionButton.superview.frame.origin.y+self.areaSelectionButton.superview.frame.size.height), (self.areaSelectionButton.superview.frame.size.width), 0);
        
        self.areaDropDownObj.headerDropDownDelegate = self;
        self.areaDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:areaListArray];
        self.areaDropDownObj.headerDataArray = [[NSMutableArray alloc] initWithArray:cityListArray];
        self.areaDropDownObj.headerLabelFont = [UIFont fontWithName:@"Verdana" size:14.0];
        self.areaDropDownObj.view.layer.borderWidth = 0.1;
        self.areaDropDownObj.view.layer.shadowOpacity = 1.0;
        self.areaDropDownObj.view.layer.shadowRadius = 5.0;
        self.areaDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.areaSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.areaDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.areaDropDownObj.textLabelColor = [UIColor blackColor];
        self.areaDropDownObj.view.backgroundColor = [UIColor whiteColor];
        self.areaDropDownObj.headerTextLabelColor = [UIColor redColor];
        if (cityListArray.count>0)
        {
            [self.areaSelectionButton.superview.superview addSubview:self.areaDropDownObj.view];
        }
        else
        {
            self.areaSelectionButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)+18.0f;
        }
        //        [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.areaDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.areaDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x+2,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.areaDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.areaDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.areaDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
    
}


#pragma mark - TEXT VIEW DELEGATES

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//    }
//    return YES;
//}

#pragma mark - TEXT FIELD DELEGATES

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGFloat yFact;
    
    if (!self.account_Info_Scroll_View.hidden)
    {
        [self.account_Info_Scroll_View setContentOffset:CGPointZero animated:NO];
        if (textField==self.mobile_txtBox) {
            mobileStr = self.mobile_txtBox.text;
            numberFieldVal = 1;
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 0; // ToolBar Inclusived case
            }
            else
            {
                yFact = 80; // ToolBar Inclusived case
            }
        }
        else if  (textField==self.house_Phone_txtBox)
        {
            numberFieldVal = 2;
            housePhoneStr = self.house_Phone_txtBox.text;
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 0; // ToolBar Inclusived case
            }
            else
            {
                yFact = 80; // ToolBar Inclusived case
            }
        }
        
        else
        {
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 45;
            }
            else
            {
                yFact = 130;
            }
        }
        if (textField.superview.frame.origin.y>yFact)
        {
            [self.account_Info_Scroll_View setContentOffset:CGPointMake(self.account_Info_Scroll_View.contentOffset.x, (textField.superview.frame.origin.y-yFact))];
        }
        else
        {
            [self.account_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
        }
    }
    else if (!self.delivery_Info_Scroll_View.hidden)
    {
        [self.delivery_Info_Scroll_View setContentOffset:CGPointZero animated:NO];
        
        if  (textField==self.house_No_TxtBox)
        {
            numberFieldVal = 3;
            houseNoStr = self.house_No_TxtBox.text;
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 0; // ToolBar Inclusived case
            }
            else
            {
                yFact = 80; // ToolBar Inclusived case
            }
            
        }
        else
        {
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 45;
            }
            else
            {
                yFact = 130;
            }
        }
        
        
        //158 = factor which is due to the "RADIO options" occupied in ScrollView. It is y origin of 1st textfld superview
        
        if (textField.superview.superview.frame.origin.y>(158-yFact))
        {
            [self.delivery_Info_Scroll_View setContentOffset:CGPointMake(self.delivery_Info_Scroll_View.contentOffset.x, (textField.superview.superview.frame.origin.y-yFact+158))];
            
        }
        else
        {
            [self.delivery_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
        }
        
        /*
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 45;
        }
        else
        {
            yFact = 130;
        }

            
        if (textField==self.floor_TxtBox||textField==self.apartment_Office_TxtBox||textField==self.direction_TxtBox)
        {
                
            [self.delivery_Info_Scroll_View setContentOffset:CGPointMake(self.delivery_Info_Scroll_View.contentOffset.x, (textField.superview.superview.frame.origin.y-yFact+158))];
        }
        else if  (textField==self.house_No_TxtBox)
        {
            numberFieldVal = 3;
            houseNoStr = self.house_No_TxtBox.text;
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 0; // ToolBar Inclusived case
            }
            else
            {
                yFact = 80; // ToolBar Inclusived case
            }
        }
        else if (textField.superview.frame.origin.y>(yFact+158))
        {
            [self.delivery_Info_Scroll_View setContentOffset:CGPointMake(self.delivery_Info_Scroll_View.contentOffset.x, (textField. superview.frame.origin.y-yFact+158))];
            
         }
        else
        {
            [self.delivery_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
        }
        
        */
    }
    else if (!self.personal_Info_Scroll_View.hidden)
    {
        [self.personal_Info_Scroll_View setContentOffset:CGPointZero animated:NO];
        
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 45;
        }
        else
        {
            yFact = 130;
        }
        if (textField.superview.frame.origin.y>yFact)
        {
            [self.personal_Info_Scroll_View setContentOffset:CGPointMake(self.personal_Info_Scroll_View.contentOffset.x, (textField. superview.frame.origin.y-yFact))];
            
        }
        else
        {
            [self.personal_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
        }
    }
    else
    {
        
    }
    
}


//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    /*
//    if ([self.account_Info_Scroll_View superview])
//    {
//         [self.account_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
//    }
//    
//    if ([self.delivery_Info_Scroll_View superview])
//    {
//        [self.delivery_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
//    }
//    if ([self.personal_Info_Scroll_View superview])
//    {
//        [self.personal_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
//    }
//    */
//    
//        if (textField == self.email_txtBox)
//        {
//            if (textField.text.length!=0)
//            {
//                if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
//                {
//                    [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
//                    self.email_txtBox.text=@"";
//    
//                }
//            }
//        }
//    
//    if (textField==self.password_txtBox) {
//        if (textField.text.length<7)
//        {
//            [ApplicationDelegate showAlertWithMessage:@"Password will be min 7 characters" title:kMESSAGE];
//        }
//    }
//    
//        if (textField == self.confm_PassWrd_txtBox)
//        {
//            if (![textField.text isEqualToString:self.password_txtBox.text])
//            {
//                
//                [ApplicationDelegate showAlertWithMessage:@"Password Mismatch" title:kMESSAGE];
//                self.confm_PassWrd_txtBox.text=@"";
//                
//            }
//        }
//}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    /*
        if ([self.account_Info_Scroll_View superview])
        {
            [self.account_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
        }
        if ([self.delivery_Info_Scroll_View superview])
        {
            [self.delivery_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
        }
        if ([self.personal_Info_Scroll_View superview])
        {
            [self.personal_Info_Scroll_View setContentOffset:CGPointZero animated:YES];
        }
    */
        //Email
        if (textField == self.email_txtBox)
        {
            if (textField.text.length!=0)
            {
                if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
                {
                    [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                    //self.email_txtBox.text=@"";
                }
            }
    
        }
    
    if (textField == self.referred_By_TxtBox)
    {
        if (textField.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                //self.email_txtBox.text=@"";
            }
        }
        
    }
    
    if (textField==self.password_txtBox) {
        if (textField.text.length<7)
        {
            [ApplicationDelegate showAlertWithMessage:@"Password will be min 7 characters" title:kMESSAGE];
        }
    }
    
        if (textField == self.confm_PassWrd_txtBox)
        {
            if (![textField.text isEqualToString:self.password_txtBox.text])
            {
  
                    [ApplicationDelegate showAlertWithMessage:@"Password Mismatch" title:kMESSAGE];
                    self.confm_PassWrd_txtBox.text=@"";
    
            }
            
        }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if ((textField==self.mobile_txtBox)||(textField==self.house_Phone_txtBox))
    {
        if (oldLength>14)
        {
            return newLength <= 14 || returnKey;
        }
    }
    return newLength <= MAXLENGTH || returnKey;
}

#pragma mark - NUMBER PAD METHODS
#pragma mark -
-(void)toolbarForNumberPadSetup
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, ApplicationDelegate.window.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.mobile_txtBox.inputAccessoryView = numberToolbar;
    self.house_Phone_txtBox.inputAccessoryView=numberToolbar;
    self.house_No_TxtBox.inputAccessoryView=numberToolbar;
}

-(void)cancelNumberPad{
    
    if (numberFieldVal==1)
    {
        self.mobile_txtBox.text=mobileStr;
    }
    if (numberFieldVal==2)
    {
        self.house_Phone_txtBox.text=housePhoneStr;
    }
    if (numberFieldVal==3)
    {
        self.house_No_TxtBox.text=houseNoStr;
    }
    
    [self.house_Phone_txtBox resignFirstResponder];
    [self.mobile_txtBox resignFirstResponder];
    [self.house_No_TxtBox resignFirstResponder];
    
}

-(void)doneWithNumberPad
{
    if (numberFieldVal==1)
    {
        if (!((self.mobile_txtBox.text.length>=6)&&(self.mobile_txtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
            self.mobile_txtBox.text=mobileStr;
            [self toolbarForNumberPadSetup];
        }
    }
    if (numberFieldVal==2)
    {
        if (!((self.house_Phone_txtBox.text.length>=6)&&(self.house_Phone_txtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
            self.house_Phone_txtBox.text=housePhoneStr;
            [self toolbarForNumberPadSetup];
        }
    }
    if (numberFieldVal==3)
    {
        if (!((self.house_No_TxtBox.text.length>=6)&&(self.house_No_TxtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
            self.house_No_TxtBox.text=houseNoStr;
            [self toolbarForNumberPadSetup];
        }
    }
    [self.mobile_txtBox resignFirstResponder];
    [self.house_Phone_txtBox resignFirstResponder];
    [self.house_No_TxtBox resignFirstResponder];
}

#pragma mark - DROP-DOWN List Delegate Method


-(void)headerSelectList:(int)selectedIndex :(NSInteger)section{
    ////////// ReEnabling the UserInteraction of all SubViews/////////
    //    for (UIView *vw in self.view.subviews) {
    //        [vw setUserInteractionEnabled:YES];
    //    }
    
    [UIView animateWithDuration:0.4f animations:^{
        self.areaDropDownObj.view.frame =
        CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                   self.areaDropDownObj.view.frame.origin.y,
                   self.areaDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.areaSelectionButton.selected = NO;
    self.area_TxtBox.text =[[areaListArray objectAtIndex:section]objectAtIndex:selectedIndex];
    self.areaObj=[areaObjArray objectAtIndex:section];
    selectedAreaId=[[self.areaObj.area_List objectAtIndex:selectedIndex]objectForKey:@"AreaId"];
    
}

-(void)selectList:(int)selectedIndex
{
    if (fromOccupationBtn) {
        [UIView animateWithDuration:0.4f animations:^{
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.signUpDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.occupationSelectionBttn.selected = NO;
        self.occupation_TxtBox.text =[occupationArray objectAtIndex:selectedIndex];
     }
else if (fromGenderBtn) {
        [UIView animateWithDuration:0.4f animations:^{
            self.signUpDropDownObj.view.frame =
            CGRectMake(self.signUpDropDownObj.view.frame.origin.x,
                       self.signUpDropDownObj.view.frame.origin.y,
                       self.signUpDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.signUpDropDownObj.view removeFromSuperview];
            }
            
        }];
        self.genderSelection_Bttn.selected = NO;
        self.gender_TxtBox.text =[genderArray objectAtIndex:selectedIndex];
    }

else
{
    [UIView animateWithDuration:0.4f animations:^{
        self.signUpDropDownObj.view.frame =
        CGRectMake(self.signUpDropDownObj.view.frame.origin.x,
                   self.signUpDropDownObj.view.frame.origin.y,
                   self.signUpDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.signUpDropDownObj.view removeFromSuperview];
        }
        
    }];
    self.howUHearAboutSelectionBttn.selected = NO;
    self.hear_About_TxtBox.text =[howUHearArray objectAtIndex:selectedIndex];

}
}

-(void)xibLoading
{
    NSString *nibName = @"SignUpViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
