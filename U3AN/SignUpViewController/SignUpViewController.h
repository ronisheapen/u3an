//
//  SignUpViewController.h
//  U3AN
//
//  Created by Vipin on 18/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Areas.h"
#import "DropDownWithHeaderSelection.h"
#import "DropDownView.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate,ListWithHeaderSelectionProtocol,ListSelectionProtocol>

@property (strong, nonatomic)Areas *areaObj;
@property (strong, nonatomic) DropDownWithHeaderSelection *areaDropDownObj;
@property (strong, nonatomic) DropDownView *signUpDropDownObj;

// Account Information View

@property (weak, nonatomic) IBOutlet UIView *account_Info_View;
@property (weak, nonatomic) IBOutlet UIScrollView *account_Info_Scroll_View;

@property (weak, nonatomic) IBOutlet UILabel *account_Information_Label;
@property (weak, nonatomic) IBOutlet UILabel *email_Label;
@property (weak, nonatomic) IBOutlet UILabel *password_Label;
@property (weak, nonatomic) IBOutlet UILabel *confirm_Password_Label;
@property (weak, nonatomic) IBOutlet UILabel *first_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *last_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *mobile_Label;
@property (weak, nonatomic) IBOutlet UILabel *house_Phone_Label;
@property (weak, nonatomic) IBOutlet UITextField *email_txtBox;
@property (weak, nonatomic) IBOutlet UITextField *password_txtBox;
@property (weak, nonatomic) IBOutlet UITextField *confm_PassWrd_txtBox;
@property (weak, nonatomic) IBOutlet UITextField *first_Name_txtBox;
@property (weak, nonatomic) IBOutlet UITextField *last_Name_txtBox;
@property (weak, nonatomic) IBOutlet UITextField *mobile_txtBox;
@property (weak, nonatomic) IBOutlet UITextField *house_Phone_txtBox;
- (IBAction)account_Info_NextBttn_Action:(id)sender;
// Delivery Information View
@property (strong, nonatomic) IBOutlet UIView *delivery_Info_View;
@property (weak, nonatomic) IBOutlet UIScrollView *delivery_Info_Scroll_View;
@property (weak, nonatomic) IBOutlet UIView *delivery_SubView1;
@property (weak, nonatomic) IBOutlet UIView *delivery_SubView2;
@property (weak, nonatomic) IBOutlet UILabel *delivery_Information_Label;
@property (weak, nonatomic) IBOutlet UILabel *building_Label;
@property (weak, nonatomic) IBOutlet UILabel *villa_House_Label;
@property (weak, nonatomic) IBOutlet UILabel *office_Label;

@property (weak, nonatomic) IBOutlet UILabel *area_Label;
@property (weak, nonatomic) IBOutlet UILabel *profile_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *block_Label;
@property (weak, nonatomic) IBOutlet UILabel *judda_Label;
@property (weak, nonatomic) IBOutlet UILabel *street_Label;
@property (weak, nonatomic) IBOutlet UILabel *house_No_Label;
@property (weak, nonatomic) IBOutlet UILabel *floor_Label;
@property (weak, nonatomic) IBOutlet UILabel *apartment_Office_Label;
@property (weak, nonatomic) IBOutlet UILabel *direction_Label;
@property (weak, nonatomic) IBOutlet UITextField *area_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *profile_Name_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *block_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *judda_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *street_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *house_No_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *floor_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *apartment_Office_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *direction_TxtBox;
@property (weak, nonatomic) IBOutlet UIButton *villa_Button;
@property (weak, nonatomic) IBOutlet UIButton *building_Button;
@property (weak, nonatomic) IBOutlet UIButton *office_Button;
@property (weak, nonatomic) IBOutlet UIButton *delivery_Nxt_Bttn;
@property (weak, nonatomic) IBOutlet UIButton *areaSelectionButton;
@property (strong, nonatomic) IBOutlet UIButton *accNxtButton;

- (IBAction)delivery_Info_NextBttn_Action:(id)sender;
- (IBAction)radioBttn_Action:(UIButton *)sender;
- (IBAction)area_SelectionBttn_Action:(UIButton *)sender;
- (IBAction)deliverViewBckBttnAction:(UIButton *)sender;

// Personal Information View
@property (strong, nonatomic) IBOutlet UIView *personal_Info_View;
@property (weak, nonatomic) IBOutlet UIScrollView *personal_Info_Scroll_View;

@property (weak, nonatomic) IBOutlet UILabel *personal_Information_Label;
@property (weak, nonatomic) IBOutlet UILabel *gender_Label;
@property (weak, nonatomic) IBOutlet UILabel *occupation_Label;
@property (weak, nonatomic) IBOutlet UILabel *hear_About_Label;
@property (weak, nonatomic) IBOutlet UILabel *referredBy_Label;
@property (weak, nonatomic) IBOutlet UITextField *gender_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *occupation_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *hear_About_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *referred_By_TxtBox;

@property (weak, nonatomic) IBOutlet UILabel *latest_Promos_Label;
@property (weak, nonatomic) IBOutlet UILabel *keep_Posted_SMS_Label;
@property (weak, nonatomic) IBOutlet UILabel *privacy_Policy_Label;
@property (weak, nonatomic) IBOutlet UIButton *latest_Promos_Bttn;
@property (weak, nonatomic) IBOutlet UIButton *keep_Posted_SMS_Bttn;
@property (weak, nonatomic) IBOutlet UIButton *privacy_Policy_Bttn;
@property (weak, nonatomic) IBOutlet UIButton *submit_Bttn;
@property (weak, nonatomic) IBOutlet UIButton *occupationSelectionBttn;
@property (weak, nonatomic) IBOutlet UIButton *howUHearAboutSelectionBttn;
@property (weak, nonatomic) IBOutlet UIButton *genderSelection_Bttn;

- (IBAction)backgroundTapAction:(id)sender;

- (IBAction)genderSelectionBttnAction:(UIButton *)sender;
- (IBAction)latest_Promos_Bttn_Action:(UIButton *)sender;
- (IBAction)keep_Posted_SMS_Bttn_Action:(UIButton *)sender;
- (IBAction)privacy_Policy_Bttn_Action:(UIButton *)sender;
- (IBAction)submit_Bttn_Action:(UIButton *)sender;
- (IBAction)occupation_SelectionBttnAction:(id)sender;
- (IBAction)how_U_HearAbout_BttnAction:(id)sender;
- (IBAction)personalViewBackBttnAction:(UIButton *)sender;

@end
