//
//  RestaurantReviewVC.m
//  U3AN
//
//  Created by Vipin on 25/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantReviewVC.h"
#import <CoreText/CoreText.h>

@interface RestaurantReviewVC ()

@end

@implementation RestaurantReviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
     [self updateViewHeading];
    [self updateRestaurantReviewView];
    [self getRestaurantReviewList];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)updateRestaurantReviewView
{
    self.review_ScrollView.layer.cornerRadius = 3.0;
    self.review_ScrollView.layer.masksToBounds = YES;

        self.restaurant_Name_Label.text=self.restaurantDetailObj.restaurant_Name;
        if (self.restaurantDetailObj.restaurant_Logo.length>0)
        {
            [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.restaurantDetailObj.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
                
                self.restaurant_ImageView.image = responseImage;
                [self.restaurant_ImageView.layer needsLayout];
                
            } errorHandler:^(NSError *error) {
                
            }];
        }
        self.rating_Count= [self.restaurantDetailObj.rating integerValue];
        for (int k=0,gap=5; k<5; k++,gap=gap+5) {
            UIImageView *star =[[UIImageView alloc] initWithFrame:CGRectMake((k*15)+gap,0,15,15)];
            star.image=[UIImage imageNamed:@"star_unsel.png"];
            [self.rating_View addSubview:star];
            if (k<self.rating_Count) {
                star.image=[UIImage imageNamed:@"star_sel.png"];
            }
            
        }
        [self.review_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f]];
        [self.restaurant_Name_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
        [self.noReviewLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f]];

    
}

-(void)getRestaurantReviewList
{
    NSMutableArray *reviewListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getRestaurantPostData]];
    //    for (UIView *sub in self.mostSellingDishesByRes_ScrollView.subviews) {
    //        [sub removeFromSuperview];
    //    }
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getRestaurantReviewWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             RestaurantReview *restaurantReviewItem = [ApplicationDelegate.mapper getRestaurantReviewDetailFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [reviewListArray addObject:restaurantReviewItem];
                         }
                     }
                     
                 }
             }
             if (reviewListArray.count==0)
             {
                 //[ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
                 self.noReviewLabel.superview.hidden = NO;
                 self.reviewList_ScrollView.hidden = YES;
             }
             else{
                 
                 self.noReviewLabel.superview.hidden = YES;
                 self.reviewList_ScrollView.hidden = NO;
                 
                 [self prepareReviewListScroll_Grid_WithArray:reviewListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}
-(NSMutableDictionary *)getRestaurantPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.restaurantDetailObj.restaurant_Id forKey:@"restId"];
    
    return postDic;
}

-(void)prepareReviewListScroll_Grid_WithArray:(NSMutableArray *)reviewListArray
{
    
    self.reviewList_ScrollView.hidden = NO;
    
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.reviewList_ScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.reviewList_ScrollView.backgroundColor = [UIColor clearColor];
    
    [self.reviewList_ScrollView setContentOffset:CGPointZero];
    

    
    for (int i=0,j=0; i<reviewListArray.count; i++,j=j+5)
    {
        RestaurantReviewCell *reviewCell = [[RestaurantReviewCell alloc] init];
        RestaurantReview *restaurantReviewItem = [reviewListArray objectAtIndex:i];
        NSMutableAttributedString *reviewString = [[NSMutableAttributedString alloc] init];
        NSString *tempString = [[NSString alloc] init];
        tempString=@"By: ";
        [reviewString appendAttributedString:[self convertIntoBlackFont:tempString]];
        tempString=restaurantReviewItem.name;
        [reviewString appendAttributedString:[self convertIntoRedFont:tempString]];
        tempString=@" At: ";
        [reviewString appendAttributedString:[self convertIntoBlackFont:tempString]];
        tempString=restaurantReviewItem.created_Time;
        [reviewString appendAttributedString:[self convertIntoRedFont:tempString]];
        [reviewString appendAttributedString:[self convertIntoRedFont:@"/\n\n"]];
        tempString=restaurantReviewItem.review_Txt;
        [reviewString appendAttributedString:[self convertIntoBlackFont:tempString]];
       // NSLog(@"tempString>>>>>>>>>>>>>>>>>>>>>> : %@",reviewString);
        [reviewCell.review_TextView setAttributedText:reviewString];
       // [reviewCell.tmpLabel setAttributedText:reviewString];
       reviewCell.frame = CGRectMake(0, (i*reviewCell.frame.size.height)+j, reviewCell.frame.size.width, reviewCell.frame.size.height);
        [self.reviewList_ScrollView addSubview:reviewCell];
        
    }
    
 [self.reviewList_ScrollView setContentSize:CGSizeMake(self.reviewList_ScrollView.frame.size.width, ((+reviewListArray.count*84)+20))];
    
}

-(NSMutableAttributedString*)convertIntoRedFont:(NSString*)aString{
    
  //  UIFont *redFont = [UIFont fontWithName:@"Tahoma" size:12.0f];
    CTFontRef redFont = CTFontCreateWithName((__bridge CFStringRef) [UIFont fontWithName:@"Tahoma" size:15.0f].fontName, [UIFont fontWithName:@"Tahoma" size:12.0f].pointSize, NULL);
    UIColor *color = [UIColor redColor];

    // Create the attributes

//    NSDictionary *attributesDict = [NSDictionary dictionaryWithObjectsAndKeys:
//                                    ( __bridge id)redFont,(id)kCTFontAttributeName,color,(id)NSForegroundColorAttributeName,nil];
//    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:aString
//                                                                       attributes:attributesDict];


    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithString:aString];
    [attrString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,aString.length)];
    
    [attrString addAttribute:NSFontAttributeName
                       value:( __bridge id)redFont
                       range:NSMakeRange(0,aString.length)];
    
    return attrString;
}

-(NSMutableAttributedString*)convertIntoBlackFont:(NSString*)aString{
    
    CTFontRef redFont = CTFontCreateWithName((__bridge CFStringRef) [UIFont fontWithName:@"Tahoma" size:15.0f].fontName, [UIFont fontWithName:@"Tahoma" size:12.0f].pointSize, NULL);
    
    //UIFont *redFont = [UIFont fontWithName:@"Tahoma" size:12.0f];
    UIColor *color = [UIColor blackColor];
    

    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithString:aString];
    [attrString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,aString.length)];
    
    [attrString addAttribute:NSFontAttributeName
                   value:( __bridge id)redFont
                   range:NSMakeRange(0,aString.length)];
    
    return attrString;
}

-(void)xibLoading
{
    NSString *nibName = @"RestaurantReviewVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
