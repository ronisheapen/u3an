//
//  RestaurantReviewVC.h
//  U3AN
//
//  Created by Vipin on 25/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantReview.h"
#import "RestaurantReviewCell.h"

@interface RestaurantReviewVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *restaurant_ImageView;
@property (weak, nonatomic) IBOutlet UIView *rating_View;
@property (weak, nonatomic) IBOutlet UILabel *restaurant_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *review_Label;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;
@property (weak, nonatomic) IBOutlet UIScrollView *review_ScrollView;
@property (assign, nonatomic)NSInteger rating_Count;
@property (weak, nonatomic) IBOutlet UIScrollView *reviewList_ScrollView;

@property (strong, nonatomic) IBOutlet UILabel *noReviewLabel;
@end
