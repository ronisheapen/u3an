//
//  RestaurantMenuListVC.m
//  U3AN
//
//  Created by Vipin on 24/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantMenuListVC.h"
#import "RestaurantMenuDetailVC.h"
#import "RestaurantReviewVC.h"

@interface RestaurantMenuListVC ()
{
    NSMutableArray *restaurantMenuSectionsListArray;
}
@end

@implementation RestaurantMenuListVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self xibLoading];
    
    self.restaurantMenuTable.backgroundColor = [UIColor whiteColor];
    
    self.restaurantMenuTable.layer.cornerRadius = 3;
    self.restaurantMenuTable.layer.masksToBounds = YES;
    self.restuntmenuheader.layer.cornerRadius = 3;
    self.restuntmenuheader.layer.masksToBounds = YES;
    
    self.menutopBackground.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MenuheadBackgroung.png"]];

}
-(void)viewWillAppear:(BOOL)animated
{
    [self setupUI];
    [self fetchRestaurantMenuSectionsFromServer];
    [self updateViewHeading];
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupUI
{
    restaurantMenuSectionsListArray = [[NSMutableArray alloc] init];
    
    self.contentContainerView.layer.cornerRadius = 2.0;
    self.contentContainerView.layer.masksToBounds = YES;
    
    self.restuarantMenuHeaderLabel.font = kMAIN_HEADING_REGULAR_WithFontSize(@"18.0f");
    self.restaurantNameLabel.font = kMAIN_HEADING_REGULAR_WithFontSize(@"15.0f");
    self.reviewsLabel.font = kMAIN_HEADING_REGULAR_WithFontSize(@"15.0f");
    
    self.restaurantNameLabel.text = self.restaurantDetailObj.restaurant_Name;
    self.rating_Count= [self.restaurantDetailObj.rating integerValue];
    for (int k=0,gap=5; k<5; k++,gap=gap+5) {
        UIImageView *star =[[UIImageView alloc] initWithFrame:CGRectMake((k*15)+gap,0,15,15)];
        star.image=[UIImage imageNamed:@"promotion_star_unsel.png"];
        [self.ratingsView addSubview:star];
        if (k<self.rating_Count) {
            star.image=[UIImage imageNamed:@"promotion_star_sel.png"];
        }
        
    }
    if (self.restaurantDetailObj.restaurant_Logo.length>0)
    {
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.restaurantDetailObj.restaurant_Logo] completionHandler:^(UIImage *responseImage)
         {
             [ApplicationDelegate loadImageWithAnimationInImageView:self.logoImageView withImage:responseImage];
             
         } errorHandler:^(NSError *error) {
             
         }];
    }
}

-(NSMutableDictionary *)getRestaurantMenuSectionPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    if ([ApplicationDelegate isValid:self.restaurantDetailObj])
    {
        if (self.restaurantDetailObj.restaurant_Id.length>0)
        {
            [postDic setObject:self.restaurantDetailObj.restaurant_Id forKey:@"restId"];
            //[postDic setObject:kLocal forKey:@"locale"];
            if (![ApplicationDelegate.language isEqualToString:kENGLISH])
            {
                [postDic setObject:k_AR_Local forKey:@"locale"];
            }
            else
            {
                [postDic setObject:kLocal forKey:@"locale"];
            }
        }
        
    }
    return postDic;
}

-(void)fetchRestaurantMenuSectionsFromServer
{
    restaurantMenuSectionsListArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *restaurantMenuSectionsDataDic = [[NSMutableDictionary alloc] initWithDictionary:[self getRestaurantMenuSectionPostData]];
    
    if (restaurantMenuSectionsDataDic.count>0)
    {
        __block BOOL responseSuccess = NO;
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine getRestaurantMenuSectionsWithDataDictionary:restaurantMenuSectionsDataDic CompletionHandler:^(NSMutableArray *responseArray)
         {
             if (!responseSuccess)
             {
                 responseSuccess = YES;
                 
                 if ([ApplicationDelegate isValid:responseArray])
                 {
                     if (responseArray.count>0)
                     {
                         for (int i=0; i<responseArray.count; i++)
                         {
                             if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                             {
                                 RestaurantMenuSection *menuSectionObj = [ApplicationDelegate.mapper getRestaurantMenuSectionObjectFromDictionary:[responseArray objectAtIndex:i]];
                                 
                                 [restaurantMenuSectionsListArray addObject:menuSectionObj];
                             }
                             
                         }
                         
                     }
                 }
                 
                 if (restaurantMenuSectionsListArray.count==0)
                 {
                     [ApplicationDelegate showAlertWithMessage:kNO_DATA_ERROR_MSG title:nil];
                 }
                 else
                 {
                     [self.restaurantMenuTable reloadData];
                 }
                 [ApplicationDelegate removeProgressHUD];
             }
             
             
         } errorHandler:^(NSError *error)
         {
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
    
}
#pragma mark - TABLEVIEW DELEGATE METHODS
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return restaurantMenuSectionsListArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 45;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        // Configure the cell...
        
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        UIImageView *separator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 43, cell.frame.size.width, 2)];
        separator.tag = 4;
        separator.image = [UIImage imageNamed:@"morepage_divider.png"];
        [cell.contentView addSubview:separator];
    }

    UIImageView *separatorLine = (UIImageView *)[cell viewWithTag:4];
    if (indexPath.row==(restaurantMenuSectionsListArray.count-1))
    {
        separatorLine.hidden = YES;
    }
    else
    {
        separatorLine.hidden = NO;
    }
    
    RestaurantMenuSection *restMenuSectionObj = (RestaurantMenuSection *)[restaurantMenuSectionsListArray objectAtIndex:indexPath.row];
    
    
    cell.textLabel.font = kMAIN_HEADING_REGULAR_WithFontSize(@"15.0f");
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text =restMenuSectionObj._name;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RestaurantMenuSection *restMenuSectionObj = (RestaurantMenuSection *)[restaurantMenuSectionsListArray objectAtIndex:indexPath.row];
    
    NSLog(@"Selected>>>>ID==%@,NAME==%@",restMenuSectionObj._id,restMenuSectionObj._name);
    RestaurantMenuDetailVC *restaurantMenuDetailVC = [[RestaurantMenuDetailVC alloc] initWithNibName:@"RestaurantMenuDetailVC" bundle:nil];
    
    restaurantMenuDetailVC.restaurantMenuObj = restMenuSectionObj;
    restaurantMenuDetailVC.restaurantDetailObj = self.restaurantDetailObj;
    
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantMenuDetailVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:restaurantMenuDetailVC animated:NO];
    }

}
- (IBAction)review_ButtonAction:(UIButton *)sender {
    
    RestaurantReviewVC *restaurantReviewVC = [[RestaurantReviewVC alloc] initWithNibName:@"RestaurantReviewVC" bundle:nil];
    
    //restaurantReviewVC.restaurantMenuObj = restMenuSectionObj;
    restaurantReviewVC.restaurantDetailObj = self.restaurantDetailObj;
    
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantReviewVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:restaurantReviewVC animated:NO];
    }
}

-(void)xibLoading
{
    NSString *nibName = @"RestaurantMenuListVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
