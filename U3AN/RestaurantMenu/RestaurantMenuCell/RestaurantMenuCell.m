//
//  RestaurantMenuCell.m
//  U3AN
//
//  Created by Vipin on 25/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantMenuCell.h"
#define MAXLENGTH 2

@implementation RestaurantMenuCell
{
    NSString *qtyStr;
}
-(id)init
{
    self = [super init];
    if (self)
    {
        self.choicesSelected = [[NSMutableArray alloc] init];
        qtyStr = @"1";
    }
    
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"RestaurantMenuCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (RestaurantMenuCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"RestaurantMenuCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (RestaurantMenuCell*)[nibViews objectAtIndex: 0];
        }
    }
    [self toolbarForNumberPadSetup];
    return self;
}

- (IBAction)order_ButtonAction:(UIButton *)sender {
    [self.delegateMenuItem orderButtonDidClicked:self];
}

- (IBAction)menuChoice_ButtonAction:(UIButton *)sender {
    [self.delegateMenuItem menuChoiceButtonDidClicked:self];
}

- (IBAction)minusButton:(id)sender {
    
  //  [self.delegateMenuItem minusButtonDidClicked:self];
    
    int TxtBox;
    
    if([self.quantity_TxtBox.text intValue]>1)
    {
    
   TxtBox= [self.quantity_TxtBox.text intValue]-1;
        
     self.quantity_TxtBox.text=[NSString stringWithFormat:@"%d", TxtBox];   
    
   }

    
}

- (IBAction)plusButton:(id)sender {
    
    int TxtBox;    
    TxtBox= [self.quantity_TxtBox.text intValue]+1;
    
    self.quantity_TxtBox.text=[NSString stringWithFormat:@"%d", TxtBox];
    
}


- (IBAction)addTo_Fav_BttnAction:(UIButton *)sender {
    [self.delegateMenuItem addFavButtonDidClicked:self];
}

#pragma mark - NUMBER PAD METHODS
#pragma mark -
-(void)toolbarForNumberPadSetup
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, ApplicationDelegate.window.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.quantity_TxtBox.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad{
    

    self.quantity_TxtBox.text=qtyStr;
    [self.quantity_TxtBox resignFirstResponder];
    
}

-(void)doneWithNumberPad
{
    if (([self.quantity_TxtBox.text longLongValue]>99)||(self.quantity_TxtBox.text.length==0)||([self.quantity_TxtBox.text integerValue]==0))
    {
        qtyStr = @"1";
    }
    else
    {
        qtyStr = self.quantity_TxtBox.text;
    }
    
    self.quantity_TxtBox.text=qtyStr;
    
    [self toolbarForNumberPadSetup];

    [self.quantity_TxtBox resignFirstResponder];
}

#pragma mark - TEXT FIELD DELEGATES

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[self.accountInfoScrollView setContentOffset:CGPointZero animated:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
//    if (textField==self.quantity_TxtBox)
//    {
//        if (oldLength>2)
//        {
//            return newLength <= 2 || returnKey;
//        }
//    }
    
    return newLength <= MAXLENGTH || returnKey;
}

@end
