//
//  RestaurantMenuDetailVC.m
//  U3AN
//
//  Created by Vipin on 25/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantMenuDetailVC.h"
#import "RestaurantReviewVC.h"
#import "CartListViewController.h"
#import "LoginViewController.h"
#define MAXLENGTH 2

@interface RestaurantMenuDetailVC ()
{
    NSMutableArray *choiceArray;
}
@end

@implementation RestaurantMenuDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self updateCartCount];
    [self updateRestaurantMenuDetailsView];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = NO;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateCartCount
{
    if ([ApplicationDelegate isLoggedIn])
    {
        [self getCartCount];
    }
    else
    {
        [self getTempCartCount];
    }
}
-(NSMutableDictionary *)getTempUserCartCountPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:ApplicationDelegate.currentDeviceId forKey:@"userId"];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}
-(NSMutableDictionary *)getUserCartCountPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}
-(void)getTempCartCount
{
    [HomeTabViewController sharedViewController].cartCountLabel.text = @"";
    [HomeTabViewController sharedViewController].cartCountLabel.hidden = YES;
    
    [ApplicationDelegate.engine getTempCartCountWithDataDictionary:[self getTempUserCartCountPostData] CompletionHandler:^(NSMutableDictionary *responseDictionary)
    {
        if ([ApplicationDelegate isValid:responseDictionary])
        {
            if (responseDictionary.count>0)
            {
                if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                {
                    if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                    {
                        if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Value"]])
                        {
                            if ([[responseDictionary objectForKey:@"Value"] longLongValue]>0)
                            {
                                [HomeTabViewController sharedViewController].cartCountLabel.text = [NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"Value"]];
                                [HomeTabViewController sharedViewController].cartCountLabel.hidden = NO;
                            }
                        }
                    }
                }
            }
        }
        
    } errorHandler:^(NSError *error)
     {
        
    }];
}
-(void)getCartCount
{
    [HomeTabViewController sharedViewController].cartCountLabel.text = @"";
    [HomeTabViewController sharedViewController].cartCountLabel.hidden = YES;
    
    [ApplicationDelegate.engine getCartCountWithDataDictionary:[self getUserCartCountPostData] CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if ([ApplicationDelegate isValid:responseDictionary])
         {
             if (responseDictionary.count>0)
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                 {
                     if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                     {
                         if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Value"]])
                         {
                             if ([[responseDictionary objectForKey:@"Value"] longLongValue]>0)
                             {
                                 [HomeTabViewController sharedViewController].cartCountLabel.text = [NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"Value"]];
                                 [HomeTabViewController sharedViewController].cartCountLabel.hidden = NO;
                             }
                         }
                     }
                 }
             }
         }
         
     } errorHandler:^(NSError *error)
     {
         
     }];
}
-(void)updateRestaurantMenuDetailsView
{
    self.restaurant_Menu_ScrollView.layer.cornerRadius = 3.0;
    self.restaurant_Menu_ScrollView.layer.masksToBounds = YES;
    if ([ApplicationDelegate isValid:self.restaurantMenuObj]) {
        self.menu_Item_Label.text=self.restaurantMenuObj._name;
        self.restaurant_Name_Label.text=self.restaurantDetailObj.restaurant_Name;
        if (self.restaurantDetailObj.restaurant_Logo.length>0)
        {
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.restaurantDetailObj.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            
            self.restaurant_ImageView.image = responseImage;
            [self.restaurant_ImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        }
        self.restaurant_rating_Count= [self.restaurantDetailObj.rating integerValue];
        for (int k=0,gap=5; k<5; k++,gap=gap+5) {
            UIImageView *star =[[UIImageView alloc] initWithFrame:CGRectMake((k*15)+gap,0,15,15)];
            star.image=[UIImage imageNamed:@"promotion_star_unsel.png"];
            [self.rating_View addSubview:star];
            if (k<self.restaurant_rating_Count) {
                star.image=[UIImage imageNamed:@"promotion_star_sel.png"];
            }
            
        }
    [self.menu_Item_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.restaurant_Menu_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.restaurant_Name_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f]];
    [self.review_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f]];
    [self getDishesMenuList];
    }
}

-(void)getDishesMenuList
{
    NSMutableArray *dishesListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *dishesData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
    //    for (UIView *sub in self.mostSellingDishesByRes_ScrollView.subviews) {
    //        [sub removeFromSuperview];
    //    }
    if (dishesData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getDishesMenuDetailsWithDataDictionary:dishesData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             DishMenuDetails *dishesMenuItem = [ApplicationDelegate.mapper getDishMenuDetailFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [dishesListArray addObject:dishesMenuItem];
                         }
                     }
                     
                 }
             }
             if (dishesListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareDishesListScroll_Grid_WithArray:dishesListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}


-(NSMutableDictionary *)getDishesPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.restaurantMenuObj._id forKey:@"menuSectionId"];
    
    return postDic;
}

-(void)prepareDishesListScroll_Grid_WithArray:(NSMutableArray *)dishesListArray
{
    self.restaurant_Menu_ScrollView.contentSize = CGSizeMake(self.restaurant_Menu_ScrollView.frame.size.width,  (self.restaurant_Menu_ScrollView.frame.size.height+self.menu_Item_Label.frame.origin.y-10));
    
    self.dishes_ScrollView.frame = CGRectMake(self.dishes_ScrollView.frame.origin.x, self.dishes_ScrollView.frame.origin.y, self.dishes_ScrollView.frame.size.width, (self.restaurant_Menu_ScrollView.contentSize.height-self.dishes_ScrollView.frame.origin.y-5));
    
    
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.dishes_ScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.dishes_ScrollView.backgroundColor = [UIColor clearColor];
    
    [self.dishes_ScrollView setContentOffset:CGPointZero];
    
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    RestaurantMenuCell *vw = [[RestaurantMenuCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.dishes_ScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<dishesListArray.count; i++)
    {
        int colIndex = i%colCount;
        RestaurantMenuCell *dishesCell = [[RestaurantMenuCell alloc] init];
        dishesCell.tag = i;
        dishesCell.layer.cornerRadius = 2.0;
        dishesCell.layer.masksToBounds = YES;
        dishesCell.dish_ImageView.layer.cornerRadius = 2.0;
        dishesCell.dish_ImageView.layer.masksToBounds = YES;
        dishesCell.menu_Choice_Button.hidden=YES;
        DishMenuDetails *dishesMenuItem = [dishesListArray objectAtIndex:i];
        dishesCell.dishesMenuItemObject = dishesMenuItem;
        dishesCell.dish_Name_Label.text=dishesMenuItem.name;
        dishesCell.quantity_TxtBox.text=@"1";
        dishesCell.quantity_TxtBox.delegate = self;
        dishesCell.delegateMenuItem=self;
        [dishesCell.dish_Name_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        dishesCell.addFavButton.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
        if (ApplicationDelegate.isLoggedIn)
        {
            dishesCell.addFavButton.hidden=NO;
       
        }
        else
        {
            dishesCell.addFavButton.hidden=YES;
        }
      //  [ApplicationDelegate loadMarqueeLabelWithText:dishesCell.dish_Name_Label.text Font:dishesCell.dish_Name_Label.font InPlaceOfLabel:dishesCell.dish_Name_Label];
        dishesCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  dishesCell.frame.size.width, dishesCell.frame.size.height);
        //
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:dishesMenuItem.dish_Thumbnail] completionHandler:^(UIImage *responseImage) {
            dishesCell.dish_ImageView.image = responseImage;
            [dishesCell.dish_ImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            dishesCell.dish_ImageView.image =[UIImage imageNamed:@"Default_Food.png"];
            
        }];
        if (dishesMenuItem.menuChoice_array.count>0) {
            dishesCell.menu_Choice_Button.hidden=NO;
        }
        
        
        self.rating_Count= [dishesMenuItem.rating integerValue];
        for (int k=0,gap=5; k<5; k++,gap=gap+5) {
            UIImageView *star =[[UIImageView alloc] initWithFrame:CGRectMake((k*18)+gap,0,18,18)];
            star.image=[UIImage imageNamed:@"promotion_star_unsel.png"];
            [dishesCell.ratingView addSubview:star];
            if (k<self.rating_Count) {
                star.image=[UIImage imageNamed:@"promotion_star_sel.png"];
            }
        }
        
        dishesCell.price_Label.text=[NSString stringWithFormat:@"KD %@",dishesMenuItem.price];
        [dishesCell.price_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        dishesCell.dish_Discription_Label.text=dishesMenuItem.description;
        [dishesCell.dish_Discription_Label setFont:[UIFont fontWithName:@"Tahoma" size:12.0f]];
        [ApplicationDelegate loadMarqueeLabelWithText:dishesCell.dish_Discription_Label.text Font:dishesCell.dish_Discription_Label.font InPlaceOfLabel:dishesCell.dish_Discription_Label];
        
        [self.dishes_ScrollView addSubview:dishesCell];
        if (colIndex == (colCount - 1))
        {
            yVal = dishesCell.frame.origin.y + dishesCell.frame.size.height;
        }
        
        scrollHeight = dishesCell.frame.origin.y + dishesCell.frame.size.height;
    }
    
    self.dishes_ScrollView.contentSize = CGSizeMake(self.dishes_ScrollView.frame.size.width,  (scrollHeight + offsetValue+100));
    
    
}


- (IBAction)review_ButtonAction:(UIButton *)sender {
    RestaurantReviewVC *restaurantReviewVC = [[RestaurantReviewVC alloc] initWithNibName:@"RestaurantReviewVC" bundle:nil];
    
    //restaurantReviewVC.restaurantMenuObj = restMenuSectionObj;
    restaurantReviewVC.restaurantDetailObj = self.restaurantDetailObj;
    
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantReviewVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:restaurantReviewVC animated:NO];
    }
}


-(void)menuChoiceButtonDidClicked:(RestaurantMenuCell *)restaurantMenuItem
{
    /*
    restaurantMenuItem.menu_Choice_Button.selected = !restaurantMenuItem.menu_Choice_Button.selected;
    if (restaurantMenuItem.menu_Choice_Button.selected)
    {
        if (self.restaurantMenuChoiceDropDownObj.view.superview) {
            [self.restaurantMenuChoiceDropDownObj.view removeFromSuperview];
        }
        
        self.restaurantMenuChoiceDropDownObj= [[DropDownWithButton alloc] initWithNibName:@"DropDownWithButton" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.restaurantMenuChoiceDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.restaurantMenuChoiceDropDownObj.view.frame = CGRectMake((restaurantMenuItem.frame.origin.x),(restaurantMenuItem.frame.origin.y+restaurantMenuItem.frame.size.height+2), (restaurantMenuItem.frame.size.width), 0);
        
        self.restaurantMenuChoiceDropDownObj.selectionBttnDropDownDelegate = self;
        self.restaurantMenuChoiceDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:choiceArray];
        
        self.restaurantMenuChoiceDropDownObj.view.layer.borderWidth = 0.1;
        self.restaurantMenuChoiceDropDownObj.view.layer.shadowOpacity = 1.0;
        self.restaurantMenuChoiceDropDownObj.view.layer.shadowRadius = 5.0;
        self.restaurantMenuChoiceDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        
        self.restaurantMenuChoiceDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.restaurantMenuChoiceDropDownObj.textLabelColor = [UIColor blackColor];
        self.restaurantMenuChoiceDropDownObj.view.backgroundColor = [UIColor greenColor];
        
        if (choiceArray.count>0)
        {
            [restaurantMenuItem.superview addSubview:self.restaurantMenuChoiceDropDownObj.view];
        }
        else
        {
            restaurantMenuItem.menu_Choice_Button.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.restaurantMenuChoiceDropDownObj.cellHeight*self.restaurantMenuChoiceDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.restaurantMenuChoiceDropDownObj.cellHeight*self.restaurantMenuChoiceDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.restaurantMenuChoiceDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.restaurantMenuChoiceDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.restaurantMenuChoiceDropDownObj.view.frame =
            CGRectMake(self.restaurantMenuChoiceDropDownObj.view.frame.origin.x+2,
                       self.restaurantMenuChoiceDropDownObj.view.frame.origin.y,
                       self.restaurantMenuChoiceDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.restaurantMenuChoiceDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.restaurantMenuChoiceDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.restaurantMenuChoiceDropDownObj.view.frame =
            CGRectMake(self.restaurantMenuChoiceDropDownObj.view.frame.origin.x,
                       self.restaurantMenuChoiceDropDownObj.view.frame.origin.y,
                       self.restaurantMenuChoiceDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.restaurantMenuChoiceDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
     */
    
    [self.view endEditing:YES];
    choiceArray = [[NSMutableArray alloc] initWithArray:restaurantMenuItem.dishesMenuItemObject.menuChoice_array];
    if (choiceArray.count>0)
    {
        if (restaurantMenuItem.choicesSelected.count==0)
        {
            for (int i=0; i<choiceArray.count; i++)
            {
                NSMutableArray *sectionSelectedChoiceEmptyArray = [[NSMutableArray alloc] init];
                
                [restaurantMenuItem.choicesSelected addObject:sectionSelectedChoiceEmptyArray];
            }
        }
    }
    //NSLog(@"%@",restaurantMenuItem.choicesSelected);
    
    self.menuChoicePopUpView.frame = CGRectMake(0, 0, ApplicationDelegate.window.rootViewController.view.frame.size.width, ApplicationDelegate.window.rootViewController.view.frame.size.height);
    
    CGFloat htFact = restaurantMenuItem.dishesMenuItemObject.menuChoice_array.count * 40.0f;
    
    if (restaurantMenuItem.dishesMenuItemObject.menuChoice_array.count>0)
    {
        if (htFact < (ApplicationDelegate.window.rootViewController.view.frame.size.height-120))
        {
//            htFact = htFact + 40.0f;
//            
//            self.menuChoicePopUpContentView.frame = CGRectMake(self.menuChoicePopUpContentView.frame.origin.x, self.menuChoicePopUpContentView.frame.origin.x, self.menuChoicePopUpContentView.frame.size.width, htFact);
//            
//            self.menuChoicePopUpContentView.center = self.menuChoicePopUpView.center;
        }
        
        self.menuChoicePopUpContentView.layer.cornerRadius = 2.0;
        self.menuChoicePopUpContentView.layer.masksToBounds = YES;
        self.menuChoicePopUpView.layer.cornerRadius = 2.0;
        self.menuChoicePopUpView.layer.masksToBounds = YES;
        
        self.menuChoicePopUpTableView.tag = restaurantMenuItem.tag;
        
        [ApplicationDelegate addViewWithPopUpAnimation:self.menuChoicePopUpView InParentView:ApplicationDelegate.window.rootViewController.view];
        self.menuChoicePopUpTableView.contentOffset = CGPointZero;
        [self.menuChoicePopUpTableView reloadData];
    }
    
}
-(void)buttonSelectList:(int)selectedIndex
{
    if (choiceArray.count>0)
    {
        if (selectedIndex<choiceArray.count)
        {
            
        }
    }
}


-(void)addFavButtonDidClicked:(RestaurantMenuCell *)restaurantMenuItem
{
    [self.view endEditing:YES];
    [self addToFavourite:restaurantMenuItem];
    
    
}

-(void)minusButtonDidClicked:(RestaurantMenuCell *)restaurantMenuItem

{
    
    
    
}


-(void)orderButtonDidClicked:(RestaurantMenuCell *)restaurantMenuItem
{
    [self.view endEditing:YES];
    
    if (([restaurantMenuItem.dishesMenuItemObject.price doubleValue]>0)&&([restaurantMenuItem.quantity_TxtBox.text longLongValue]>0))
    {
        if ([self isMinCountSelectionMatchingBeforeOrder:restaurantMenuItem])
        {
            if (ApplicationDelegate.isLoggedIn)
            {
                NSMutableDictionary *postDic = [self getAddToCartPostDataWithUserName:[ApplicationDelegate logged_User_Name] AndRestuarantMenuCellObject:restaurantMenuItem];
                
                if (postDic.count>0)
                {
                    __block BOOL dataFound = NO;
                    
                    [ApplicationDelegate addProgressHUDToView:self.view];
                    
                    [ApplicationDelegate.engine addToCartWithDataDictionary:postDic CompletionHandler:^(NSMutableDictionary *responseDictionary)
                     {
                         if (!dataFound)
                         {
                             dataFound = YES;
                             
                             if ([ApplicationDelegate isValid:responseDictionary])
                             {
                                 NSLog(@"%@",responseDictionary);
                                 
                                 if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                                 {
                                     [ApplicationDelegate showAlertWithMessage:@"An item added to Cart successfully" title:@""];
                                     [self viewWillAppear:NO];
                                     
                                     /*
                                     CartListViewController *cartListViewController = [[CartListViewController alloc] initWithNibName:@"CartListViewController" bundle:nil];
                                     if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[cartListViewController class]])
                                     {
                                         [ApplicationDelegate.subHomeNav pushViewController:cartListViewController animated:NO];
                                     }
                                     */
                                     
                                 }
                             }
                             [ApplicationDelegate removeProgressHUD];
                         }
                         
                     } errorHandler:^(NSError *error) {
                         
                         [ApplicationDelegate removeProgressHUD];
                         
                     }];
                }
            }
            else
            {
                self.restaurantMenuCellObj=restaurantMenuItem;
                
                if ([ApplicationDelegate checkingForGUEST_User_LoggedIn])
                {
                    [self orderItemAsGuest];
                }
                else
                {
                    self.guestUserPopUpView.frame = CGRectMake(0, 0, ApplicationDelegate.window.rootViewController.view.frame.size.width, ApplicationDelegate.window.rootViewController.view.frame.size.height);
                    
                    self.guestUserInnerView.layer.cornerRadius = 2.0;
                    self.guestUserInnerView.layer.masksToBounds = YES;
                    
                    [ApplicationDelegate addViewWithPopUpAnimation:self.guestUserPopUpView InParentView:ApplicationDelegate.window.rootViewController.view];
                }
                
                
            }
        }
        else
        {
            
            //[ApplicationDelegate showAlertWithMessage:@"Please login before make any order" title:nil];
            [ApplicationDelegate showAlertWithMessage:@"Please select the minimum item choices first." title:nil];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, you can't order this item right now. Please try later." title:@""];
        [self.dishes_ScrollView setContentOffset:CGPointZero];
    }
    
}
-(BOOL)isMinCountSelectionMatchingBeforeOrder:(RestaurantMenuCell *)restaurantMenuItem
{
    BOOL minCountMatchingFlag = YES;
    
    for (int i = 0; i<restaurantMenuItem.dishesMenuItemObject.menuChoice_array.count; i++)
    {
        NSMutableDictionary *sectionDic = [[NSMutableDictionary alloc] initWithDictionary:[restaurantMenuItem.dishesMenuItemObject.menuChoice_array objectAtIndex:i]];
        
        long long minCount = 0;
        
        long long selectedCount = 0;
        
        if ([ApplicationDelegate isValid:sectionDic])
        {
            if ([ApplicationDelegate isValid:[sectionDic objectForKey:@"MinItem"]])
            {
                minCount = [[sectionDic objectForKey:@"MinItem"] longLongValue];
            }
            
            NSMutableArray *optionsArrFromServerForItem = [[NSMutableArray alloc] initWithArray:[sectionDic objectForKey:@"MenuItemChoiceDetails"]];
            
            if ([ApplicationDelegate isValid:optionsArrFromServerForItem])
            {
                for (int j=0; j<optionsArrFromServerForItem.count; j++)
                {
                    NSMutableDictionary *optionDic = [[NSMutableDictionary alloc] initWithDictionary:[optionsArrFromServerForItem objectAtIndex:j]];
                    
                    if ([ApplicationDelegate isValid:optionDic])
                    {
                        if (restaurantMenuItem.choicesSelected.count>0)
                        {
                            NSMutableArray *sectionChoicesSelectedArray = [restaurantMenuItem.choicesSelected objectAtIndex:i];
                            if ([sectionChoicesSelectedArray containsObject:optionDic])
                            {
                                selectedCount++;
                            }
                        }
                        
                    }
                }
            }
            
        }
        //NSLog(@"MIN>>>%lld",minCount);
        //NSLog(@"SEL COUNT>>>%lld",selectedCount);
        
        if (selectedCount<minCount)
        {
            minCountMatchingFlag = NO;
            break;
        }
    }
    return minCountMatchingFlag;
}
-(NSMutableDictionary *)getAddToCartPostDataWithUserName:(NSString *)userName AndRestuarantMenuCellObject:(RestaurantMenuCell *)restaurantMenuItem
{
    NSString *itemChoiceIdsString = @"";
    
    NSMutableArray *itemChoiceIDArr = [[NSMutableArray alloc] init];
    
    //NSLog(@"TOTAL CHOICES>>>%@",restaurantMenuItem.choicesSelected);
    
    for (int i=0; i<restaurantMenuItem.choicesSelected.count; i++)
    {
        NSMutableArray *sectionChoiceSelectedArray = [restaurantMenuItem.choicesSelected objectAtIndex:i];
        
        //NSLog(@"SECTION %d CHOICES>>>%@",i,sectionChoiceSelectedArray);
        
        for (int j=0; j<sectionChoiceSelectedArray.count; j++)
        {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[sectionChoiceSelectedArray objectAtIndex:j]];
            
            if ([ApplicationDelegate isValid:dic])
            {
                if ([ApplicationDelegate isValid:[dic objectForKey:@"ItemId"]])
                {
                    [itemChoiceIDArr addObject:[dic objectForKey:@"ItemId"]];
                }
            }
        }
        
    }
    
    if (itemChoiceIDArr.count>0)
    {
        itemChoiceIdsString = [itemChoiceIDArr componentsJoinedByString:@","];
    }
    
    //NSLog(@"ITEM CHOICE IDs>>>%@",itemChoiceIdsString);
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:itemChoiceIdsString forKey:@"ItemChoiceIds"];
    //[postDic setObject:@"" forKey:@"ItemChoiceQty"];
    [postDic setObject:[NSNumber numberWithLongLong:[restaurantMenuItem.dishesMenuItemObject.menuItemId longLongValue]] forKey:@"ItemId"];
    [postDic setObject:[NSNumber numberWithLongLong:[restaurantMenuItem.quantity_TxtBox.text longLongValue]] forKey:@"Quantity"];
    
    [postDic setObject:[NSNumber numberWithLongLong:[ApplicationDelegate.selected_AreaID longLongValue]] forKey:@"AreaId"];
    [postDic setObject:[NSNumber numberWithLongLong:[self.restaurantDetailObj.restaurant_Id longLongValue]] forKey:@"RestaurantId"];
    [postDic setObject:userName forKey:@"Username"];
    
    return postDic;
}
- (IBAction)menuChoicePopUpDismissAction:(id)sender
{
    if ([self.menuChoicePopUpView superview])
    {
        [self.menuChoicePopUpView removeFromSuperview];
    }
}


-(void)orderItemAsGuest
{
//    if ([self isMinCountSelectionMatchingBeforeOrder:self.restaurantMenuCellObj])
//    {
        NSMutableDictionary *postDic = [self getAddToCartPostDataWithUserName:[ApplicationDelegate currentDeviceId] AndRestuarantMenuCellObject:self.restaurantMenuCellObj];
        
        if (postDic.count>0)
        {
            __block BOOL dataFound = NO;
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            [ApplicationDelegate.engine addToTempCartWithDataDictionary:postDic CompletionHandler:^(NSMutableDictionary *responseDictionary)
             {
                 if (!dataFound)
                 {
                     dataFound = YES;
                     
                     if ([ApplicationDelegate isValid:responseDictionary])
                     {
                         NSLog(@"%@",responseDictionary);
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             [ApplicationDelegate showAlertWithMessage:@"An item added to Cart successfully" title:@""];
                             [self viewWillAppear:NO];
                             /*
                             CartListViewController *cartListViewController = [[CartListViewController alloc] initWithNibName:@"CartListViewController" bundle:nil];
                             if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[cartListViewController class]])
                             {
                                 [ApplicationDelegate.subHomeNav pushViewController:cartListViewController animated:NO];
                             }
                             */
                             
                         }
                     }
                     [ApplicationDelegate removeProgressHUD];
                 }
                 
             } errorHandler:^(NSError *error) {
                 
                 [ApplicationDelegate removeProgressHUD];
                 
             }];
        }
  //  }
}


#pragma mark - ADD TO FAVOURITE

-(void)addToFavourite:(RestaurantMenuCell *)restaurantMenuItem
{
    NSMutableDictionary *favData = [[NSMutableDictionary alloc] initWithDictionary:[self setFavPostData:restaurantMenuItem]];
    if (favData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine addToFavouriteWithDataDictionary:favData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]] )
                     {
                         if ([[responseDictionary objectForKey:@"Status"] isEqualToString:[NSString stringWithFormat: @"Success"]])
                         {
                             
                             [ApplicationDelegate showAlertWithMessage:@"Successfully added to favourites" title:nil];
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                     
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
    
}


-(NSMutableDictionary *)setFavPostData:(RestaurantMenuCell *)restaurantMenuItem
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:restaurantMenuItem.dishesMenuItemObject.menuItemId forKey:@"ItemId"];
    
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"UserId"];
    
    
    return postDic;
}



#pragma mark - UITABLEVIEW METHODS

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Code comes here..
        
    }
    
    NSMutableArray *sectionDataArr = [[NSMutableArray alloc] initWithArray:[[choiceArray objectAtIndex:indexPath.section] objectForKey:@"MenuItemChoiceDetails"]];
    
    if ([ApplicationDelegate isValid:sectionDataArr])
    {
        cell.textLabel.text =[NSString stringWithFormat:@"%@",[[sectionDataArr objectAtIndex:indexPath.row] objectForKey:@"Name"]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        
        RestaurantMenuCell *restaurantMenuCell = [self.dishes_ScrollView.subviews objectAtIndex:tableView.tag];
        
        NSMutableArray *currentSectionDataArray = [[NSMutableArray alloc] initWithArray:[[restaurantMenuCell.dishesMenuItemObject.menuChoice_array objectAtIndex:indexPath.section] objectForKey:@"MenuItemChoiceDetails"]];
        
        NSMutableDictionary *selectedChoiceDic = [currentSectionDataArray objectAtIndex:indexPath.row];
        
        UIImageView *accesoryImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        
        if (restaurantMenuCell.choicesSelected.count>indexPath.section)
        {
            NSMutableArray *sectionChoicesSelectedArray = [restaurantMenuCell.choicesSelected objectAtIndex:indexPath.section];
            
            if ([sectionChoicesSelectedArray containsObject:selectedChoiceDic])
            {
                accesoryImg.image=[UIImage imageNamed:@"Tickbox_Selected.png"];
            }
            else
            {
                accesoryImg.image=[UIImage imageNamed:@"Tickbox_Unselected.png"];
            }
            
            cell.accessoryView = accesoryImg;
        }
        
        
        [cell.textLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f]];
        [cell.textLabel setTextColor:[UIColor blackColor]];
        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }
        else
        {
            cell.textLabel.textAlignment = NSTextAlignmentRight;
        }
        
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.frame = [ApplicationDelegate getDynamicAdjustedLabelFrameForLabel:cell.textLabel WithFont:cell.textLabel.font TextContent:cell.textLabel.text];
        [cell.textLabel sizeToFit];
        
    }
    else
    {
        return 0;
    }
    

    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 20.0)];
    sectionHeaderView.backgroundColor = [UIColor redColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 0, sectionHeaderView.frame.size.width, 20.0)];
    
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"Verdana" size:11.0]];
    [sectionHeaderView addSubview:headerLabel];
    
    //headerLabel.text =[NSString stringWithFormat:@"%@",[headerDataArray objectAtIndex:section]];
    
    headerLabel.text = @"YOUR CHOICE OF MEAT ARAYES";
    
    return sectionHeaderView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *sectionDataArr = [[NSMutableArray alloc] initWithArray:[[choiceArray objectAtIndex:section] objectForKey:@"MenuItemChoiceDetails"]];
    if ([ApplicationDelegate isValid:sectionDataArr])
    {
        return [sectionDataArr count];
    }
    else
    {
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // return 40;
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 290, 15)];
    
    NSMutableArray *sectionDataArr = [[NSMutableArray alloc] initWithArray:[[choiceArray objectAtIndex:indexPath.section] objectForKey:@"MenuItemChoiceDetails"]];
    
    NSString *cellTxt;
    if ([ApplicationDelegate isValid:sectionDataArr])
    {
        cellTxt = [NSString stringWithFormat:@"%@",[[sectionDataArr objectAtIndex:indexPath.row] objectForKey:@"Name"]];
    }
    
    
    CGRect textFram = [ApplicationDelegate getDynamicAdjustedLabelFrameForLabel:lbl WithFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f] TextContent:cellTxt];
    
    if (textFram.size.height>0)
    {
        return textFram.size.height + 30;
    }
    else
    {
        return 40.0f;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return choiceArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RestaurantMenuCell *restaurantMenuCell = [self.dishes_ScrollView.subviews objectAtIndex:tableView.tag];
    
    NSMutableArray *currentSectionDataArray = [[NSMutableArray alloc] initWithArray:[[restaurantMenuCell.dishesMenuItemObject.menuChoice_array objectAtIndex:indexPath.section] objectForKey:@"MenuItemChoiceDetails"]];
    
    NSMutableDictionary *selectedChoiceDic = [currentSectionDataArray objectAtIndex:indexPath.row];
    
    UITableViewCell *myCell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *accesoryImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    BOOL isMultipleSelectionAllowed = NO;
    
    if ([ApplicationDelegate isValid:[selectedChoiceDic objectForKey:@"MaxItem"]])
    {
        if ([[selectedChoiceDic objectForKey:@"MaxItem"] isEqualToString:@"1"])
        {
            isMultipleSelectionAllowed = YES;
        }
        
    }
    if (restaurantMenuCell.choicesSelected.count>indexPath.section)
    {
        NSMutableArray *sectionChoicesSelectedArray = [restaurantMenuCell.choicesSelected objectAtIndex:indexPath.section];
        
        if (isMultipleSelectionAllowed)
        {
            // MULTIPLE SELECTION ALLOWED == 1
            
            if ([sectionChoicesSelectedArray containsObject:selectedChoiceDic])
            {
                accesoryImg.image=[UIImage imageNamed:@"Tickbox_Unselected.png"];
                [sectionChoicesSelectedArray removeObject:selectedChoiceDic];
            }
            else
            {
                accesoryImg.image=[UIImage imageNamed:@"Tickbox_Selected.png"];
                [sectionChoicesSelectedArray addObject:selectedChoiceDic];
            }
            [restaurantMenuCell.choicesSelected replaceObjectAtIndex:indexPath.section withObject:sectionChoicesSelectedArray];
            
            myCell.accessoryView = accesoryImg;
            
        }
        else
        {
            // SINGLE SELECTION ONLY ALLOWED
            
            if ([sectionChoicesSelectedArray containsObject:selectedChoiceDic])
            {
                [sectionChoicesSelectedArray removeAllObjects];
                accesoryImg.image=[UIImage imageNamed:@"Tickbox_Unselected.png"];
            }
            else
            {
                [sectionChoicesSelectedArray removeAllObjects];
                accesoryImg.image=[UIImage imageNamed:@"Tickbox_Selected.png"];
                [sectionChoicesSelectedArray addObject:selectedChoiceDic];
            }
            
            [restaurantMenuCell.choicesSelected replaceObjectAtIndex:indexPath.section withObject:sectionChoicesSelectedArray];
            
            myCell.accessoryView = accesoryImg;
            
            [self.menuChoicePopUpTableView reloadData];
            
        }
    }
    
    
    //[restaurantMenuCell needsUpdateConstraints];
}
#pragma mark - GuestPopUp View METHODS


- (IBAction)guestPopUpViewDismissAction:(UIButton *)sender {
    if ([self.guestUserPopUpView superview])
    {
        [self.guestUserPopUpView removeFromSuperview];
    }
}

- (IBAction)continueAsGuestBttnAction:(id)sender {
    
    if ([self.guestUserPopUpView superview])
    {
        [self.guestUserPopUpView removeFromSuperview];
    }
    [ApplicationDelegate loginGuestUser];
    [self orderItemAsGuest];
    
}

- (IBAction)loginBttnAction:(UIButton *)sender {
    
    if ([self.guestUserPopUpView superview])
    {
        [self.guestUserPopUpView removeFromSuperview];
    }
    LoginViewController *loginVc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    loginVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[loginVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:loginVc animated:NO];
    }

}

#pragma mark - TEXT FIELD DELEGATES

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[self.accountInfoScrollView setContentOffset:CGPointZero animated:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.restaurant_Menu_ScrollView setContentOffset:CGPointZero animated:NO];
    [self.restaurant_Menu_ScrollView setContentOffset:CGPointMake(self.restaurant_Menu_ScrollView.contentOffset.x, (self.restaurant_Menu_ScrollView.contentSize.height - self.restaurant_Menu_ScrollView.bounds.size.height)) animated:NO];
    
    [self.dishes_ScrollView setContentOffset:CGPointZero animated:NO];
    CGFloat yFact;
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        yFact = 45;
    }
    else
    {
        yFact = 130;
    }
    
    if ((textField.superview.frame.origin.y+textField.frame.origin.y+textField.superview.superview.frame.origin.y)>(yFact))
    {
        [self.dishes_ScrollView setContentOffset:CGPointMake(self.dishes_ScrollView.contentOffset.x, (textField.superview.frame.origin.y+textField.frame.origin.y+textField.superview.superview.frame.origin.y-yFact))];
    }
    else
    {
        [self.dishes_ScrollView setContentOffset:CGPointZero animated:YES];
    }
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}

-(void)xibLoading
{
    NSString *nibName = @"RestaurantMenuDetailVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end

