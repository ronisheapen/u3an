//
//  MostSellingDishesVC.m
//  U3AN
//
//  Created by Vipin on 31/12/14.
//  Copyright (c) 2014 Mawaqaa. All rights reserved.
//

#import "MostSellingDishesVC.h"
#import "Dishes.h"
#import "Restaurant.h"
#import "Cuisine.h"
#import "CuisineCell.h"
#import "RestaurantDetailsVC.h"
#import "MostSellingDishesByRestaurantVC.h"
#import "MostSellingDishesByCuisinesVC.h"

@interface MostSellingDishesVC ()

@end

@implementation MostSellingDishesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self loadMostsellingScrollView];
   
}

-(void)viewWillDisappear:(BOOL)animated
{
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    if ([[HomeTabViewController sharedViewController].mostSellingDropDownObj.view superview])
    {
        [[HomeTabViewController sharedViewController].mostSellingDropDownObj.view removeFromSuperview];
    }
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=NO;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadMostsellingScrollView
{
    if ([[HomeTabViewController sharedViewController].mostSellingBttnLabel.text isEqualToString:@"Most Selling"])
    {
        [self getMostSellingDishesList];
    }
    else if ([[HomeTabViewController sharedViewController].mostSellingBttnLabel.text isEqualToString:@"By Restaurant"])
    {
        [self getMostSellingRestaurantList];
    }
    else if ([[HomeTabViewController sharedViewController].mostSellingBttnLabel.text isEqualToString:@"By Cuisine"])
    {
        [self getMostSellingCuisineList];
    }
    
}

-(void)getMostSellingDishesList
{
    NSMutableArray *dishesListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *dishesData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
    for (UIView *sub in self.mostSellingScrollView.subviews) {
        [sub removeFromSuperview];
    }
    if (dishesData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getMostSellingDishesWithDataDictionary:dishesData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Dishes *dishesItem = [ApplicationDelegate.mapper getMostSellingDishesListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             if (!([dishesItem.restaurent_Status caseInsensitiveCompare:@"hidden"]==NSOrderedSame))
                             {
                                 
                                 [dishesListArray addObject:dishesItem];
                             }
                             
                         }
                     }
                     
                 }
             }
             if (dishesListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareMostSellingDishesListScroll_Grid_WithArray:dishesListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(void)getMostSellingRestaurantList
{
    NSMutableArray *restaurantListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
    for (UIView *sub in self.mostSellingScrollView.subviews) {
        [sub removeFromSuperview];
    }
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getMostSellingRestaurantWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Restaurant *restaurantItem = [ApplicationDelegate.mapper getRestaurantListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             if (!([restaurantItem.restaurant_Status caseInsensitiveCompare:@"hidden"]==NSOrderedSame))
                             {
                                 
                                 [restaurantListArray addObject:restaurantItem];
                             }
                         }
                     }
                     
                 }
             }
             if (restaurantListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareMostSellingRestaurantListScroll_Grid_WithArray:restaurantListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(void)getMostSellingCuisineList
{
    NSMutableArray *cuisineListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *cuisineData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
    for (UIView *sub in self.mostSellingScrollView.subviews) {
        [sub removeFromSuperview];
    }
    if (cuisineData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCuisinesWithDataDictionary:cuisineData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Cuisine *cuisineItem = [ApplicationDelegate.mapper getMostSellingCusineListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [cuisineListArray addObject:cuisineItem];
                         }
                     }
                     
                 }
             }
             if (cuisineListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareMostSellingCuisineListScroll_Grid_WithArray:cuisineListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getDishesPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}
-(void)prepareMostSellingDishesListScroll_Grid_WithArray:(NSMutableArray *)dishesListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.mostSellingScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.mostSellingScrollView.backgroundColor = [UIColor clearColor];
    
    [self.mostSellingScrollView setContentOffset:CGPointZero];

    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    MostSellingDishCell *vw = [[MostSellingDishCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.mostSellingScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<dishesListArray.count; i++)
    {
        int colIndex = i%colCount;
        MostSellingDishCell *dishesCell = [[MostSellingDishCell alloc] init];
        
        dishesCell.layer.cornerRadius = 2.0;
        dishesCell.layer.masksToBounds = YES;
        dishesCell.mostSellingCell_Img.layer.cornerRadius = 2.0;
        dishesCell.mostSellingCell_Img.layer.masksToBounds = YES;
        Dishes *dishesItem = [dishesListArray objectAtIndex:i];
        dishesCell.dishesObj=dishesItem;
        dishesCell.restaurantStatus_Label.text=dishesItem.restaurent_Status;
        dishesCell.delegateDishesItem=self;
        [dishesCell.restaurantStatus_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:13.0f]];
        if (![dishesItem.restaurent_Status isEqualToString:[NSString stringWithFormat: @"OPEN"]])
        {
            dishesCell.restaurantStatus_Label.textColor=[UIColor redColor];
        }
        else
        {
            // restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
        }
        dishesCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  dishesCell.frame.size.width, dishesCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:dishesItem.dish_Thumbnail] completionHandler:^(UIImage *responseImage) {
            dishesCell.mostSellingCell_Img.image = responseImage;
            [dishesCell.mostSellingCell_Img.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        dishesCell.restaurantName_Label.text=dishesItem.restaurant_Name;
       // NSLog(@"Font families: %@", [UIFont fontNamesForFamilyName:@"Tahoma"]);
        [dishesCell.restaurantName_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        dishesCell.dishName_Label.text=dishesItem.dish_Name;
        [dishesCell.dishName_Label setFont:[UIFont fontWithName:@"Tahoma" size:10.0f]];
        [ApplicationDelegate loadMarqueeLabelWithText:dishesCell.dishName_Label.text Font:dishesCell.dishName_Label.font InPlaceOfLabel:dishesCell.dishName_Label];
        [self.mostSellingScrollView addSubview:dishesCell];
        if (colIndex == (colCount - 1))
        {
            yVal = dishesCell.frame.origin.y + dishesCell.frame.size.height;
        }
        
        scrollHeight = dishesCell.frame.origin.y + dishesCell.frame.size.height;
    }
    
    self.mostSellingScrollView.contentSize = CGSizeMake(self.mostSellingScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
}

-(void)prepareMostSellingRestaurantListScroll_Grid_WithArray:(NSMutableArray *)restaurantListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.mostSellingScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.mostSellingScrollView.backgroundColor = [UIColor clearColor];
    
    [self.mostSellingScrollView setContentOffset:CGPointZero];
    
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    RestaurantCell *vw = [[RestaurantCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.mostSellingScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<restaurantListArray.count; i++)
    {
        int colIndex = i%colCount;
        RestaurantCell *restaurantCell = [[RestaurantCell alloc] init];
        restaurantCell.layer.cornerRadius = 2.0;
        restaurantCell.layer.masksToBounds = YES;
        restaurantCell.restaurant_ImageView.layer.cornerRadius = 2.0;
        restaurantCell.restaurant_ImageView.layer.masksToBounds = YES;
        restaurantCell.delegateRestaurantItem=self;
        Restaurant *restaurantItem = [restaurantListArray objectAtIndex:i];
        restaurantCell.restaurantObj=restaurantItem;
        restaurantCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  restaurantCell.frame.size.width, restaurantCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:restaurantItem.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            
            restaurantCell.restaurant_ImageView.image = responseImage;
            [restaurantCell.restaurant_ImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        restaurantCell.restaurantName_Label.text=restaurantItem.restaurant_Name;
        [restaurantCell.restaurantName_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        restaurantCell.restaurantStatusLabel.text=restaurantItem.restaurant_Status;
        [restaurantCell.restaurantStatusLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:13.0f]];
        if (![restaurantItem.restaurant_Status isEqualToString:[NSString stringWithFormat: @"OPEN"]])
        {
            restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
        }
        else
        {
            // restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
        }

       // restaurantCell.dishName_Label.text=dishesItem.dish_Name;
        [self.mostSellingScrollView addSubview:restaurantCell];
        if (colIndex == (colCount - 1))
        {
            yVal = restaurantCell.frame.origin.y + restaurantCell.frame.size.height;
        }
        
        scrollHeight = restaurantCell.frame.origin.y + restaurantCell.frame.size.height;
    }
    
    self.mostSellingScrollView.contentSize = CGSizeMake(self.mostSellingScrollView.frame.size.width,  (scrollHeight + offsetValue));
    

}

-(void)prepareMostSellingCuisineListScroll_Grid_WithArray:(NSMutableArray *)cuisineListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.mostSellingScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.mostSellingScrollView.backgroundColor = [UIColor clearColor];
    
    [self.mostSellingScrollView setContentOffset:CGPointZero];
    
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    CuisineCell *vw = [[CuisineCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.mostSellingScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<cuisineListArray.count; i++)
    {
        int colIndex = i%colCount;
        CuisineCell *cuisineCell = [[CuisineCell alloc] init];
        
        cuisineCell.layer.cornerRadius = 2.0;
        cuisineCell.layer.masksToBounds = YES;
        cuisineCell.cuisineCellImageView.layer.cornerRadius = 2.0;
        cuisineCell.cuisineCellImageView.layer.masksToBounds = YES;
        cuisineCell.delegateCuisineItem=self;
        Cuisine *cuisineItem = [cuisineListArray objectAtIndex:i];
        cuisineCell.cuisineObj=cuisineItem;
        cuisineCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  cuisineCell.frame.size.width, cuisineCell.frame.size.height);
        
//        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:restaurantItem.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
//            
//            restaurantCell.restaurant_ImageView.image = responseImage;
//            [restaurantCell.restaurant_ImageView.layer needsLayout];
//            
//        } errorHandler:^(NSError *error) {
//            
//        }];
        cuisineCell.cuisineTitleLabel.text=cuisineItem.cuisine_Name;
        [cuisineCell.cuisineTitleLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        // restaurantCell.dishName_Label.text=dishesItem.dish_Name;
        [self.mostSellingScrollView addSubview:cuisineCell];
        if (colIndex == (colCount - 1))
        {
            yVal = cuisineCell.frame.origin.y + cuisineCell.frame.size.height;
        }
        
        scrollHeight = cuisineCell.frame.origin.y + cuisineCell.frame.size.height;
    }
    
    self.mostSellingScrollView.contentSize = CGSizeMake(self.mostSellingScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
    

}

#pragma mark -Restaurant Delegate Method

-(void) restaurantItemDidClicked:(RestaurantCell *)restaurantCategoryItem
{
    [HomeTabViewController sharedViewController].mostSellingDropDownBttn.selected=NO;

    if ([HomeTabViewController sharedViewController].mostSellingDropDownObj.view.superview) {
        [[HomeTabViewController sharedViewController].mostSellingDropDownObj.view removeFromSuperview];
    }
    if ([restaurantCategoryItem.restaurantStatusLabel.text isEqualToString:[NSString stringWithFormat: @"OPEN"]]) {
        
        
        MostSellingDishesByRestaurantVC *mostSellingDishesByResVC = [[MostSellingDishesByRestaurantVC alloc] initWithNibName:@"MostSellingDishesByRestaurantVC" bundle:nil]
        ;
        mostSellingDishesByResVC.view.backgroundColor = [UIColor clearColor];
        mostSellingDishesByResVC.restaurantObj=restaurantCategoryItem.restaurantObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[mostSellingDishesByResVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:mostSellingDishesByResVC animated:NO];
        }
    }
}

-(void) dishesItemDidClicked:(MostSellingDishCell *)dishesCategoryItem
{
    [HomeTabViewController sharedViewController].mostSellingDropDownBttn.selected=NO;
    
    if ([HomeTabViewController sharedViewController].mostSellingDropDownObj.view.superview) {
        [[HomeTabViewController sharedViewController].mostSellingDropDownObj.view removeFromSuperview];
    }
    if ([dishesCategoryItem.restaurantStatus_Label.text isEqualToString:[NSString stringWithFormat: @"OPEN"]]) {
        
        RestaurantDetailsVC *restaurantDetailVC = [[RestaurantDetailsVC alloc] initWithNibName:@"RestaurantDetailsVC" bundle:nil]
        ;
        restaurantDetailVC.view.backgroundColor = [UIColor clearColor];
        restaurantDetailVC.restaurantObj=nil;
        restaurantDetailVC.dishesObj=dishesCategoryItem.dishesObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantDetailVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantDetailVC animated:NO];
        }
    }
}

-(void) cuisineItemDidClicked:(CuisineCell *)cuisineCategoryItem
{
    [HomeTabViewController sharedViewController].mostSellingDropDownBttn.selected=NO;
    
    if ([HomeTabViewController sharedViewController].mostSellingDropDownObj.view.superview) {
        [[HomeTabViewController sharedViewController].mostSellingDropDownObj.view removeFromSuperview];
    }
    MostSellingDishesByCuisinesVC *mostSellingDishesByCuisineVC = [[MostSellingDishesByCuisinesVC alloc] initWithNibName:@"MostSellingDishesByCuisinesVC" bundle:nil]
    ;
    mostSellingDishesByCuisineVC.view.backgroundColor = [UIColor clearColor];
   // mostSellingDishesByCuisineVC.restaurantObj=nil;
    mostSellingDishesByCuisineVC.cuisineObj=cuisineCategoryItem.cuisineObj;
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[mostSellingDishesByCuisineVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:mostSellingDishesByCuisineVC animated:NO];
    }

}

@end
