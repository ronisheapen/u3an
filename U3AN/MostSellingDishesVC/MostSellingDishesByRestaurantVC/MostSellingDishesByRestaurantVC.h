//
//  MostSellingDishesByRestaurantVC.h
//  U3AN
//
//  Created by Vipin on 29/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MostSellingDishByResCell.h"
#import "Restaurant.h"

@interface MostSellingDishesByRestaurantVC : UIViewController<dishesByResItemDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mostSellingDishesByRes_ScrollView;
@property (strong, nonatomic)Restaurant *restaurantObj;
@end
