//
//  MostSellingDishesByRestaurantVC.m
//  U3AN
//
//  Created by Vipin on 29/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MostSellingDishesByRestaurantVC.h"
#import "RestaurantDetailsVC.h"

@interface MostSellingDishesByRestaurantVC ()

@end

@implementation MostSellingDishesByRestaurantVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self getMostSellingDishesByRestaurantList];
    
}


-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getMostSellingDishesByRestaurantList
{
    NSMutableArray *dishesListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *dishesData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
    for (UIView *sub in self.mostSellingDishesByRes_ScrollView.subviews) {
        [sub removeFromSuperview];
    }
    if (dishesData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getMostSellingDishesByRestaurantListWithDataDictionary:dishesData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Dishes *dishesItem = [ApplicationDelegate.mapper getMostSellingDishesListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [dishesListArray addObject:dishesItem];
                         }
                     }
                     
                 }
             }
             if (dishesListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareMostSellingDishesByRestaurantListScroll_Grid_WithArray:dishesListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getDishesPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.restaurantObj.restaurant_Id forKey:@"RestaurantId"];
    
    return postDic;
}

-(void)prepareMostSellingDishesByRestaurantListScroll_Grid_WithArray:(NSMutableArray *)dishesListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.mostSellingDishesByRes_ScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.mostSellingDishesByRes_ScrollView.backgroundColor = [UIColor clearColor];
    
    [self.mostSellingDishesByRes_ScrollView setContentOffset:CGPointZero];
    
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    MostSellingDishByResCell *vw = [[MostSellingDishByResCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.mostSellingDishesByRes_ScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<dishesListArray.count; i++)
    {
        int colIndex = i%colCount;
        MostSellingDishByResCell *dishesCell = [[MostSellingDishByResCell alloc] init];
        
        dishesCell.layer.cornerRadius = 2.0;
        dishesCell.layer.masksToBounds = YES;
        dishesCell.mostSellingDish_ImageView.layer.cornerRadius = 2.0;
        dishesCell.mostSellingDish_ImageView.layer.masksToBounds = YES;
        Dishes *dishesItem = [dishesListArray objectAtIndex:i];
        dishesCell.dishesObj=dishesItem;
        dishesCell.restaurantStatus_Label.text=dishesItem.restaurent_Status;
        dishesCell.delegateDishesByResItem=self;
        [dishesCell.restaurantStatus_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:13.0f]];
        if (![dishesItem.restaurent_Status isEqualToString:[NSString stringWithFormat: @"OPEN"]])
        {
            dishesCell.restaurantStatus_Label.textColor=[UIColor redColor];
        }
        else
        {
            // restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
        }
        dishesCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  dishesCell.frame.size.width, dishesCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:dishesItem.dish_Thumbnail] completionHandler:^(UIImage *responseImage) {
            dishesCell.mostSellingDish_ImageView.image = responseImage;
            [dishesCell.mostSellingDish_ImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
           dishesCell.mostSellingDish_ImageView.image =[UIImage imageNamed:@"Default_Food.png"];
            
        }];
        dishesCell.restaurantName_Label.text=dishesItem.restaurant_Name;
        // NSLog(@"Font families: %@", [UIFont fontNamesForFamilyName:@"Tahoma"]);
        [dishesCell.restaurantName_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        dishesCell.dishName_Label.text=dishesItem.dish_Name;
        [dishesCell.dishName_Label setFont:[UIFont fontWithName:@"Tahoma" size:10.0f]];
        [ApplicationDelegate loadMarqueeLabelWithText:dishesCell.dishName_Label.text Font:dishesCell.dishName_Label.font InPlaceOfLabel:dishesCell.dishName_Label];

        [self.mostSellingDishesByRes_ScrollView addSubview:dishesCell];
        if (colIndex == (colCount - 1))
        {
            yVal = dishesCell.frame.origin.y + dishesCell.frame.size.height;
        }
        
        scrollHeight = dishesCell.frame.origin.y + dishesCell.frame.size.height;
    }
    
    self.mostSellingDishesByRes_ScrollView.contentSize = CGSizeMake(self.mostSellingDishesByRes_ScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
}

-(void)dishesByResItemDidClicked:(MostSellingDishByResCell *)dishesByResCategoryItem
{
    if ([dishesByResCategoryItem.restaurantStatus_Label.text isEqualToString:[NSString stringWithFormat: @"OPEN"]]) {
        
        RestaurantDetailsVC *restaurantDetailVC = [[RestaurantDetailsVC alloc] initWithNibName:@"RestaurantDetailsVC" bundle:nil]
        ;
        restaurantDetailVC.view.backgroundColor = [UIColor clearColor];
        restaurantDetailVC.restaurantObj=nil;
        restaurantDetailVC.dishesObj=dishesByResCategoryItem.dishesObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantDetailVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantDetailVC animated:NO];
        }
    }

}

@end
