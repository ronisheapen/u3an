//
//  MostSellingDishCell.h
//  U3AN
//
//  Created by Vipin on 02/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dishes.h"

@protocol dishesItemDelegate;
@interface MostSellingDishCell : UIView
@property(weak,nonatomic)id<dishesItemDelegate> delegateDishesItem;
@property (weak, nonatomic) IBOutlet UIImageView *mostSellingCell_Img;
@property (weak, nonatomic) IBOutlet UILabel *restaurantName_Label;
@property (strong, nonatomic)Dishes *dishesObj;
@property (weak, nonatomic) IBOutlet UILabel *dishName_Label;
@property (weak, nonatomic) IBOutlet UILabel *restaurantStatus_Label;

@end

@protocol dishesItemDelegate <NSObject>
- (void) dishesItemDidClicked:(MostSellingDishCell *) dishesCategoryItem;
@end