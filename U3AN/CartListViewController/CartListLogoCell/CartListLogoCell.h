//
//  CartListLogoCell.h
//  U3AN
//
//  Created by Vipin on 05/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartListLogoCell : UIView
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *minimumOrderLabel;

@end
