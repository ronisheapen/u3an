//
//  CartListLogoCell.m
//  U3AN
//
//  Created by Vipin on 05/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "CartListLogoCell.h"

@implementation CartListLogoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"CartListLogoCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (CartListLogoCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

@end
