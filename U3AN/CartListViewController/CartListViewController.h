//
//  CartListViewController.h
//  U3AN
//
//  Created by Vipin on 05/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartListItemCell.h"
#import "CartInformation.h"

@interface CartListViewController : UIViewController<cartListItemDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *foodCartTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *grandTotalTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *grandTotalValueLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *cartListScrollView;
@property (strong, nonatomic) IBOutlet UIView *cartListMainView;
@property (strong, nonatomic)CartInformation *cartInfoItem;
@property (strong, nonatomic) IBOutlet UIButton *next_Bttn;

- (IBAction)placeOrderButtonAction:(UIButton *)sender;
- (IBAction)nextButtonAction:(id)sender;
@end
