 //
//  CartListViewController.m
//  U3AN
//
//  Created by Vipin on 05/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "CartListViewController.h"
#import <CoreText/CoreText.h>
#import "CartListLogoCell.h"
#import "OrderConfirmationVCViewController.h"

@interface CartListViewController ()
{
    BOOL isOrderAmountMatchingWithMinimumAmount;
}
@end

@implementation CartListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
    
    self.cartListMainView.layer.borderColor = [UIColor redColor].CGColor;
    self.cartListMainView.layer.borderWidth = 2;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];

    isOrderAmountMatchingWithMinimumAmount = YES;
    
    self.next_Bttn.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];

    self.foodCartTitleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f];
    self.grandTotalTitleLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.grandTotalValueLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.cartListScrollView.contentOffset = CGPointZero;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:@"fdf" forKey:@"sdd"];
    
    self.cartListMainView.layer.cornerRadius = 3.0;
    self.cartListMainView.layer.masksToBounds = YES;
    if (ApplicationDelegate.isLoggedIn) {
        [self getCartInfoList];
    }
    else
    {
        [self getTempCartInfoList];
    }
    
    
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}
-(void)prepareCartListScrollWithListArray:(NSMutableArray *)listArray
{
    [self.cartListScrollView setContentOffset:CGPointZero animated:NO];
    
    for (UIView *vw in self.cartListScrollView.subviews)
    {
        [vw removeFromSuperview];
    }
    isOrderAmountMatchingWithMinimumAmount = YES;
    
    NSString *orderCountIssuedRestaurantName = @"";
    NSString *orderCountIssuedRestaurantMinAmount = @"";
    
    CGFloat yOffset = 0.0f;
    //CGFloat total_Order_Amount = 0.0f;
    
    CGFloat grandTotalAmount = 0.0f;
    
    if ([ApplicationDelegate isValid:listArray])
    {
        for (int i=0; i<listArray.count; i++)
        {
           // NSMutableDictionary *singleOrderDataDic = [[NSMutableDictionary alloc] initWithDictionary:[listArray objectAtIndex:i]];
            
            CGFloat restaurantOrderAmount = 0.0f;
            
            RestaurantCart *restCartObj = [listArray objectAtIndex:i];
            
            if (restCartObj.restaurant_Id.length>0)
            {
                CartListLogoCell *logoCell = [[CartListLogoCell alloc] init];
                logoCell.frame = CGRectMake(0, (yOffset + 5), self.cartListScrollView.frame.size.width, logoCell.frame.size.height);
                logoCell.tag = i;
                
                if (restCartObj.restaurant_Thumbnail.length>0)
                {
                    [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:restCartObj.restaurant_Thumbnail] completionHandler:^(UIImage *responseImage) {
                        
                        if ([ApplicationDelegate isValid:responseImage])
                        {
                            [ApplicationDelegate loadImageWithAnimationInImageView:logoCell.logoImageView withImage:responseImage];
                        }
                        
                    } errorHandler:^(NSError *error) {
                        
                    }];
                }
                logoCell.minimumOrderLabel.text = [NSString stringWithFormat:@"Minimum Order Amount: KD %.2f",[restCartObj.minimum_Amount floatValue]];
                logoCell.minimumOrderLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                [self.cartListScrollView addSubview:logoCell];
                
                yOffset = logoCell.frame.origin.y + logoCell.frame.size.height;
                
                UIView *listContainerOuterView  = [[UIView alloc] initWithFrame:CGRectMake(0, (yOffset+5), self.cartListScrollView.frame.size.width, ((restCartObj.cart_InfoArray.count*100)+35+2))];
                listContainerOuterView.backgroundColor = [UIColor colorWithRed:(119.0/255.0) green:(119/255.0) blue:(119.0/255.0) alpha:1.0];
                
                UIView *listContainerInnerView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, (listContainerOuterView.frame.size.width-2), (listContainerOuterView.frame.size.height-2))];
                
                listContainerInnerView.backgroundColor = [UIColor colorWithRed:(230.0/255.0) green:(230/255.0) blue:(230.0/255.0) alpha:1.0];
                
                listContainerOuterView.layer.cornerRadius = 2.0;
                listContainerOuterView.layer.masksToBounds = YES;
                
                listContainerInnerView.layer.cornerRadius = 2.0;
                listContainerInnerView.layer.masksToBounds = YES;
                
                [listContainerOuterView addSubview:listContainerInnerView];
                
                CGFloat listY_Fact = 0.0f;
              //  CGFloat order_Amount=0.0f;
                
                for (int j=0; j<restCartObj.cart_InfoArray.count; j++)
                {
                    CartDetails *cartDetailItem = [ApplicationDelegate.mapper getCartDetailFromDictionary:[restCartObj.cart_InfoArray objectAtIndex:j]];
                    
                    CartListItemCell *listCell = [[CartListItemCell alloc] init];
                    listCell.cartDetailObj=cartDetailItem;
                    listCell.frame = CGRectMake(0, listY_Fact, listContainerInnerView.frame.size.width, listCell.frame.size.height);
                    listCell.delegateCartListItem=self;
                  //  listCell.orderItemLabel.text=cartDetailItem.item_Name;
                  // listCell.orderItemLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                    NSMutableAttributedString *itemNameString = [[NSMutableAttributedString alloc] init];

                    [itemNameString appendAttributedString:[self convertIntoRedFont:[NSString stringWithFormat:@"%@ ",cartDetailItem.quantity]]];
                    [itemNameString appendAttributedString:[self convertIntoGrayFont:cartDetailItem.item_Name]];
                    [listCell.orderItemLabel setAttributedText:itemNameString];
                    
                    listCell.orderItemPriceLabel.text=cartDetailItem.item_Price;
                    
                    restaurantOrderAmount = ([cartDetailItem.item_Price floatValue]*[cartDetailItem.quantity longLongValue])+restaurantOrderAmount;
                    
                  //  order_Amount = [cartDetailItem.item_Price floatValue]+order_Amount;
                    
                    listCell.orderItemPriceLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
                    if (cartDetailItem.item_Thumbnail.length>0)
                    {
                        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:cartDetailItem.item_Thumbnail] completionHandler:^(UIImage *responseImage) {
                            
                            if ([ApplicationDelegate isValid:responseImage])
                            {
                                [ApplicationDelegate loadImageWithAnimationInImageView:listCell.cuisineImageView withImage:responseImage];
                            }
                            
                        } errorHandler:^(NSError *error) {
                            
                        }];
                    }
                    listCell.tag = (i*1000)+j;
                    [listContainerInnerView addSubview:listCell];
                    
                    listY_Fact = ((j+1)*listCell.frame.size.height);
                }
                
             //   total_Order_Amount=total_Order_Amount+order_Amount;
                UILabel *yourOrderAmountTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, listY_Fact + 3, 140, 25)];
                yourOrderAmountTitleLabel.backgroundColor = [UIColor clearColor];
                yourOrderAmountTitleLabel.textColor = [UIColor redColor];
                yourOrderAmountTitleLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
                yourOrderAmountTitleLabel.text = [NSString stringWithFormat:@"Your Order Amount:"];
                [listContainerInnerView addSubview:yourOrderAmountTitleLabel];
                
                UILabel *yourOrderAmountValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, listY_Fact + 3, (listContainerInnerView.frame.size.width - 170), 25)];
                yourOrderAmountValueLabel.backgroundColor = [UIColor clearColor];
                yourOrderAmountValueLabel.textColor = [UIColor redColor];
                yourOrderAmountValueLabel.text =[NSString stringWithFormat:@"KD %.2f",restaurantOrderAmount];
                yourOrderAmountValueLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
                yourOrderAmountValueLabel.textAlignment = NSTextAlignmentRight;
                [listContainerInnerView addSubview:yourOrderAmountValueLabel];
                
                if (isOrderAmountMatchingWithMinimumAmount)
                {
                    if ([restCartObj.restaurant_Total floatValue]<[restCartObj.minimum_Amount floatValue])
                    {
                        isOrderAmountMatchingWithMinimumAmount = NO;
                        orderCountIssuedRestaurantName = restCartObj.restaurant_Name;
                        orderCountIssuedRestaurantMinAmount = restCartObj.minimum_Amount;
                    }
                }
                
                
                [self.cartListScrollView addSubview:listContainerOuterView];
                
                yOffset = listContainerOuterView.frame.origin.y + listContainerOuterView.frame.size.height;
            }
            
            grandTotalAmount = grandTotalAmount + restaurantOrderAmount;
            
//            if ([ApplicationDelegate isValid:singleOrderDataDic])
//            {
//                if (singleOrderDataDic.count>0)
//                {
//                    CartListLogoCell *logoCell = [[CartListLogoCell alloc] init];
//                    logoCell.frame = CGRectMake(0, (yOffset + 5), self.cartListScrollView.frame.size.width, logoCell.frame.size.height);
//                    logoCell.tag = i;
//                    [self.cartListScrollView addSubview:logoCell];
//                    
//                    yOffset = logoCell.frame.origin.y + logoCell.frame.size.height;
//                    
//                    UIView *listContainerOuterView  = [[UIView alloc] initWithFrame:CGRectMake(0, (yOffset+5), self.cartListScrollView.frame.size.width, (300+35+2))];
//                    listContainerOuterView.backgroundColor = [UIColor colorWithRed:(119.0/255.0) green:(119/255.0) blue:(119.0/255.0) alpha:1.0];
//                    
//                    UIView *listContainerInnerView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, (listContainerOuterView.frame.size.width-2), (listContainerOuterView.frame.size.height-2))];
//                    
//                    listContainerInnerView.backgroundColor = [UIColor colorWithRed:(230.0/255.0) green:(230/255.0) blue:(230.0/255.0) alpha:1.0];
//                    
//                    listContainerOuterView.layer.cornerRadius = 2.0;
//                    listContainerOuterView.layer.masksToBounds = YES;
//                    
//                    listContainerInnerView.layer.cornerRadius = 2.0;
//                    listContainerInnerView.layer.masksToBounds = YES;
//                    
//                    [listContainerOuterView addSubview:listContainerInnerView];
//                    
//                    CGFloat listY_Fact = 0.0f;
//                    
//                    for (int j=0; j<4; j++)
//                    {
//                        CartListItemCell *listCell = [[CartListItemCell alloc] init];
//                        listCell.frame = CGRectMake(0, listY_Fact, listContainerInnerView.frame.size.width, listCell.frame.size.height);
//                        listCell.orderItemLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
//                        listCell.orderItemPriceLabel.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
//                        listCell.tag = (i*1000)+j;
//                        [listContainerInnerView addSubview:listCell];
//                        
//                        listY_Fact = (j*listCell.frame.size.height);
//                    }
//                    
//                    UILabel *yourOrderAmountTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, listY_Fact + 7, 140, 25)];
//                    yourOrderAmountTitleLabel.backgroundColor = [UIColor clearColor];
//                    yourOrderAmountTitleLabel.textColor = [UIColor redColor];
//                    yourOrderAmountTitleLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
//                    yourOrderAmountTitleLabel.text = @"Your Order Amount:";
//                    [listContainerInnerView addSubview:yourOrderAmountTitleLabel];
//                    
//                    UILabel *yourOrderAmountValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, listY_Fact + 7, (listContainerInnerView.frame.size.width - 170), 25)];
//                    yourOrderAmountValueLabel.backgroundColor = [UIColor clearColor];
//                    yourOrderAmountValueLabel.textColor = [UIColor redColor];
//                    yourOrderAmountValueLabel.text = @"KD 2.000";
//                    yourOrderAmountValueLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
//                    yourOrderAmountValueLabel.textAlignment = NSTextAlignmentRight;
//                    [listContainerInnerView addSubview:yourOrderAmountValueLabel];
//                    
//                    [self.cartListScrollView addSubview:listContainerOuterView];
//                   
//                    yOffset = listContainerOuterView.frame.origin.y + listContainerOuterView.frame.size.height;
//                    BREESERIF-REGULAR
//                }
//            }
            

        }
    }
    if (isOrderAmountMatchingWithMinimumAmount)
    {
        self.next_Bttn.enabled = YES;
    }
    else
    {
        self.next_Bttn.enabled = NO;
        
        if ((orderCountIssuedRestaurantName.length>0)&&(orderCountIssuedRestaurantMinAmount.length>0))
        {
            UIAlertView *minOrderAlert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Minimum Order Amount for %@ is KD %.2f. Please place more order to continue shopping.",orderCountIssuedRestaurantName,[orderCountIssuedRestaurantMinAmount floatValue]] delegate:self cancelButtonTitle:@"Place Order" otherButtonTitles:kCANCEL, nil];
            
            minOrderAlert.tag = 6;
            
            [minOrderAlert show];
        }
    }
    self.grandTotalValueLabel.text=[NSString stringWithFormat:@"KD %.2f",grandTotalAmount];
    self.cartListScrollView.contentSize = CGSizeMake(0, yOffset+10);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getCartInfoList
{
    NSMutableArray *cartListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    
    NSLog(@"Dattaaa%@",userData);
    
    self.next_Bttn.enabled=NO;
    
    if (userData.count>0)
    {
        //NSLog(@"user data...%@",userData);
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCartInfoWithDataDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         NSLog(@"ResponseDictionary...%@",responseDictionary);
                        //NSLog(@"%@",[responseDictionary objectForKey:@"Status"]);
                        if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             self.cartInfoItem = [ApplicationDelegate.mapper getCartInfoListFromDictionary:responseDictionary];
                             
                             
                             if (self.cartInfoItem.cart_InfoList_Array.count>0)
                             {
                                 for (int i=0; i<self.cartInfoItem.cart_InfoList_Array.count; i++)
                                 {
                                     if ([ApplicationDelegate isValid:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]])
                                     {
                                         RestaurantCart *restaurantCartItem = [ApplicationDelegate.mapper getRestaurantCartListFromDictionary:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]];
                                         
                                         [cartListArray addObject:restaurantCartItem];
                                     }
                                     
                                 }
                                 
                             }
                             
                         }
                         else
                         {
                             // hai
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
                 
             }
         
             if (cartListArray.count==0)
             {
                 
                 for (UIView *vw in self.cartListScrollView.subviews)
                 {
                     [vw removeFromSuperview];
                 }
                 self.next_Bttn.enabled=NO;
                 self.grandTotalValueLabel.text=@"";
                 
                 UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Cart is empty. Please place your order." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 alrt.tag = 5;
                 [alrt show];
             }
             else
             {
                 self.next_Bttn.enabled=YES;
                 [self prepareCartListScrollWithListArray:cartListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
 
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
        [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];

    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

     
-(void)getTempCartInfoList
{
    
    NSMutableArray *cartListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDictionary:[self getTempUserPostData]];
    self.next_Bttn.enabled=NO;
    if (userData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getTempCartInfoWithDataDictionary:userData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             self.cartInfoItem = [ApplicationDelegate.mapper getCartInfoListFromDictionary:responseDictionary];
                             
                             
                                if (self.cartInfoItem.cart_InfoList_Array.count>0)
                                    {
                                    for (int i=0; i<self.cartInfoItem.cart_InfoList_Array.count; i++)
                                        {
                                            if ([ApplicationDelegate isValid:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]])
                                                {
                                                    RestaurantCart *restaurantCartItem = [ApplicationDelegate.mapper getRestaurantCartListFromDictionary:[self.cartInfoItem.cart_InfoList_Array objectAtIndex:i]];
                                                                      
                                                    [cartListArray addObject:restaurantCartItem];
                                                }
                                        }
                                                              
                                    }
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
             }
             
             
             if (cartListArray.count==0)
             {
                 [ApplicationDelegate clearGuestUserSelection];
                 
                 for (UIView *vw in self.cartListScrollView.subviews)
                 {
                     [vw removeFromSuperview];
                 }
                 self.next_Bttn.enabled=NO;
                 self.grandTotalValueLabel.text=@"";

                 UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Cart is empty. Please place your order." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 alrt.tag = 5;
                 [alrt show];
                 
             }
             else
             {
                 self.next_Bttn.enabled=YES;
                 [self prepareCartListScrollWithListArray:cartListArray];
                 
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getTempUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:ApplicationDelegate.currentDeviceId forKey:@"userId"];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

-(NSMutableAttributedString*)convertIntoRedFont:(NSString*)aString{
    
    CTFontRef redFont = CTFontCreateWithName((__bridge CFStringRef) [UIFont fontWithName:@"Tahoma" size:15.0f].fontName, [UIFont fontWithName:@"Tahoma" size:13.0f].pointSize, NULL);
    UIColor *color = [UIColor redColor];

    
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithString:aString];
    [attrString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,aString.length)];
    
    [attrString addAttribute:NSFontAttributeName
                       value:( __bridge id)redFont
                       range:NSMakeRange(0,aString.length)];
    
    return attrString;
}

-(NSMutableAttributedString*)convertIntoGrayFont:(NSString*)aString{
    
    CTFontRef redFont = CTFontCreateWithName((__bridge CFStringRef) [UIFont fontWithName:@"Tahoma" size:15.0f].fontName, [UIFont fontWithName:@"Tahoma" size:13.0f].pointSize, NULL);
    
    UIColor *color = [UIColor grayColor];
    
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithString:aString];
    [attrString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,aString.length)];
    
    [attrString addAttribute:NSFontAttributeName
                       value:( __bridge id)redFont
                       range:NSMakeRange(0,aString.length)];
    
    return attrString;
}

-(void)cartListItemDidClickedForRemoval:(CartListItemCell *)cartListCategoryItem
{

    NSMutableDictionary *cartPostDic = [[NSMutableDictionary alloc] initWithDictionary:[self getCartPostData: cartListCategoryItem]];
        
        if (cartPostDic.count>0)
        {
            __block BOOL dataFound = NO;
            
            if (ApplicationDelegate.isLoggedIn)
            {
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            [ApplicationDelegate.engine removeFromCartWithDataDictionary:cartPostDic CompletionHandler:^(NSMutableDictionary *responseDictionary)
             {
                 BOOL success = NO;
                 
                 if (!dataFound)
                 {
                     dataFound = YES;
                     
                     if ([ApplicationDelegate isValid:responseDictionary])
                     {
                         if (responseDictionary.count>0)
                         {
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                             {
                                 if ([[responseDictionary objectForKey:@"Status"] isEqualToString:@"Success"])
                                 {
                                     success = YES;
                                    
                                 }
                             }
                         }
                     }
                     
                     [ApplicationDelegate removeProgressHUD];
                     if (success)
                     {
                         [self viewWillAppear:YES];
                     }
                 }
                 
             } errorHandler:^(NSError *error) {
                 
                 [ApplicationDelegate removeProgressHUD];
                 
             }];
            }
            else
            {
                [ApplicationDelegate addProgressHUDToView:self.view];
                [ApplicationDelegate.engine removeFromTempCartWithDataDictionary:cartPostDic CompletionHandler:^(NSMutableDictionary *responseDictionary)
                 {
                     BOOL success = NO;
                     
                     if (!dataFound)
                     {
                         dataFound = YES;
                         
                         if ([ApplicationDelegate isValid:responseDictionary])
                         {
                             if (responseDictionary.count>0)
                             {
                                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                                 {
                                     if ([[responseDictionary objectForKey:@"Status"] isEqualToString:@"Success"])
                                     {
                                         
                                         success = YES;
                                         
                                     }
                                 }
                             }
                         }
                         
                         [ApplicationDelegate removeProgressHUD];
                         if (success)
                         {
                             [self viewWillAppear:YES];
                         }
                     }
                     
                 } errorHandler:^(NSError *error) {
                     
                     [ApplicationDelegate removeProgressHUD];
                     
                 }];
            }
        }

}


-(NSMutableDictionary *)getCartPostData:(CartListItemCell *)cartlistItem
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:cartlistItem.cartDetailObj.shopping_CartId forKey:@"ShoppingCartId"];
    if (ApplicationDelegate.isLoggedIn) {
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"Username"];
    }
    else
    {
        [postDic setObject:ApplicationDelegate.currentDeviceId forKey:@"Username"];
    }
    
    return postDic;
}


- (IBAction)placeOrderButtonAction:(UIButton *)sender {
    [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
}

- (IBAction)nextButtonAction:(id)sender
{
    if (isOrderAmountMatchingWithMinimumAmount)
    {
        OrderConfirmationVCViewController *orderConfirmVc = [[OrderConfirmationVCViewController alloc] initWithNibName:@"OrderConfirmationVCViewController" bundle:nil];
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[OrderConfirmationVCViewController class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:orderConfirmVc animated:NO];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please confirm that your order amount meeting minimum amount in all case." title:kMESSAGE];
    }
}
#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //CART EMPTY
    
    if (alertView.tag==5)
    {
        if (buttonIndex==0)
        {
            if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
            {
                [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
            }
            
        }
        
    }
    // MIN ORDER AMOUNT FAILED
    
    if (alertView.tag==6)
    {
        if (buttonIndex==0)
        {
            if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
            {
                [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
            }
            
        }
        
    }
    
}

-(void)xibLoading
{
    NSString *nibName = @"CartListViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}
@end
