//
//  SpecialOfferOfRestaurant.m
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SpecialOfferOfRestaurant.h"

@interface SpecialOfferOfRestaurant ()

@end

@implementation SpecialOfferOfRestaurant

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.specialOfferLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        self.specialOfferLabel.textAlignment = NSTextAlignmentRight;
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self updateAboutRestaurantView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewHeading
{
    //   [[HomeTabViewController sharedViewController] resetTabSelection];
    // [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)updateAboutRestaurantView
{
    self.specialOfferMainScrollView.layer.cornerRadius = 3.0;
    self.specialOfferMainScrollView.layer.masksToBounds = YES;
    if ([ApplicationDelegate isValid:self.restaurantDetailObj]) {
        self.restaurantNameLabel.text=self.restaurantDetailObj.restaurant_Name;
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.restaurantDetailObj.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            
            self.restaurantImageView.image = responseImage;
            [self.restaurantImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
    }
    [self.restaurantNameLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.specialOfferLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f]];
    [self getSpecialOffersOfRestaurantList];
}

-(void)getSpecialOffersOfRestaurantList
{
    NSMutableArray *promotionListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *dishesData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
    //    for (UIView *sub in self.mostSellingDishesByRes_ScrollView.subviews) {
    //        [sub removeFromSuperview];
    //    }
    if (dishesData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getRestaurantByPromotionsListWithDataDictionary:dishesData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Promotions *promotionItem = [ApplicationDelegate.mapper getRestaurantsByPromotionsListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [promotionListArray addObject:promotionItem];
                         }
                     }
                     
                 }
             }
             if (promotionListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self preparePromotionsListScroll_Grid_WithArray:promotionListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getDishesPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.restaurantDetailObj.restaurant_Id forKey:@"RestaurantId"];
    
    return postDic;
}

-(void)preparePromotionsListScroll_Grid_WithArray:(NSMutableArray *)dishesListArray
{
    self.specialOfferMainScrollView.contentSize = CGSizeMake(self.specialOfferMainScrollView.frame.size.width,  (self.specialOfferMainScrollView.frame.size.height+self.specialOfferLabel.frame.origin.y-10));
    
    self.specialOfferCellScrollView.frame = CGRectMake(self.specialOfferCellScrollView.frame.origin.x, self.specialOfferCellScrollView.frame.origin.y, self.specialOfferCellScrollView.frame.size.width, (self.specialOfferMainScrollView.contentSize.height-self.specialOfferCellScrollView.frame.origin.y-5));
    
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.specialOfferCellScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.specialOfferCellScrollView.backgroundColor = [UIColor clearColor];
    
    [self.specialOfferCellScrollView setContentOffset:CGPointZero];
    
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    SpecialOfferOfRestaurantCell *vw = [[SpecialOfferOfRestaurantCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.specialOfferCellScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<dishesListArray.count; i++)
    {
        int colIndex = i%colCount;
        SpecialOfferOfRestaurantCell *dishesCell = [[SpecialOfferOfRestaurantCell alloc] init];
        
        dishesCell.layer.cornerRadius = 2.0;
        dishesCell.layer.masksToBounds = YES;
        dishesCell.dishImageView.layer.cornerRadius = 2.0;
        dishesCell.dishImageView.layer.masksToBounds = YES;
        Promotions *promotionItem = [dishesListArray objectAtIndex:i];
        //       // dishesCell.dishesObj=dishesItem;
        dishesCell.dishNameLabel.text=promotionItem.dish_Name;
        dishesCell.delegateSpecialOfferItem=self;
        [dishesCell.dishNameLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        //
        dishesCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  dishesCell.frame.size.width, dishesCell.frame.size.height);
        //
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:promotionItem.dish_Thumbnail] completionHandler:^(UIImage *responseImage) {
            dishesCell.dishImageView.image = responseImage;
            [dishesCell.dishImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            dishesCell.dishImageView.image =[UIImage imageNamed:@"Default_Food.png"];
            
        }];
        self.rating_Count= [promotionItem.rating integerValue];
        for (int k=0,gap=5; k<5; k++,gap=gap+5) {
            UIImageView *star =[[UIImageView alloc] initWithFrame:CGRectMake((k*18)+gap,0,18,18)];
            star.image=[UIImage imageNamed:@"promotion_star_unsel.png"];
            [dishesCell.rating_ImageView addSubview:star];
            if (k<self.rating_Count) {
            star.image=[UIImage imageNamed:@"promotion_star_sel.png"];
            }
        
        }
        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            dishesCell.dishPriceLabel.textAlignment = NSTextAlignmentLeft;
        }
        else
        {
            dishesCell.dishPriceLabel.textAlignment = NSTextAlignmentRight;
        }
        dishesCell.dishPriceLabel.text=[NSString stringWithFormat:@"KD %@",promotionItem.dish_Price];
        //         NSLog(@"Font families: %@", [UIFont fontNamesForFamilyName:@"Tahoma"]);
        [dishesCell.dishPriceLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
                dishesCell.dishDescriptionLabel.text=promotionItem.dish_Discription;
        dishesCell.dishDescriptionLabel.text=promotionItem.dish_Discription;
                [dishesCell.dishDescriptionLabel setFont:[UIFont fontWithName:@"Tahoma" size:12.0f]];
                [ApplicationDelegate loadMarqueeLabelWithText:dishesCell.dishDescriptionLabel.text Font:dishesCell.dishDescriptionLabel.font InPlaceOfLabel:dishesCell.dishDescriptionLabel];
        [self.specialOfferCellScrollView addSubview:dishesCell];
        if (colIndex == (colCount - 1))
        {
            yVal = dishesCell.frame.origin.y + dishesCell.frame.size.height;
        }
        
        scrollHeight = dishesCell.frame.origin.y + dishesCell.frame.size.height;
    }
    
       self.specialOfferCellScrollView.contentSize = CGSizeMake(self.specialOfferCellScrollView.frame.size.width,  (scrollHeight + offsetValue));

    
}

-(void)specialOfferItemDidClicked:(SpecialOfferOfRestaurantCell *)specialOfferCategoryItem
{
    if ([ApplicationDelegate isValid:self.restaurantDetailObj])
    {
        RestaurantMenuListVC *restaurantMenuListVC = [[RestaurantMenuListVC alloc] initWithNibName:@"RestaurantMenuListVC" bundle:nil];
        
        restaurantMenuListVC.restaurantDetailObj = self.restaurantDetailObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[RestaurantMenuListVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantMenuListVC animated:NO];
        }
    }
}

@end
