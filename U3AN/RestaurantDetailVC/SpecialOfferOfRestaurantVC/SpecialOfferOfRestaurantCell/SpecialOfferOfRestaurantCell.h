//
//  SpecialOfferOfRestaurantCell.h
//  U3AN
//
//  Created by Vipin on 05/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol specialOfferItemDelegate;
@interface SpecialOfferOfRestaurantCell : UIView
@property(weak,nonatomic)id<specialOfferItemDelegate> delegateSpecialOfferItem;

@property (weak, nonatomic) IBOutlet UIView *rating_ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *dishImageView;
@property (weak, nonatomic) IBOutlet UILabel *dishNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dishDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dishPriceLabel;

@end
@protocol specialOfferItemDelegate <NSObject>
- (void) specialOfferItemDidClicked:(SpecialOfferOfRestaurantCell *) specialOfferCategoryItem;
@end
