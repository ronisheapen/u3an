//
//  AboutRestaurantVC.m
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AboutRestaurantVC.h"

@interface AboutRestaurantVC ()

@end

@implementation AboutRestaurantVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.aboutRestaurantLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        self.aboutRestaurantLabel.textAlignment = NSTextAlignmentRight;
    }
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self updateAboutRestaurantView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewHeading
{
    //   [[HomeTabViewController sharedViewController] resetTabSelection];
    // [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
        
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)updateAboutRestaurantView
{
    self.aboutRestaurantScrollView.layer.cornerRadius = 3.0;
    self.aboutRestaurantScrollView.layer.masksToBounds = YES;
    if ([ApplicationDelegate isValid:self.restaurantDetailObj]) {
        self.restaurantDescriptionTxtView.text=self.restaurantDetailObj.summary;
        self.restaurantNameLabel.text=self.restaurantDetailObj.restaurant_Name;
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.restaurantDetailObj.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            
            self.restaurantImageView.image = responseImage;
            [self.restaurantImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
    }
    [self.restaurantNameLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.aboutRestaurantLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f]];
    
}

@end
