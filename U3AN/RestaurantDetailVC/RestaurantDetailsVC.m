//
//  RestaurantDetailsVC.m
//  U3AN
//
//  Created by Vipin on 19/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantDetailsVC.h"
#import "AboutRestaurantVC.h"
#import "MostSellingDishesOfRestaurantVC.h"
#import "SpecialOfferOfRestaurant.h"

#import "RestaurantMenuListVC.h"

@interface RestaurantDetailsVC ()

@end
NSMutableArray *areaListArray, *areaItemListArray;
@implementation RestaurantDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self updateDetailView];
    [self getAreaDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewHeading
{
 //   [[HomeTabViewController sharedViewController] resetTabSelection];
   // [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)updateDetailView
{
    self.restaurantDetailsContainerScroll.layer.cornerRadius = 3.0;
    self.restaurantDetailsContainerScroll.layer.masksToBounds = YES;
    if ([ApplicationDelegate isValid:self.restaurantObj]) {
        self.restaurant_Heading_Label.text=self.restaurantObj.restaurant_Name;
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.restaurantObj.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            
            self.restaurantImageView.image = responseImage;
            [self.restaurantImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
    }
    else
    {
    self.restaurant_Heading_Label.text=self.dishesObj.restaurant_Name;
    [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.dishesObj.restaurant_Thumbnail] completionHandler:^(UIImage *responseImage) {
        
        self.restaurantImageView.image = responseImage;
        [self.restaurantImageView.layer needsLayout];
        
    } errorHandler:^(NSError *error) {
        
    }];
    }
    //self.restaurantStatusDetailView.hidden=YES;
    [self.restaurant_Heading_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    self.aboutRestaurantButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.mostSellingButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.specialOfferButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.aboutRestaurantButton.titleLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f]];
    [self.mostSellingButton.titleLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f]];
    [self.specialOfferButton.titleLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f]];
    
}

-(void)getAreaDetails
{
    areaListArray=[[NSMutableArray alloc] init];
    areaItemListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getAreaPostData]];
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getAreasByRestaurantWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             AreasByRestaurant *areaItem = [ApplicationDelegate.mapper getAreaByRestaurantListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [areaListArray addObject:areaItem.area_Name];
                             [areaItemListArray addObject:areaItem];
                         }
                     }
                 }
                 
             }
        [ApplicationDelegate removeProgressHUD];
              if ([ApplicationDelegate isValid:areaListArray])
              {
                  if (areaListArray.count!=0)
                  {
                      if ([ApplicationDelegate isValid:[areaListArray objectAtIndex:0]])
                      {
                          self.areaLabel.text =[areaListArray objectAtIndex:0];
                          self.areaObj=[areaItemListArray objectAtIndex:0];
                          ApplicationDelegate.selected_AreaID=self.areaObj.area_Id;
                          [self getRestaurantDetails];
             
                      }
                  }
                  else
                  {
                      UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:kNO_DATA_ERROR_MSG delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      alrt.tag = 5;
                      [alrt show];
                      
                  }
              }
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getAreaPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
       if ([ApplicationDelegate isValid:self.restaurantObj]) {
    [postDic setObject:self.restaurantObj.restaurant_Id forKey:@"restId"];
        }
        else{
                [postDic setObject:self.dishesObj.restaurant_Id forKey:@"restId"];
        }
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}


- (IBAction)areaSelectionButtonAction:(UIButton *)sender {
    
    
    self.areaSelectionButton.selected = !self.areaSelectionButton.selected;
    if (self.areaSelectionButton.selected)
    {
        if (self.areaDropDownObj.view.superview) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
        self.areaDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.areaDropDownObj.cellHeight = 40.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.areaDropDownObj.view.frame = CGRectMake((self.areaSelectionButton.superview.frame.origin.x),(self.areaSelectionButton.superview.frame.origin.y+self.areaSelectionButton.superview.frame.size.height), (self.areaSelectionButton.superview.frame.size.width), 0);
        
        self.areaDropDownObj.dropDownDelegate = self;
        self.areaDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:areaListArray];
        
        self.areaDropDownObj.view.layer.borderWidth = 0.1;
        self.areaDropDownObj.view.layer.shadowOpacity = 1.0;
        self.areaDropDownObj.view.layer.shadowRadius = 5.0;
        self.areaDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
//        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
//            if (vw!=self.self.areaSelectionButton) {
//                [vw setUserInteractionEnabled:NO];
//            }
//        }
        //////////////
        self.areaDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.areaDropDownObj.textLabelColor = [UIColor blackColor];
        self.areaDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (areaListArray.count>0)
        {
            [self.restaurantDetailsContainerScroll addSubview:self.areaDropDownObj.view];
        }
        else
        {
            self.areaSelectionButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)+18.0f;
        }
        if (self.areaDropDownObj.dataArray.count!=0) {

        [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        }
        [UIView animateWithDuration:0.09f animations:^{
            self.areaDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.areaDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x+2,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
//        if (self.areaDropDownObj.dataArray.count!=0) {
//            
//            [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, self.restaurantDetailsContainerScroll.contentOffset.y) animated:YES];
//        }
        self.areaDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.areaDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
//        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
//            [vw setUserInteractionEnabled:YES];
//        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.areaDropDownObj.view removeFromSuperview];
            }
            
        }];
    }

}

- (IBAction)aboutRestaurantBttnAction:(UIButton *)sender {
    if ([ApplicationDelegate isValid:self.restaurantDetailObj])
    {
    AboutRestaurantVC *aboutRestaurantVC = [[AboutRestaurantVC alloc] initWithNibName:@"AboutRestaurantVC" bundle:nil]
    ;
    aboutRestaurantVC.view.backgroundColor = [UIColor clearColor];
    aboutRestaurantVC.restaurantDetailObj=self.restaurantDetailObj;
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[aboutRestaurantVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:aboutRestaurantVC animated:NO];
    }
    }
}

- (IBAction)mostSellingDishBttnAction:(UIButton *)sender {
    if ([ApplicationDelegate isValid:self.restaurantDetailObj])
    {
    MostSellingDishesOfRestaurantVC *mostSellingDishesOfRestaurantVC = [[MostSellingDishesOfRestaurantVC alloc] initWithNibName:@"MostSellingDishesOfRestaurantVC" bundle:nil]
    ;
    mostSellingDishesOfRestaurantVC.view.backgroundColor = [UIColor clearColor];
    mostSellingDishesOfRestaurantVC.restaurantDetailObj=self.restaurantDetailObj;
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[mostSellingDishesOfRestaurantVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:mostSellingDishesOfRestaurantVC animated:NO];
    }
    }
}

- (IBAction)specialOfferBttnAction:(UIButton *)sender {
    if ([ApplicationDelegate isValid:self.restaurantDetailObj])
    {
        SpecialOfferOfRestaurant *specialOfferOfRestaurantVC = [[SpecialOfferOfRestaurant alloc] initWithNibName:@"SpecialOfferOfRestaurant" bundle:nil]
        ;
        specialOfferOfRestaurantVC.view.backgroundColor = [UIColor clearColor];
        specialOfferOfRestaurantVC.restaurantDetailObj=self.restaurantDetailObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[specialOfferOfRestaurantVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:specialOfferOfRestaurantVC animated:NO];
        }
    }

}

- (IBAction)showMenuAction:(id)sender
{
    if ([ApplicationDelegate isValid:self.restaurantDetailObj])
    {
        RestaurantMenuListVC *restaurantMenuListVC = [[RestaurantMenuListVC alloc] initWithNibName:@"RestaurantMenuListVC" bundle:nil];
        
        restaurantMenuListVC.restaurantDetailObj = self.restaurantDetailObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[RestaurantMenuListVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantMenuListVC animated:NO];
        }
    }
}

#pragma mark - DROP-DOWN List Delegate Method

-(void)selectList:(int)selectedIndex
{
    
    ////////// ReEnabling the UserInteraction of all SubViews/////////
    for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        [vw setUserInteractionEnabled:YES];
    }
    ///////////////////
    
    [UIView animateWithDuration:0.4f animations:^{
        self.areaDropDownObj.view.frame =
        CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                   self.areaDropDownObj.view.frame.origin.y,
                   self.areaDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.areaSelectionButton.selected = NO;
    self.areaLabel.text =[areaListArray objectAtIndex:selectedIndex];
    self.areaObj=[areaItemListArray objectAtIndex:selectedIndex];
    ApplicationDelegate.selected_AreaID=self.areaObj.area_Id;
    [self getRestaurantDetails];
    
}

#pragma mark - 

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
        UITouch *touch = [touches anyObject];
        CGPoint location = [touch locationInView:[touch view]];
    
    if (self.areaDropDownObj.view.superview)
    {
        if (!CGRectContainsPoint(self.areaDropDownObj.view.frame, location))
        {
            self.areaSelectionButton.selected = NO;
            [self.areaDropDownObj.view removeFromSuperview];
            for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
                [vw setUserInteractionEnabled:YES];
            }
        }
        
        
        
    }
    
}

#pragma mark - GET Restaurant Details

-(void)getRestaurantDetails
{
    //areaListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getRestaurantPostData]];
    if (restaurantPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getRestaurantDetailWithDataDictionary:restaurantPostData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             self.restaurantDetailObj = [ApplicationDelegate.mapper getRestaurantDetailsFromDictionary:[responseArray objectAtIndex:i]];
                             
                            // [areaListArray addObject:areaItem.area_Name];
                         }
                     }
                     
                 }
             }
             [ApplicationDelegate removeProgressHUD];
             
             [self updateAndDisplayRestaurantDetailView];
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getRestaurantPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:self.areaObj.area_Id forKey:@"areaId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
   // [postDic setObject:@"" forKey:@"cuisineId"];
if ([ApplicationDelegate isValid:self.restaurantObj]) {
        [postDic setObject:self.restaurantObj.restaurant_Id forKey:@"restId"];
    }
    else
    {
        [postDic setObject:self.dishesObj.restaurant_Id forKey:@"restId"];
    }
   
    return postDic;
}

-(void)updateAndDisplayRestaurantDetailView
{
    self.restaurantStatusDetailView.hidden=NO;
    self.statusTxtLabel.text=self.restaurantDetailObj.restaurant_Status;
    [self.statusTxtLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:10.0f]];
    self.minAmountTxtLabel.text=[NSString stringWithFormat:@"KD %@",self.restaurantDetailObj.min_Amount];
    self.workingHoursTxtLabel.text=self.restaurantDetailObj.working_Hours;
    self.deliveryTimeTxtLabel.text=[NSString stringWithFormat:@"%@ Minutes",self.restaurantDetailObj.delivery_Time];
    self.deliveryChargesTxtLabel.text=self.restaurantDetailObj.u3anCharge;
    self.cusinesTxtLabel.text=self.restaurantDetailObj.cuisine;
    [ApplicationDelegate loadMarqueeLabelWithText:self.cusinesTxtLabel.text Font:self.cusinesTxtLabel.font InPlaceOfLabel:self.cusinesTxtLabel];
    
//    if ([self.restaurantDetailObj.accepts_Cash isEqualToString:@"1"] )
//    {
//        self.cash_ImageView.hidden=NO;
//    }
//    else
//    {
//        self.cash_ImageView.hidden=YES;
//    }
    
    self.cash_ImageView.hidden=NO;
    
    if ([self.restaurantDetailObj.accepts_KNET isEqualToString:@"1"] )
    {
        self.kNET_ImageView.hidden=NO;
    }
    else
    {
        self.kNET_ImageView.hidden=YES;
    }
    
    if ([self.restaurantDetailObj.accepts_CC isEqualToString:@"1"] )
    {
        CGFloat xPos = 224.0f;
        
        if (self.kNET_ImageView.hidden)
        {
            xPos = self.kNET_ImageView.frame.origin.x;
        }
        self.visa_ImageView.frame = CGRectMake(xPos, self.visa_ImageView.frame.origin.y, self.visa_ImageView.frame.size.width, self.visa_ImageView.frame.size.height);
        self.visa_ImageView.hidden=NO;
    }
    else
    {
        self.visa_ImageView.hidden=YES;
    }
    
    
     self.restaurantDetailsContainerScroll.contentSize = CGSizeMake(self.restaurantDetailsContainerScroll.frame.size.width,  self.restaurantStatusDetailView.frame.origin.y+self.restaurantStatusDetailView.frame.size.height-50);
    
}
#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //NO DATA AVAILBLE FROM AREA FETCHING
    
    if (alertView.tag==5)
    {
        if (buttonIndex==0)
        {
            if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
            {
                [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
            }
            
        }
        
    }
    
}

-(void)xibLoading
{
    NSString *nibName = @"RestaurantDetailsVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
