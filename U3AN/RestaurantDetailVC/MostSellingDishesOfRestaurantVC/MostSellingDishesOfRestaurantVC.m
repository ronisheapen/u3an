//
//  MostSellingDishesOfRestaurantVC.m
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MostSellingDishesOfRestaurantVC.h"

@interface MostSellingDishesOfRestaurantVC ()

@end

@implementation MostSellingDishesOfRestaurantVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.mostSellingDishLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        self.mostSellingDishLabel.textAlignment = NSTextAlignmentRight;
    }
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self updateAboutRestaurantView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewHeading
{
    //   [[HomeTabViewController sharedViewController] resetTabSelection];
    // [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)updateAboutRestaurantView
{
    self.mostSellingDishesMainScrollView.layer.cornerRadius = 3.0;
    self.mostSellingDishesMainScrollView.layer.masksToBounds = YES;
    if ([ApplicationDelegate isValid:self.restaurantDetailObj]) {
        self.restaurantNameLabel.text=self.restaurantDetailObj.restaurant_Name;
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:self.restaurantDetailObj.restaurant_Logo] completionHandler:^(UIImage *responseImage) {
            
            self.restaurantImageView.image = responseImage;
            [self.restaurantImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
    }
    [self.restaurantNameLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.mostSellingDishLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f]];
    [self getMostSellingDishesByRestaurantList];
}

-(void)getMostSellingDishesByRestaurantList
{
    NSMutableArray *dishesListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *dishesData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
//    for (UIView *sub in self.mostSellingDishesByRes_ScrollView.subviews) {
//        [sub removeFromSuperview];
//    }
    if (dishesData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getMostSellingDishesByRestaurantListWithDataDictionary:dishesData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Dishes *dishesItem = [ApplicationDelegate.mapper getMostSellingDishesListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [dishesListArray addObject:dishesItem];
                         }
                     }
                     
                 }
             }
             if (dishesListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else{
                 [self prepareMostSellingDishesByRestaurantListScroll_Grid_WithArray:dishesListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getDishesPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.restaurantDetailObj.restaurant_Id forKey:@"RestaurantId"];
    
    return postDic;
}

-(void)prepareMostSellingDishesByRestaurantListScroll_Grid_WithArray:(NSMutableArray *)dishesListArray
{
    self.mostSellingDishesMainScrollView.contentSize = CGSizeMake(self.mostSellingDishesMainScrollView.frame.size.width,  (self.mostSellingDishesMainScrollView.frame.size.height+self.mostSellingDishLabel.frame.origin.y-10));
    
    self.mostSellingDishesCellScrollView.frame = CGRectMake(self.mostSellingDishesCellScrollView.frame.origin.x, self.mostSellingDishesCellScrollView.frame.origin.y, self.mostSellingDishesCellScrollView.frame.size.width, (self.mostSellingDishesMainScrollView.contentSize.height-self.mostSellingDishesCellScrollView.frame.origin.y-5));
    
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.mostSellingDishesCellScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.mostSellingDishesCellScrollView.backgroundColor = [UIColor clearColor];
    
    [self.mostSellingDishesCellScrollView setContentOffset:CGPointZero];
    
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    MostSellingDishOfRestaurantCell *vw = [[MostSellingDishOfRestaurantCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.mostSellingDishesCellScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<dishesListArray.count; i++)
    {
        int colIndex = i%colCount;
        MostSellingDishOfRestaurantCell *dishesCell = [[MostSellingDishOfRestaurantCell alloc] init];
        
        dishesCell.layer.cornerRadius = 2.0;
        dishesCell.layer.masksToBounds = YES;
        dishesCell.dishesImageView.layer.cornerRadius = 2.0;
        dishesCell.dishesImageView.layer.masksToBounds = YES;
        Dishes *dishesItem = [dishesListArray objectAtIndex:i];
//       // dishesCell.dishesObj=dishesItem;
        dishesCell.dishNameLabel.text=dishesItem.dish_Name;
        dishesCell.delegateMostSellingDishesItem=self;
        [dishesCell.dishNameLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
//
        dishesCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  dishesCell.frame.size.width, dishesCell.frame.size.height);
//        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:dishesItem.dish_Thumbnail] completionHandler:^(UIImage *responseImage) {
            dishesCell.dishesImageView.image = responseImage;
            [dishesCell.dishesImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            dishesCell.dishesImageView.image =[UIImage imageNamed:@"Default_Food.png"];
            
        }];
        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            dishesCell.dishPriceLabel.textAlignment = NSTextAlignmentLeft;
        }
        else
        {
            dishesCell.dishPriceLabel.textAlignment = NSTextAlignmentRight;
        }
        dishesCell.dishPriceLabel.text=[NSString stringWithFormat:@"KD %@",dishesItem.dish_Price];
//         NSLog(@"Font families: %@", [UIFont fontNamesForFamilyName:@"Tahoma"]);
        [dishesCell.dishPriceLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        dishesCell.dishDescriptionLabel.text=dishesItem.dish_Description;
        [dishesCell.dishDescriptionLabel setFont:[UIFont fontWithName:@"Tahoma" size:12.0f]];
        [ApplicationDelegate loadMarqueeLabelWithText:dishesCell.dishDescriptionLabel.text Font:dishesCell.dishDescriptionLabel.font InPlaceOfLabel:dishesCell.dishDescriptionLabel];

        [self.mostSellingDishesCellScrollView addSubview:dishesCell];
        if (colIndex == (colCount - 1))
        {
            yVal = dishesCell.frame.origin.y + dishesCell.frame.size.height;
        }
        
        scrollHeight = dishesCell.frame.origin.y + dishesCell.frame.size.height;
    }
    
    self.mostSellingDishesCellScrollView.contentSize = CGSizeMake(self.mostSellingDishesCellScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
    
}

-(void)dishesItemDidClicked:(MostSellingDishOfRestaurantCell *)dishesCategoryItem
{
    if ([ApplicationDelegate isValid:self.restaurantDetailObj])
    {
        RestaurantMenuListVC *restaurantMenuListVC = [[RestaurantMenuListVC alloc] initWithNibName:@"RestaurantMenuListVC" bundle:nil];
        restaurantMenuListVC.restaurantDetailObj = self.restaurantDetailObj;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[RestaurantMenuListVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantMenuListVC animated:NO];
        }
    }
}
@end
