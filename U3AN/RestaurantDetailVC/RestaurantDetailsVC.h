//
//  RestaurantDetailsVC.h
//  U3AN
//
//  Created by Vipin on 19/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import "AreasByRestaurant.h"
#import "DropDownView.h"
#import "RestaurantDetail.h"
#import "Dishes.h"

@interface RestaurantDetailsVC : UIViewController<ListSelectionProtocol,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *restaurant_Heading_Label;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *restaurantDetailsContainerScroll;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (weak, nonatomic) IBOutlet UIButton *aboutRestaurantButton;
@property (weak, nonatomic) IBOutlet UIButton *mostSellingButton;
@property (weak, nonatomic) IBOutlet UIButton *specialOfferButton;
@property (weak, nonatomic) IBOutlet UIView *restaurantStatusDetailView;
@property (weak, nonatomic) IBOutlet UILabel *statusTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *minAmountTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *workingHoursTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryTimeTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryChargesTxtLabel;
@property (weak, nonatomic) IBOutlet UILabel *cusinesTxtLabel;
@property (weak, nonatomic) IBOutlet UIButton *areaSelectionButton;
@property (weak, nonatomic) IBOutlet UIImageView *visa_ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *kNET_ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cash_ImageView;

@property (strong, nonatomic) DropDownView *areaDropDownObj;
@property (strong, nonatomic)Restaurant *restaurantObj;
@property (strong, nonatomic)Dishes *dishesObj;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;
@property (strong, nonatomic)AreasByRestaurant *areaObj;
- (IBAction)areaSelectionButtonAction:(UIButton *)sender;
- (IBAction)aboutRestaurantBttnAction:(UIButton *)sender;
- (IBAction)mostSellingDishBttnAction:(UIButton *)sender;
- (IBAction)specialOfferBttnAction:(UIButton *)sender;

- (IBAction)showMenuAction:(id)sender;


@end
