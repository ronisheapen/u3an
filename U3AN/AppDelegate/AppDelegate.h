//
//  AppDelegate.h
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "HomeTabViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "AppEngine.h"
#import "ObjectMapper.h"
#import "AppEngine.h"
#import "UserData.h"
#import "SplashView.h"

#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)


@interface AppDelegate : UIResponder <UIApplicationDelegate,MBProgressHUDDelegate>
{
    Reachability* internetReachable;
    NSMutableDictionary *pushNotificationDic;
}
@property (assign, nonatomic)BOOL isLoggedIn;
@property (assign, nonatomic)BOOL isGustUser;
@property (strong, nonatomic)NSString *authn_Key;
@property (strong, nonatomic)NSString *logged_User_Name;
@property (strong, nonatomic)NSString *selected_AreaID;
@property (strong, nonatomic)NSString  *currentDeviceId;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AppEngine *engine;
@property (strong, nonatomic)SplashView *splashObj;
@property (strong, nonatomic)HomeTabViewController *homeTabObj;
@property (strong, nonatomic)UINavigationController *homeNav;
@property (strong, nonatomic)UINavigationController *subHomeNav;
@property(strong,nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic)NSString *language;
@property (strong, nonatomic)ObjectMapper *mapper;
@property (strong, nonatomic)UserData *userDetailObj;

@property (strong, nonatomic) NSString *deviceTokenString;
@property (assign, nonatomic) BOOL pushNotificationReceivedFlag;
@property (assign, nonatomic) NSInteger pushNotificationTypeID;
@property (assign, nonatomic) BOOL newsLetterStatusFlag;
//@property (assign, nonatomic) long long pushNotificationItemID;

-(BOOL)checkNetworkAvailability;
-(void)showAlertWithMessage:(NSString *)message title:(NSString *)title;
-(void)addProgressHUDToView:(UIView *)parentview;
-(void)removeProgressHUD;
- (NSString *)fileWithFilename:(NSString *)fileName;
-(NSString *)getViewTitle:(UIViewController *)vc;
-(NSString *)getHTTPCorrectedURLFromUrl:(NSString *)urlString;
-(BOOL)isValid:(id)sender;
-(void)loadImageWithAnimationInImageView:(UIImageView *)sender withImage:(UIImage*)responseImage;
-(BOOL)isEmailValidWithEmailID:(NSString *)text;
-(UserData *)getUserDataFromPlist;
-(void)loadMarqueeLabelWithText:(NSString *)lblText Font:(UIFont *)fontVal InPlaceOfLabel:(UILabel *)uilbl;
-(CGRect)getDynamicAdjustedLabelFrameForLabel:(UILabel *)labelObject WithFont:(UIFont *)labelFont TextContent:(NSString *)textContent;
-(void)addViewWithPopUpAnimation:(UIView *)popView InParentView:(UIView *)parent;
-(NSString *) stringByStrippingHTMLFromString:(NSString *)dataString;

-(BOOL)checkingForGUEST_User_LoggedIn;
-(void)loginGuestUser;
-(void)clearGuestUserSelection;

-(BOOL)readNewsLetterStatusFromPreference;
-(void)saveNewsLetterStatusWithStatus:(BOOL)status;
-(void)changeLanguage;

@end
