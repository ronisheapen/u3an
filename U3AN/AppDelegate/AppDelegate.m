//
//  AppDelegate.m
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AppDelegate.h"
#import "AllRestaurantsVC.h"
#import "NewRestaurantsVC.h"
#import "MostSellingDishesVC.h"
#import "PromotionsViewController.h"


@implementation AppDelegate
@synthesize deviceTokenString;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
   // [self adjustStatusBarLayoutForiOS7Onwards];
    ApplicationDelegate.language = kARABIC;
    
    [self changeLanguage];
    
    [self initializer];
    self.window = [[UIWindow alloc] initWithFrame:
                   [[UIScreen mainScreen] bounds]];
    
    self.homeTabObj = [[HomeTabViewController alloc]
                    initWithNibName:@"HomeTabViewController" bundle:nil];
    self.homeNav = [[UINavigationController alloc] initWithRootViewController:[HomeTabViewController sharedViewController]];
    self.homeNav.navigationBar.translucent = NO;
    self.homeNav.navigationBar.hidden = YES;
    
    self.HUD = [[MBProgressHUD alloc] initWithView:self.homeTabObj.view];
      [ApplicationDelegate.HUD.delegate self];
    
    self.splashObj = [[SplashView alloc] initWithNibName:@"SplashView" bundle:nil];
    UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:self.splashObj];
    nv.navigationBar.translucent = NO;
    nv.navigationBar.hidden = YES;
    self.window.rootViewController = nv;
    
    //self.window.rootViewController = self.homeNav;
    [self.window makeKeyAndVisible];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    ApplicationDelegate.pushNotificationReceivedFlag = NO;
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark
-(void)initializer
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
    
    
    ///////////////Enable PushNotification
    
    pushNotificationDic = [[NSMutableDictionary alloc] init];
    
    self.deviceTokenString = @"";
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    
    self.newsLetterStatusFlag = [self readNewsLetterStatusFromPreference];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    ////////////////
    
    self.engine = [[AppEngine alloc] initWithHostName:kHOST_URL customHeaderFields:nil];
    [self.engine emptyCache];
    [self.engine useCache];
    
    self.mapper = [[ObjectMapper alloc] init];
    self.userDetailObj = [[UserData alloc] init];
    self.userDetailObj=[self getUserDataFromPlist];
    if ([self.userDetailObj.loginStatus isEqualToString:@"success"])
    {
        self.isLoggedIn=YES;
        self.authn_Key=self.userDetailObj.authKey;
        self.logged_User_Name=self.userDetailObj.userName;
        //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
        self.newsLetterStatusFlag = self.userDetailObj.newsletterStatus;
    }
    else
    {
        self.isLoggedIn=NO;
        self.authn_Key=self.userDetailObj.authKey;
        self.logged_User_Name=self.userDetailObj.userName;
    }
    
    UIDevice *device = [UIDevice currentDevice];
    
    self.currentDeviceId = [[device identifierForVendor]UUIDString];
}

-(UserData *)getUserDataFromPlist
{
    UserData  *userObj = [[UserData alloc] init];
    NSString *plistPath= [self fileWithFilename:@"UserDetails.plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
    userObj=[self.mapper getUserDataFromDictionary:data];
    return userObj;
}
-(BOOL)checkingForGUEST_User_LoggedIn
{
    BOOL status = NO;
    
    NSString *plistPath= [self fileWithFilename:@"UserDetails.plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
    
    if ([[data objectForKey:@"isGuestUser"] boolValue])
    {
        status = YES;
        
        //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
        
        ApplicationDelegate.newsLetterStatusFlag = NO;
        
        [data setObject:[NSNumber numberWithBool:NO] forKey:@"newsletterStatus"];
        [data writeToFile:plistPath atomically:YES];
    }
    return status;
}
-(void)loginGuestUser
{
    NSString *plistPath= [self fileWithFilename:@"UserDetails.plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
    
    [data setObject:[NSNumber numberWithBool:YES] forKey:@"isGuestUser"];
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    ApplicationDelegate.newsLetterStatusFlag = NO;
    
    [data setObject:[NSNumber numberWithBool:NO] forKey:@"newsletterStatus"];
    [data writeToFile:plistPath atomically:YES];
}
-(void)clearGuestUserSelection
{
    NSString *plistPath= [self fileWithFilename:@"UserDetails.plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
    
    [data setObject:[NSNumber numberWithBool:NO] forKey:@"isGuestUser"];
    
    ApplicationDelegate.newsLetterStatusFlag = NO;
    
    [data setObject:[NSNumber numberWithBool:NO] forKey:@"newsletterStatus"];
    
    [data writeToFile:plistPath atomically:YES];
}

#pragma mark -

-(void)adjustStatusBarLayoutForiOS7Onwards
{
    // Set "View controller-based status bar appearance” to NO in your info.list file
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0)
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    
    if (internetStatus == NotReachable)
    {
        UIAlertView *erroralert = [[UIAlertView alloc] initWithTitle:kMESSAGE message:kNETWORK_ERROR_MSG delegate:nil cancelButtonTitle:kOK otherButtonTitles:nil, nil];
        //        [erroralert show];
    }
    else
    {
        
    }
}

-(BOOL)checkNetworkAvailability
{
    BOOL status;
    Reachability *network = [Reachability reachabilityForInternetConnection];
    NetworkStatus statusOfConnection = [network currentReachabilityStatus];
    if (statusOfConnection == NotReachable)
    {
        status = NO;
    }
    else
    {
        status = YES;
    }
    return status;
}

-(void)showAlertWithMessage:(NSString *)message title:(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle: title
                              message: message
                              delegate: nil
                              cancelButtonTitle: kOK
                              otherButtonTitles: nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Show alert here
        [alertView show];
    });
    //[alertView show];
}
-(void)addProgressHUDToView:(UIView *)parentview
{
    if ([ApplicationDelegate.HUD superview]) {
        [ApplicationDelegate.HUD removeFromSuperview];
    }
    ApplicationDelegate.HUD.center = parentview.center;
    [parentview addSubview:ApplicationDelegate.HUD];
    [ApplicationDelegate.HUD removeFromSuperViewOnHide];
    [ApplicationDelegate.HUD show:YES];
}
-(void)removeProgressHUD
{
    if ([ApplicationDelegate.HUD superview]) {
        [ApplicationDelegate.HUD hide:YES];
    }
}
- (NSString *)fileWithFilename:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask,
                                                         YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *documentDirPath = [documentsDir
                                 stringByAppendingPathComponent:fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:documentDirPath];
    
    if (!success) {
        NSMutableDictionary *dat = [[NSMutableDictionary alloc] init];
        [dat writeToFile:documentDirPath atomically:YES];
    }
    
    return documentDirPath;
}


-(BOOL)isValid:(id)sender
{
    BOOL valid = NO;
    if ((sender!=nil)&&(![sender isEqual:[NSNull null]]))
    {
        valid=YES;
    }
    return valid;
}

-(NSString *)getViewTitle:(UIViewController *)vc
{
    NSString *title = @"";
    if ([self isValid:vc])
    {
        if ([vc.nibName isEqualToString:@"MostSellingDishesVC"])
        {
            title = localize(@"Most Selling Dishes");
        }
        if ([vc.nibName isEqualToString:@"AllRestaurantsVC"])
        {
            title = localize(@"All Restuarants");
        }
        if ([vc.nibName isEqualToString:@"NewRestaurantsVC"])
        {
            title = localize(@"New Restaurants");
        }
        if ([vc.nibName isEqualToString:@"AddRestaurantViewController"])
        {
            title = localize(@"Add your Restaurant");
        }
        if ([vc.nibName isEqualToString:@"ConsultationViewController"])
        {
            title = localize(@"Consultation");
        }
        if ([vc.nibName isEqualToString:@"FAQViewController"])
        {
            title = localize(@"FAQ");
        }
        if ([vc.nibName isEqualToString:@"FeedBackViewController"])
        {
            title = localize(@"Give Us Your Feedback");
        }
        if ([vc.nibName isEqualToString:@"OrderDishesViewController"])
        {
            title = localize(@"Order Dishes");
        }
        if ([vc.nibName isEqualToString:@"PrivacyViewController"])
        {
            title = localize(@"Privacy");
        }
        if ([vc.nibName isEqualToString:@"PromotionsViewController"])
        {
            title = localize(@"Promotions");
        }
        if ([vc.nibName isEqualToString:@"SettingsViewController"])
        {
            title = localize(@"Settings");
        }
        if ([vc.nibName isEqualToString:@"RestaurantMenuListVC"])
        {
            title = localize(@"Restaurant Menu");
        }
        if ([vc.nibName isEqualToString:@"RestaurantMenuDetailVC"])
        {
            title = localize(@"Restaurant Menu");
        }
        if ([vc.nibName isEqualToString:@"SocialResponsibilityViewController"])
        {
            title = localize(@"Social Responsibility");
        }
        if ([vc.nibName isEqualToString:@"TermsViewController"])
        {
            title = localize(@"Terms and Conditions");
        }
        if ([vc.nibName isEqualToString:@"MostSellingDishesByRestaurantVC"])
        {
            title = localize(@"Most Selling Dishes");
        }
        if ([vc.nibName isEqualToString:@"SearchViewController"])
        {
            title = localize(@"Detailed Search");
        }
        if ([vc.nibName isEqualToString:@"LoginViewController"])
        {
            title = localize(@"User Login");
        }
        if ([vc.nibName isEqualToString:@"SignUpViewController"])
        {
            title = localize(@"Sign Up");
        }
        if ([vc.nibName isEqualToString:@"PromotionsViewController"])
        {
            title = localize(@"Promotions");
        }
        if ([vc.nibName isEqualToString:@"PromotionDetailVC"])
        {
            title = localize(@"Promotions");
        }
    
        if ([vc.nibName isEqualToString:@"GiftVoucherDetailVC"])
        {
            title = localize(@"Gift Voucher");
        }
        if ([vc.nibName isEqualToString:@"GiftVoucherViewController"])
        {
            title = localize(@"Gift Voucher");
        }
        if ([vc.nibName isEqualToString:@"BuyGiftVoucherVC"])
        {
            title = localize(@"Gift Voucher");
        }
        
        if ([vc.nibName isEqualToString:@"MyAccountVC"])
        {
            title = localize(@"My Account");
        }
        if ([vc.nibName isEqualToString:@"MyFavouritesVC"])
        {
            title = localize(@"My Favourites");
        }
        if ([vc.nibName isEqualToString:@"AccountInfoListVC"])
        {
            title = localize(@"Account Information");
        }
        
        if ([vc.nibName isEqualToString:@"AccountInformationVC"])
        {
            title = localize(@"Account Information");
        }
        if ([vc.nibName isEqualToString:@"AddAddressVC"])
        {
            title = localize(@"Add Address");
        }
        if ([vc.nibName isEqualToString:@"AddressListVC"])
        {
            title = localize(@"Address List");
        }
        if ([vc.nibName isEqualToString:@"ChangePasswordVC"])
        {
            title = localize(@"Change Password");
        }
        if ([vc.nibName isEqualToString:@"PersonalInformationVC"])
        {
            title = localize(@"Personal Information");
        }
        if ([vc.nibName isEqualToString:@"OrderlistViewController"])
        {
            title = localize(@"My Orders");
        }
        if ([vc.nibName isEqualToString:@"OrderDetailViewController"])
        {
            title = localize(@"My Orders");
        }
        if ([vc.nibName isEqualToString:@"OrderItemListViewController"])
        {
            title = localize(@"My Orders");
        }
        if ([vc.nibName isEqualToString:@"U3ANCreditViewController"])
        {
            title = localize(@"U3AN Credit");
        }
        if ([vc.nibName isEqualToString:@"BuyU3ANCreditVC"])
        {
            title = localize(@"U3AN Credit");
        }
        if ([vc.nibName isEqualToString:@"OrderConfirmationVCViewController"])
        {
            title = localize(@"Order Confirmation");
        }
        if ([vc.nibName isEqualToString:@"OrderConfirmationResultVC"])
        {
            title = localize(@"Payment");
        }
        
        if ([vc.nibName isEqualToString:@"CartListViewController"])
        {
            title = localize(@"Cart List");
        }
        if ([vc.nibName isEqualToString:@"RestaurantReviewVC"])
        {
            title = localize(@"Restaurant Review");
        }
        if ([vc.nibName isEqualToString:@"LiveChatViewController"])
        {
            title = localize(@"Live Chat");
        }
        
        if ([vc.nibName isEqualToString:@"RestaurantDetailsVC"])
        {
            NSMutableArray *vcArr = [[NSMutableArray alloc] initWithArray:vc.navigationController.viewControllers];
            if (vcArr.count>2)
            {
            if ([[vcArr objectAtIndex:(vcArr.count-2)] isKindOfClass:[AllRestaurantsVC class]])
            {
                [[HomeTabViewController sharedViewController] resetTabSelection];
                [HomeTabViewController sharedViewController].allRestaurantsButton.selected=YES;
                title = localize(@"All Restaurants");
            }
          else if ([[vcArr objectAtIndex:(vcArr.count-2)] isKindOfClass:[NewRestaurantsVC class]])
            {
                [[HomeTabViewController sharedViewController] resetTabSelection];
                [HomeTabViewController sharedViewController].uNewrestaurantsButton.selected=YES;
                title = localize(@"New Restaurants");
            }
           else if ([[vcArr objectAtIndex:(vcArr.count-2)] isKindOfClass:[MostSellingDishesVC class]])
           {
               [[HomeTabViewController sharedViewController] resetTabSelection];
               [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
            title = localize(@"Most Selling Dishes");
           }
                else
                {
                    title = localize(@"Restaurant Details");
                }
            }
        }
        if ([vc.nibName isEqualToString:@"AboutRestaurantVC"])
        {
            //            if ([[HomeTabViewController sharedViewController].mostSellingButton isSelected]) {
            //                title = @"Most Selling Dishes";
            //            }
            //            else if ([[HomeTabViewController sharedViewController].allRestaurantsButton isSelected])
            //            {
            //                title = @"All Restuarants";
            //            }
            //            else if ([[HomeTabViewController sharedViewController].uNewrestaurantsButton isSelected])
            //            {
            //                title = @"New Restaurants";
            //            }
            //            else{
            //                title = @"About Restaurant";
            //            }
            
            NSMutableArray *vcArr = [[NSMutableArray alloc] initWithArray:vc.navigationController.viewControllers];
            if (vcArr.count>3)
            {
                if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[AllRestaurantsVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].allRestaurantsButton.selected=YES;
                    title = localize(@"All Restaurants");
                }
                else if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[NewRestaurantsVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].uNewrestaurantsButton.selected=YES;
                    title = localize(@"New Restaurants");
                }
                else if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[MostSellingDishesVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
                    title = localize(@"Most Selling Dishes");
                }
                else
                {
                    title = localize(@"About Restaurant");
                }
            }
            
        }
        
        if ([vc.nibName isEqualToString:@"MostSellingDishesOfRestaurantVC"])
        {
            
            NSMutableArray *vcArr = [[NSMutableArray alloc] initWithArray:vc.navigationController.viewControllers];
            if (vcArr.count>3)
            {
                if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[AllRestaurantsVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].allRestaurantsButton.selected=YES;
                    title = localize(@"All Restaurants");
                }
                else if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[NewRestaurantsVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].uNewrestaurantsButton.selected=YES;
                    title = localize(@"New Restaurants");
                }
                else if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[MostSellingDishesVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
                    title = localize(@"Most Selling Dishes");
                }
                else
                {
                    title = localize(@"MostSelling Dishes Of Restaurant");
                }
            }
            
        }
        if ([vc.nibName isEqualToString:@"SpecialOfferOfRestaurant"])
        {
            
            NSMutableArray *vcArr = [[NSMutableArray alloc] initWithArray:vc.navigationController.viewControllers];
            if (vcArr.count>3)
            {
                if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[AllRestaurantsVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].allRestaurantsButton.selected=YES;
                    title = localize(@"All Restaurants");
                }
                else if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[NewRestaurantsVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].uNewrestaurantsButton.selected=YES;
                    title = localize(@"New Restaurants");
                }
                else if ([[vcArr objectAtIndex:(vcArr.count-3)] isKindOfClass:[MostSellingDishesVC class]])
                {
                    [[HomeTabViewController sharedViewController] resetTabSelection];
                    [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
                    title = localize(@"Most Selling Dishes");
                }
                else
                {
                    title = localize(@"SpecialOffer Of Restaurant");
                }
            }
            
        }

        if ([vc.nibName isEqualToString:@"MostSellingDishesByCuisinesVC"])
        {
            title = localize(@"Most Selling Dishes");
        }


    }
    return title;
}
-(NSString *)getHTTPCorrectedURLFromUrl:(NSString *)urlString
{
    NSString *correctedURLString;
    if ([ApplicationDelegate isValid:urlString])
    {
        if (urlString.length>0) {
            if (![urlString hasPrefix:@"http"])
            {
                correctedURLString = [NSString stringWithFormat:@"http://%@",urlString];
            }
            else
            {
                correctedURLString = urlString;
            }
        }
    }
    
    return correctedURLString;
}
-(void)loadImageWithAnimationInImageView:(UIImageView *)sender withImage:(UIImage*)responseImage
{
    [UIView transitionWithView:sender
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        sender.image=responseImage;
                        [sender.layer needsLayout];
                    } completion:nil];
    
}

-(void)loadMarqueeLabelWithText:(NSString *)lblText Font:(UIFont *)fontVal InPlaceOfLabel:(UILabel *)uilbl
{
    for (UIView *lb in uilbl.superview.subviews)
    {
        if ([lb isKindOfClass:[MarqueeLabel class]])
        {
            if (lb.frame.origin.x==uilbl.frame.origin.x)
            {
                [lb removeFromSuperview];
            }
            
        }
        
    }
    
    uilbl.hidden = YES;
    
    if (lblText.length>0)
    {
        MarqueeLabel *nameLabel = [[MarqueeLabel alloc] initWithFrame:uilbl.frame];
//        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
//        {
            nameLabel.marqueeType = MLContinuous;
//        }
//        else
//        {
//            nameLabel.marqueeType = MLContinuousReverse;
//        }
        
        //nameLabel.scrollDuration = 8.0;
        CGSize messageSize = [lblText sizeWithAttributes:@{NSFontAttributeName:fontVal}];
        float duration = messageSize.width / 20;
        nameLabel.scrollDuration = duration;
        
        nameLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
        nameLabel.fadeLength = 10.0f;
        nameLabel.continuousMarqueeExtraBuffer = 10.0f;
        nameLabel.textColor=uilbl.textColor;
        nameLabel.font = fontVal;
        nameLabel.text = lblText;
        nameLabel.textAlignment = uilbl.textAlignment;
        nameLabel.backgroundColor = [UIColor clearColor];
        [uilbl.superview addSubview:nameLabel];
    }
    
}
-(CGRect)getDynamicAdjustedLabelFrameForLabel:(UILabel *)labelObject WithFont:(UIFont *)labelFont TextContent:(NSString *)textContent
{
    CGRect rect = CGRectZero;
    
    CGSize constraintSize = CGSizeMake(labelObject.frame.size.width, MAXFLOAT);
    UIFont *cellFont = labelFont;
    CGSize labelSize = [textContent sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    rect = CGRectMake(labelObject.frame.origin.x, labelObject.frame.origin.y, labelSize.width, labelSize.height);
    
    return rect;
}
-(void)addViewWithPopUpAnimation:(UIView *)popView InParentView:(UIView *)parent
{
    [parent setUserInteractionEnabled:YES];
    [ApplicationDelegate.window setUserInteractionEnabled:YES];
    ////////////////////////////////////////POP UP////////////////////////////////////
    CABasicAnimation *forwardAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    forwardAnimation.duration = 0.65;
    forwardAnimation.timingFunction = [CAMediaTimingFunction functionWithControlPoints:0.5f :1.7f :0.6f :0.85f];
    forwardAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    forwardAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    ////////////////////////////////////////////////////////////////////////////////////
    
    
    ///////ANIMATION SETTING///////
    
    // To animate we require following code:
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.animations = [NSArray arrayWithObjects:forwardAnimation,  nil];
    animationGroup.delegate = self;
    animationGroup.duration = forwardAnimation.duration;
    animationGroup.removedOnCompletion = NO;
    animationGroup.fillMode = kCAFillModeForwards;
    
    [parent addSubview:popView];
    
    [UIView animateWithDuration:animationGroup.duration
                          delay:0.0
                        options:0
                     animations:^{
                         
                         [popView.layer addAnimation:animationGroup
                                              forKey:kCATransitionReveal];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         if (finished)
                         {
                             
                         }
                     }];
}
-(NSString *) stringByStrippingHTMLFromString:(NSString *)dataString
{
    NSRange r;
    NSString *s = dataString;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}
#pragma mark - EMAIL VALIDATOR
-(BOOL)isEmailValidWithEmailID:(NSString *)text {
    NSString *questionableEmail = text;
    
    
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:questionableEmail] == YES) {
        
        //  NSLog(@"VALID EMAIL");
        return YES;
    }
    else{
        // NSLog(@"INVALID EMAIL");
        return NO;
    }
    
}

#pragma mark - PUSH NOTIFICATION METHODS
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    NSString *tokenStr = [newDeviceToken description];
    NSString *pushToken = [[[tokenStr
                             stringByReplacingOccurrencesOfString:@"<" withString:@""]
                            stringByReplacingOccurrencesOfString:@">" withString:@""]
                           stringByReplacingOccurrencesOfString:@" " withString:@""] ;
    //    3bc549e1249e2f5b4c62a27be874f8a5620621aaeba59e84f0be5c612b4a6572
    NSLog(@"dev token>>>%@",pushToken);
    self.deviceTokenString = pushToken;
    
    [self postDeviceToken];
    
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    // [self alert: error.localizedDescription title: @"Error"];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // Received a push notification from the server
    // Only pop alert if applicationState is active (if not, the user already saw the alert)
    
    pushNotificationDic = [[NSMutableDictionary alloc] initWithDictionary:userInfo];
    
    //NSLog(@"%@",[userInfo description]);
    
    //1 = PROMOTIONS
    //2 = NEW RESTAURANTS
    
    self.pushNotificationReceivedFlag = YES;
    
    if ([self isValid:[userInfo objectForKey:@"id"]])
    {
        self.pushNotificationTypeID = [[userInfo objectForKey:@"id"] integerValue];
    }
    
    //if (application.applicationState == UIApplicationStateActive)
    if (self.window.rootViewController==self.homeNav)
    {
        if ([self.subHomeNav.view superview])
        {
            NSString * message = [NSString stringWithFormat:@"%@",
                                  [[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            //[self alert: message title: @"U3AN"];
            
            if ((self.pushNotificationTypeID==1)||(self.pushNotificationTypeID==2))
            {
                
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle: @"U3AN"
                                          message: message
                                          delegate: self
                                          cancelButtonTitle: kCANCEL
                                          otherButtonTitles: @"View",nil];
                alertView.tag = 5;
                dispatch_async(dispatch_get_main_queue(), ^{
                    //Show alert here
                    [alertView show];
                });
            }
            else
            {
                [self alert: message title: @"U3AN"];
            }
        }
        
    }
    else
    {
        ApplicationDelegate.pushNotificationReceivedFlag = NO;
        ApplicationDelegate.pushNotificationTypeID = 0;
    }

    
}

- (void)alert:(NSString *)message title:(NSString *)title
{
    //    NSLog(@"displaying alert. title: %@, message: %@", title, message);
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle: title
                              message: message
                              delegate: self
                              cancelButtonTitle: kOK
                              otherButtonTitles: nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Show alert here
        [alertView show];
    });
    //    [alertView show];
}
-(void)postDeviceToken
{
    if (deviceTokenString.length>0)
    {
        NSMutableDictionary *pushDataDic = [[NSMutableDictionary alloc] initWithDictionary:[self getPushNotificationPostDictionaryData]];
        if (pushDataDic.count>0)
        {
            __block BOOL dataFound = NO;
            
            [self.engine sendPushNotificationDeviceTokenDataWithDataDictionary:pushDataDic CompletionHandler:^(NSMutableDictionary *responseDictionary)
             {
                 if (!dataFound)
                 {
                     dataFound = YES;
                     
                     //NSLog(@"%@",responseDictionary);
                     
                 }
                 
                 
             } errorHandler:^(NSError *error) {
                 
             }];
        }
        
    }
}
-(NSMutableDictionary *)getPushNotificationPostDictionaryData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    if (self.deviceTokenString.length>0)
    {
        [postDic setObject:self.deviceTokenString forKey:@"pushtoken"];
        [postDic setObject:[NSNumber numberWithInteger:1] forKey:@"pushplatform"];
    }
    
    return postDic;
}

#pragma mark - FOR NEWS LETTER SUBSCRIPTION

-(BOOL)readNewsLetterStatusFromPreference
{
    BOOL status = NO;
    
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *userDic = [[NSMutableDictionary alloc] initWithDictionary:[defaults objectForKey:@"user"]];
    
    if (userDic.count==0)
    {
        [userDic setObject:[NSNumber numberWithBool:status] forKey:@"newsletterStatus"];
        
        [defaults setObject:userDic forKey:@"user"];
        
        [defaults synchronize];
    }
    else
    {
        if ([ApplicationDelegate isValid:[userDic objectForKey:@"newsletterStatus"]])
        {
            if ([[userDic objectForKey:@"newsletterStatus"] boolValue])
            {
                status = YES;
            }
        }
    }
    */
    
    NSString *plistPath= [self fileWithFilename:@"UserDetails.plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
    //UserData  *userObj=[self.mapper getUserDataFromDictionary:data];
    
    UserData  *userObj = [[UserData alloc] init];
    if(![ApplicationDelegate isValid:[data objectForKey:@"Authkey"]] )
    {
        userObj.authKey=@"";
    }
    else
    {
        userObj.authKey=[data objectForKey:@"Authkey"];
    }
    if(![ApplicationDelegate isValid:[data objectForKey:@"Status"]] )
    {
        userObj.loginStatus=@"";
    }
    else
    {
        userObj.loginStatus=[data objectForKey:@"Status"];
    }
    if(![ApplicationDelegate isValid:[data objectForKey:@"UserName"]] )
    {
        userObj.userName=@"";
    }
    else
    {
        userObj.userName=[data objectForKey:@"UserName"];
    }
    if(![ApplicationDelegate isValid:[data objectForKey:@"newsletterStatus"]] )
    {
        userObj.newsletterStatus = NO;
    }
    else
    {
        userObj.newsletterStatus=[[data objectForKey:@"newsletterStatus"] boolValue];
    }
    
    if (![userObj.loginStatus isEqualToString:@"success"])
    {
        [data setObject:[NSNumber numberWithBool:status] forKey:@"newsletterStatus"];
        
        [data writeToFile:plistPath atomically:YES];
        
    }
    else
    {
        status = userObj.newsletterStatus;
    }
    
    return status;
}

-(void)saveNewsLetterStatusWithStatus:(BOOL)status
{
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *userDic = [[NSMutableDictionary alloc] initWithDictionary:[defaults objectForKey:@"user"]];
    
    [userDic setObject:[NSNumber numberWithBool:status] forKey:@"newsletterStatus"];
    
    [defaults setObject:userDic forKey:@"user"];
    
    [defaults synchronize];
    */
    
    UserData  *userObj = [[UserData alloc] init];
    NSString *plistPath= [self fileWithFilename:@"UserDetails.plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
    userObj=[self.mapper getUserDataFromDictionary:data];
    
    if ([userObj.loginStatus isEqualToString:@"success"])
    {
        [data setObject:[NSNumber numberWithBool:status] forKey:@"newsletterStatus"];
        
        [data writeToFile:plistPath atomically:YES];
    }
    
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //NOTIFICATION
    if (self.pushNotificationReceivedFlag)
    {
        if (alertView.tag==5)
        {
            if (buttonIndex==1)
            {
                if (self.pushNotificationTypeID==1)
                {
                    // PROMOTIONS
                    [self.homeNav.visibleViewController viewWillAppear:NO];
                }
                else if (self.pushNotificationTypeID==2)
                {
                    // NEW RESTAURANT
                    [self.homeNav.visibleViewController viewWillAppear:NO];
                }
                else
                {
                    
                }
            }
            else
            {
                
            }
        }
    }
    
}
#pragma mark - NOT USED
-(void)loadPromotionsFromPushNotification
{
    if (self.window.rootViewController==self.homeNav)
    {
        if ([self.subHomeNav.view superview])
        {
            if ([self.subHomeNav.visibleViewController isKindOfClass:[PromotionsViewController class]])
            {
                [self.subHomeNav.visibleViewController viewWillAppear:NO];
            }
            else
            {
                PromotionsViewController *promotionsVc = [[PromotionsViewController alloc] initWithNibName:@"PromotionsViewController" bundle:nil];
                promotionsVc.view.backgroundColor = [UIColor clearColor];
                
                if (![self.subHomeNav.visibleViewController isKindOfClass:[promotionsVc class]])
                {
                    [self.subHomeNav pushViewController:promotionsVc animated:NO];
                }
            }
        }
    }
}
-(void)loadNewRestaurantFromPushNotification
{
    if (self.window.rootViewController==self.homeNav)
    {
        if ([self.subHomeNav.view superview])
        {
            if ([self.subHomeNav.visibleViewController isKindOfClass:[NewRestaurantsVC class]])
            {
                [self.subHomeNav.visibleViewController viewWillAppear:NO];
            }
            else
            {
                NewRestaurantsVC *newRestaurantsVc = [[NewRestaurantsVC alloc] initWithNibName:@"NewRestaurantsVC" bundle:nil];
                newRestaurantsVc.view.backgroundColor = [UIColor clearColor];
                
                if (![self.subHomeNav.visibleViewController isKindOfClass:[newRestaurantsVc class]])
                {
                    [self.subHomeNav pushViewController:newRestaurantsVc animated:NO];
                }
            }
        }
    }
}

-(void)changeLanguage
{
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        ApplicationDelegate.language=kENGLISH;
        [[Localizations sharedInstance] setPreferred:@"en" fallback:@"ar"];
    }
    
    else
    {
        ApplicationDelegate.language=kARABIC;
        [[Localizations sharedInstance] setPreferred:@"ar" fallback:@"en"];
    }
}
@end
