//
//  HomeTabViewController.h
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface HomeTabViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ListSelectionProtocol>

+(HomeTabViewController *)sharedViewController;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;
@property (strong, nonatomic) IBOutlet UIImageView *navigationBackImage;
@property (weak, nonatomic) IBOutlet UIButton *searchFilterButton;
@property (weak, nonatomic) IBOutlet UIButton *mostSellingButton;
@property (weak, nonatomic) IBOutlet UIButton *allRestaurantsButton;
@property (weak, nonatomic) IBOutlet UIButton *uNewrestaurantsButton;
@property (weak, nonatomic) IBOutlet UIImageView *u3an_InnerPages_Bg;
@property (strong, nonatomic) DropDownView *mostSellingDropDownObj;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;

@property (strong, nonatomic) IBOutlet UIView *moreMenuView;
@property (strong, nonatomic) IBOutlet UITableView *moreMenuTable;
@property (strong, nonatomic) IBOutlet UIView *moreMenuBannerView;
@property (strong, nonatomic) IBOutlet UIImageView *moreMenuBannerImageView;

@property (strong, nonatomic) IBOutlet UIImageView *moreMenuViewBg;
@property (weak, nonatomic) IBOutlet UIView *mostSellingHeaderButtonView;
@property (weak, nonatomic) IBOutlet UILabel *mostSellingBttnLabel;
@property (weak, nonatomic) IBOutlet UIButton *mostSellingDropDownBttn;

@property (strong, nonatomic) IBOutlet UIView *cartControlView;
@property (strong, nonatomic) IBOutlet UILabel *cartCountLabel;
- (IBAction)cartButtonAction:(id)sender;


-(void)resetTabSelection;
-(void)updateMoreMenuItems;

- (IBAction)moreMenuBannerAction:(id)sender;

- (IBAction)footerButtonAction:(id)sender;
- (IBAction)navigationBackBttnAction:(UIButton *)sender;
- (IBAction)mostSellingDropDwnBttnAction:(UIButton *)sender;
- (IBAction)filterButtonAction:(UIButton *)sender;

@end
