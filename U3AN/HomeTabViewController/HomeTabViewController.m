    //
//  HomeTabViewController.m
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "HomeTabViewController.h"
#import "MostSellingDishesVC.h"
#import "AllRestaurantsVC.h"
#import "NewRestaurantsVC.h"
#import "OrderDishesViewController.h"
#import "PromotionsViewController.h"
#import "ConsultationViewController.h"
#import "FAQViewController.h"
#import "FeedBackViewController.h"
#import "PrivacyViewController.h"
#import "AddRestaurantViewController.h"
#import "TermsViewController.h"
#import "SocialResponsibilityViewController.h"
#import "SettingsViewController.h"
#import "LaunchingViewController.h"
#import "SearchViewController.h"
#import "LoginViewController.h"
#import "GiftVoucherViewController.h"
#import "U3ANCreditViewController.h"
#import "LiveChatViewController.h"
#import "AccountInformationVC.h"
#import "OrderConfirmationResultVC.h"
#import "RestaurantMenuDetailVC.h"
#import "MyAccountVC.h"

#import "RestaurantDetailsVC.h"
#import "Banner.h"

#import "CartListViewController.h"

static HomeTabViewController *sharedObj = NULL;

@interface HomeTabViewController ()
{
    NSArray *moreMenuListArray,*moreMenuNormalIconsArray,*moreMenuSelectedIconsArray;
    BOOL moreMenuItemSelectionFlag;
    NSUInteger moreItemSelectedIndex;
    NSMutableArray *dropDownNamesArray;
    NSMutableDictionary *bannerDetails;
}

@end

@implementation HomeTabViewController

+(HomeTabViewController *)sharedViewController
{
    
    if ( !sharedObj || sharedObj == NULL )
    {
        
        sharedObj = [[HomeTabViewController alloc] initWithNibName:@"HomeTabViewController" bundle:nil];
    }
    
    return sharedObj;
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //ApplicationDelegate.language =kARABIC;
    //NSLog(@"%@",ApplicationDelegate.language);
    
    self.cartCountLabel.layer.cornerRadius = 5.0;
    self.cartCountLabel.layer.masksToBounds = YES;
    self.cartCountLabel.hidden = YES;
    
    [self updateMoreMenuItems];
    bannerDetails = [[NSMutableDictionary alloc] init];
    
    [self.headingLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:16.0f]];
    [self.mostSellingBttnLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self footerUITabArrangement];
    
    [self fetchBannerDetails];
    
    [self viewAdjustmentForiOS7StatusBarIssue];
    
    if (ApplicationDelegate.pushNotificationReceivedFlag)
    {
        
        if (ApplicationDelegate.pushNotificationTypeID==1)
        {
            //PROMOTIONS
            
            moreItemSelectedIndex=2;
            moreMenuItemSelectionFlag = YES;
            [self selectMoreMenuItem:2];
        }
        else if (ApplicationDelegate.pushNotificationTypeID==2)
        {
            //NEW RESTAURANT FROM PUSH NOTIFICATION
            [self footerButtonAction:self.uNewrestaurantsButton];
        }
        else
        {
            
        }
        ApplicationDelegate.pushNotificationTypeID = 0;
        ApplicationDelegate.pushNotificationReceivedFlag = NO;
    }
    else
    {
      [self loadInitialOrderView];
    }
}

-(void)viewAdjustmentForiOS7StatusBarIssue
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue]<7.0)
    {
        self.headerView.frame = CGRectMake(0, 0, self.headerView.frame.size.width, 40);
    }
    self.containerView.frame = CGRectMake(self.containerView.frame.origin.x,(self.headerView.frame.size.height), self.containerView.frame.size.width,(self.view.frame.size.height-self.headerView.frame.size.height-self.footerView.frame.size.height) );
    
    self.u3an_InnerPages_Bg.frame = self.containerView.frame;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)footerButtonAction:(id)sender {
    
    [self didSelectTabItemAtIndex:[sender tag]];
}

- (IBAction)navigationBackBttnAction:(UIButton *)sender
{
    if ([ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[OrderConfirmationResultVC class]])
    {
        if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
        {
            BOOL dataFound = NO;
            
            NSInteger detailVcLatestIndex = 0;
            
            for (UIViewController *vc in ApplicationDelegate.subHomeNav.viewControllers)
            {
                if ([ApplicationDelegate isValid:vc])
                {
                    if ([vc isKindOfClass:[RestaurantMenuDetailVC class]])
                    {
                        dataFound = YES;
                        detailVcLatestIndex = [ApplicationDelegate.subHomeNav.viewControllers indexOfObject:vc];
                    }
                }
                
            }
            
            if (dataFound)
            {
                if ([ApplicationDelegate isValid:[ApplicationDelegate.subHomeNav.viewControllers objectAtIndex:detailVcLatestIndex]])
                {
                    [ApplicationDelegate.subHomeNav popToViewController:[ApplicationDelegate.subHomeNav.viewControllers objectAtIndex:detailVcLatestIndex] animated:NO];
                }
            }
            
        }
    }
    else
    {
        if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
        {
            [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
        }
    }
    
}
    
    
//if([[ApplicationDelegate.subHomeNav.viewControllers objectAtIndex:ApplicationDelegate.subHomeNav.viewControllers.count-3 ] isKindOfClass:[AccountInformationVC class]])

   // [self contentHeaderUIArrangement];


- (IBAction)mostSellingDropDwnBttnAction:(UIButton *)sender {
    
    dropDownNamesArray = [[NSMutableArray alloc] initWithObjects:localize(@"Most Selling"),localize(@"By Restaurant"),localize(@"By Cuisine"), nil];

        self.mostSellingDropDownBttn.selected = !self.mostSellingDropDownBttn.selected;
    
        if (self.mostSellingDropDownBttn.selected)
        {
            if (self.mostSellingDropDownObj.view.superview) {
                [self.mostSellingDropDownObj.view removeFromSuperview];
            }
            
            self.mostSellingDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
            
            //////////// Customize the dropDown view Frame as your requirement
            
            self.mostSellingDropDownObj.cellHeight = 35.0f;
            
//            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
//            {
                self.mostSellingDropDownObj.view.frame = CGRectMake((self.mostSellingHeaderButtonView.frame.origin.x),(self.mostSellingHeaderButtonView.frame.origin.y+self.mostSellingHeaderButtonView.frame.size.height), (self.mostSellingHeaderButtonView.frame.size.width), 0);

            self.mostSellingDropDownObj.dropDownDelegate = self;
            self.mostSellingDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:dropDownNamesArray];
            
            self.mostSellingDropDownObj.view.layer.borderWidth = 0.1;
            self.mostSellingDropDownObj.view.layer.shadowOpacity = 1.0;
            self.mostSellingDropDownObj.view.layer.shadowRadius = 5.0;
            self.mostSellingDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
            
            ///////Disabling Other part of the view except the DropDown View 	Container////////
//            for (UIView *vw in self.view.subviews) {
//                if (vw!=self.self.mostSellingHeaderButtonView) {
//                    [vw setUserInteractionEnabled:NO];
//                }
//            }
            //////////////
            self.mostSellingDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
            self.mostSellingDropDownObj.textLabelColor = [UIColor whiteColor];
            self.mostSellingDropDownObj.view.backgroundColor = [UIColor brownColor];
            
            if (dropDownNamesArray.count>0)
            {
                [self.view addSubview:self.mostSellingDropDownObj.view];
            }
            else
            {
                self.mostSellingDropDownBttn.selected=NO;
            }
            
            /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
            CGFloat tableHT = 140.0f;
            
            if ((self.mostSellingDropDownObj.cellHeight*self.mostSellingDropDownObj.dataArray.count)<tableHT)
            {
                tableHT = (self.mostSellingDropDownObj.cellHeight*self.mostSellingDropDownObj.dataArray.count)+18.0f;
            }
            
            [UIView animateWithDuration:0.09f animations:^{
                self.mostSellingDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
                self.mostSellingDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
                self.mostSellingDropDownObj.view.frame =
                CGRectMake(self.mostSellingDropDownObj.view.frame.origin.x+2,
                           self.mostSellingDropDownObj.view.frame.origin.y,
                           self.mostSellingDropDownObj.view.frame.size.width-4,
                           tableHT);
            }];
        }
        else
        {
            self.mostSellingDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
            self.mostSellingDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
            
            ////////// ReEnabling the UserInteraction of all SubViews/////////
            for (UIView *vw in self.view.subviews) {
                [vw setUserInteractionEnabled:YES];
            }
            ///////////////////
            
            [UIView animateWithDuration:0.4f animations:^{
                self.mostSellingDropDownObj.view.frame =
                CGRectMake(self.mostSellingDropDownObj.view.frame.origin.x,
                           self.mostSellingDropDownObj.view.frame.origin.y,
                           self.mostSellingDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.mostSellingDropDownObj.view removeFromSuperview];
                }
                
            }];
        }
    

}

- (IBAction)filterButtonAction:(UIButton *)sender {
    
    if ([ApplicationDelegate.subHomeNav.viewControllers count]>0)
    {
        
        if ([ApplicationDelegate.subHomeNav.visibleViewController.nibName isEqualToString:@"SearchViewController"])
        {
            SearchViewController *searchVc = (SearchViewController *)ApplicationDelegate.subHomeNav.visibleViewController;
            searchVc.advance_Search_View.hidden=NO;
        }
    }

    
}


-(void)loadInitialOrderView
{
    [self resetTabSelection];
    for (UIView *vw in self.containerView.subviews)
    {
        [vw removeFromSuperview];
    }
//    MostSellingDishesVC *mostSellingVc = [[MostSellingDishesVC alloc] initWithNibName:@"MostSellingDishesVC" bundle:nil];
//    mostSellingVc.view.backgroundColor = [UIColor clearColor];
//    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:mostSellingVc];
//    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
//    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
//    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
//    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
//    [self.containerView addSubview:ApplicationDelegate.subHomeNav.view];
    self.headerView.hidden=YES;
    self.u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    for (UIView *vw in self.containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    [self.containerView addSubview:ApplicationDelegate.subHomeNav.view];

}

-(void)didSelectTabItemAtIndex:(NSInteger)itemIndex
{
    //self.headerView.hidden=NO;
   //self.u3an_InnerPages_Bg.hidden=NO;
    [self resetTabSelection];
  
    
    if (itemIndex==4)
    {
                //MORE ACTION
        self.moreButton.selected = !self.moreButton.selected;
        
                if ([self.moreMenuView superview])
                {
                    // HIDE MORE MENU
        
                    [self hideMoreMenu];
                    if ([ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[MostSellingDishesVC class]]) {
                        self.mostSellingButton.selected=YES;
                        self.moreButton.selected=NO;
                    }
                    
                    else if ([ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[AllRestaurantsVC class]])
                    {
                        self.allRestaurantsButton.selected=YES;
                        self.moreButton.selected=NO;
                    }
                    else if ([ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[NewRestaurantsVC class]])
                    {
                        self.uNewrestaurantsButton.selected=YES;
                        self.moreButton.selected=NO;
                    }
                
                }
                else
                {
                    // SHOW MORE MENU
        
                    [self showMoreMenu];
                }
    }
    else
    {
        self.headerView.hidden=NO;
        self.u3an_InnerPages_Bg.hidden=NO;
        moreMenuItemSelectionFlag = NO;
        if ([self.moreMenuView superview])
        {
            [self hideMoreMenu];
        }
        
        switch (itemIndex)
        {
            case 1:
            {
                //Load MostSellingDishes
                self.mostSellingButton.selected=YES;
                [self loadMostSellingDishesView];
                
                
            }
                break;
            case 2:
            {
                //Load All Restaurants
                self.allRestaurantsButton.selected=YES;
                [self loadAllRestaurantsView];
            }
                break;
            case 3:
            {
                //Load New Restaurants
                self.uNewrestaurantsButton.selected=YES;
                [self loadNewRestaurantsView];
            }
                break;
            default:
                break;
        }
    }
    
}

- (IBAction)cartButtonAction:(id)sender
{
    BOOL validCountFlag = NO;
    
    if (self.cartCountLabel.text.length>0)
    {
        if ([self.cartCountLabel.text longLongValue]>0)
        {
            validCountFlag = YES;
            
        }
    }
    
    if (validCountFlag)
    {
        CartListViewController *cartListViewController = [[CartListViewController alloc] initWithNibName:@"CartListViewController" bundle:nil];
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[cartListViewController class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:cartListViewController animated:NO];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:localize(@"Cart is empty. Please place your order.") title:@""];
    }
}

-(void)resetTabSelection
{
    self.mostSellingButton.selected=NO;
    self.allRestaurantsButton.selected=NO;
    self.uNewrestaurantsButton.selected=NO;
    self.moreButton.selected=NO;
}


-(void)loadMostSellingDishesView
{
    MostSellingDishesVC *mostSellingVc = [[MostSellingDishesVC alloc] initWithNibName:@"MostSellingDishesVC" bundle:nil]
    ;
    mostSellingVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[mostSellingVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:mostSellingVc animated:NO];
    }
    
}

-(void)loadAllRestaurantsView
{

    AllRestaurantsVC *allRestaurantsVc = [[AllRestaurantsVC alloc] initWithNibName:@"AllRestaurantsVC" bundle:nil];
    allRestaurantsVc.view.backgroundColor = [UIColor clearColor];
    allRestaurantsVc.isClickedOnBannerFlag = NO;
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[allRestaurantsVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:allRestaurantsVc animated:NO];
    }
    
}

-(void)loadNewRestaurantsView
{

    NewRestaurantsVC *newRestaurantsVc = [[NewRestaurantsVC alloc] initWithNibName:@"NewRestaurantsVC" bundle:nil];
    newRestaurantsVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[newRestaurantsVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:newRestaurantsVc animated:NO];
    }
    else
    {
        [ApplicationDelegate.subHomeNav.visibleViewController viewWillAppear:NO];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:touch.view];
    
    if (!CGRectContainsPoint(self.moreMenuView.frame, location))
    {
        if ([self.moreMenuView superview])
        {
            [self hideMoreMenu];
            
        }
    }
    [self.view endEditing:YES];
    [self.containerView endEditing:YES];
    
    if (self.mostSellingDropDownObj.view.superview) {
        [self.mostSellingDropDownObj.view removeFromSuperview];
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }

    }
    
}

#pragma mark - MORE MENU METHODS


-(void)updateMoreMenuItems
{
    if (!ApplicationDelegate.isLoggedIn)
    {
        moreMenuListArray = [[NSArray alloc] initWithObjects:localize(@"Language"),localize(@"Order Your Dishes"),localize(@"Live Chat"),localize(@"Promotions"),localize(@"Consultation"),localize(@"Add Restaurant"),localize(@"Feedback"),localize(@"Terms and conditions"),localize(@"FAQ"),localize(@"Privacy"),localize(@"Social Responsibility"),localize(@"Settings"),nil];
        
        moreMenuNormalIconsArray = [[NSArray alloc] initWithObjects:@"language.png",@"order.png",@"livechat.png",@"promotions.png",@"consultation.png",@"add_restuarant.png",@"feeding.png",@"terms.png",@"faq.png",@"privacy.png",@"social-responsibility.png",@"settings2.png", nil];
        
        moreMenuSelectedIconsArray = [[NSArray alloc] initWithObjects:@"language_sel.png",@"order_sel.png",@"livechat_sel.png",@"promotions_sel.png",@"consultation_sel.png",@"add_restuarant_sel.png",@"feeding_sel.png",@"terms_sel.png",@"faq_sel.png",@"privacy_sel.png",@"social-responsibility_sel.png",@"settings2_sel.png", nil];
    }
    else
    {
        
        moreMenuListArray = [[NSArray alloc] initWithObjects:localize(@"Language"),localize(@"My Account"),localize(@"Order Your Dishes"),localize(@"Live Chat"),localize(@"Promotions"),localize(@"U3AN Credit"),localize(@"Gift Voucher"),localize(@"Consultation"),localize(@"Add Restaurant"),localize(@"Feedback"),localize(@"Terms and conditions"),localize(@"FAQ"),localize(@"Privacy"),localize(@"Social Responsibility"),localize(@"Settings"),nil];
        moreMenuNormalIconsArray = [[NSArray alloc] initWithObjects:@"language.png",@"myaccount123.png",@"order.png",@"livechat.png",@"promotions.png",@"credit.png",@"gift.png",@"consultation.png",@"add_restuarant.png",@"feeding.png",@"terms.png",@"faq.png",@"privacy.png",@"social-responsibility.png",@"settings2.png", nil];
        moreMenuSelectedIconsArray = [[NSArray alloc] initWithObjects:@"language_sel.png",@"myaccount123_sel.png",@"order_sel.png",@"livechat_sel.png",@"promotions_sel.png",@"credit_sel.png",@"gift_sel.png",@"consultation_sel.png",@"add_restuarant_sel.png",@"feeding_sel.png",@"terms_sel.png",@"faq_sel.png",@"privacy_sel.png",@"social-responsibility_sel.png",@"settings2_sel.png", nil];
    }
    
}



-(NSMutableDictionary *)getRestaurantPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:kCountryId forKey:@"countryId"];
    [postDic setObject:@"" forKey:@"cuisineId"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
//    [postDic setObject:kLocal forKey:@"locale"];
    [postDic setObject:@"" forKey:@"restName"];
    
    return postDic;
}

-(void)showMoreMenu
{
    [self fetchBannerDetails];
    
    self.containerView.userInteractionEnabled = NO;
    
    float xFact,moreMenuViewHeight;
    
    // MORE VIEW HEIGHT ADJUSTMENT
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        moreMenuViewHeight = 340.0f;
        self.moreMenuTable.scrollEnabled=YES;
    }
    else
    {
        
        moreMenuViewHeight = 425.0f;
    }
    
    float xPos = - self.moreMenuView.frame.size.width;
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        xPos = self.view.frame.size.width;
    }
    
    self.moreMenuView.frame = CGRectMake(xPos, (self.view.frame.size.height-moreMenuViewHeight-self.footerView.frame.size.height-1), self.moreMenuView.frame.size.width, moreMenuViewHeight);
    
    
    xFact = 0;
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        xFact=self.view.frame.size.width-self.moreMenuView.frame.size.width;
    }
    
    self.moreMenuTable.delegate=self;
    self.moreMenuTable.dataSource=self;
    
    [self.view insertSubview:self.moreMenuView atIndex:6];
    
    
    [UIView beginAnimations:@"animateView" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect viewFrame = [self.moreMenuView frame];
    viewFrame.origin.x  = xFact;
    self.moreMenuView.frame = viewFrame;
    self.moreMenuViewBg.alpha = 1.0f;
    [UIView commitAnimations];
    [self.moreMenuTable reloadData];
    
}
-(void)hideMoreMenu
{
    self.containerView.userInteractionEnabled = YES;
    
    [UIView beginAnimations:@"animateView" context:nil];
    CGRect viewFrame = [self.moreMenuView frame];
    
    viewFrame.origin.x = - self.moreMenuView.frame.size.width;
    
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        viewFrame.origin.x  = self.view.frame.size.width;
    }
    
    
    
    [UIView setAnimationDuration:0.5];
    self.moreMenuView.frame = viewFrame;
    [UIView commitAnimations];
    [self performSelector:@selector(removeMoreMenu) withObject:nil afterDelay:0.5];
}

-(void)removeMoreMenu
{
    self.containerView.userInteractionEnabled = YES;
    [self.moreMenuView removeFromSuperview];
}

-(NSString *)getMoreTabItemTitle:(NSUInteger)itemIndex
{
    NSString *titleString=@"";
    titleString = [moreMenuListArray objectAtIndex:itemIndex];
    
    return titleString;
}
-(void)fetchBannerDetails
{
    __block BOOL dataFound = NO;
    
    bannerDetails = [[NSMutableDictionary alloc] init];
    
    self.moreMenuBannerImageView.image = nil;
    
    [ApplicationDelegate.engine getBannerDetailsFromServerWithCompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if (!dataFound)
         {
             dataFound = YES;
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     bannerDetails = [[NSMutableDictionary alloc] initWithDictionary:responseDictionary];
                     [self prepareBannerView];
                 }
             }
             
         }
        
    } errorHandler:^(NSError *error)
     {
        
    }];
}
-(void)prepareBannerView
{
    self.moreMenuBannerImageView.image = nil;
    
    if ([ApplicationDelegate isValid:bannerDetails])
    {
        if (bannerDetails.count>0)
        {
            Banner *banner = [ApplicationDelegate.mapper getBannerObjectFromDictionary:bannerDetails];
            
            if ((banner.restaurantID.length>0)&&(banner.bannerImageUrl.length>0))
            {
                [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:banner.bannerImageUrl] completionHandler:^(UIImage *responseImage)
                 {
                     self.moreMenuBannerImageView.image = responseImage;
                    
                } errorHandler:^(NSError *error) {
                    
                }];
            }
            
        }
    }
    else
    {
        bannerDetails = [[NSMutableDictionary alloc] init];
        
    }
}

- (IBAction)moreMenuBannerAction:(id)sender
{
 
    if ([self.moreMenuView superview])
    {
        [self hideMoreMenu];
    }
    
    if ([ApplicationDelegate isValid:bannerDetails])
    {
        if (bannerDetails.count>0)
        {
            [self resetTabSelection];
            
            self.headerView.hidden=NO;
            self.u3an_InnerPages_Bg.hidden=NO;
            moreMenuItemSelectionFlag = NO;
            
            
            self.allRestaurantsButton.selected=YES;
            // [self loadAllRestaurantsView];
            
            Banner *banner = [ApplicationDelegate.mapper getBannerObjectFromDictionary:bannerDetails];
            
            if ((banner.restaurantID.length>0)&&(banner.bannerImageUrl.length>0))
            {
                AllRestaurantsVC *allRestaurantsVc = [[AllRestaurantsVC alloc] initWithNibName:@"AllRestaurantsVC" bundle:nil];
                allRestaurantsVc.view.backgroundColor = [UIColor clearColor];
                allRestaurantsVc.isClickedOnBannerFlag = YES;
                allRestaurantsVc.bannerRestID = banner.restaurantID;
                if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[allRestaurantsVc class]])
                {
                    [ApplicationDelegate.subHomeNav pushViewController:allRestaurantsVc animated:NO];
                }
            }
            
        }
        else
        {
            if ([ApplicationDelegate isValid:self.moreMenuBannerImageView.image])
            {
                [ApplicationDelegate showAlertWithMessage:localize(@"No data available. Please try later.") title:nil];
            }
        }
    }
}
#pragma mark - TABLEVIEW DELEGATE METHODS
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return moreMenuListArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        return 38.0f;
    }
    else
    {
        return 38.5f;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        // Configure the cell...
        
        cell.selectionStyle = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        UIImageView *iconImgView;
        UILabel *nameLabel;
        
        if ([UIScreen mainScreen].bounds.size.height==480) // FOR IPHONE UPTO 4S SCREEN SIZE
        {
            
            iconImgView= [[UIImageView alloc] initWithFrame:CGRectMake(12, 9, 26, 26)];
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(48, 9, (tableView.frame.size.width-58), 26)];
            
        }
        else // FOR IPHONE 5 SCREEN SIZE ONWARDS
        {
            iconImgView= [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 38, 38)];
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(56, 8, (tableView.frame.size.width-63), 38)];
            
        }
        
        
        iconImgView.backgroundColor = [UIColor clearColor];
        iconImgView.tag=50;
        nameLabel.tag=60;
        [cell addSubview:iconImgView];
        [cell addSubview:nameLabel];
    }
    
    UIImageView *cellImage = (UIImageView *)[cell viewWithTag:50];
    cellImage.backgroundColor = [UIColor clearColor];
    
    UILabel* cellLabel = (UILabel*)[cell viewWithTag: 60];
    [cellLabel setBackgroundColor:[UIColor clearColor]];
    cellLabel.text = [self getMoreTabItemTitle:indexPath.row];
    
    if ([UIScreen mainScreen].bounds.size.height==480) // FOR IPHONE UPTO 4S SCREEN SIZE
    {
        float xIconPos = 12.0f;
        float xLabelPos = 48.0f;
        cellLabel.textAlignment = NSTextAlignmentLeft;
        if ([ApplicationDelegate.language isEqualToString:kARABIC])
        {
            xIconPos = self.moreMenuTable.frame.size.width - 36;
            xLabelPos = 10;
            cellLabel.textAlignment = NSTextAlignmentRight;
        }
        cellImage.frame=CGRectMake(xIconPos, 9, 26, 26);
        cellLabel.frame = CGRectMake(xLabelPos, 9, (tableView.frame.size.width-58), 26);
        
    }
    else // FOR IPHONE 5 SCREEN SIZE ONWARDS
    {
        float xIconPos = 10.0f;
        float xLabelPos = 56.0f;
        cellLabel.textAlignment = NSTextAlignmentLeft;
        if ([ApplicationDelegate.language isEqualToString:kARABIC])
        {
            xIconPos = self.moreMenuTable.frame.size.width - 40;
            xLabelPos = 10;
            cellLabel.textAlignment = NSTextAlignmentRight;
        }
        cellImage.frame=CGRectMake(xIconPos, 12, 30, 30);
        cellLabel.frame = CGRectMake(xLabelPos, 12, (tableView.frame.size.width-60), 30);
        
    }
    
    
    if (moreMenuItemSelectionFlag &&(moreItemSelectedIndex==indexPath.row))
    {
        cellLabel.textColor = [UIColor whiteColor];
        cellImage.image = [UIImage imageNamed:[moreMenuSelectedIconsArray objectAtIndex:indexPath.row]];
    }
    else
    {
        cellLabel.textColor = [UIColor whiteColor];
        cellImage.image = [UIImage imageNamed:[moreMenuNormalIconsArray objectAtIndex:indexPath.row]];
        
    }
    cellLabel.textColor = [UIColor whiteColor];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    moreItemSelectedIndex=indexPath.row;
    moreMenuItemSelectionFlag = YES;
    
    //    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self selectMoreMenuItem:indexPath.row];
    [self.moreMenuTable reloadData];
}
#pragma mark - METHODS FOR LOADING VIEWS FROM MORE TAB

-(void)selectMoreMenuItem:(NSInteger)index
{
    self.headerView.hidden=NO;
    self.u3an_InnerPages_Bg.hidden=NO;
    
    [UIView beginAnimations:@"animateView" context:nil];
    CGRect viewFrame = [self.moreMenuView frame];
    
    viewFrame.origin.x = - self.moreMenuView.frame.size.width;
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        viewFrame.origin.x  = self.view.frame.size.width;
    }
    
    self.moreMenuView.frame = viewFrame;
    [UIView setAnimationDuration:0.5];
    [UIView commitAnimations];
    [self performSelector:@selector(removeMoreMenu) withObject:nil afterDelay:0.5];
    
    if (ApplicationDelegate.isLoggedIn) {
        switch (index)
        {
            case 0:
            {
                [self loadLanguageChanger];

            }
                break;
            case 1:
            {
                // Order
                [self loadMyAccount];
            }
                break;
            case 2:
            {
                // Live Chat
                [self loadOrderDishesView];
            }
                break;
            case 3:
            {
                // Live Chat
                [self loadLiveChatView];
            }
                break;
            case 4:
            {
                // Promotions
                [self loadPromotionsView];
            }
                break;
            case 5:
            {
                // U3AN Credit
                [self loadU3ANCreditView];
            }
                break;
            case 6:
            {
                //Gift Voucher
                [self loadGiftVoucherView];
                
            }
                break;
            case 7:
            {
                // Consultation
                [self loadConsultationView];
            }
                break;
                
            case 8:
            {
                //Add Restaurants
                [self loadAddRestaurantView];
                
            }
                break;
            case 9:
            {
                // FeedBack
                [self loadFeedbackView];
            }
                break;
            case 10:
            {
                // Terms
                [self loadTermsView];
            }
                break;
            case 11:
            {
                // FAQ
                [self loadFAQView];
            }
                break;
            case 12:
            {
                //Privacy
                [self loadPrivacyView];
                
            }
                break;
            case 13:
            {
                //Social Responsibility
                [self loadSocialResponsibilityView];
            }
                break;
            
            case 14:
            {
                //Settings
                [self loadSettingsView];
            }
                break;
                
            default:
                break;
                
        }
    }
    else
    {
    switch (index)
    {
        case 0:
        {
            [self loadLanguageChanger];

            
        }
            break;
        case 1:
        {
            // Order
            [self loadOrderDishesView];
        }
            break;
        case 2:
        {
            // Live Chat
            [self loadLiveChatView];
        }
            break;
        case 3:
        {
            // Promotions
            [self loadPromotionsView];
        }
            break;
        case 4:
        {
            // Consultation
            [self loadConsultationView];
        }
            break;
            
        case 5:
        {
            //Add Restaurants
             [self loadAddRestaurantView];
            
        }
            break;
        case 6:
        {
            // FeedBack
            [self loadFeedbackView];
        }
            break;
        case 7:
        {
            // Terms
            [self loadTermsView];
        }
            break;
        case 8:
        {
            // FAQ
            [self loadFAQView];
        }
            break;
        case 9:
        {
            //Privacy
             [self loadPrivacyView];
            
        }
            break;
        case 10:
        {
            //Social Responsibility
            [self loadSocialResponsibilityView];
        }
            break;
        
        case 11:
        {
            //Settings
            [self loadSettingsView];
        }
            break;
            
        default:
            break;
            
    }
    }
}

-(void)loadLanguageChanger{
    ApplicationDelegate.changeLanguage;
    [self footerUITabArrangement];
    [self viewDidLoad];
    [self viewWillAppear:YES];
    //[self viewDidAppear:YES];
}

-(void)loadMyAccount{
    MyAccountVC *myAccountVC = [[MyAccountVC alloc] initWithNibName:@"MyAccountVC" bundle:nil]
    ;
    myAccountVC.view.backgroundColor = [UIColor clearColor];
    
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[myAccountVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:myAccountVC animated:NO];
    }
}

-(void)loadOrderDishesView
{
//    OrderDishesViewController *orderDishesVc = [[OrderDishesViewController alloc] initWithNibName:@"OrderDishesViewController" bundle:nil]
//    ;
//    orderDishesVc.view.backgroundColor = [UIColor clearColor];
//    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[orderDishesVc class]])
//    {
//        [ApplicationDelegate.subHomeNav pushViewController:orderDishesVc animated:NO];
//    }
    
    
#pragma mark - CHANGED by RATHEESH
    
    //********************************* TO REST BACK TO PREV CHANGE ---- UNCOMMENT THE /*---*/ in this method. COMMENT THE PUSH_VIEWCONTROLLER STATMENTS INCLUDING CONDITION ***************************************************
    
    /*
    [self resetTabSelection];
    for (UIView *vw in self.containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    */
    
    
    //    MostSellingDishesVC *mostSellingVc = [[MostSellingDishesVC alloc] initWithNibName:@"MostSellingDishesVC" bundle:nil];
    //    mostSellingVc.view.backgroundColor = [UIColor clearColor];
    //    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:mostSellingVc];
    //    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    //    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    //    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    //    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    //    [self.containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
    self.headerView.hidden=YES;
    self.u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    
    
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[launchingVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:launchingVc animated:NO];
    }
    else
    {
        [ApplicationDelegate.subHomeNav.visibleViewController viewWillAppear:NO];
    }
    

    /*
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
    for (UIView *vw in self.containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    [self.containerView addSubview:ApplicationDelegate.subHomeNav.view];
    */
}

-(void)loadPromotionsView
{
    
    PromotionsViewController *poromotionsVc = [[PromotionsViewController alloc] initWithNibName:@"PromotionsViewController" bundle:nil];
    poromotionsVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[poromotionsVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:poromotionsVc animated:NO];
    }
    else
    {
        [ApplicationDelegate.subHomeNav.visibleViewController viewWillAppear:NO];
    }
    
}

-(void)loadConsultationView
{
    
    ConsultationViewController *consultationVc = [[ConsultationViewController alloc] initWithNibName:@"ConsultationViewController" bundle:nil];
    consultationVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[consultationVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:consultationVc animated:NO];
    }
    
}

-(void)loadAddRestaurantView
{
     AddRestaurantViewController *addRestaurantVc = [[AddRestaurantViewController alloc] initWithNibName:@"AddRestaurantViewController" bundle:nil]
    ;
    addRestaurantVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[addRestaurantVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:addRestaurantVc animated:NO];
    }
    
}

-(void)loadFeedbackView
{
    
    FeedBackViewController *feedBackVc = [[FeedBackViewController alloc] initWithNibName:@"FeedBackViewController" bundle:nil];
    feedBackVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[feedBackVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:feedBackVc animated:NO];
    }
    
}

-(void)loadTermsView
{
    
    TermsViewController *termsVc = [[TermsViewController alloc] initWithNibName:@"TermsViewController" bundle:nil];
    termsVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[termsVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:termsVc animated:NO];
    }
    
}
-(void)loadFAQView
{
    FAQViewController *faqVc = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:nil]
    ;
    faqVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[faqVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:faqVc animated:NO];
    }
    
}

-(void)loadPrivacyView
{
    
    PrivacyViewController *privacyVc = [[PrivacyViewController alloc] initWithNibName:@"PrivacyViewController" bundle:nil];
    privacyVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[privacyVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:privacyVc animated:NO];
    }
    
}

-(void)loadSocialResponsibilityView
{
    
    SocialResponsibilityViewController *socialVc = [[SocialResponsibilityViewController alloc] initWithNibName:@"SocialResponsibilityViewController" bundle:nil];
    socialVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[socialVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:socialVc animated:NO];
    }
    
}
-(void)loadU3ANCreditView
{
    
    U3ANCreditViewController *creditVc = [[U3ANCreditViewController alloc] initWithNibName:@"U3ANCreditViewController" bundle:nil];
    creditVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[creditVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:creditVc animated:NO];
    }
    
}
-(void)loadGiftVoucherView
{
    
    GiftVoucherViewController *giftVoucherVc = [[GiftVoucherViewController alloc] initWithNibName:@"GiftVoucherViewController" bundle:nil];
    giftVoucherVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[giftVoucherVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:giftVoucherVc animated:NO];
    }
    
}
-(void)loadSettingsView
{
    
    SettingsViewController *settingVc = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    settingVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[settingVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:settingVc animated:NO];
    }
//    LoginViewController *loginVc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//    loginVc.view.backgroundColor = [UIColor clearColor];
//    
//    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[loginVc class]])
//    {
//        [ApplicationDelegate.subHomeNav pushViewController:loginVc animated:NO];
//    }
   
}

-(void)loadLiveChatView
{
    
    LiveChatViewController *socialVc = [[LiveChatViewController alloc] initWithNibName:@"LiveChatViewController" bundle:nil];
    socialVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[socialVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:socialVc animated:NO];
    }
    
}

#pragma mark - DROP-DOWN List Delegate Method

-(void)selectList:(int)selectedIndex
{
    
    ////////// ReEnabling the UserInteraction of all SubViews/////////
    for (UIView *vw in self.view.subviews) {
        [vw setUserInteractionEnabled:YES];
    }
    ///////////////////
    
    [UIView animateWithDuration:0.4f animations:^{
        self.mostSellingDropDownObj.view.frame =
        CGRectMake(self.mostSellingDropDownObj.view.frame.origin.x,
                   self.mostSellingDropDownObj.view.frame.origin.y,
                   self.mostSellingDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.mostSellingDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.mostSellingDropDownBttn.selected = NO;
    
    self.mostSellingBttnLabel.text =[dropDownNamesArray objectAtIndex:selectedIndex];
   
    if ([ApplicationDelegate.subHomeNav.viewControllers count]>0)
    {
        
        if ([ApplicationDelegate.subHomeNav.visibleViewController.nibName isEqualToString:@"MostSellingDishesVC"])
        {
            [ApplicationDelegate.subHomeNav.visibleViewController viewWillAppear:YES];
        }
    }
    
}

-(void)footerUITabArrangement
{
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [self.mostSellingButton setImage:[UIImage imageNamed:@"mostsellingdishes"] forState:UIControlStateNormal];
        [self.mostSellingButton setImage:[UIImage imageNamed:@"mostsellingdishes_sel"] forState:UIControlStateSelected];
        [self.allRestaurantsButton setImage:[UIImage imageNamed:@"allrestaurants"] forState:UIControlStateNormal];
        [self.allRestaurantsButton setImage:[UIImage imageNamed:@"allrestaurants_sel"] forState:UIControlStateSelected];
        [self.uNewrestaurantsButton setImage:[UIImage imageNamed:@"newrestuart"] forState:UIControlStateNormal];
        [self.uNewrestaurantsButton setImage:[UIImage imageNamed:@"newrestuart_sel"] forState:UIControlStateSelected];
        [self.moreButton setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:@"more_sel"] forState:UIControlStateSelected];
        
        self.mostSellingButton.frame = CGRectMake(0, self.mostSellingButton.frame.origin.y, self.mostSellingButton.frame.size.width, self.mostSellingButton.frame.size.height);
        self.allRestaurantsButton.frame = CGRectMake(82, self.allRestaurantsButton.frame.origin.y, self.allRestaurantsButton.frame.size.width, self.allRestaurantsButton.frame.size.height);
        self.uNewrestaurantsButton.frame = CGRectMake(164, self.uNewrestaurantsButton.frame.origin.y, self.uNewrestaurantsButton.frame.size.width, self.uNewrestaurantsButton.frame.size.height);
        self.moreButton.frame = CGRectMake(246, self.moreButton.frame.origin.y, self.moreButton.frame.size.width, self.moreButton.frame.size.height);
    }
    else
    {
        [self.mostSellingButton setImage:[UIImage imageNamed:@"mostsellingdishes_ar"] forState:UIControlStateNormal];
        [self.mostSellingButton setImage:[UIImage imageNamed:@"mostsellingdishes_sel_ar"] forState:UIControlStateSelected];
        [self.allRestaurantsButton setImage:[UIImage imageNamed:@"allrestaurants_ar"] forState:UIControlStateNormal];
        [self.allRestaurantsButton setImage:[UIImage imageNamed:@"allrestaurants_sel_ar"] forState:UIControlStateSelected];
        [self.uNewrestaurantsButton setImage:[UIImage imageNamed:@"newrestuart_ar"] forState:UIControlStateNormal];
        [self.uNewrestaurantsButton setImage:[UIImage imageNamed:@"newrestuart_sel_ar"] forState:UIControlStateSelected];
        [self.moreButton setImage:[UIImage imageNamed:@"more_ar"] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:@"more_sel_ar"] forState:UIControlStateSelected];
        
        self.mostSellingButton.frame = CGRectMake(244, self.mostSellingButton.frame.origin.y, self.mostSellingButton.frame.size.width, self.mostSellingButton.frame.size.height);
        self.allRestaurantsButton.frame = CGRectMake(164, self.allRestaurantsButton.frame.origin.y, self.allRestaurantsButton.frame.size.width, self.allRestaurantsButton.frame.size.height);
        self.uNewrestaurantsButton.frame = CGRectMake(82, self.uNewrestaurantsButton.frame.origin.y, self.uNewrestaurantsButton.frame.size.width, self.uNewrestaurantsButton.frame.size.height);
        self.moreButton.frame = CGRectMake(0, self.moreButton.frame.origin.y, self.moreButton.frame.size.width, self.moreButton.frame.size.height);
    }
}

@end
