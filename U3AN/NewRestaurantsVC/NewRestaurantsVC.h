//
//  NewRestaurantsVC.h
//  U3AN
//
//  Created by Vipin on 02/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantCell.h"

@interface NewRestaurantsVC : UIViewController<restaurantItemDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *restaurant_ScrollView;

@end
