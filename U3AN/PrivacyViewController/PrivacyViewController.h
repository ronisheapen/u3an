//
//  PrivacyViewController.h
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *privacyWebView;

@end
