//
//  AddRestaurantViewController.m
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AddRestaurantViewController.h"
#define MAXLENGTH 25

@interface AddRestaurantViewController ()
{
    bool haveDelivery;
    NSMutableArray *cuisineNameArray,*cuisineObjArray;
    NSString *phoneStr;
}

@end

@implementation AddRestaurantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    self.add_Your_Restaurant_Label.text = localize(@"Add Your Restaurant");
    self.intro_Label.text = localize(@"To join our corporation, please fill in the forms below and we will contact you as soon as possible");
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self setUpUI];
    [self getCuisineList];
}

-(void)setUpUI
{
    [self.add_Restaurant_ScrollView setContentOffset:CGPointZero];
    
    [self.have_Delivery setBackgroundImage:[UIImage imageNamed:@"tick_box2.png"] forState:UIControlStateNormal];
    [self.have_Delivery setBackgroundImage:[UIImage imageNamed:@"tick_box1.png"] forState:UIControlStateSelected];
    self.add_Restaurant_ScrollView.contentSize = CGSizeMake(self.add_Restaurant_ScrollView.frame.size.width,self.submit_Button.frame.origin.y+self.submit_Button.frame.size.height+150);
    self.add_Restaurant_ScrollView.layer.cornerRadius = 2.0;
    self.add_Restaurant_ScrollView.layer.masksToBounds = YES;
    [self.add_Your_Restaurant_Label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.intro_Label setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];
    [self.first_Name_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.last_Name_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.restaurant_Name_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.restaurant_Address_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.telephone_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.email_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.have_Delivery_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.cuisine_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    
    [self resetFields];
    
    [self toolbarForNumberPadSetup];
}
-(void)resetFields
{
    self.first_Name_TxtBox.text = @"";
    self.last_Name_TxtBox.text = @"";
    self.restaurant_Name_TxtBox.text = @"";
    self.restaurant_Address_TxtView.text = @"";
    self.telephone_TxtBox.text = @"";
    self.email_TxtBox.text = @"";
    self.have_Delivery.selected = NO;
    haveDelivery = NO;
    self.cuisines_LabelTxt.text = @"--Select Cuisine--";
    self.cuisine_selection_Bttn.selected = NO;
}
- (IBAction)backgroundTapAction:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)have_Delivery_BttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    self.have_Delivery.selected = !self.have_Delivery.selected;
    
    if (self.have_Delivery.selected==YES) {
        haveDelivery=YES;
    }
    else{
        haveDelivery=NO;
    }
    
}

- (IBAction)cuisine_SelectionBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    [self showCuisineList];
}

- (IBAction)submit_BttnAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    if (self.first_Name_TxtBox.text.length>0 && self.last_Name_TxtBox.text.length>0&&self.restaurant_Name_TxtBox.text.length>0&& self.restaurant_Address_TxtView.text.length>0&&self.telephone_TxtBox.text.length>0&& self.email_TxtBox.text.length>0&&self.cuisines_LabelTxt.text.length>0)
    {
        if (!((self.telephone_TxtBox.text.length>=6)&&(self.telephone_TxtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
        }
        else if (![ApplicationDelegate isEmailValidWithEmailID:self.email_TxtBox.text])
        {
            [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
            self.email_TxtBox.text=@"";
        }
        else if ([self.cuisines_LabelTxt.text isEqualToString:@"--Select Cuisine--"])
        {
            [ApplicationDelegate showAlertWithMessage:@"Please Select Cuisine Type" title:kMESSAGE];
        }
        else
        {
            [self add_NewRestaurant];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
    }

    
}


-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)getCuisineList
{
    cuisineObjArray=[[NSMutableArray alloc] init];
    cuisineNameArray=[[NSMutableArray alloc] init];
    
    NSMutableDictionary *cuisineData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];
    
    if (cuisineData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCuisinesWithDataDictionary:cuisineData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Cuisine *cuisineItem = [ApplicationDelegate.mapper getMostSellingCusineListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [cuisineObjArray addObject:cuisineItem];
                             [cuisineNameArray addObject:cuisineItem.cuisine_Name];
                         }
                     }
                 }
             }
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
}

-(void)showCuisineList
{
    self.cuisine_selection_Bttn.selected = !self.cuisine_selection_Bttn.selected;
    if (self.cuisine_selection_Bttn.selected)
    {
        if (self.cuisineDropDownObj.view.superview) {
            [self.cuisineDropDownObj.view removeFromSuperview];
        }
        
        self.cuisineDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.cuisineDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.cuisineDropDownObj.view.frame = CGRectMake((self.cuisine_selection_Bttn.superview.frame.origin.x),(self.cuisine_selection_Bttn.superview.frame.origin.y+self.cuisine_selection_Bttn.superview.frame.size.height), (self.cuisine_selection_Bttn.superview.frame.size.width), 0);
        
        self.cuisineDropDownObj.dropDownDelegate = self;
        self.cuisineDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:cuisineNameArray];
        
        self.cuisineDropDownObj.view.layer.borderWidth = 0.1;
        self.cuisineDropDownObj.view.layer.shadowOpacity = 1.0;
        self.cuisineDropDownObj.view.layer.shadowRadius = 5.0;
        self.cuisineDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
        //            if (vw!=self.self.cuisineSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.cuisineDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.cuisineDropDownObj.textLabelColor = [UIColor blackColor];
        self.cuisineDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (cuisineNameArray.count>0)
        {
            [self.cuisine_selection_Bttn.superview.superview addSubview:self.cuisineDropDownObj.view];
        }
        else
        {
            self.cuisine_selection_Bttn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)+10.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.cuisineDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.cuisineDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x+2,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.cuisineDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.cuisineDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cuisineDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
}


-(NSMutableDictionary *)getDishesPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

#pragma mark - REGISTER USER

-(void)add_NewRestaurant
{
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getAddRestaurantPostData]];
    if (restaurantData.count>0)
    {
        __block BOOL dataFound = NO;
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine addNewRestaurantWithDataDictionary:restaurantData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             if (!dataFound)
             {
                 dataFound = YES;
                 
                 if ([ApplicationDelegate isValid:responseDictionary])
                 {
                     if (responseDictionary.count>0)
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             [ApplicationDelegate showAlertWithMessage:@"Restaurant added successfully!!" title:nil];
                             
                             [self viewWillAppear:NO];
                         }
                         
                         //                     UserData *userItem = [ApplicationDelegate.mapper getUserDataFromDictionary:responseArray];
                         //
                         //                     if ([userItem.loginStatus isEqualToString:[NSString stringWithFormat: @"success"]])
                         //                     {
                         //                         [ApplicationDelegate showAlertWithMessage:userItem.loginStatus title:nil];
                         //
                         //                     }
                         //                     else
                         //                     {
                         //                         [ApplicationDelegate showAlertWithMessage:userItem.loginStatus title:nil];
                         //                     }
                     }
                 }
                 
                 
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
    
}


-(NSMutableDictionary *)getAddRestaurantPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:self.cuisines_LabelTxt.text forKey:@"Cuisine"];
    [postDic setObject:[NSNumber numberWithBool:haveDelivery]
                forKey:@"Delivery"];
    [postDic setObject:self.email_TxtBox.text forKey:@"email"];
    [postDic setObject:self.first_Name_TxtBox.text forKey:@"FirstName"];
    [postDic setObject:self.last_Name_TxtBox.text forKey:@"LastName"];
    [postDic setObject:self.restaurant_Name_TxtBox.text forKey:@"RestaurantName"];
    [postDic setObject:self.restaurant_Address_TxtView.text forKey:@"Restaurant_Address"];
    [postDic setObject:self.telephone_TxtBox.text forKey:@"Telephone"];
    return postDic;
}

#pragma mark - DROP-DOWN List Delegate Method

-(void)selectList:(int)selectedIndex
{
    [UIView animateWithDuration:0.4f animations:^{
        self.cuisineDropDownObj.view.frame =
        CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                   self.cuisineDropDownObj.view.frame.origin.y,
                   self.cuisineDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.cuisineDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.cuisine_selection_Bttn.selected = NO;
    self.cuisines_LabelTxt.text =[cuisineNameArray objectAtIndex:selectedIndex];
    self.cuisineObj=[cuisineObjArray objectAtIndex:selectedIndex];
}

#pragma mark - TEXT VIEW DELEGATES

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.add_Restaurant_ScrollView setContentOffset:CGPointZero animated:NO];
    CGFloat yFact;
    
    if (textView.superview.frame.origin.y>yFact)
    {
        [self.add_Restaurant_ScrollView setContentOffset:CGPointMake(self.add_Restaurant_ScrollView.contentOffset.x, (textView.superview.frame.origin.y-20))];
    }
    else
    {
        [self.add_Restaurant_ScrollView setContentOffset:CGPointZero animated:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    //[self.add_Restaurant_ScrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - TEXT FIELD DELEGATES

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGFloat yFact;
    

        [self.add_Restaurant_ScrollView setContentOffset:CGPointZero animated:NO];
 if  (textField==self.telephone_TxtBox)
        {
            phoneStr = self.telephone_TxtBox.text;
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 0; // ToolBar Inclusived case
            }
            else
            {
                yFact = 80; // ToolBar Inclusived case
            }
        }
        
        else
        {
            if ([UIScreen mainScreen].bounds.size.height==480)
            {
                yFact = 45;
            }
            else
            {
                yFact = 130;
            }
        }
        if (textField.superview.frame.origin.y>yFact)
        {
            [self.add_Restaurant_ScrollView setContentOffset:CGPointMake(self.add_Restaurant_ScrollView.contentOffset.x, (textField.superview.frame.origin.y-yFact))];
        }
        else
        {
            [self.add_Restaurant_ScrollView setContentOffset:CGPointZero animated:YES];
        }
    

    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{

    //[self.add_Restaurant_ScrollView setContentOffset:CGPointZero animated:YES];

    
//    if (textField == self.email_TxtBox)
//    {
//        if (textField.text.length!=0)
//        {
//            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
//            {
//                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
//                self.email_TxtBox.text=@"";
//                
//            }
//        }
//    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

       // [self.add_Restaurant_ScrollView setContentOffset:CGPointZero animated:YES];

    //Email
    if (textField == self.email_TxtBox)
    {
        if (textField.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
            {
                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
                self.email_TxtBox.text=@"";
            }
        }
        
    }

    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if (textField==self.telephone_TxtBox)
    {
        if (oldLength>14)
        {
            return newLength <= 14 || returnKey;
        }
    }
    return newLength <= MAXLENGTH || returnKey;
}


#pragma mark - NUMBER PAD METHODS
#pragma mark -
-(void)toolbarForNumberPadSetup
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, ApplicationDelegate.window.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.telephone_TxtBox.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad{
    

    self.telephone_TxtBox.text=phoneStr;
    [self.telephone_TxtBox resignFirstResponder];
    
}

-(void)doneWithNumberPad
{
    if (!((self.telephone_TxtBox.text.length>=6)&&(self.telephone_TxtBox.text.length<=15)))
    {
        [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
        self.telephone_TxtBox.text=phoneStr;
        [self toolbarForNumberPadSetup];
    }
    [self.telephone_TxtBox resignFirstResponder];
}

-(void)xibLoading
{
    NSString *nibName = @"AddRestaurantViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_ar"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
