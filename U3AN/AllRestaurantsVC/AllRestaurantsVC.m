//
//  AllRestaurantsVC.m
//  U3AN
//
//  Created by Vipin on 02/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AllRestaurantsVC.h"
#import "Restaurant.h"
#import "RestaurantDetailsVC.h"

@interface AllRestaurantsVC ()

@end

@implementation AllRestaurantsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    
    if (self.isClickedOnBannerFlag)
    {
        if (self.bannerRestID.length>0)
        {
            [self loadRestaurantFromBannerViewWithRestaurantID:self.bannerRestID];
        }
        else
        {
            self.isClickedOnBannerFlag = NO;
            [self getRestaurantDetailsByCountry];
            
        }
    }
    else
    {
      [self getRestaurantDetailsByCountry];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.isClickedOnBannerFlag = NO;
}

-(void)updateViewHeading
{
        [[HomeTabViewController sharedViewController] resetTabSelection];
        [HomeTabViewController sharedViewController].allRestaurantsButton.selected=YES;
        if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
        {
            [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
            [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
        }
        else
        {
            [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
            [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
            
        }
        [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
        [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
        [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
        [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadRestaurantFromBannerViewWithRestaurantID:(NSString *)restId
{
    NSMutableArray *restaurantListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getRestaurantPostData]];
    if (restaurantData.count>0)
    {
        __block BOOL dataFound = NO;
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getRestaurantsByCountryWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             if (!dataFound)
             {
                 dataFound = YES;
                 
                 if ([ApplicationDelegate isValid:responseArray])
                 {
                     if (responseArray.count>0)
                     {
                         for (int i=0; i<responseArray.count; i++)
                         {
                             if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                             {
                                 Restaurant *restaurantItem = [ApplicationDelegate.mapper getRestaurantListFromDictionary:[responseArray objectAtIndex:i]];
                                 
                                 
                                 if ([restaurantItem.restaurant_Id isEqualToString:restId])
                                 {
                                     if ([restaurantItem.restaurant_Status caseInsensitiveCompare:@"open"]==NSOrderedSame)
                                     {
                                         [restaurantListArray addObject:[responseArray objectAtIndex:i]];
                                     }
                                     
                                     break;
                                 }
                                 
                             }
                         }
                         
                     }
                 }
                 if (restaurantListArray.count==0)
                 {
                     [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
                 }
                 else
                 {
                     Restaurant *restaurantItem = [ApplicationDelegate.mapper getRestaurantListFromDictionary:[restaurantListArray objectAtIndex:0]];
                     
                     RestaurantDetailsVC *restaurantDetailVC = [[RestaurantDetailsVC alloc] initWithNibName:@"RestaurantDetailsVC" bundle:nil];
                     restaurantDetailVC.view.backgroundColor = [UIColor clearColor];
                     restaurantDetailVC.restaurantObj=restaurantItem;
                     
                     if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantDetailVC class]])
                     {
                         [ApplicationDelegate.subHomeNav pushViewController:restaurantDetailVC animated:NO];
                     }
                     
                 }
             }
             
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}
-(void)getRestaurantDetailsByCountry
{
    NSMutableArray *restaurantListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getRestaurantPostData]];
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getRestaurantsByCountryWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Restaurant *restaurantItem = [ApplicationDelegate.mapper getRestaurantListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             if (!([restaurantItem.restaurant_Status caseInsensitiveCompare:@"hidden"]==NSOrderedSame))
                             {
                                 
                                 [restaurantListArray addObject:restaurantItem];
                             }
                             
                         }
                     }

                 }
             }
             if (restaurantListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
             else
             {
                 [self prepareRestaurantListScroll_Grid_WithArray:restaurantListArray];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getRestaurantPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:kCountryId forKey:@"countryId"];
    [postDic setObject:@"" forKey:@"cuisineId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:@"" forKey:@"restName"];
    
    return postDic;
}

#pragma mark - RESTAURANTS IN SCROLLVIEW METHODS

-(void)prepareRestaurantListScroll_Grid_WithArray:(NSMutableArray *)restaurantListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.restaurant_ScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.restaurant_ScrollView.backgroundColor = [UIColor clearColor];
    
    [self.restaurant_ScrollView setContentOffset:CGPointZero];
    //self.restaurant_ScrollView.contentSize = CGSizeMake((restaurantListArray.count*self.restaurant_ScrollView.frame.size.width), 0);
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    RestaurantCell *vw = [[RestaurantCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.restaurant_ScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<restaurantListArray.count; i++)
    {
        int colIndex = i%colCount;
        RestaurantCell *restaurantCell = [[RestaurantCell alloc] init];
        
        restaurantCell.layer.cornerRadius = 2.0;
        restaurantCell.layer.masksToBounds = YES;
        restaurantCell.restaurant_ImageView.layer.cornerRadius = 2.0;
        restaurantCell.restaurant_ImageView.layer.masksToBounds = YES;
        
        Restaurant *restaurantItem = [restaurantListArray objectAtIndex:i];
        restaurantCell.restaurantObj=restaurantItem;
        restaurantCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  restaurantCell.frame.size.width, restaurantCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:restaurantItem.restaurant_Logo] completionHandler:^(UIImage *responseImage) {

            restaurantCell.restaurant_ImageView.image = responseImage;
            [restaurantCell.restaurant_ImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        restaurantCell.delegateRestaurantItem=self;
        restaurantCell.restaurantName_Label.text=restaurantItem.restaurant_Name;
        [restaurantCell.restaurantName_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:12.0f]];
        restaurantCell.restaurantStatusLabel.text=restaurantItem.restaurant_Status;
        [restaurantCell.restaurantStatusLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:13.0f]];
    
#pragma mark - CHANGED by RATHEESH
        
        //Added Sort Order For AllRestaurants case. Updated Mapper class also.
        
        restaurantCell.restaurantSortOrderLabel.superview.hidden = YES;
        
        if (i<40)
        {
            restaurantCell.restaurantSortOrderLabel.superview.hidden = NO;
            [restaurantCell.restaurantSortOrderLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:9.0f]];
            restaurantCell.restaurantSortOrderLabel.text = restaurantItem.SortOrder;
        }
        
        
/////////////
        
        if (![restaurantItem.restaurant_Status isEqualToString:[NSString stringWithFormat: @"OPEN"]])
        {
            restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
        }
            else
            {
               // restaurantCell.restaurantStatusLabel.textColor=[UIColor redColor];
            }
        [self.restaurant_ScrollView addSubview:restaurantCell];
        
        if (colIndex == (colCount - 1))
        {
            yVal = restaurantCell.frame.origin.y + restaurantCell.frame.size.height;
        }
        
        scrollHeight = restaurantCell.frame.origin.y + restaurantCell.frame.size.height;
    }

    self.restaurant_ScrollView.contentSize = CGSizeMake(self.restaurant_ScrollView.frame.size.width,  (scrollHeight + offsetValue));

}

#pragma mark -Restaurant Delegate Method

-(void) restaurantItemDidClicked:(RestaurantCell *)restaurantCategoryItem
{
    if ([restaurantCategoryItem.restaurantStatusLabel.text isEqualToString:[NSString stringWithFormat: @"OPEN"]])
    {
        RestaurantDetailsVC *restaurantDetailVC = [[RestaurantDetailsVC alloc] initWithNibName:@"RestaurantDetailsVC" bundle:nil];
        restaurantDetailVC.view.backgroundColor = [UIColor clearColor];
        restaurantDetailVC.restaurantObj=restaurantCategoryItem.restaurantObj;
    
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[restaurantDetailVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:restaurantDetailVC animated:NO];
        }
    
    }
}

@end
