//
//  SettingsViewController.m
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SettingsViewController.h"
#import "MyAccountVC.h"
#import "LoginViewController.h"
#import "SignUpViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self updateViewHeading];
    [self setupUI];
    
}


-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    if (!ApplicationDelegate.isLoggedIn) {
        self.settingsInnerViewForNotLogin.hidden=NO;
        self.settingsInnerView.hidden=YES;
    }
    else
    {
        self.settingsInnerViewForNotLogin.hidden=YES;
        self.settingsInnerView.hidden=NO;
    }
    
    self.settingsInnerView.layer.cornerRadius = 3.0;
    self.settingsInnerView.layer.masksToBounds = YES;
    self.settingsInnerViewForNotLogin.layer.cornerRadius = 3.0;
    self.settingsInnerViewForNotLogin.layer.masksToBounds = YES;
    [self.settingsLbl setFont:[UIFont fontWithName:@"Tahoma-Bold" size:16.0f]];
}

- (IBAction)myAccountBttnAction:(UIButton *)sender {
    
    MyAccountVC *myAccountVC = [[MyAccountVC alloc] initWithNibName:@"MyAccountVC" bundle:nil]
    ;
    myAccountVC.view.backgroundColor = [UIColor clearColor];
    
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[myAccountVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:myAccountVC animated:NO];
    }
}

- (IBAction)logoutButtonAction:(UIButton *)sender {
    
    NSString *plistPath= [ApplicationDelegate fileWithFilename:@"UserDetails.plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
    
    //add elements to data file and write data to file
    
    [data setObject:@"" forKey:@"Authkey"];
    [data setObject:@"" forKey:@"Status"];
    [data setObject:@"" forKey:@"UserName"];
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    ApplicationDelegate.newsLetterStatusFlag = NO;
    
    [data setObject:[NSNumber numberWithBool:NO] forKey:@"newsletterStatus"];
    
    [data writeToFile: plistPath atomically:YES];
    
    
    
    ApplicationDelegate.isLoggedIn=NO;
    ApplicationDelegate.authn_Key=@"";
    ApplicationDelegate.logged_User_Name=@"";
    self.settingsInnerViewForNotLogin.hidden=NO;
    self.settingsInnerView.hidden=YES;
    [[HomeTabViewController sharedViewController] updateMoreMenuItems];
    
//    if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
//    {
//        
//        [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
//        
//    }
    
    ApplicationDelegate.subHomeNav.viewControllers = nil;
    
    [[HomeTabViewController sharedViewController] viewWillAppear:NO];

}

- (IBAction)logingButtonAction:(UIButton *)sender {
    
        LoginViewController *loginVc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        loginVc.view.backgroundColor = [UIColor clearColor];
    
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[loginVc class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:loginVc animated:NO];
        }
}

- (IBAction)signUpButtonAction:(UIButton *)sender {
    
    SignUpViewController *signUpVc = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
    signUpVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[signUpVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:signUpVc animated:NO];
    }
}

- (IBAction)settingDismissAction:(UIButton *)sender {
    if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
    {
        
        [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
        
    }
}

-(void)xibLoading
{
    NSString *nibName = @"SettingsViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
