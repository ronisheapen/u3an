//
//  MyFavouritesVC.h
//  U3AN
//
//  Created by Vipin on 26/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteDetails.h"
#import "MyFavouriteCell.h"

@interface MyFavouritesVC : UIViewController<favouriteItemDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *favScrollView;
@property (strong, nonatomic) NSMutableArray *custAddressListArray;

@end
