//
//  MyFavouriteCell.h
//  U3AN
//
//  Created by Vipin on 03/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteDetails.h"

@protocol favouriteItemDelegate;

@interface MyFavouriteCell : UIView

@property(weak,nonatomic)id<favouriteItemDelegate> delegateFavouritetItem;
@property(nonatomic,retain)FavouriteDetails *favouriteItemObject;
@property (strong, nonatomic) IBOutlet UIImageView *dishImageView;
@property (strong, nonatomic) IBOutlet UILabel *dishNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dishDetailLabel;
@end

@protocol favouriteItemDelegate <NSObject>

- (void) removeFavButtonDidClicked:(MyFavouriteCell *) favouriteMenuItem;

@end