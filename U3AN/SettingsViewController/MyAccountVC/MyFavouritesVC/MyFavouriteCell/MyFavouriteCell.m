//
//  MyFavouriteCell.m
//  U3AN
//
//  Created by Vipin on 03/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MyFavouriteCell.h"

@implementation MyFavouriteCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"MyFavouriteCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (MyFavouriteCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

- (IBAction)favouriteRemoveButtonAction:(UIButton *)sender {
    
    [self.delegateFavouritetItem removeFavButtonDidClicked:self];
}

@end
