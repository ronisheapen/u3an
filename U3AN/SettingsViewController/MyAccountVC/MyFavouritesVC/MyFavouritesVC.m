//
//  MyFavouritesVC.m
//  U3AN
//
//  Created by Vipin on 26/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MyFavouritesVC.h"
#import "LaunchingViewController.h"

@interface MyFavouritesVC ()

@end

@implementation MyFavouritesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
{
    
    [self updateViewHeading];
    [self setupUI];
}
else
{
    [self loadHomeView];
}
    
    

}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    
    self.favScrollView.layer.cornerRadius = 3.0;
    self.favScrollView.layer.masksToBounds = YES;
    [self getFavouriteDetails];
}


-(void)getFavouriteDetails
{
    NSMutableArray *favouriteListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        [ApplicationDelegate.engine getFavouriteWithDataDictionary:postData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             FavouriteDetails *favouriteItem = [ApplicationDelegate.mapper getFavouriteDetailsFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [favouriteListArray addObject:favouriteItem];
                         }
                     }
                     
                 }
             }
             if (favouriteListArray.count==0)
             {
                 [ApplicationDelegate removeProgressHUD];
                 
                 UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:nil message:@"No items available in favourites." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 alrt.tag = 6;
                 [alrt show];
                 
//                 [ApplicationDelegate showAlertWithMessage:@"No items available in favourites." title:nil];
//                 
//                 if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
//                 {
//                     [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
//                 }
                 
             }
             
             [self prepareFavouritListScroll_Grid_WithArray:favouriteListArray];
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    return postDic;
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}

#pragma mark - RESTAURANTS IN SCROLLVIEW METHODS

-(void)prepareFavouritListScroll_Grid_WithArray:(NSMutableArray *)restaurantListArray
{
    // CLEARING ALL SUBVIEWS
    
    for (UIView *sub in self.favScrollView.subviews) {
        [sub removeFromSuperview];
    }
    self.favScrollView.backgroundColor = [UIColor clearColor];
    
    [self.favScrollView setContentOffset:CGPointZero];
    //self.restaurant_ScrollView.contentSize = CGSizeMake((restaurantListArray.count*self.restaurant_ScrollView.frame.size.width), 0);
    CGFloat offsetValue = 10.0f;
    int colCount = 0;
    float yVal = 0.0f;
    float scrollHeight = 0.0f;
    
    MyFavouriteCell *vw = [[MyFavouriteCell alloc] init];
    
    CGFloat viewWidth = vw.frame.size.width;
    
    for (int i=1; ((i*viewWidth)<(self.favScrollView.frame.size.width-offsetValue)); i++)
    {
        colCount = i;
    }
    for (int i=0; i<restaurantListArray.count; i++)
    {
        int colIndex = i%colCount;
        MyFavouriteCell *favCell = [[MyFavouriteCell alloc] init];
        
        favCell.layer.cornerRadius = 3.0;
        favCell.layer.masksToBounds = YES;
        favCell.dishImageView.layer.cornerRadius = 2.0;
        favCell.dishImageView.layer.masksToBounds = YES;
        
        FavouriteDetails *favouriteItem = [restaurantListArray objectAtIndex:i];
        favCell.favouriteItemObject=favouriteItem;
        favCell.frame = CGRectMake(((colIndex*viewWidth)+(offsetValue*(colIndex+1))), (yVal+offsetValue),  favCell.frame.size.width, favCell.frame.size.height);
        
        [ApplicationDelegate.engine getImageFromServerASYNCWithUrl:[ApplicationDelegate getHTTPCorrectedURLFromUrl:favouriteItem.thumbnail] completionHandler:^(UIImage *responseImage) {
            
            favCell.dishImageView.image = responseImage;
            [favCell.dishImageView.layer needsLayout];
            
        } errorHandler:^(NSError *error) {
            
        }];
        favCell.delegateFavouritetItem=self;
        favCell.dishNameLabel.text=favouriteItem.name;
        [favCell.dishNameLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:13.0f]];
        favCell.dishDetailLabel.text=favouriteItem.fav_Description;
        [favCell.dishDetailLabel setFont:[UIFont fontWithName:@"Tahoma" size:12.0f]];
        [ApplicationDelegate loadMarqueeLabelWithText:favCell.dishDetailLabel.text Font:favCell.dishDetailLabel.font InPlaceOfLabel:favCell.dishDetailLabel];
        [self.favScrollView addSubview:favCell];
        
        if (colIndex == (colCount - 1))
        {
            yVal = favCell.frame.origin.y + favCell.frame.size.height;
        }
        
        scrollHeight = favCell.frame.origin.y + favCell.frame.size.height;
    }
    
    self.favScrollView.contentSize = CGSizeMake(self.favScrollView.frame.size.width,  (scrollHeight + offsetValue));
    
}

-(void)removeFavButtonDidClicked:(MyFavouriteCell *)favouriteMenuItem
{
    [self removeFromFavourite:favouriteMenuItem];
}

-(void)removeFromFavourite:(MyFavouriteCell *)favouriteMenuItem
{
    NSMutableDictionary *favData = [[NSMutableDictionary alloc] initWithDictionary:[self setFavPostData:favouriteMenuItem]];
    if (favData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine removeFromFavouriteWithDataDictionary:favData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             [ApplicationDelegate removeProgressHUD];
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]] )
                     {
                         if ([[responseDictionary objectForKey:@"Status"] isEqualToString:[NSString stringWithFormat: @"Success"]])
                         {
                             
                             UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:@"" message:@"Item removed from favourites Sucessfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             alrt.tag = 5;
                             [alrt show];
                             
                             //[self setupUI];
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                     
                 }
             }
             

             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
    
}


-(NSMutableDictionary *)setFavPostData:(MyFavouriteCell *)favouriteMenuItem
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:favouriteMenuItem.favouriteItemObject.itemID forKey:@"ItemId"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"UserId"];
    
    return postDic;
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //ITEM REMOVE
    
    if (alertView.tag==5)
    {
        if (buttonIndex==0)
        {
            [self setupUI];
            
        }
        
    }
    
    //NO FAVS
    
    if (alertView.tag==6)
    {
        if (buttonIndex==0)
        {
            if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
            {
                [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
            }
            
        }
        
    }
    
}
@end
