//
//  ChangePasswordVC.m
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "LaunchingViewController.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.changPsswdScrollView.contentSize = CGSizeMake(self.changPsswdScrollView.frame.size.width,  self.changePassBttn.superview.frame.origin.y+self.changePassBttn.superview.frame.size.height+10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    [self setupUI];
    }
    else{
         [self loadHomeView];
    }
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.changPsswdScrollView.layer.cornerRadius = 3.0;
    self.changPsswdScrollView.layer.masksToBounds = YES;
    self.changePasswordLbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f];
    self.oldPasswordLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.unewPasswordLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.reTypeNewPassLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    
}

- (IBAction)changePasswdBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (self.oldPasswdTxtBox.text.length>0 && self.uNewPasswdTxt.text.length>0&&self.reNewPasswdTxt.text.length>0) {
        
        
        if (![self.reNewPasswdTxt.text isEqualToString:self.uNewPasswdTxt.text])
        {
            [ApplicationDelegate showAlertWithMessage:@"Password mismatch occured. Please try again." title:kMESSAGE];
            self.reNewPasswdTxt.text=@"";
            self.uNewPasswdTxt.text = @"";
            
        }
        else
        {
            if (self.uNewPasswdTxt.text.length<7)
            {
                [ApplicationDelegate showAlertWithMessage:@"Password will be min 7 characters" title:kMESSAGE];
                self.reNewPasswdTxt.text=@"";
                self.uNewPasswdTxt.text = @"";
            }
            else
            {
                [self changePassword];
            }
            
        }
    
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
    }
}

- (IBAction)cancelBttnAction:(UIButton *)sender {
    
    self.oldPasswdTxtBox.text=@"";
    self.uNewPasswdTxt.text=@"";
    self.reNewPasswdTxt.text=@"";
}

-(void)changePassword
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getCustPostData]];
    if (postData.count>0)
    {
        __block BOOL dataFound = NO;
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine changePasswordWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             if (!dataFound)
             {
                 dataFound = YES;
                 
                 BOOL success = NO;
                 
                 if ([ApplicationDelegate isValid:responseDictionary])
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             
                             success = YES;
                             
                         }
                         
                     }
                 }
                 
                 if (success)
                 {
                     [ApplicationDelegate showAlertWithMessage:@"Password changed successfully" title:nil];
                     
                     self.changePassBttn.enabled=NO;
                 }
                 else
                 {
                     [ApplicationDelegate showAlertWithMessage:@"Please check your old password" title:nil];
                     
                     self.changePassBttn.enabled=YES;
                 }
                 
                 self.uNewPasswdTxt.text = @"";
                 self.oldPasswdTxtBox.text = @"";
                 self.reNewPasswdTxt.text = @"";
                 
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getCustPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:ApplicationDelegate.authn_Key forKey:@"authKey"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.uNewPasswdTxt.text forKey:@"newPassword"];
    [postDic setObject:self.oldPasswdTxtBox.text forKey:@"oldPassword"];
    
    
    return postDic;
}

#pragma mark - TEXT FIELD DELEGATES

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[self.changPsswdScrollView setContentOffset:CGPointZero animated:YES];
    if (textField==self.uNewPasswdTxt) {
        if (textField.text.length<7)
        {
            [ApplicationDelegate showAlertWithMessage:@"Password will be min 7 characters" title:kMESSAGE];
        }
    }
    
    if (textField == self.reNewPasswdTxt)
    {
        if (![textField.text isEqualToString:self.uNewPasswdTxt.text])
        {
            
            [ApplicationDelegate showAlertWithMessage:@"Password Mismatch" title:kMESSAGE];
            self.reNewPasswdTxt.text=@"";
            
        }
    }
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.changePassBttn.enabled=YES;
    [self.changPsswdScrollView setContentOffset:CGPointZero animated:NO];
    CGFloat yFact;

        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 45;
        }
        else
        {
            yFact = 130;
        }
    
    
    if ((textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y)>yFact)
    {
        [self.changPsswdScrollView setContentOffset:CGPointMake(self.changPsswdScrollView.contentOffset.x, (textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y-yFact))];
    }
    else
    {
        [self.changPsswdScrollView setContentOffset:CGPointZero animated:YES];
    }
    
}
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if (textField==self.uNewPasswdTxt) {
//        if (textField.text.length<7)
//        {
//            [ApplicationDelegate showAlertWithMessage:@"Password will be min 7 characters" title:kMESSAGE];
//        }
//    }
//    
//    if (textField == self.reNewPasswdTxt)
//    {
//        if (![textField.text isEqualToString:self.uNewPasswdTxt.text])
//        {
//            
//            [ApplicationDelegate showAlertWithMessage:@"Password Mismatch" title:kMESSAGE];
//            self.reNewPasswdTxt.text=@"";
//            
//        }
//    }
//    [textField resignFirstResponder];
//}

@end
