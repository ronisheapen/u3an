//
//  AccountInformationVC.m
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AccountInformationVC.h"
#import "CustomerAddressDetails.h"
#import "LaunchingViewController.h"

#define MAXLENGTH 25

@interface AccountInformationVC ()
{
    NSString *mobStr, *houseNumStr;
    NSInteger numberFieldVal; // 1 = mobile number, 2 = House Number
}

@end

@implementation AccountInformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
    self.accountInfoScrollView.contentSize = CGSizeMake(self.accountInfoScrollView.frame.size.width,  self.saveBttn.superview.frame.origin.y+self.saveBttn.superview.frame.size.height+10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    [self setupUI];
    //[self getCustomerAddresses];
    mobStr = @"";
    houseNumStr = @"";
    [self toolbarForNumberPadSetup];
    
    [self getCustomerAddresses];
    }
    else
    {
        [self loadHomeView];
    }
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    
    self.accountInfoScrollView.layer.cornerRadius = 3.0;
    self.accountInfoScrollView.layer.masksToBounds = YES;
    self.accountInfoLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:16.0f];
    self.firstNameLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.lastNameLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.mobileLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.houseNumberLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];

}

-(void)loadUserDetails
{
    if (self.custAddressListArray.count!=0) {
    
    CustomerAddressDetails * custDetailItem= [self.custAddressListArray objectAtIndex:0];
    self.firstNameTxtBox.text=custDetailItem.firstName;
    self.lastNameTxtBox.text=custDetailItem.lastName;
    self.mobileTxtBox.text=custDetailItem.mobile;
    self.houseNumberTxtBox.text=custDetailItem.housePhone;
    }
}

-(void)editCustomerDetails
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getCustPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine editCustomerDetailsWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                 {
                     if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                     {
                         self.saveBttn.enabled=NO;
                         
                         [ApplicationDelegate showAlertWithMessage:@"Profile updated successfully." title:nil];
                         
                         if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
                         {
                             [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
                         }
                     }
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getCustPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    CustomerAddressDetails * custDetailItem= [self.custAddressListArray objectAtIndex:0];
    
    [postDic setObject:ApplicationDelegate.authn_Key forKey:@"authKey"];
    [postDic setObject:custDetailItem.birthday forKey:@"birthday"];
    [postDic setObject:custDetailItem.company forKey:@"company"];
    [postDic setObject:self.firstNameTxtBox.text forKey:@"firstName"];
    [postDic setObject:custDetailItem.gender forKey:@"gender"];
    [postDic setObject:self.houseNumberTxtBox.text forKey:@"housePhone"];
    [postDic setObject:@"" forKey:@"howToContact"];
    [postDic setObject:self.lastNameTxtBox.text forKey:@"lastName"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.mobileTxtBox.text forKey:@"mobile"];
    [postDic setObject:custDetailItem.occupation forKey:@"occupation"];
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    
    //[postDic setObject:[NSNumber numberWithBool:custDetailItem.subscribed_to_news] forKey:@"subscribedToNewsletter"];
    [postDic setObject:[NSNumber numberWithBool:ApplicationDelegate.newsLetterStatusFlag] forKey:@"subscribedToNewsletter"];
    [postDic setObject:[NSNumber numberWithBool:custDetailItem.subscribed_to_sms] forKey:@"subscribedToSMS"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    [postDic setObject:custDetailItem.workPhone forKey:@"workPhone"];
    
    
    return postDic;
}

-(void)getCustomerAddresses
{
    self.custAddressListArray= [[NSMutableArray alloc] init];
    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    if (custPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             NSMutableArray *addressArray = [[NSMutableArray alloc] init];
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             
                             
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
                             {
                                 addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
                             }
                             if (addressArray.count>0)
                             {
                                 for (int i=0; i<addressArray.count; i++)
                                 {
                                     if ([ApplicationDelegate isValid:[addressArray objectAtIndex:i]])
                                     {
                                         CustomerAddressDetails *addressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:i]];
                                         
                                         [self.custAddressListArray addObject:addressItem];
                                     }
                                     
                                 }
                                 
                                 [self loadUserDetails];
                             }
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}


- (IBAction)backgroundTapAction:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)saveButtonAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
     if (self.firstNameTxtBox.text.length>0 && self.lastNameTxtBox.text.length>0&&self.mobileTxtBox.text.length>0&&self.houseNumberTxtBox.text.length>0)
     {
         [self editCustomerDetails];
     }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please Fill All Mandatory Fields" title:kMESSAGE];
    }
    
}


#pragma mark - TEXT FIELD DELEGATES

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[self.accountInfoScrollView setContentOffset:CGPointZero animated:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.accountInfoScrollView setContentOffset:CGPointZero animated:NO];
    CGFloat yFact;
    if (textField==self.mobileTxtBox)
    {
        numberFieldVal = 1;
        mobStr = self.mobileTxtBox.text;
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 0; // ToolBar Inclusived case
        }
        else
        {
            yFact = 80; // ToolBar Inclusived case
        }
    }
    else if (textField==self.houseNumberTxtBox)
    {
        numberFieldVal = 2;
        houseNumStr = self.houseNumberTxtBox.text;
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 0; // ToolBar Inclusived case
        }
        else
        {
            yFact = 80; // ToolBar Inclusived case
        }
    }
    else
    {
        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 45;
        }
        else
        {
            yFact = 130;
        }
    }
    
    
    if ((textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y)>yFact)
    {
        [self.accountInfoScrollView setContentOffset:CGPointMake(self.accountInfoScrollView.contentOffset.x, (textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y-yFact))];
    }
    else
    {
        [self.accountInfoScrollView setContentOffset:CGPointZero animated:YES];
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    self.saveBttn.enabled=YES;
    
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if ((textField==self.mobileTxtBox)||(textField==self.houseNumberTxtBox))
    {
        if (oldLength>14)
        {
            return newLength <= 14 || returnKey;
        }
    }
    
    return newLength <= MAXLENGTH || returnKey;
}

#pragma mark - NUMBER PAD METHODS
#pragma mark -

-(void)toolbarForNumberPadSetup
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, ApplicationDelegate.window.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    
    self.mobileTxtBox.inputAccessoryView = numberToolbar;
    self.houseNumberTxtBox.inputAccessoryView = numberToolbar;
}
-(void)cancelNumberPad
{
    if (numberFieldVal==1)
    {
        self.mobileTxtBox.text=mobStr;
    }
    if (numberFieldVal==2)
    {
        self.houseNumberTxtBox.text=houseNumStr;
    }
    [self.view endEditing:YES];
    
}

-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
    if (numberFieldVal==1)
    {
        if (!((self.mobileTxtBox.text.length>=6)&&(self.mobileTxtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
            self.mobileTxtBox.text=mobStr;
        }
    }
    if (numberFieldVal==2)
    {
        if (!((self.houseNumberTxtBox.text.length>=6)&&(self.houseNumberTxtBox.text.length<=15)))
        {
            [ApplicationDelegate showAlertWithMessage:kINAVLID_PHONE_MSG title:kMESSAGE];
            self.houseNumberTxtBox.text=houseNumStr;
        }
    }
    
    [self toolbarForNumberPadSetup];
}

-(void)xibLoading
{
    NSString *nibName = @"AccountInformationVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
