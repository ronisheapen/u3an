//
//  AddressListVC.h
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressListVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *addressListTable;
@property (strong, nonatomic) IBOutlet UILabel *addressListLbl;
@property (strong, nonatomic) NSMutableArray *custAddressListArray;
@property (strong, nonatomic) IBOutlet UIScrollView *addressListScrollView;

@end
