//
//  AddressListCell.h
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *streetLabel;

@end
