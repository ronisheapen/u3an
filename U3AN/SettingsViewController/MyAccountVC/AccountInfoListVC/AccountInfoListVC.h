//
//  AccountInfoListVC.h
//  U3AN
//
//  Created by Vipin on 23/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountInfoListVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *accountInfoListTable;
@property (strong, nonatomic) NSMutableArray *custAddressListArray;

@end
