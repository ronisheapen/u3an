//
//  AddAddressVC.h
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownWithHeaderSelection.h"
#import "Areas.h"
#import "CustomerAddressDetails.h"

@interface AddAddressVC : UIViewController<UITextFieldDelegate,ListWithHeaderSelectionProtocol>
@property (strong, nonatomic) DropDownWithHeaderSelection *areaDropDownObj;
@property (strong, nonatomic) CustomerAddressDetails *customerDetailObj;
@property (strong, nonatomic) IBOutlet UIScrollView *deliveryInfoScrollView;
@property (strong, nonatomic) IBOutlet UILabel *deliveryInfoLbl;
@property (strong, nonatomic) IBOutlet UILabel *villaHouseLbl;
@property (strong, nonatomic) IBOutlet UILabel *buildingLbl;
@property (strong, nonatomic) IBOutlet UILabel *officeLbl;
@property (strong, nonatomic) IBOutlet UIButton *villaBttn;
@property (strong, nonatomic) IBOutlet UIButton *buildingBttn;
@property (strong, nonatomic) IBOutlet UIButton *officeBttn;
@property (strong, nonatomic) IBOutlet UILabel *areaLbl;
@property (strong, nonatomic) IBOutlet UILabel *blockLbl;
@property (strong, nonatomic) IBOutlet UILabel *profileNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *streetLbl;
@property (strong, nonatomic) IBOutlet UILabel *juddaLbl;
@property (strong, nonatomic) IBOutlet UILabel *buildingNoLbl;
@property (strong, nonatomic) IBOutlet UILabel *floorNoLbl;
@property (strong, nonatomic) IBOutlet UILabel *apartmentNoLbl;
@property (strong, nonatomic) IBOutlet UILabel *extraDirectionLbl;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *areaSelectBttn;
@property (strong, nonatomic) IBOutlet UITextField *blockTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *areaTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *profileNameTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *streetTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *juddaTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *buildingNoTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *floorNoTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *apartmentNoTxtBox;
@property (strong, nonatomic) IBOutlet UITextView *extraDirectionTxtView;
@property (strong, nonatomic)Areas *areaObj;
@property (strong, nonatomic) NSMutableArray *custAddressListArray;

- (IBAction)backgroundTapAction:(id)sender;

- (IBAction)radioButtonAction:(UIButton *)sender;
- (IBAction)saveButtonAction:(UIButton *)sender;
- (IBAction)areaSelectionBttnAction:(UIButton *)sender;

@end
