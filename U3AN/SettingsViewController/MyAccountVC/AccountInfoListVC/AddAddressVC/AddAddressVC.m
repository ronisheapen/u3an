//
//  AddAddressVC.m
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AddAddressVC.h"
#import "AddressListVC.h"
#import "LaunchingViewController.h"
#define MAXLENGTH 25

@interface AddAddressVC (){
    NSString *selectedAreaId,*adderss_Type;
    NSMutableArray *areaListArray,*cityListArray,*areaObjArray;
}

@end

@implementation AddAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
    [self.villaBttn setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.villaBttn setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    [self.buildingBttn setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.buildingBttn setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    [self.officeBttn setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.officeBttn setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    self.deliveryInfoScrollView.contentSize = CGSizeMake(self.deliveryInfoScrollView.frame.size.width,  self.saveButton.superview.frame.origin.y+self.saveButton.superview.frame.size.height+10);
    
    self.villaBttn.selected=YES;
    adderss_Type=@"0";
    
    [self radioButtonAction:self.villaBttn];
    
    
   
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    if (areaListArray.count==0) {
        [self getAreaDetails];
    }
    [self setupUI];

    if ([ApplicationDelegate isValid:self.customerDetailObj])
    {
        [self loadUserDetails];
    }
    }
    else
    {
        [self loadHomeView];
    }
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.deliveryInfoScrollView.layer.cornerRadius = 3.0;
    self.deliveryInfoScrollView.layer.masksToBounds = YES;
    [self.deliveryInfoLbl setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.villaHouseLbl setFont:[UIFont fontWithName:@"Tahoma" size:13.0f]];
    [self.buildingLbl setFont:[UIFont fontWithName:@"Tahoma" size:13.0f]];
    [self.officeLbl setFont:[UIFont fontWithName:@"Tahoma" size:13.0f]];
    [self.areaLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.blockLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.profileNameLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.streetLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.juddaLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.buildingNoLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.floorNoLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.apartmentNoLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.extraDirectionLbl setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    
}

-(void)clearTextValues
{
    self.areaTxtBox.text=@"";
    self.blockTxtBox.text=@"";
    self.buildingNoTxtBox.text=@"";
    self.extraDirectionTxtView.text=@"";
    self.floorNoTxtBox.text=@"";
    self.juddaTxtBox.text=@"";
    self.profileNameTxtBox.text=@"";
    self.streetTxtBox.text=@"";
    self.apartmentNoTxtBox.text=@"";
    
}

-(void)loadUserDetails
{
    
    selectedAreaId=self.customerDetailObj.areaID;
    self.areaTxtBox.text=self.customerDetailObj.areaName;
    self.blockTxtBox.text=self.customerDetailObj.block;
    self.buildingNoTxtBox.text=self.customerDetailObj.buildingNo;
    self.extraDirectionTxtView.text=self.customerDetailObj.extraDirections;
    self.floorNoTxtBox.text=self.customerDetailObj.floor;
    self.juddaTxtBox.text=self.self.customerDetailObj.judda;
    self.profileNameTxtBox.text=self.customerDetailObj.profileName;
    self.streetTxtBox.text=self.customerDetailObj.street;
    self.apartmentNoTxtBox.text=self.customerDetailObj.suit;
    self.villaBttn.selected = NO;
    self.buildingBttn.selected = NO;
    self.officeBttn.selected = NO;
    
    if ([self.customerDetailObj.type caseInsensitiveCompare:@"villa"]==NSOrderedSame)
    {
        self.villaBttn.selected = YES;
         adderss_Type=@"0";
    }
    if ([self.customerDetailObj.type caseInsensitiveCompare:@"building"]==NSOrderedSame)
    {
        self.buildingBttn.selected = YES;
         adderss_Type=@"1";
    }
    if ([self.customerDetailObj.type caseInsensitiveCompare:@"office"]==NSOrderedSame)
    {
        self.officeBttn.selected = YES;
         adderss_Type=@"2";
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAddressDetails
{
    NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:[self getAddressPostData]];
    if (addressData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine addAddressDetailsWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]] )
                     {
                         if ([[responseDictionary objectForKey:@"Status"] isEqualToString:[NSString stringWithFormat: @"Success"]])
                         {
                             
                             [ApplicationDelegate showAlertWithMessage:@"Address added successfully." title:nil];
                             [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                     
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
    
}


-(NSMutableDictionary *)getAddressPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:selectedAreaId forKey:@"areaId"];
    [postDic setObject:self.blockTxtBox.text forKey:@"block"];
    [postDic setObject:self.buildingNoTxtBox.text forKey:@"buildingNo"];
    [postDic setObject:self.extraDirectionTxtView.text forKey:@"extraDirections"];
    [postDic setObject:self.floorNoTxtBox.text forKey:@"floor"];
    [postDic setObject:@"" forKey:@"isPrimary"];
    [postDic setObject:self.juddaTxtBox.text forKey:@"judda"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.profileNameTxtBox.text forKey:@"profileName"];
    [postDic setObject:self.streetTxtBox.text forKey:@"street"];
    [postDic setObject:self.apartmentNoTxtBox.text forKey:@"suite"];
    [postDic setObject:adderss_Type forKey:@"type"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];

    
    return postDic;
}

-(void)editAddressDetails
{
    NSMutableDictionary *addressData = [[NSMutableDictionary alloc] initWithDictionary:[self getEditAddressPostData]];
    if (addressData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine editAddressWithDataDictionary:addressData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]] )
                     {
                         if ([[responseDictionary objectForKey:@"Status"] isEqualToString:[NSString stringWithFormat: @"Success"]])
                         {
                             
                             [ApplicationDelegate showAlertWithMessage:@"Address updated successfully." title:nil];
                             [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                     
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
    
}

-(NSMutableDictionary *)getEditAddressPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    BOOL isPrim;
    if ([self.customerDetailObj.isPrimary caseInsensitiveCompare:@"true"]==NSOrderedSame)
    {
        isPrim=true;
    }
    else
    {
        isPrim=false;
    }
    [postDic setObject:self.customerDetailObj.addressID forKey:@"AddressId"];
    [postDic setObject:selectedAreaId forKey:@"AreaId"];
    [postDic setObject:self.areaTxtBox.text forKey:@"AreaName"];
    [postDic setObject:self.blockTxtBox.text forKey:@"Block"];
    [postDic setObject:self.buildingNoTxtBox.text forKey:@"BuildingNo"];
    [postDic setObject:self.extraDirectionTxtView.text forKey:@"ExtraDirections"];
    [postDic setObject:self.customerDetailObj.firstName forKey:@"FirstName"];
    [postDic setObject:self.floorNoTxtBox.text forKey:@"Floor"];
    [postDic setObject:self.customerDetailObj.gender forKey:@"Gender"];
    [postDic setObject:self.customerDetailObj.housePhone forKey:@"HousePhone"];
    [postDic setObject:@"" forKey:@"Id"];
    [postDic setObject:[NSNumber numberWithBool:isPrim] forKey:@"IsPrimary"];
    [postDic setObject:self.juddaTxtBox.text forKey:@"Judda"];
    [postDic setObject:self.customerDetailObj.lastName forKey:@"LastName"];
    [postDic setObject:self.customerDetailObj.mobile forKey:@"Mobile"];
    [postDic setObject:self.profileNameTxtBox.text forKey:@"ProfileName"];
    [postDic setObject:self.streetTxtBox.text forKey:@"Street"];
    [postDic setObject:self.apartmentNoTxtBox.text forKey:@"Suite"];
    [postDic setObject:adderss_Type forKey:@"Type"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"UserId"];
    [postDic setObject:self.customerDetailObj.workPhone forKey:@"WorkPhone"];
    
    
    return postDic;
}


//-(void)getCustomerAddresses
//{
//    self.custAddressListArray= [[NSMutableArray alloc] init];
//    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
//    if (custPostData.count>0)
//    {
//        [ApplicationDelegate addProgressHUDToView:self.view];
//        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
//         {
//             NSMutableArray *addressArray = [[NSMutableArray alloc] init];
//             if ([ApplicationDelegate isValid:responseDictionary])
//             {
//                 if (responseDictionary.count>0)
//                 {
//                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
//                     {
//                         
//                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
//                         {
//                             
//                             
//                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
//                             {
//                                 addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
//                             }
//                             if (addressArray.count>0)
//                             {
//                                 for (int i=0; i<addressArray.count; i++)
//                                 {
//                                     if ([ApplicationDelegate isValid:[addressArray objectAtIndex:i]])
//                                     {
//                                         CustomerAddressDetails *addressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:i]];
//                                         
//                                         [self.custAddressListArray addObject:addressItem];
//                                     }
//                                     
//                                 }
//                            }
//                             
//                             
//                         }
//                         else
//                         {
//                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
//                         }
//                     }}
//             }
//             
//             [ApplicationDelegate removeProgressHUD];
//             
//         } errorHandler:^(NSError *error) {
//             
//             [ApplicationDelegate removeProgressHUD];
//             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
//         }];
//        
//    }
//    
//}
//
//-(NSMutableDictionary *)getUserPostData
//{
//    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
//    
//    [postDic setObject:kLocal forKey:@"locale"];
//    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
//    
//    
//    return postDic;
//}



- (IBAction)backgroundTapAction:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)radioButtonAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (sender.tag==1)
    {
        [self clearTextValues];
        self.villaBttn.selected = YES;
        self.buildingBttn.selected = NO;
        self.officeBttn.selected = NO;
        adderss_Type=@"0";
        
        
    }
    else if (sender.tag==2)
    {
        [self clearTextValues];
        self.villaBttn.selected = NO;
        self.buildingBttn.selected = YES;
        self.officeBttn.selected = NO;
        adderss_Type=@"1";
        
    }
    else if (sender.tag==3)
    {
        [self clearTextValues];
        self.villaBttn.selected = NO;
        self.buildingBttn.selected = NO;
        self.officeBttn.selected = YES;
        adderss_Type=@"2";
    }
    
    CGFloat yFact = 653;
    
    self.floorNoLbl.hidden = YES;
    self.floorNoTxtBox.superview.hidden = YES;
    
    self.apartmentNoLbl.hidden = YES;
    self.apartmentNoTxtBox.superview.hidden=YES;
    
    if (![adderss_Type isEqualToString:@"0"])
    {
        yFact = 825;
        
        self.floorNoLbl.hidden = NO;
        self.floorNoTxtBox.superview.hidden = NO;
        
        self.apartmentNoLbl.hidden = NO;
        self.apartmentNoTxtBox.superview.hidden=NO;
        
    }
    
    self.extraDirectionLbl.frame = CGRectMake(self.extraDirectionLbl.frame.origin.x, yFact, self.extraDirectionLbl.frame.size.width, self.extraDirectionLbl.frame.size.height);
    self.extraDirectionTxtView.superview.frame = CGRectMake(self.extraDirectionTxtView.superview.frame.origin.x, (self.extraDirectionLbl.frame.origin.y+self.extraDirectionLbl.frame.size.height+8), self.extraDirectionTxtView.superview.frame.size.width, self.extraDirectionTxtView.superview.frame.size.height);
    
    self.saveButton.frame = CGRectMake(self.saveButton.frame.origin.x, (self.extraDirectionTxtView.superview.frame.origin.y+self.extraDirectionTxtView.superview.frame.size.height+40), self.saveButton.frame.size.width, self.saveButton.frame.size.height);
    
    self.saveButton.center = CGPointMake(self.extraDirectionTxtView.superview.center.x, self.saveButton.frame.origin.y);
    
    self.deliveryInfoScrollView.contentSize = CGSizeMake(self.deliveryInfoScrollView.frame.size.width, (self.saveButton.frame.origin.y+self.saveButton.frame.size.height+110));
    self.deliveryInfoScrollView.contentOffset = CGPointZero;
                                                         
    
}

- (IBAction)saveButtonAction:(UIButton *)sender
{
    
    [self.view endEditing:YES];
    
    BOOL validOk = NO;
    
    if (adderss_Type.length>0)
    {
        if ([adderss_Type isEqualToString:@"0"])
        {
            if ((self.areaTxtBox.text.length>0) &&( self.blockTxtBox.text.length>0)&&(self.profileNameTxtBox.text.length>0)&&(self.streetTxtBox.text.length>0)&&(self.buildingNoTxtBox.text.length>0))
            {
                validOk = YES;
            }
        }
        else
        {
            if ((self.areaTxtBox.text.length>0) &&( self.blockTxtBox.text.length>0)&&(self.profileNameTxtBox.text.length>0)&&(self.streetTxtBox.text.length>0)&&(self.buildingNoTxtBox.text.length>0)&&(self.floorNoTxtBox.text.length>0)&&(self.apartmentNoTxtBox.text.length>0))
            {
                validOk = YES;
            }
        }
        if (validOk)
        {
            if ([ApplicationDelegate isValid:self.customerDetailObj])
            {
                [self editAddressDetails];
            }
            else
            {
                [self addAddressDetails];
            }
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Please fill all mandatory fields(*)" title:@""];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please select the address type." title:@""];
    }
}

- (IBAction)areaSelectionBttnAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if (areaListArray.count!=0) {
        [self showAreaList];
    }
    else
    {
        [self getAreaDetails];
    }
}


#pragma mark - GET AREA LIST

-(void)getAreaDetails
{
    areaListArray=[[NSMutableArray alloc] init];
    areaObjArray=[[NSMutableArray alloc] init];
    cityListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getAreaPostData]];
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        __block BOOL success = NO;
        
        [ApplicationDelegate.engine getAreasWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             if (!success)
             {
                 success = YES;
                 if ([ApplicationDelegate isValid:responseArray])
                 {
                     if (responseArray.count>0)
                     {
                         for (int i=0; i<responseArray.count; i++)
                         {
                             if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                             {
                                 Areas *areaItem = [ApplicationDelegate.mapper getAreaListFromDictionary:[responseArray objectAtIndex:i]];
                                 
                                 [cityListArray addObject:areaItem.city_Name];
                                 [areaObjArray addObject:areaItem];
                                 if(areaItem.area_List.count!=0)
                                 {
                                     NSMutableArray *areaItemListArray=[[NSMutableArray alloc] init];
                                     
                                     for (int j=0; j<areaItem.area_List.count; j++)
                                     {
                                         if(![ApplicationDelegate isValid:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]] )
                                         {
                                             [areaItemListArray addObject:@""];
                                         }
                                         else
                                         {
                                             [areaItemListArray addObject:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]];
                                         }
                                         
                                     }
                                     //     NSLog(@"AREA_ITEM_LIST%@",areaItemListArray);
                                     [areaListArray addObject:areaItemListArray];
                                     //   NSLog(@"AREA_LIST%@",areaListArray);
                                 }
                                 
                             }
                             
                         }
                         NSLog(@"AREA_LIST%@",areaListArray);
                         
                         
                     }
                 }
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getAreaPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:kCountryId forKey:@"countryId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

#pragma mark - SHOW AREA LIST

-(void)showAreaList
{
    self.areaSelectBttn.selected = !self.areaSelectBttn.selected;
    if (self.areaSelectBttn.selected)
    {
        if (self.areaDropDownObj.view.superview) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
        self.areaDropDownObj= [[DropDownWithHeaderSelection alloc] initWithNibName:@"DropDownWithHeaderSelection" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.areaDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.areaDropDownObj.view.frame = CGRectMake((self.areaSelectBttn.superview.frame.origin.x),(self.areaSelectBttn.superview.frame.origin.y+self.areaSelectBttn.superview.frame.size.height), (self.areaSelectBttn.superview.frame.size.width), 0);
        
        self.areaDropDownObj.headerDropDownDelegate = self;
        self.areaDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:areaListArray];
        self.areaDropDownObj.headerDataArray = [[NSMutableArray alloc] initWithArray:cityListArray];
        self.areaDropDownObj.headerLabelFont = [UIFont fontWithName:@"Verdana" size:14.0];
        self.areaDropDownObj.view.layer.borderWidth = 0.1;
        self.areaDropDownObj.view.layer.shadowOpacity = 1.0;
        self.areaDropDownObj.view.layer.shadowRadius = 5.0;
        self.areaDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.areaSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////please
        self.areaDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.areaDropDownObj.textLabelColor = [UIColor blackColor];
        self.areaDropDownObj.view.backgroundColor = [UIColor whiteColor];
        self.areaDropDownObj.headerTextLabelColor = [UIColor redColor];
        if (cityListArray.count>0)
        {
            [self.areaSelectBttn.superview.superview addSubview:self.areaDropDownObj.view];
        }
        else
        {
            self.areaSelectBttn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)+18.0f;
        }
        //        [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.areaDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.areaDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x+2,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.areaDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.areaDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.areaDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    
    
}

#pragma mark - DROP-DOWN List Delegate Method


-(void)headerSelectList:(int)selectedIndex :(NSInteger)section{
    ////////// ReEnabling the UserInteraction of all SubViews/////////
    //    for (UIView *vw in self.view.subviews) {
    //        [vw setUserInteractionEnabled:YES];
    //    }
    
    [UIView animateWithDuration:0.4f animations:^{
        self.areaDropDownObj.view.frame =
        CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                   self.areaDropDownObj.view.frame.origin.y,
                   self.areaDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.areaSelectBttn.selected = NO;
    self.areaTxtBox.text =[[areaListArray objectAtIndex:section]objectAtIndex:selectedIndex];
    self.areaObj=[areaObjArray objectAtIndex:section];
    selectedAreaId=[[self.areaObj.area_List objectAtIndex:selectedIndex]objectForKey:@"AreaId"];
    
}

#pragma mark - TEXT VIEW DELEGATES

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.deliveryInfoScrollView setContentOffset:CGPointZero animated:NO];
    //textFieldFlag = YES;
    CGFloat yFact;
    
    if ([UIScreen mainScreen].bounds.size.height==480)
    {
        yFact = 20;
    }
    else
    {
        yFact = 80;
    }
    
    if ((textView.superview.frame.origin.y+textView.superview.superview.frame.origin.y)>yFact)
    {
        [self.deliveryInfoScrollView setContentOffset:CGPointMake(self.deliveryInfoScrollView.contentOffset.x, (textView.superview.frame.origin.y+textView.superview.superview.frame.origin.y-yFact))];
    }
    else
    {
        [self.deliveryInfoScrollView setContentOffset:CGPointZero animated:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    
    [self.view endEditing:YES];
    
}

- (IBAction)tapOutsideTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - TEXT FIELD DELEGATES
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[self.deliveryInfoScrollView setContentOffset:CGPointZero animated:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.deliveryInfoScrollView setContentOffset:CGPointZero animated:NO];
    //textFieldFlag = YES;
    CGFloat yFact;

        if ([UIScreen mainScreen].bounds.size.height==480)
        {
            yFact = 45;
        }
        else
        {
            yFact = 130;
        }
    
    
    if ((textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y)>yFact)
    {
        [self.deliveryInfoScrollView setContentOffset:CGPointMake(self.deliveryInfoScrollView.contentOffset.x, (textField.superview.frame.origin.y+textField.superview.superview.frame.origin.y-yFact))];
    }
    else
    {
        [self.deliveryInfoScrollView setContentOffset:CGPointZero animated:YES];
    }
    
}
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self.orderConfirmScrollView setContentOffset:CGPointZero animated:YES];
    //textFieldFlag = NO;
    //Email
//    if (textField == self.emailValueField)
//    {
//        if (textField.text.length!=0)
//        {
//            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
//            {
//                [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email address" title:kMESSAGE];
//                self.emailValueField.text=@"";
//            }
//        }
//        
//    }
}

-(void)xibLoading
{
    NSString *nibName = @"AddAddressVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
