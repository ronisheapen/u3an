//
//  AccountInfoListVC.m
//  U3AN
//
//  Created by Vipin on 23/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AccountInfoListVC.h"
#import "AccountInformationVC.h"
#import "AddAddressVC.h"
#import "AddressListVC.h"
#import "ChangePasswordVC.h"
#import "PersonalInformationVC.h"
#import "LaunchingViewController.h"

@interface AccountInfoListVC ()
{
    NSArray *accountInfoListArray;
}
@end

@implementation AccountInfoListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    accountInfoListArray=[[NSArray alloc] initWithObjects:localize(@"Account Information"),localize(@"Change Password"),localize(@"Personal Information"),localize(@"Add Address"),localize(@"Address List"),nil];
    self.accountInfoListTable.layer.cornerRadius = 3.0;
    self.accountInfoListTable.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
        
    }
    else
    {
        [self loadHomeView];
    }
}


-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

#pragma mark - UITABLEVIEW METHODS

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Code comes here..
        
        UIImageView *separatorIMg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 63, (tableView.frame.size.width-10), 2)];
        separatorIMg.backgroundColor = [UIColor clearColor];
        [separatorIMg setImage:[UIImage imageNamed:@"morepage_divider.png"]];
        [cell addSubview:separatorIMg];
        
    }
    cell.textLabel.text =[NSString stringWithFormat:@"%@",[accountInfoListArray objectAtIndex:indexPath.row]];
    cell.textLabel.textColor = [UIColor redColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    cell.backgroundColor = [UIColor clearColor];
    //    cell.backgroundColor = [UIColor colorWithRed:(245.0f/255.0f) green:(245.0f/255.0f) blue:(245.0f/255.0f) alpha:1.0];
    
    //    UIImageView * separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 267, 1)];
    //    separatorLineView.backgroundColor = [UIColor clearColor];
    //    separatorLineView.image = [UIImage imageNamed:@"line_black.png"];
    //    [cell.contentView addSubview:separatorLineView];
    
    //    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    //  [cell.textLabel setFont:self.textLabelFont];
    // [cell.textLabel setTextColor:self.textLabelColor];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        cell.textLabel.textAlignment = NSTextAlignmentRight;
    }
    return cell;
}

//-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
//    [headerView setBackgroundColor:[UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.75]];
//    //headerView.backgroundColor = [UIColor clearColor];
//
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)];
//    label.text = [self tableView:self.menuTableView titleForHeaderInSection:section];
//    label.font = [UIFont fontWithName:@"Arial" size:14.0];
//    label.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
//    label.backgroundColor = [UIColor clearColor];
//    [headerView addSubview:label];
//
//    return headerView;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return accountInfoListArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        AccountInformationVC *accountInfoVC = [[AccountInformationVC alloc] initWithNibName:@"AccountInformationVC" bundle:nil]
        ;
        accountInfoVC.view.backgroundColor = [UIColor clearColor];
      //  accountInfoVC.custAddressListArray=self.custAddressListArray;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[accountInfoVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:accountInfoVC animated:NO];
        }
        
    }
    else if(indexPath.row==1)
    {
        ChangePasswordVC *changePassVC = [[ChangePasswordVC alloc] initWithNibName:@"ChangePasswordVC" bundle:nil]
        ;
        changePassVC.view.backgroundColor = [UIColor clearColor];
        
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[changePassVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:changePassVC animated:NO];
        }
    }
    else if(indexPath.row==2)
    {
        PersonalInformationVC *personalInfoVC = [[PersonalInformationVC alloc] initWithNibName:@"PersonalInformationVC" bundle:nil]
        ;
        personalInfoVC.view.backgroundColor = [UIColor clearColor];
       // personalInfoVC.custAddressListArray=self.custAddressListArray;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[personalInfoVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:personalInfoVC animated:NO];
        }
        
    }
    else if(indexPath.row==3)
    {
        AddAddressVC *addAddressVC = [[AddAddressVC alloc] initWithNibName:@"AddAddressVC" bundle:nil]
        ;
        addAddressVC.view.backgroundColor = [UIColor clearColor];
        addAddressVC.customerDetailObj=nil;
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[addAddressVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:addAddressVC animated:NO];
        }
        
    }
    else if(indexPath.row==4)
    {
        AddressListVC *addressListVC = [[AddressListVC alloc] initWithNibName:@"AddressListVC" bundle:nil]
        ;
        addressListVC.view.backgroundColor = [UIColor clearColor];
        
       // addressListVC.custAddressListArray=self.custAddressListArray;

        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[addressListVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:addressListVC animated:NO];
        }
    }
    
}

@end
