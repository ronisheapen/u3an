//
//  PersonalInformationVC.h
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface PersonalInformationVC : UIViewController<UITextFieldDelegate,ListSelectionProtocol>

@property (strong, nonatomic) IBOutlet UILabel *personalInfoLbl;
@property (strong, nonatomic) IBOutlet UILabel *genderLbl;
@property (strong, nonatomic) IBOutlet UILabel *maleLbl;
@property (strong, nonatomic) IBOutlet UILabel *femaleLbl;
@property (strong, nonatomic) IBOutlet UILabel *occupationLbl;
@property (strong, nonatomic) IBOutlet UILabel *newsletterLbl;
@property (strong, nonatomic) IBOutlet UILabel *u3anSMSLbl;
@property (strong, nonatomic) IBOutlet UIButton *saveBttn;
@property (strong, nonatomic) IBOutlet UIButton *maleBttn;
@property (strong, nonatomic) IBOutlet UIButton *femaleBttn;
@property (strong, nonatomic) IBOutlet UIButton *occupationSelectBttn;
@property (strong, nonatomic) IBOutlet UITextField *occupationTxtBox;
@property (strong, nonatomic) NSMutableArray *custAddressListArray;
@property (strong, nonatomic) IBOutlet UIButton *newsLetterBttn;
@property (strong, nonatomic) IBOutlet UIButton *postSMSBttn;
@property (strong, nonatomic) DropDownView *occupationDropDownObj;
@property (strong, nonatomic) IBOutlet UIScrollView *personalInfoScrollView;

- (IBAction)saveButtonAction:(UIButton *)sender;
- (IBAction)genderRadioBttnAction:(UIButton *)sender;
- (IBAction)occupationSelectionBttnAction:(UIButton *)sender;
- (IBAction)newsLetterBttnAction:(UIButton *)sender;
- (IBAction)postSMSBttnAction:(UIButton *)sender;

@end
