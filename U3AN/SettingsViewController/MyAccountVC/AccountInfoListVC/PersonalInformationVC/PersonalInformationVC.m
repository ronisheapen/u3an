//
//  PersonalInformationVC.m
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "PersonalInformationVC.h"
#import "CustomerAddressDetails.h"
#import "LaunchingViewController.h"

@interface PersonalInformationVC ()
{
    bool  keepPostedSMS;
    NSString * genderType;
    NSMutableArray *occupationArray ;
}

@end

@implementation PersonalInformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    
   occupationArray = [[NSMutableArray alloc] initWithObjects:@"Other",@"Professional Services",@"Self-employed/Owner", nil];
    // Do any additional setup after loading the view from its nib.
    [self.maleBttn setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.maleBttn setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    [self.femaleBttn setBackgroundImage:[UIImage imageNamed:@"radio_unselected.png"] forState:UIControlStateNormal];
    [self.femaleBttn setBackgroundImage:[UIImage imageNamed:@"radio_selected.png"] forState:UIControlStateSelected];
    [self.newsLetterBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.newsLetterBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    [self.postSMSBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.postSMSBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    keepPostedSMS=NO;
    
    self.personalInfoScrollView.contentSize = CGSizeMake(self.personalInfoScrollView.frame.size.width,  self.saveBttn.superview.frame.origin.y+self.saveBttn.superview.frame.size.height+10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {

    [self updateViewHeading];
    [self setupUI];
    [self getCustomerAddresses];
    }
    else
    {
        [self loadHomeView];
    }
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    
    self.personalInfoScrollView.layer.cornerRadius = 3.0;
    self.personalInfoScrollView.layer.masksToBounds = YES;
    self.personalInfoLbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:16.0f];
    self.genderLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.maleLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.femaleLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.occupationLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.newsletterLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.u3anSMSLbl.font = [UIFont fontWithName:@"Tahoma" size:14.0f];

}

-(void)loadUserDetails
{
    CustomerAddressDetails * custDetailItem= [self.custAddressListArray objectAtIndex:0];
    self.occupationTxtBox.text=custDetailItem.occupation;
    self.maleBttn.selected = NO;
    self.femaleBttn.selected = NO;
    
    if ([custDetailItem.gender caseInsensitiveCompare:@"male"]==NSOrderedSame)
    {
        self.maleBttn.selected = YES;
        genderType=@"Male";
    }
    if ([custDetailItem.gender caseInsensitiveCompare:@"female"]==NSOrderedSame)
    {
        self.femaleBttn.selected = YES;
        genderType=@"Female";
    }
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    /*
    if (custDetailItem.subscribed_to_news) {
        self.newsLetterBttn.selected=YES;
        newsLetter=YES;
    }
    else
    {
        self.newsLetterBttn.selected=NO;
        newsLetter=NO;
    }
     */
    
    self.newsLetterBttn.selected = ApplicationDelegate.newsLetterStatusFlag;
    
    /////////**********************
    
    if (custDetailItem.subscribed_to_sms) {
        self.postSMSBttn.selected=YES;
        keepPostedSMS=YES;
    }
    else
    {
        self.postSMSBttn.selected=NO;
        keepPostedSMS=NO;
    }
}

-(void)getCustomerAddresses
{
    self.custAddressListArray= [[NSMutableArray alloc] init];
    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    if (custPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             NSMutableArray *addressArray = [[NSMutableArray alloc] init];
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             
                             
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
                             {
                                 addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
                             }
                             if (addressArray.count>0)
                             {
                                 for (int i=0; i<addressArray.count; i++)
                                 {
                                     if ([ApplicationDelegate isValid:[addressArray objectAtIndex:i]])
                                     {
                                         CustomerAddressDetails *addressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:i]];
                                         
                                         [self.custAddressListArray addObject:addressItem];
                                     }
                                     
                                 }
                                 
                                 [self loadUserDetails];
                             }
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}


- (IBAction)saveButtonAction:(UIButton *)sender {
    
    [self editCustomerDetails];

}

- (IBAction)genderRadioBttnAction:(UIButton *)sender {
    self.saveBttn.enabled=YES;
    
    if (sender.tag==1) {
        self.maleBttn.selected = YES;
        self.femaleBttn.selected = NO;
        
        genderType=@"Male";
        
        
    }
    else if (sender.tag==2)
    {
        self.maleBttn.selected = NO;
        self.femaleBttn.selected = YES;
        genderType=@"Female";
        
    }
}

- (IBAction)occupationSelectionBttnAction:(UIButton *)sender {
    self.occupationSelectBttn.selected = !self.occupationSelectBttn.selected;
    self.saveBttn.enabled=YES;
    if (self.occupationSelectBttn.selected)
    {
        if (self.occupationDropDownObj.view.superview) {
            [self.occupationDropDownObj.view removeFromSuperview];
        }
        
        self.occupationDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.occupationDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.occupationDropDownObj.view.frame = CGRectMake((self.occupationSelectBttn.superview.frame.origin.x),(self.occupationSelectBttn.superview.frame.origin.y+self.occupationSelectBttn.superview.frame.size.height), (self.occupationSelectBttn.superview.frame.size.width), 0);
        
        self.occupationDropDownObj.dropDownDelegate = self;
        self.occupationDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:occupationArray];
        
        self.occupationDropDownObj.view.layer.borderWidth = 0.1;
        self.occupationDropDownObj.view.layer.shadowOpacity = 1.0;
        self.occupationDropDownObj.view.layer.shadowRadius = 5.0;
        self.occupationDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.occupationSelectionBttn.superview) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.occupationDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.occupationDropDownObj.textLabelColor = [UIColor grayColor];
        self.occupationDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (occupationArray.count>0)
        {
            [self.occupationSelectBttn.superview.superview addSubview:self.occupationDropDownObj.view];
        }
        else
        {
            self.occupationSelectBttn.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 140.0f;
        
        if ((self.occupationDropDownObj.cellHeight*self.occupationDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.occupationDropDownObj.cellHeight*self.occupationDropDownObj.dataArray.count)+18.0f;
        }
        
        [UIView animateWithDuration:0.09f animations:^{
            self.occupationDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.occupationDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.occupationDropDownObj.view.frame =
            CGRectMake(self.occupationDropDownObj.view.frame.origin.x+2,
                       self.occupationDropDownObj.view.frame.origin.y,
                       self.occupationDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.occupationDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.occupationDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.occupationDropDownObj.view.frame =
            CGRectMake(self.occupationDropDownObj.view.frame.origin.x,
                       self.occupationDropDownObj.view.frame.origin.y,
                       self.occupationDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.occupationDropDownObj.view removeFromSuperview];
            }
            
        }];
    }

}

- (IBAction)newsLetterBttnAction:(UIButton *)sender {
    self.newsLetterBttn.selected = !self.newsLetterBttn.selected;
    self.saveBttn.enabled=YES;
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    
    ApplicationDelegate.newsLetterStatusFlag = self.newsLetterBttn.selected;
    [ApplicationDelegate saveNewsLetterStatusWithStatus:ApplicationDelegate.newsLetterStatusFlag];
    
}

- (IBAction)postSMSBttnAction:(UIButton *)sender {
    
    self.postSMSBttn.selected = !self.postSMSBttn.selected;
    self.saveBttn.enabled=YES;
    
    if (self.postSMSBttn.selected==YES) {
        keepPostedSMS=YES;
    }
    else{
        keepPostedSMS=NO;
    }
}
-(void)editCustomerDetails
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getCustPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine editCustomerDetailsWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                 {
                     self.saveBttn.enabled=NO;
                     
                     [ApplicationDelegate showAlertWithMessage:@"Profile updated successfully." title:nil];
                     
                     if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
                     {
                         [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
                     }
                     
                 }
                 else
                 {
                    [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}
-(NSMutableDictionary *)getCustPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    if ([ApplicationDelegate isValid:self.custAddressListArray])
    {
        if (self.custAddressListArray.count>0)
        {
            if ([ApplicationDelegate isValid:[self.custAddressListArray objectAtIndex:0]])
            {
                CustomerAddressDetails * custDetailItem= [self.custAddressListArray objectAtIndex:0];
                [postDic setObject:ApplicationDelegate.authn_Key forKey:@"authKey"];
                [postDic setObject:custDetailItem.birthday forKey:@"birthday"];
                [postDic setObject:custDetailItem.company forKey:@"company"];
                [postDic setObject:custDetailItem.firstName forKey:@"firstName"];
                [postDic setObject:genderType forKey:@"gender"];
                [postDic setObject:custDetailItem.housePhone forKey:@"housePhone"];
                [postDic setObject:@"" forKey:@"howToContact"];
                [postDic setObject:custDetailItem.lastName forKey:@"lastName"];
                //[postDic setObject:kLocal forKey:@"locale"];
                if (![ApplicationDelegate.language isEqualToString:kENGLISH])
                {
                    [postDic setObject:k_AR_Local forKey:@"locale"];
                }
                else
                {
                    [postDic setObject:kLocal forKey:@"locale"];
                }
                [postDic setObject:custDetailItem.mobile forKey:@"mobile"];
                [postDic setObject:self.occupationTxtBox.text forKey:@"occupation"];
                //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
                [postDic setObject:[NSNumber numberWithBool:ApplicationDelegate.newsLetterStatusFlag] forKey:@"subscribedToNewsletter"];
                [postDic setObject:[NSNumber numberWithBool:keepPostedSMS] forKey:@"subscribedToSMS"];
                [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
                [postDic setObject:custDetailItem.workPhone forKey:@"workPhone"];
            }
        }
    }
    
    return postDic;
}


-(void)selectList:(int)selectedIndex
{
 
        [UIView animateWithDuration:0.4f animations:^{
            self.occupationDropDownObj.view.frame =
            CGRectMake(self.occupationDropDownObj.view.frame.origin.x,
                       self.occupationDropDownObj.view.frame.origin.y,
                       self.occupationDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.occupationDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.occupationSelectBttn.selected = NO;
        self.occupationTxtBox.text =[occupationArray objectAtIndex:selectedIndex];
    }


-(void)xibLoading
{
    NSString *nibName = @"PersonalInformationVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
