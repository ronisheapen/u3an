//
//  OrderlistViewController.m
//  U3AN
//
//  Created by Vineeth on 01/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "OrderlistViewController.h"
#import "OrderDetailViewController.h"
#import "LaunchingViewController.h"

@interface OrderlistViewController ()

@end

@implementation OrderlistViewController
@synthesize orderListArray;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.orderListTable.layer.cornerRadius=2.0;
    self.orderListTable.layer.masksToBounds=YES;
    // Do any additional setup after loading the view from its nib.
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    [self getOrderData];
    }
    else
    {
        [self loadHomeView];
    }
    
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    //   [[HomeTabViewController sharedViewController] resetTabSelection];
    // [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getOrderData
{
    if ([self getUserPostData].count==0) {
        return;
    }
  
     self.orderListArray= [[NSMutableArray alloc] init];
    
    __block BOOL dataFound = NO;
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine OrderDetailsWithDataDictionary:[self getUserPostData] CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if (!dataFound)
         {
             dataFound = YES;
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"TransactionList"]])
                     {
                         if ([[responseDictionary objectForKey:@"TransactionList"] count]>0)
                         {
                             for (NSMutableDictionary *dic in [responseDictionary objectForKey:@"TransactionList"])
                             {
                                 if ([ApplicationDelegate isValid:dic])
                                 {
                                     if (dic.count>0)
                                     {
                                         [self.orderListArray addObject:dic];
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
             
         }
         
         [self.orderListTable reloadData];
         [ApplicationDelegate removeProgressHUD];
        
    } errorHandler:^(NSError *error) {
         [ApplicationDelegate removeProgressHUD];
    }];
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width-20, 18)];
    [label setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f]];
    [label setTextColor:[UIColor redColor]];
    NSString *string = localize(@"My Orders");
    
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        label.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        label.textAlignment = NSTextAlignmentRight;
    }
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]]; //your background color...
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addressTableIdentifier = @"orderident";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
    
    if (cell == nil)
    {
        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [tableView registerNib:[UINib nibWithNibName:@"ordercell" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
        }
        else
        {
            [tableView registerNib:[UINib nibWithNibName:@"ordercell_a" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
        }
    }
    
    MyOrderDeliveryDetail *orderDelDetail = [ApplicationDelegate.mapper getOrderDeliveryDetailsFromDictionary:[self.orderListArray objectAtIndex:indexPath.row]];
    
    UILabel *lbl_text=(UILabel *)[cell viewWithTag:1];
    lbl_text.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
    lbl_text.text=orderDelDetail.order_Date;
    
    UILabel *lbl_areablock=(UILabel *)[cell viewWithTag:2];
    lbl_areablock.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    NSString *areablock=[NSString stringWithFormat:@"%@,%@",orderDelDetail.delivery_Area,orderDelDetail.delivery_Block];
    lbl_areablock.text=areablock;
    
    [ApplicationDelegate loadMarqueeLabelWithText:lbl_areablock.text Font:lbl_areablock.font InPlaceOfLabel:lbl_areablock];
    
    UILabel *price_text=(UILabel *)[cell viewWithTag:3];
    price_text.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    price_text.text=[NSString stringWithFormat:@"KD %@",orderDelDetail.total];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return orderListArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyOrderDeliveryDetail *orderDelDetail = [ApplicationDelegate.mapper getOrderDeliveryDetailsFromDictionary:[self.orderListArray objectAtIndex:indexPath.row]];
    
    OrderDetailViewController *orderVC = [[OrderDetailViewController alloc] initWithNibName:@"OrderDetailViewController" bundle:nil]
    ;
    orderVC.passtransactionid=orderDelDetail.transactionID;

    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[orderVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:orderVC animated:NO];
    }
 
}

@end
