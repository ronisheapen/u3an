//
//  OrderDetailViewController.m
//  U3AN
//
//  Created by Vineeth on 03/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "OrderItemListViewController.h"
#import "LaunchingViewController.h"

@interface OrderDetailViewController ()
{
    MyOrderDeliveryDetail *selectedMyOrderDeliveryDetail;
    float postRatingValue;
    NSString *selected_restaurantID;
}
@end

@implementation OrderDetailViewController
@synthesize orderDetailListArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myOrdersHeaderLabel.text = localize(@"My Orders");
    
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.myOrdersHeaderLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        self.myOrdersHeaderLabel.textAlignment = NSTextAlignmentRight;
    }
    [self.myOrdersHeaderLabel setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f]];
    self.orderListTable.layer.cornerRadius=2.0;
    self.orderListTable.layer.masksToBounds=YES;
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    [self getOrderData];
    }
    else
    {
        [self loadHomeView];
    }
    
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    //   [[HomeTabViewController sharedViewController] resetTabSelection];
    // [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

//- (void)setUpEditableRateView {
//    RateView *rateView = [[RateView alloc] initWithFrame:CGRectMake(0, 40, self.view.bounds.size.width, 20) fullStar:[UIImage imageNamed:@"StarFullLarge.png"] emptyStar:[UIImage imageNamed:@"StarEmptyLarge.png"]];
//    rateView.padding = 20;
//    rateView.alignment = RateViewAlignmentCenter;
//    rateView.editable = YES;
//    rateView.delegate = self;
//    [self.view addSubview:rateView];
//    
//    // Set up a label view to display rate
//    self.rateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, self.view.bounds.size.width, 20)];
//    self.rateLabel.textAlignment = UITextAlignmentCenter;
//    self.rateLabel.text = @"Tap above to rate";
//    [self.view addSubview:self.rateLabel];
//}

-(void)getOrderData
{
    self.orderDetailListArray= [[NSMutableArray alloc] init];
    selectedMyOrderDeliveryDetail = [[MyOrderDeliveryDetail alloc] init];
    
    if ([self getUserPostData].count==0) {
        return;
    }
    
    __block BOOL dataFound = NO;
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine OrderDetailsWithDataDictionary:[self getUserPostData] CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if (!dataFound)
         {
             dataFound = YES;
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"TransactionList"]])
                     {
                         if ([[responseDictionary objectForKey:@"TransactionList"] count]>0)
                         {
                             for (NSMutableDictionary *dic in [responseDictionary objectForKey:@"TransactionList"])
                             {
                                 if ([ApplicationDelegate isValid:dic])
                                 {
                                     if (dic.count>0)
                                     {
                                         MyOrderDeliveryDetail *orderDelDetail = [ApplicationDelegate.mapper getOrderDeliveryDetailsFromDictionary:dic];
                                         
                                         if ((orderDelDetail.transactionID.length>0)&&(self.passtransactionid.length>0))
                                         {
                                             if ([orderDelDetail.transactionID isEqualToString:self.passtransactionid])
                                             {
                                                 //MATCH FOUND CASE
                                                 
                                                 selectedMyOrderDeliveryDetail = orderDelDetail;
                                                 
                                                 [self populateDataInView];
                                                 
                                                 break;
                                                 
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
             
         }

         [self.orderListTable reloadData];
        
         [ApplicationDelegate removeProgressHUD];
        
    } errorHandler:^(NSError *error) {
        [ApplicationDelegate removeProgressHUD];
    }];
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    return postDic;
}

-(void)populateDataInView
{
    self.header_date_lbl.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.header_area_lbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    self.header_price_lbl.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:14.0f];
    
    self.header_date_lbl.text=selectedMyOrderDeliveryDetail.order_Date;
    
    NSString *areablock=[NSString stringWithFormat:@"%@,%@",selectedMyOrderDeliveryDetail.delivery_Area,selectedMyOrderDeliveryDetail.delivery_Block];
    
    self.header_area_lbl.text=areablock;
    
    [ApplicationDelegate loadMarqueeLabelWithText:self.header_area_lbl.text Font:self.header_area_lbl.font InPlaceOfLabel:self.header_area_lbl];
    
    self.header_price_lbl.text=[NSString stringWithFormat:@"KD %@",selectedMyOrderDeliveryDetail.total];
    
    
    self.orderDetailListArray = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary *dic in selectedMyOrderDeliveryDetail.orders_Array)
    {
        if ([ApplicationDelegate isValid:dic])
        {
            if (dic.count>0)
            {
                MyOrderOrdersDetail *orderItem = [ApplicationDelegate.mapper getOrderDetailsFromDictionary:dic];
                
                [self.orderDetailListArray addObject:orderItem];
            }
        }
    }
    if (self.orderDetailListArray.count==0)
    {
        [ApplicationDelegate showAlertWithMessage:kNO_DATA_ERROR_MSG title:kMESSAGE];
    }
    [self.orderListTable reloadData];
}

-(void)postItemRating
{
    NSMutableDictionary *ratingPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (ratingPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine postItemRatingWithDataDictionary:ratingPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                 {
                     if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                     {
                         [self getOrderData];
                     }
                    
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}


-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:[NSNumber numberWithLongLong:[selected_restaurantID longLongValue]] forKey:@"ItemOrRestaurantId"];
    [postDic setObject:[NSNumber numberWithInteger:(NSInteger)postRatingValue] forKey:@"Rating"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"UserId"];
    [postDic setObject:@"Restaurant" forKey:@"flag"];

    return postDic;
}

#pragma mark - Table View Delegates

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 55)];
    view.backgroundColor=[UIColor whiteColor];
   
 /*   UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    [label setTextColor:[UIColor redColor]];
    NSString *string =@"My Orders";
  
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]]; //your background color...*/
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *addressTableIdentifier = @"orderdetailident";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
    
    if (cell == nil)
    {
        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [tableView registerNib:[UINib nibWithNibName:@"orderdetailcell" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
        }
        else
        {
            [tableView registerNib:[UINib nibWithNibName:@"orderdetailcell_a" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
        }
    }
    
    MyOrderOrdersDetail *orderItem = [self.orderDetailListArray objectAtIndex:indexPath.row];
    
//    NSLog(@"%@",orderItem.charges_ForU3an);
//    NSLog(@"%@",orderItem.delivery_Charges);
//    NSLog(@"%@",orderItem.delivery_Time);
//    NSLog(@"%@",orderItem.discount);
//    NSLog(@"%@",orderItem.general_Request);
//    NSLog(@"%@",orderItem.order_ID);
//    NSLog(@"%@",orderItem.order_Mode);
//    NSLog(@"%@",orderItem.order_Source);
//    NSLog(@"%@",orderItem.order_Status);
//    NSLog(@"%@",orderItem.order_Type);
//    NSLog(@"%@",orderItem.payment_Method);
//    NSLog(@"%@",orderItem.rating);
//    NSLog(@"%@",orderItem.restaurant);
//    NSLog(@"%@",orderItem.restaurant_ID);
//    NSLog(@"%@",orderItem.subTotal);
//    NSLog(@"%@",orderItem.total);
//    NSLog(@"%@",orderItem.orderItems_Array);
    
    
    UILabel *lbl_text=(UILabel *)[[[cell viewWithTag:1] subviews] objectAtIndex:0];
    lbl_text.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    lbl_text.text=orderItem.restaurant;
    
    [ApplicationDelegate loadMarqueeLabelWithText:lbl_text.text Font:lbl_text.font InPlaceOfLabel:lbl_text];
    
    UILabel *lbl_date=(UILabel *)[cell viewWithTag:2];
    lbl_date.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    lbl_date.text=selectedMyOrderDeliveryDetail.order_Date;
    
    UILabel *lbl_deliveryTime=(UILabel *)[cell viewWithTag:8];
    lbl_deliveryTime.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    lbl_deliveryTime.text=orderItem.delivery_Time;
    
    UILabel *lbl_review=(UILabel *)[cell viewWithTag:5];
    lbl_review.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
    lbl_review.text = localize(@"Review");
    
    DLStarRatingControl *customNumberOfStars =(DLStarRatingControl *)[[[cell viewWithTag:4] subviews] objectAtIndex:0];
    customNumberOfStars.isFractionalRatingEnabled = NO;
    customNumberOfStars.delegate=self;
    customNumberOfStars.tag=1000+indexPath.row;
    customNumberOfStars.rating = [orderItem.rating floatValue];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu",(unsigned long)orderDetailListArray.count);
    return orderDetailListArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyOrderOrdersDetail *orderItem = [self.orderDetailListArray objectAtIndex:indexPath.row];
    
    OrderItemListViewController *orderItemVC = [[OrderItemListViewController alloc] initWithNibName:@"OrderItemListViewController" bundle:nil]
    ;
    orderItemVC.passtransactionid=self.passtransactionid;
    orderItemVC.orderId = orderItem.order_ID;
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[orderItemVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:orderItemVC animated:NO];
    }
    
}

#pragma mark - UNRateViewDelegate
//
//- (void)rateView:(RateView *)rateView changedToNewRate:(NSNumber *)rate {
//   // self.rateLabel.text = [NSString stringWithFormat:@"Rate: %d", rate.intValue];
//}


#pragma mark -
#pragma mark Delegate implementation of NIB instatiated DLStarRatingControl

-(void)newRating:(DLStarRatingControl *)control :(float)rating {
    //NSLog(@"%f",rating);
  //  self.stars.text = [NSString stringWithFormat:@"%0.1f star rating",rating];
    //control.delegate.
   MyOrderOrdersDetail *orderItem = [self.orderDetailListArray objectAtIndex:control.tag-1000];
    postRatingValue = rating;
    selected_restaurantID = orderItem.restaurant_ID;
    //NSLog(@"%ld", (long)control.tag);
    [self postItemRating];
}


@end
