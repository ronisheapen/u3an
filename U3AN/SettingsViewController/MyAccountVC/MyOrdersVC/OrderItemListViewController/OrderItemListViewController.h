//
//  OrderItemListViewController.h
//  U3AN
//
//  Created by Vipin on 10/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"

@interface OrderItemListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,DLStarRatingDelegate>
@property (strong, nonatomic) IBOutlet UIView *orderItem_InnerView;
@property (strong, nonatomic) IBOutlet UILabel *restaurant_NameLbl;
@property (strong, nonatomic) IBOutlet UILabel *date_Label;
@property (strong, nonatomic) IBOutlet UILabel *deliveryTimeLabel;

@property (strong, nonatomic) IBOutlet DLStarRatingControl *restaurantStarRating;

@property (strong, nonatomic) IBOutlet UILabel *review_Label;

@property (strong, nonatomic) IBOutlet UITableView *orderItem_ListTable;

@property (strong, nonatomic) NSMutableArray *orderItemsListArray;

@property (strong, nonatomic) NSString *passtransactionid;
@property (strong, nonatomic) NSString *orderId;

@property (weak, nonatomic) IBOutlet UILabel *item_Label;
@property (weak, nonatomic) IBOutlet UILabel *specialRequest_Label;
@property (weak, nonatomic) IBOutlet UILabel *rating_Label;
@property (weak, nonatomic) IBOutlet UILabel *price_Label;
@property (weak, nonatomic) IBOutlet UILabel *total_label;
@property (weak, nonatomic) IBOutlet UILabel *quantity_Label;

@end
