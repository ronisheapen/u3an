//
//  OrderItemListViewController.m
//  U3AN
//
//  Created by Vipin on 10/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "OrderItemListViewController.h"
#import "MyOrderDeliveryDetail.h"
#import "LaunchingViewController.h"

@interface OrderItemListViewController ()
{
    MyOrderDeliveryDetail *selectedMyOrderDeliveryDetail;
    MyOrderOrdersDetail *selectedMyOrderDetail;
    
    
}
@end

@implementation OrderItemListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    self.item_Label.text = localize(@"Item");
    self.specialRequest_Label.text = localize(@"Special Request");
    self.rating_Label.text = localize(@"Rating / Review");
    self.quantity_Label.text = localize(@"Qty");
    self.price_Label.text = localize(@"Price");
    self.total_label.text = localize(@"Total");
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    [self setupUI];
    [self getOrderData];
    }
    else
    {
        [self loadHomeView];
    }
        
    
}


-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    //   [[HomeTabViewController sharedViewController] resetTabSelection];
    // [HomeTabViewController sharedViewController].mostSellingButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.orderItem_InnerView.layer.cornerRadius = 3.0;
    self.orderItem_InnerView.layer.masksToBounds = YES;
    [self.restaurant_NameLbl setFont:[UIFont fontWithName:@"Tahoma" size:13.0f]];
    [self.date_Label setFont:[UIFont fontWithName:@"Tahoma" size:13.0f]];
    [self.deliveryTimeLabel setFont:[UIFont fontWithName:@"Tahoma" size:13.0f]];
    [self.review_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:13.0f]];
}

-(void)getOrderData
{
    self.orderItemsListArray = [[NSMutableArray alloc] init];
    selectedMyOrderDeliveryDetail = [[MyOrderDeliveryDetail alloc] init];
    selectedMyOrderDetail = [[MyOrderOrdersDetail alloc] init];
    
    if ([self getUserPostData].count==0) {
        return;
    }
    
    __block BOOL dataFound = NO;
    [ApplicationDelegate addProgressHUDToView:self.view];
    [ApplicationDelegate.engine OrderDetailsWithDataDictionary:[self getUserPostData] CompletionHandler:^(NSMutableDictionary *responseDictionary)
     {
         if (!dataFound)
         {
             dataFound = YES;
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"TransactionList"]])
                     {
                         if ([[responseDictionary objectForKey:@"TransactionList"] count]>0)
                         {
                             for (NSMutableDictionary *dic in [responseDictionary objectForKey:@"TransactionList"])
                             {
                                 if ([ApplicationDelegate isValid:dic])
                                 {
                                     if (dic.count>0)
                                     {
                                         MyOrderDeliveryDetail *orderDelDetail = [ApplicationDelegate.mapper getOrderDeliveryDetailsFromDictionary:dic];
                                         
                                         if ((orderDelDetail.transactionID.length>0)&&(self.passtransactionid.length>0))
                                         {
                                             if ([orderDelDetail.transactionID isEqualToString:self.passtransactionid])
                                             {
                                                 //MATCH FOUND CASE
                                                 
                                                 
                                                 selectedMyOrderDeliveryDetail = orderDelDetail;
                                                 
                                                 [self getSelectedOrderDetail];
                                                 
                                                 break;
                                                 
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
             
         }
         
         
         [ApplicationDelegate removeProgressHUD];
         
     } errorHandler:^(NSError *error) {
         [ApplicationDelegate removeProgressHUD];
     }];
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    return postDic;
}

-(void)getSelectedOrderDetail
{
    if ((selectedMyOrderDeliveryDetail.orders_Array.count>0)&&(self.orderId.length>0))
    {
        for (NSMutableDictionary *dic in selectedMyOrderDeliveryDetail.orders_Array)
        {
            if ([ApplicationDelegate isValid:dic])
            {
                MyOrderOrdersDetail *orderDetail = [ApplicationDelegate.mapper getOrderDetailsFromDictionary:dic];
                
                if (orderDetail.order_ID.length>0)
                {
                    if ([orderDetail.order_ID isEqualToString:self.orderId])
                    {
                        selectedMyOrderDetail = orderDetail;
                        
                        [self populateData];
                        
                        break;
                    }
                }
            }
            
        }
    }
}

-(void)populateData
{
    
    self.restaurant_NameLbl.text = selectedMyOrderDetail.restaurant;
    
    [ApplicationDelegate loadMarqueeLabelWithText:self.restaurant_NameLbl.text Font:self.restaurant_NameLbl.font InPlaceOfLabel:self.restaurant_NameLbl];
    
    self.date_Label.text = selectedMyOrderDeliveryDetail.order_Date;
    
    self.deliveryTimeLabel.text = selectedMyOrderDetail.delivery_Time;
    
    self.restaurantStarRating.rating = [selectedMyOrderDetail.rating floatValue];
    
    self.orderItemsListArray = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary *dic in selectedMyOrderDetail.orderItems_Array)
    {
        if ([ApplicationDelegate isValid:dic])
        {
            if (dic.count>0)
            {
                MyOrderItemDetail *orderItem = [ApplicationDelegate.mapper getOrderItemDetailsFromDictionary:dic];
                
                [self.orderItemsListArray addObject:orderItem];
            }
        }
    }
    if (self.orderItemsListArray.count==0)
    {
        [ApplicationDelegate showAlertWithMessage:kNO_DATA_ERROR_MSG title:kMESSAGE];
    }
    [self.orderItem_ListTable reloadData];
}

-(void)postItemRatingWithPostData:(NSMutableDictionary *)postData
{
    
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine postItemRatingWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                 {
                     if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                     {
                         [self getOrderData];
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}


-(NSMutableDictionary *)getPostDataWithOrderDetail:(MyOrderItemDetail *)orderItem AndRating:(float)rate
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:[NSNumber numberWithLongLong:[orderItem.item_ID longLongValue]] forKey:@"ItemOrRestaurantId"];
    [postDic setObject:[NSNumber numberWithInteger:(NSInteger)rate] forKey:@"Rating"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"UserId"];
    [postDic setObject:@"Item" forKey:@"flag"];
    
    return postDic;
}

-(NSMutableDictionary *)getPostDataForOrderDeliveryDetailWithRating:(float)rate
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:[NSNumber numberWithLongLong:[selectedMyOrderDetail.restaurant_ID longLongValue]] forKey:@"ItemOrRestaurantId"];
    [postDic setObject:[NSNumber numberWithInteger:(NSInteger)rate] forKey:@"Rating"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"UserId"];
    [postDic setObject:@"Restaurant" forKey:@"flag"];
    
    return postDic;
}

#pragma mark - UITABLEVIEW DELEGATE METHODS

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor=[UIColor clearColor];
    
    /*   UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
     [label setFont:[UIFont boldSystemFontOfSize:16]];
     [label setTextColor:[UIColor redColor]];
     NSString *string =@"My Orders";
     
     [label setText:string];
     [view addSubview:label];
     [view setBackgroundColor:[UIColor whiteColor]]; //your background color...*/
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != (self.orderItemsListArray.count))
    {
        static NSString *addressTableIdentifier = @"orderitemident";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
        
        if (cell == nil)
        {
            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
            {
                [tableView registerNib:[UINib nibWithNibName:@"orderItemCell" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
                cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
            }
            else
            {
                [tableView registerNib:[UINib nibWithNibName:@"orderItemCell_a" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
                cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
            }
            
        }
        
        MyOrderItemDetail *orderItem = [self.orderItemsListArray objectAtIndex:indexPath.row];
        
        UILabel *lbl_text=(UILabel *)[cell viewWithTag:11];
        lbl_text.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
        lbl_text.text=orderItem.item_Name;
        
        //    [ApplicationDelegate loadMarqueeLabelWithText:lbl_text.text Font:lbl_text.font InPlaceOfLabel:lbl_text];
        
        UILabel *lbl_spclReq=(UILabel *)[cell viewWithTag:12];
        lbl_spclReq.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
        lbl_spclReq.text=orderItem.special_Request;
        
        UILabel *lbl_qty=(UILabel *)[cell viewWithTag:13];
        lbl_qty.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
        lbl_qty.text=orderItem.item_Quantity;
        
        UILabel *lbl_price=(UILabel *)[cell viewWithTag:14];
        lbl_price.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
        lbl_price.text=orderItem.item_Price;
        
        UILabel *lbl_total=(UILabel *)[cell viewWithTag:15];
        lbl_total.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
        float total = [orderItem.item_Price floatValue] * [orderItem.item_Quantity floatValue];
        lbl_total.text=[NSString stringWithFormat:@"%f",total];
        
        DLStarRatingControl *customNumberOfStars =(DLStarRatingControl *)[[[cell viewWithTag:3] subviews] objectAtIndex:0];
        customNumberOfStars.isFractionalRatingEnabled = NO;
        customNumberOfStars.delegate=self;
        customNumberOfStars.tag=indexPath.row+1100;
        customNumberOfStars.rating = [orderItem.rating floatValue];
        
        return cell;
    }
    else
    {
        static NSString *summaryTableIdentifier = @"orderitemsummaryident";
        
        UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:summaryTableIdentifier];
        
        if (cell1 == nil)
        {
            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
            {
                [tableView registerNib:[UINib nibWithNibName:@"orderItemSummaryCell" bundle:nil] forCellReuseIdentifier:summaryTableIdentifier];
                cell1 = [tableView dequeueReusableCellWithIdentifier:summaryTableIdentifier];
            }
            else
            {
                [tableView registerNib:[UINib nibWithNibName:@"orderItemSummaryCell_a" bundle:nil] forCellReuseIdentifier:summaryTableIdentifier];
                cell1 = [tableView dequeueReusableCellWithIdentifier:summaryTableIdentifier];
            }
        }
        
        UILabel *lbl_genReqName=(UILabel *)[cell1 viewWithTag:1];
        lbl_genReqName.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
        lbl_genReqName.text = localize(@"General Request");
        
        UILabel *lbl_genReqValue=(UILabel *)[cell1 viewWithTag:2];
        lbl_genReqValue.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
        
        UILabel *lbl_subTotalName=(UILabel *)[cell1 viewWithTag:3];
        lbl_subTotalName.font = [UIFont fontWithName:@"Tahoma-Bold" size:12.0f];
        lbl_subTotalName.text = localize(@"Sub Total");
        
        UILabel *lbl_delChargesName=(UILabel *)[cell1 viewWithTag:4];
        lbl_delChargesName.font = [UIFont fontWithName:@"Tahoma-Bold" size:12.0f];
        lbl_delChargesName.text = localize(@"Delivery Charges");
        
        UILabel *lbl_totalName=(UILabel *)[cell1 viewWithTag:5];
        lbl_totalName.font = [UIFont fontWithName:@"Tahoma-Bold" size:12.0f];
        lbl_totalName.text = localize(@"Total");
        
        UILabel *lbl_subTotalValue=(UILabel *)[cell1 viewWithTag:6];
        lbl_subTotalValue.font = [UIFont fontWithName:@"Tahoma-Bold" size:12.0f];
        lbl_subTotalValue.text = [NSString stringWithFormat:localize(@"KD %@"),selectedMyOrderDetail.subTotal];
        
        UILabel *lbl_delChargesValue=(UILabel *)[cell1 viewWithTag:7];
        lbl_delChargesValue.font = [UIFont fontWithName:@"Tahoma-Bold" size:12.0f];
        lbl_delChargesValue.text = [NSString stringWithFormat:localize(@"KD %@"),selectedMyOrderDetail.delivery_Charges];
        
        UILabel *lbl_totalValue=(UILabel *)[cell1 viewWithTag:8];
        lbl_totalValue.font = [UIFont fontWithName:@"Tahoma-Bold" size:12.0f];
        lbl_totalValue.text = [NSString stringWithFormat:localize(@"KD %@"),selectedMyOrderDetail.total];
        
        return cell1;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu",(unsigned long)self.orderItemsListArray.count);
    if (self.orderItemsListArray.count>0)
    {
        return (self.orderItemsListArray.count+1);
    }
    else
    {
        return 0;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.orderItemsListArray.count>0)
    {
        if (indexPath.row == (self.orderItemsListArray.count))
        {
            return 75;
        }
        else
        {
            return 65;
        }
    }
    else
    {
       return 65;
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma mark -
#pragma mark Delegate implementation of NIB instatiated DLStarRatingControl

-(void)newRating:(DLStarRatingControl *)control :(float)rating {
    
    if(control==self.restaurantStarRating)
    {
        [self postItemRatingWithPostData:[self getPostDataForOrderDeliveryDetailWithRating:rating]];
    }
    else
    {
        NSInteger indexVal = control.tag-1100;
        
        if (indexVal<self.orderItemsListArray.count)
        {
            MyOrderItemDetail *orderItem = (MyOrderItemDetail *)[self.orderItemsListArray objectAtIndex:indexVal];
            
            [self postItemRatingWithPostData:[self getPostDataWithOrderDetail:orderItem AndRating:rating]];

        }
    }

}

-(void)xibLoading
{
    NSString *nibName = @"OrderItemListViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
