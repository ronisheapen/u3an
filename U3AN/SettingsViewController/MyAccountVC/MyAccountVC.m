//
//  MyAccountVC.m
//  U3AN
//
//  Created by Vipin on 23/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MyAccountVC.h"
#import "AccountInfoListVC.h"
#import "MyFavouritesVC.h"
#import "OrderlistViewController.h"
#import "LaunchingViewController.h"

@interface MyAccountVC ()
{
    NSArray *accountArray;
}

@end

@implementation MyAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    self.myAccountLbl.text = localize(@"My Account");
    self.welcomeLbl.text = localize(@"Welcome:");
    self.u3anCreditLbl.text = localize(@"U3AN Credit:");
    // Do any additional setup after loading the view from its nib.
    accountArray=[[NSArray alloc] initWithObjects:localize(@"Account Information"),localize(@"My Orders"),localize(@"My Favourites"),nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    [self setupUI];
    [self getCustomerAddresses];
    }
    else
    {
        [self loadHomeView];
    }
}


-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}
-(void)setupUI
{
    self.myAccountScrollView.layer.cornerRadius = 3.0;
    self.myAccountScrollView.layer.masksToBounds = YES;
    self.myAccountTable.layer.cornerRadius = 3.0;
    self.myAccountTable.layer.masksToBounds = YES;
    [self.myAccountLbl setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    [self.u3anCreditLbl setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];
    [self.creditLbl setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];
    [self.welcomeLbl setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];
    [self.userNameLbl setFont:[UIFont fontWithName:@"Tahoma" size:14.0f]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getCustomerAddresses
{
    self.custAddressListArray= [[NSMutableArray alloc] init];
    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    if (custPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             NSMutableArray *addressArray = [[NSMutableArray alloc] init];
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             
                                 
                                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
                                 {
                                     addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
                                 }
                                 if (addressArray.count>0)
                                 {
                                     for (int i=0; i<addressArray.count; i++)
                                     {
                                         if ([ApplicationDelegate isValid:[addressArray objectAtIndex:i]])
                                         {
                                             CustomerAddressDetails *addressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:i]];
                                             
                                             [self.custAddressListArray addObject:addressItem];
                                         }
                                         
                                     }
                                     
                                     CustomerAddressDetails *tempAddressItem =[self.custAddressListArray objectAtIndex:0];
                                     self.userNameLbl.text=tempAddressItem.firstName;
                                 }
                             
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"U3ANCredit"]])
                             {
                                  self.creditLbl.text= [responseDictionary objectForKey:@"U3ANCredit"];
                             }
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }}
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}

-(void)loadHomeView
{

    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }

    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}


#pragma mark - UITABLEVIEW METHODS

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([ApplicationDelegate isValid:accountArray])
        {
            if (accountArray.count>0)
            {
                if (indexPath.row<(accountArray.count-1))
                {
                    UIImageView *separatorIMg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 48, (tableView.frame.size.width-10), 2)];
                    separatorIMg.backgroundColor = [UIColor clearColor];
                    [separatorIMg setImage:[UIImage imageNamed:@"morepage_divider.png"]];
                    [cell addSubview:separatorIMg];
                }
            }
        }
        
        
        
        // Code comes here..
    }
    cell.textLabel.text =[NSString stringWithFormat:@"%@",[accountArray objectAtIndex:indexPath.row]];
        cell.textLabel.textColor = [UIColor redColor];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:15.0f];
    cell.backgroundColor = [UIColor clearColor];
    //    cell.backgroundColor = [UIColor colorWithRed:(245.0f/255.0f) green:(245.0f/255.0f) blue:(245.0f/255.0f) alpha:1.0];
    
    //    UIImageView * separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 267, 1)];
    //    separatorLineView.backgroundColor = [UIColor clearColor];
    //    separatorLineView.image = [UIImage imageNamed:@"line_black.png"];
    //    [cell.contentView addSubview:separatorLineView];
    
    //    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
  //  [cell.textLabel setFont:self.textLabelFont];
   // [cell.textLabel setTextColor:self.textLabelColor];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        cell.textLabel.textAlignment = NSTextAlignmentRight;
    }
    return cell;
}

//-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
//    [headerView setBackgroundColor:[UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.75]];
//    //headerView.backgroundColor = [UIColor clearColor];
//
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)];
//    label.text = [self tableView:self.menuTableView titleForHeaderInSection:section];
//    label.font = [UIFont fontWithName:@"Arial" size:14.0];
//    label.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
//    label.backgroundColor = [UIColor clearColor];
//    [headerView addSubview:label];
//
//    return headerView;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return accountArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        AccountInfoListVC *accountInfoListVC = [[AccountInfoListVC alloc] initWithNibName:@"AccountInfoListVC" bundle:nil]
        ;
        accountInfoListVC.view.backgroundColor = [UIColor clearColor];
        accountInfoListVC.custAddressListArray=self.custAddressListArray;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[accountInfoListVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:accountInfoListVC animated:NO];
        }

    }
    else if(indexPath.row==1)
    {
        OrderlistViewController *orderVC = [[OrderlistViewController alloc] initWithNibName:@"OrderlistViewController" bundle:nil]
        ;
        
        //   orderVC.custAddressListArray=self.custAddressListArray;
        //
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[orderVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:orderVC animated:NO];
        }

    }
    
    else if(indexPath.row==2)
    {
        MyFavouritesVC *favouriteVC = [[MyFavouritesVC alloc] initWithNibName:@"MyFavouritesVC" bundle:nil]
        ;
        favouriteVC.view.backgroundColor = [UIColor clearColor];
        favouriteVC.custAddressListArray=self.custAddressListArray;
        
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[favouriteVC class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:favouriteVC animated:NO];
        }
    }
    
}

-(void)xibLoading
{
    NSString *nibName = @"MyAccountVC";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}
@end
