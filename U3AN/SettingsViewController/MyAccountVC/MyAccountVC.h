//
//  MyAccountVC.h
//  U3AN
//
//  Created by Vipin on 23/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *myAccountTable;
@property (strong, nonatomic) NSMutableArray *custAddressListArray;
@property (strong, nonatomic) IBOutlet UILabel *userNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *creditLbl;
@property (strong, nonatomic) IBOutlet UILabel *welcomeLbl;
@property (strong, nonatomic) IBOutlet UILabel *u3anCreditLbl;
@property (strong, nonatomic) IBOutlet UIScrollView *myAccountScrollView;
@property (strong, nonatomic) IBOutlet UILabel *myAccountLbl;

@end
