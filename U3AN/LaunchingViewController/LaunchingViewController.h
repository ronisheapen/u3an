//
//  LaunchingViewController.h
//  U3AN
//
//  Created by Vipin on 14/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "Areas.h"
#import "DropDownWithHeaderSelection.h"
#import "Cuisine.h"
#import "RestaurantDetail.h"
#import "LoginViewController.h"
@interface LaunchingViewController : UIViewController<ListSelectionProtocol,ListWithHeaderSelectionProtocol>
@property (weak, nonatomic) IBOutlet UILabel *cuisineTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (strong, nonatomic) DropDownWithHeaderSelection *areaDropDownObj;
@property (strong, nonatomic) DropDownView *cuisineDropDownObj;
@property (strong, nonatomic)Areas *areaObj;
@property (strong, nonatomic)Cuisine *cuisineObj;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;
@property (weak, nonatomic) IBOutlet UIButton *areaSelectionButton;
@property (weak, nonatomic) IBOutlet UIButton *cuisineSelectionButton;
@property (weak, nonatomic) IBOutlet UILabel *orderCount_Label;
@property (weak, nonatomic) IBOutlet UIButton *loginBttn;

- (IBAction)findRestaurant_ButtonAction:(UIButton *)sender;
- (IBAction)selectCuisine_ButtonAction:(UIButton *)sender;
- (IBAction)selectArea_ButtonAction:(UIButton *)sender;
- (IBAction)loginAction:(UIButton *)sender;


@end
