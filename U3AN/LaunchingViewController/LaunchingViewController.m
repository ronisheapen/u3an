//
//  LaunchingViewController.m
//  U3AN
//
//  Created by Vipin on 14/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "LaunchingViewController.h"
#import "Cuisine.h"
#import "SearchViewController.h"

@interface LaunchingViewController ()

@end
NSMutableArray *areaListArray,*restaurantObjArray,*cityListArray,*cuisineNameArray,*areaObjArray,*cuisineObjArray;
NSString *selectedAreaId;
@implementation LaunchingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self getAreaDetails];
    [self getCuisineList];
    [self getOrderCount];
    if (!ApplicationDelegate.isLoggedIn)
    {
        self.loginBttn.hidden = NO;
    }
    else
    {
        self.loginBttn.hidden = YES;
    }
}

-(void)updateViewHeading
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];

    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
    [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getOrderCount
{

    [self.orderCount_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:14.0f]];
    
    self.orderCount_Label.text=[NSString stringWithFormat:@"-- ORDERS "];
    
        [ApplicationDelegate addProgressHUDToView:self.view];
        __block BOOL success = NO;
        
        [ApplicationDelegate.engine getOrderCountCompletionHandler:^(NSString *responseString)
         {
             NSLog(@"%@",responseString);
             
             if (!success)
             {
                 success = YES;
                 
                 if ([ApplicationDelegate isValid:responseString])
                 {
                     if (responseString.length>0)
                     {
                         NSString *orderStrippedText = @"--";
                         
                         NSArray *numSplitArray = [responseString componentsSeparatedByString:@"<"];
                         
                         if ([ApplicationDelegate isValid:numSplitArray])
                         {
                             if ([numSplitArray count]>0)
                             {
                                 if (numSplitArray.count>1)
                                 {
                                     NSLog(@"========ORDER CRASH IN SERVICE=======");
                                 }
                                 if ([ApplicationDelegate isValid:[numSplitArray objectAtIndex:0]])
                                 {
                                     orderStrippedText = [[NSString stringWithFormat:@"%@",[numSplitArray objectAtIndex:0]] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                                 }
                             }
                         }
                         
                         self.orderCount_Label.text=[NSString stringWithFormat:@"%@ ORDERS ",orderStrippedText];
                         
                         [self.orderCount_Label setFont:[UIFont fontWithName:@"Tahoma-Bold" size:14.0f]];
                         
                     }
                 }
                 
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {

             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    
}
-(void)getAreaDetails
{
    areaListArray=[[NSMutableArray alloc] init];
    areaObjArray=[[NSMutableArray alloc] init];
    cityListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getAreaPostData]];
    if (restaurantData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        __block BOOL success = NO;
        
        [ApplicationDelegate.engine getAreasWithDataDictionary:restaurantData CompletionHandler:^(NSMutableArray *responseArray)
         {
             if (!success)
             {
                 success = YES;
                 if ([ApplicationDelegate isValid:responseArray])
                 {
                     if (responseArray.count>0)
                     {
                         for (int i=0; i<responseArray.count; i++)
                         {
                             if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                             {
                                 Areas *areaItem = [ApplicationDelegate.mapper getAreaListFromDictionary:[responseArray objectAtIndex:i]];
                                 
                                 [cityListArray addObject:areaItem.city_Name];
                                 [areaObjArray addObject:areaItem];
                                 if(areaItem.area_List.count!=0)
                                 {
                                     NSMutableArray *areaItemListArray=[[NSMutableArray alloc] init];

                                     for (int j=0; j<areaItem.area_List.count; j++)
                                     {
                                         if(![ApplicationDelegate isValid:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]] )
                                         {
                                             [areaItemListArray addObject:@""];
                                         }
                                         else
                                         {
                                             [areaItemListArray addObject:[[areaItem.area_List objectAtIndex:j] objectForKey:@"AreaName"]];
                                         }
                                         
                                     }
                                //     NSLog(@"AREA_ITEM_LIST%@",areaItemListArray);
                                     [areaListArray addObject:areaItemListArray];
                                  //   NSLog(@"AREA_LIST%@",areaListArray);
                                 }
                                 
                             }
                             
                         }
                         NSLog(@"AREA_LIST%@",areaListArray);
                         
                         
                     }
                 }
                 [ApplicationDelegate removeProgressHUD];
                 
                 if ([ApplicationDelegate isValid:areaListArray]) {
                     if (areaListArray.count!=0) {
                         if ([ApplicationDelegate isValid:[areaListArray objectAtIndex:0]]) {
                             if ([ApplicationDelegate isValid:[[areaListArray objectAtIndex:0]objectAtIndex:0]]) {
                             self.areaLabel.text =[[areaListArray objectAtIndex:0]objectAtIndex:0];
                             }
                             self.areaObj=[areaObjArray objectAtIndex:0];
                                  if ([ApplicationDelegate isValid:[[self.areaObj.area_List objectAtIndex:0]objectForKey:@"AreaId"]])
                                       {
                                           selectedAreaId=[[self.areaObj.area_List objectAtIndex:0]objectForKey:@"AreaId"];
                                           ApplicationDelegate.selected_AreaID=selectedAreaId;
                                       }
                             
                         }}}
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getAreaPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:kCountryId forKey:@"countryId"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

-(void)getCuisineList
{
    cuisineObjArray=[[NSMutableArray alloc] init];
    cuisineNameArray=[[NSMutableArray alloc] init];

    NSMutableDictionary *cuisineData = [[NSMutableDictionary alloc] initWithDictionary:[self getDishesPostData]];

    if (cuisineData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCuisinesWithDataDictionary:cuisineData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             Cuisine *cuisineItem = [ApplicationDelegate.mapper getMostSellingCusineListFromDictionary:[responseArray objectAtIndex:i]];
                             
                             [cuisineObjArray addObject:cuisineItem];
                             [cuisineNameArray addObject:cuisineItem.cuisine_Name];
                         }
                     }
                     
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             if ([ApplicationDelegate isValid:cuisineNameArray]) {
                 if (cuisineNameArray.count!=0) {
                     if ([ApplicationDelegate isValid:[cuisineNameArray objectAtIndex:0]])
                     {
             self.cuisineTypeLabel.text =[cuisineNameArray objectAtIndex:0];
                     }}}
                     if ([ApplicationDelegate isValid:cuisineObjArray])
                     {
                         if (cuisineObjArray.count!=0)
                         {
                             if ([ApplicationDelegate isValid:[cuisineObjArray objectAtIndex:0]])
                             {
                                 self.cuisineObj=[cuisineObjArray objectAtIndex:0];
                             }}}
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getDishesPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}

- (IBAction)findRestaurant_ButtonAction:(UIButton *)sender {
    
    [self getRestaurantDetails];
}

- (IBAction)selectCuisine_ButtonAction:(UIButton *)sender {
    if (cuisineObjArray.count!=0) {
        [self showCuisineList];

    }
    else
    {
        [self getCuisineList];

    }
}
- (IBAction)selectArea_ButtonAction:(UIButton *)sender{
    if (areaListArray.count!=0) {
        [self showAreaList];
    }
    else
    {
        [self getAreaDetails];
        [self showAreaList];
    }
}

- (IBAction)loginAction:(UIButton *)sender {
        LoginViewController *loginVc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        loginVc.view.backgroundColor = [UIColor clearColor];
    
        if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[loginVc class]])
        {
            [ApplicationDelegate.subHomeNav pushViewController:loginVc animated:NO];
        }
}



-(void)showAreaList
{
    self.areaSelectionButton.selected = !self.areaSelectionButton.selected;
    if (self.areaSelectionButton.selected)
    {
        if (self.areaDropDownObj.view.superview) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
        self.areaDropDownObj= [[DropDownWithHeaderSelection alloc] initWithNibName:@"DropDownWithHeaderSelection" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.areaDropDownObj.cellHeight = 35.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.areaDropDownObj.view.frame = CGRectMake((self.areaSelectionButton.superview.frame.origin.x),(self.areaSelectionButton.superview.frame.origin.y+self.areaSelectionButton.superview.frame.size.height), (self.areaSelectionButton.superview.frame.size.width), 0);
        
        self.areaDropDownObj.headerDropDownDelegate = self;
        self.areaDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:areaListArray];
        self.areaDropDownObj.headerDataArray = [[NSMutableArray alloc] initWithArray:cityListArray];
        self.areaDropDownObj.headerLabelFont = [UIFont fontWithName:@"Verdana" size:14.0];
        self.areaDropDownObj.view.layer.borderWidth = 0.1;
        self.areaDropDownObj.view.layer.shadowOpacity = 1.0;
        self.areaDropDownObj.view.layer.shadowRadius = 5.0;
        self.areaDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
        //        for (UIView *vw in self.view.subviews) {
        //            if (vw!=self.self.areaSelectionButton) {
        //                [vw setUserInteractionEnabled:NO];
        //            }
        //        }
        //////////////
        self.areaDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.areaDropDownObj.textLabelColor = [UIColor blackColor];
        self.areaDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        self.areaDropDownObj.headerTextLabelColor = [UIColor redColor];
        
        if (cityListArray.count>0)
        {
            [self.areaSelectionButton.superview.superview addSubview:self.areaDropDownObj.view];
        }
        else
        {
            self.areaSelectionButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.areaDropDownObj.cellHeight*self.areaDropDownObj.dataArray.count)+18.0f;
        }
        //        [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.areaDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.areaDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x+2,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.areaDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.areaDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.areaDropDownObj.view.frame =
            CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                       self.areaDropDownObj.view.frame.origin.y,
                       self.areaDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.areaDropDownObj.view removeFromSuperview];
            }
            
        }];
    }
    

}

-(void)showCuisineList
{
    self.cuisineSelectionButton.selected = !self.cuisineSelectionButton.selected;
    if (self.cuisineSelectionButton.selected)
    {
        if (self.cuisineDropDownObj.view.superview) {
            [self.cuisineDropDownObj.view removeFromSuperview];
        }
        
        self.cuisineDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
        
        //////////// Customize the dropDown view Frame as your requirement
        
        self.cuisineDropDownObj.cellHeight = 40.0f;
        
        //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //            {
        self.cuisineDropDownObj.view.frame = CGRectMake((self.cuisineSelectionButton.superview.frame.origin.x),(self.cuisineSelectionButton.superview.frame.origin.y+self.cuisineSelectionButton.superview.frame.size.height), (self.cuisineSelectionButton.superview.frame.size.width), 0);
        
        self.cuisineDropDownObj.dropDownDelegate = self;
        self.cuisineDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:cuisineNameArray];
        
        self.cuisineDropDownObj.view.layer.borderWidth = 0.1;
        self.cuisineDropDownObj.view.layer.shadowOpacity = 1.0;
        self.cuisineDropDownObj.view.layer.shadowRadius = 5.0;
        self.cuisineDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
        
        ///////Disabling Other part of the view except the DropDown View 	Container////////
//        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
//            if (vw!=self.self.cuisineSelectionButton) {
//                [vw setUserInteractionEnabled:NO];
//            }
//        }
        //////////////
        self.cuisineDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
        self.cuisineDropDownObj.textLabelColor = [UIColor blackColor];
        self.cuisineDropDownObj.view.backgroundColor = [UIColor whiteColor];
        
        if (cuisineNameArray.count>0)
        {
            [self.cuisineSelectionButton.superview.superview addSubview:self.cuisineDropDownObj.view];
        }
        else
        {
            self.cuisineSelectionButton.selected=NO;
        }
        
        /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
        CGFloat tableHT = 120.0f;
        
        if ((self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)<tableHT)
        {
            tableHT = (self.cuisineDropDownObj.cellHeight*self.cuisineDropDownObj.dataArray.count)+18.0f;
        }
   //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
        
        [UIView animateWithDuration:0.09f animations:^{
            self.cuisineDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
            self.cuisineDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x+2,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width-4,
                       tableHT);
        }];
    }
    else
    {
        self.cuisineDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
        self.cuisineDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
        
        ////////// ReEnabling the UserInteraction of all SubViews/////////
        for (UIView *vw in self.view.subviews) {
            [vw setUserInteractionEnabled:YES];
        }
        ///////////////////
        
        [UIView animateWithDuration:0.4f animations:^{
            self.cuisineDropDownObj.view.frame =
            CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                       self.cuisineDropDownObj.view.frame.origin.y,
                       self.cuisineDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cuisineDropDownObj.view removeFromSuperview];
            }
            
        }];
    }

}


#pragma mark - GET Restaurant Details

-(void)getRestaurantDetails
{
    restaurantObjArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *restaurantPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getRestaurantPostData]];
    if (restaurantPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getRestaurantByAreaWithDataDictionary:restaurantPostData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             self.restaurantDetailObj = [ApplicationDelegate.mapper getRestaurantDetailsForORDER_DISHES_FromDictionary:[responseArray objectAtIndex:i]];
                             
                             if (!([self.restaurantDetailObj.restaurant_Status caseInsensitiveCompare:@"hidden"]==NSOrderedSame))
                             {
                                 [restaurantObjArray addObject:self.restaurantDetailObj];
                             }
                             
                             
                         }
                     }
                     
                 }
             }
             [ApplicationDelegate removeProgressHUD];
             
             if (restaurantObjArray.count!=0)
             {
             [self updateAndDisplaySearchView];
             }
             else
             {
            [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
             }
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
}

-(NSMutableDictionary *)getRestaurantPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    if ([ApplicationDelegate isValid:selectedAreaId])
    {
        [postDic setObject:selectedAreaId forKey:@"areaId"];
    }
    else
    {
    [postDic setObject:@"" forKey:@"areaId"];
    }
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    if ([ApplicationDelegate isValid:self.cuisineObj.cuisine_Id])
    {
       [postDic setObject:self.cuisineObj.cuisine_Id forKey:@"cuisineId"];
    }
    else
    {
        [postDic setObject:@"" forKey:@"cuisineId"];
    }
    
        [postDic setObject:@""forKey:@"restName"];

    
    return postDic;
}


-(void)updateAndDisplaySearchView
{
    
    SearchViewController *searchVC = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil]
    ;
    searchVC.view.backgroundColor = [UIColor clearColor];
    searchVC.areaListArray=areaListArray;
    searchVC.cityNameArray=cityListArray;
    searchVC.areaObjArray=areaObjArray;
    searchVC.cuisineObjArray=cuisineObjArray;
    searchVC.cuisineListArray=cuisineNameArray;
    searchVC.restaurantObjtArray=restaurantObjArray;
    searchVC.cuisine_Id=self.cuisineObj.cuisine_Id;
    searchVC.area_Id=selectedAreaId;
    ApplicationDelegate.selected_AreaID=selectedAreaId;
    searchVC.area_Label.text=self.areaLabel.text;
    searchVC.cuisine_Label.text=self.cuisineTypeLabel.text;
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[searchVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:searchVC animated:NO];
    }
    

}


#pragma mark - DROP-DOWN List Delegate Method


-(void)headerSelectList:(int)selectedIndex :(NSInteger)section{
    ////////// ReEnabling the UserInteraction of all SubViews/////////
//    for (UIView *vw in self.view.subviews) {
//        [vw setUserInteractionEnabled:YES];
//    }
    ///////////////////
    
    [UIView animateWithDuration:0.4f animations:^{
        self.areaDropDownObj.view.frame =
        CGRectMake(self.areaDropDownObj.view.frame.origin.x,
                   self.areaDropDownObj.view.frame.origin.y,
                   self.areaDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.areaDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.areaSelectionButton.selected = NO;
    self.areaLabel.text =[[areaListArray objectAtIndex:section]objectAtIndex:selectedIndex];
    self.areaObj=[areaObjArray objectAtIndex:section];
    selectedAreaId=[[self.areaObj.area_List objectAtIndex:selectedIndex]objectForKey:@"AreaId"];
    ApplicationDelegate.selected_AreaID=selectedAreaId;

}

-(void)selectList:(int)selectedIndex
{
    [UIView animateWithDuration:0.4f animations:^{
        self.cuisineDropDownObj.view.frame =
        CGRectMake(self.cuisineDropDownObj.view.frame.origin.x,
                   self.cuisineDropDownObj.view.frame.origin.y,
                   self.cuisineDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.cuisineDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.cuisineSelectionButton.selected = NO;
    self.cuisineTypeLabel.text =[cuisineNameArray objectAtIndex:selectedIndex];
    self.cuisineObj=[cuisineObjArray objectAtIndex:selectedIndex];
}

-(void)xibLoading
{
    NSString *nibName = @"LaunchingViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
