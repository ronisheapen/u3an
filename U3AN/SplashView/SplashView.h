//
//  SplashView.h
//  U3AN
//
//  Created by Ratheesh on 15/06/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashView : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
