//
//  SplashView.m
//  U3AN
//
//  Created by Ratheesh on 15/06/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SplashView.h"
#import "AppDelegate.h"

@interface SplashView ()

@end

@implementation SplashView

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self performSelector:@selector(loadMainViewMethod) withObject:nil afterDelay:1.0];
}
- (void)loadMainViewMethod
{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [ApplicationDelegate.window.layer addAnimation:transition forKey:nil];
    
    if (ApplicationDelegate.window.rootViewController) {
        ApplicationDelegate.window.rootViewController = nil;
    }
    
    ApplicationDelegate.window.rootViewController=ApplicationDelegate.homeNav;
    
}

@end
