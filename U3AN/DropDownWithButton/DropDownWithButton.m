//
//  DropDownWithButton.m
//  U3AN
//
//  Created by Vipin on 27/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "DropDownWithButton.h"

@interface DropDownWithButton ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation DropDownWithButton
@synthesize selectionBttnDropDownDelegate,dataArray,headerDataArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //NSLog(@"%@",self.dataArray);
//    dataArray = [[NSMutableArray alloc] initWithObjects:@"Kanji",@"Kappa",@"Payar",@"Pappadam", nil];
//    [self.dropDownWithSelectionButtonTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITABLEVIEW METHODS

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Code comes here..
        
//        UIButton *selectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        selectionButton.frame = CGRectMake(0.0f, 5.0f, 15.0f, 15.0f);
//        selectionButton.backgroundColor = [UIColor clearColor];
//        selectionButton.tag = 2;
//        [selectionButton setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
//        [selectionButton setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
//        [selectionButton addTarget:self action:@selector(selectChoice:) forControlEvents:UIControlEventTouchUpInside];
//        
//        UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, (cell.frame.size.width-25), 25)];
//        cellLabel.backgroundColor = [UIColor clearColor];
//        cellLabel.textColor = self.textLabelColor;
//        cellLabel.font = self.textLabelFont;
//        cellLabel.tag = 3;
//        
//        [cell addSubview:cellLabel];
//        [cell addSubview:selectionButton];
    }
    
   // NSArray *array = [dataArray objectAtIndex:indexPath.section];
    cell.textLabel.text =[NSString stringWithFormat:@"%@",[dataArray objectAtIndex:indexPath.row]];
    //    cell.textLabel.textColor = [UIColor grayColor];
    
//    UILabel *cellLabel = (UILabel *)[cell viewWithTag:3];
//    cellLabel.backgroundColor = [UIColor clearColor];
//    cellLabel.textColor = self.textLabelColor;
//    cellLabel.font = self.textLabelFont;
//    cellLabel.frame = CGRectMake(20, (cell.center.y-13),(cell.frame.size.width-25), 25);
//    cellLabel.text = [NSString stringWithFormat:@"%@",[dataArray objectAtIndex:indexPath.row]];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *accesoryImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    if (1)
    {
        accesoryImg.image=[UIImage imageNamed:@"Tickbox_Unselected.png"];
    }
    else
    {
        accesoryImg.image=[UIImage imageNamed:@"Tickbox_Selected.png"];
    }
    
    UIButton *selectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectionButton.frame = CGRectMake(0.0f, 5.0f, 15.0f, 15.0f);
    selectionButton.backgroundColor = [UIColor clearColor];
    selectionButton.tag = 2;
    [selectionButton setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [selectionButton setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    [selectionButton addTarget:self action:@selector(selectChoice:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.accessoryView = accesoryImg;
    
//    UIButton *tickButton = (UIButton *)[cell viewWithTag:2];
//    //tickButton.frame = CGRectMake(0.0f, 5.0f, 15.0f, 15.0f);
//    tickButton.frame = CGRectMake(0, (cell.center.y-8),15, 15);
//    tickButton.backgroundColor = [UIColor clearColor];
//    if (indexPath.row%2==0)
//    {
//        tickButton.selected = NO;
//    }
//    else
//    {
//        tickButton.selected = YES;
//    }
    
    [cell.textLabel setFont:self.textLabelFont];
    [cell.textLabel setTextColor:self.textLabelColor];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        cell.textLabel.textAlignment = NSTextAlignmentRight;
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 50.0)];
    sectionHeaderView.backgroundColor = [UIColor redColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(10, 0, sectionHeaderView.frame.size.width, 25.0)];
    
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"Verdana" size:14.0]];
    [sectionHeaderView addSubview:headerLabel];
    
    headerLabel.text =[NSString stringWithFormat:@"%@",[headerDataArray objectAtIndex:section]];
    
    
    return sectionHeaderView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellHeight;
}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView *view = [[UIView alloc] init];
//    view.frame=CGRectMake(10, 10, 1, 30);
//    view.backgroundColor = [UIColor redColor];
//    return view;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *myCell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *accesoryImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    accesoryImg.image=[UIImage imageNamed:@"Tickbox_Selected.png"];
    myCell.accessoryView = accesoryImg;
    
    [self.selectionBttnDropDownDelegate buttonSelectList:indexPath.row];
}

-(IBAction)selectChoice:(UIButton *)sender
{
    sender.selected = !sender.selected;
}

@end
