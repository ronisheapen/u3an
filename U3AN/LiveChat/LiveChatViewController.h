//
//  LiveChatViewController.h
//  U3AN
//
//  Created by satheesh on 4/1/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveChatViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *liveChatWeb;

@end
