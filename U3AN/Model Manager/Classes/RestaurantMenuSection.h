//
//  RestaurantMenuSection.h
//  U3AN
//
//  Created by Ratheesh on 24/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantMenuSection : NSObject

@property(nonatomic,retain)NSString *_name;
@property(nonatomic,retain)NSString *_id;

@end
