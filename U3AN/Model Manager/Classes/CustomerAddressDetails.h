//
//  CustomerAddressDetails.h
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerAddressDetails : NSObject

@property(nonatomic,retain)NSString *areaID;
@property(nonatomic,retain)NSString *areaName;
@property(nonatomic,retain)NSString *birthday;
@property(nonatomic,retain)NSString *block;
@property(nonatomic,retain)NSString *buildingNo;
@property(nonatomic,retain)NSString *extraDirections;
@property(nonatomic,retain)NSString *firstName;
@property(nonatomic,retain)NSString *floor;
@property(nonatomic,retain)NSString *gender;
@property(nonatomic,retain)NSString *housePhone;
@property(nonatomic,retain)NSString *addressID;
@property(nonatomic,retain)NSString *isPrimary;
@property(nonatomic,retain)NSString *judda;
@property(nonatomic,retain)NSString *lastName;
@property(nonatomic,retain)NSString *mobile;
@property(nonatomic,retain)NSString *profileName;
@property(nonatomic,retain)NSString *street;
@property(nonatomic,retain)NSString *suit;
@property(nonatomic,retain)NSString *type;
@property(nonatomic,retain)NSString *workPhone;
@property(nonatomic,retain)NSString *company;
@property(nonatomic,retain)NSString *email;
@property(nonatomic,retain)NSString *occupation;
//@property(nonatomic,retain)NSString *subscribed_to_news;
@property (nonatomic, assign) BOOL subscribed_to_news;
//@property(nonatomic,retain)NSString *subscribed_to_sms;
@property (nonatomic, assign) BOOL subscribed_to_sms;


@end
