//
//  RestaurantDetail.h
//  U3AN
//
//  Created by Vipin on 22/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantDetail : NSObject
@property(nonatomic,retain)NSString *accepts_CC;
@property(nonatomic,retain)NSString *accepts_Cash;
@property(nonatomic,retain)NSString *accepts_KNET;
@property(nonatomic,retain)NSString *delivery_Time;
@property(nonatomic,retain)NSString *min_Amount;
@property(nonatomic,retain)NSString *restaurant_Id;
@property(nonatomic,retain)NSString *restaurant_Logo;
@property(nonatomic,retain)NSString *restaurant_Name;
@property(nonatomic,retain)NSString *summary;
@property(nonatomic,retain)NSString *rating;
@property(nonatomic,retain)NSString *delivery_Charges;
@property(nonatomic,retain)NSString *u3anCharge;
@property(nonatomic,retain)NSString *restaurant_Status;
@property(nonatomic,retain)NSString *working_Hours;
@property(nonatomic,retain)NSString *opening_Time;
@property(nonatomic,retain)NSString *closing_Time;
@property(nonatomic,retain)NSString *cuisine;
@property(nonatomic,retain)NSArray *coverageArea_TimingArray;
@end
