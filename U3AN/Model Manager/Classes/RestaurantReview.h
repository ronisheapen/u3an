//
//  RestaurantReview.h
//  U3AN
//
//  Created by Vipin on 26/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantReview : NSObject
@property(nonatomic,retain)NSString *created_Time;
@property(nonatomic,retain)NSString *review_Txt;
@property(nonatomic,retain)NSString *name;

@end
