//
//  RestaurantCart.m
//  U3AN
//
//  Created by Vipin on 09/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantCart.h"

@implementation RestaurantCart

@synthesize restaurant_Name,restaurant_Id,minimum_Amount,restaurant_Thumbnail,accepts_CC,accepts_Cash,accepts_KNET,delivery_Charges,discount,restaurant_Total,subtotal,delivery_TimingArray,cart_InfoArray;

- (id)init
{
    self = [super init];
    if (self)
    {
        self.cart_InfoArray = [[NSMutableArray alloc] init];
        self.delivery_TimingArray=[[NSMutableArray alloc]init];
        
    }
    return self;
}
@end
