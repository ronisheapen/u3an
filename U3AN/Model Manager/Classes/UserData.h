//
//  UserData.h
//  U3AN
//
//  Created by Vipin on 03/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject
@property(nonatomic,retain)NSString *authKey;
@property(nonatomic,retain)NSString *loginStatus;
@property(nonatomic,retain)NSString *userName;
@property(nonatomic,assign)BOOL newsletterStatus;
@end
