//
//  AreasByRestaurant.h
//  U3AN
//
//  Created by Vipin on 10/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AreasByRestaurant : NSObject
@property(nonatomic,retain)NSString *area_Name;
@property(nonatomic,retain)NSString *area_Id;
@property(nonatomic,retain)NSString *city_Name;
@property(nonatomic,retain)NSString *city_Id;

@end
