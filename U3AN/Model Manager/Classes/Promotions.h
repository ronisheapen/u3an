//
//  Promotions.h
//  U3AN
//
//  Created by Vipin on 05/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Promotions : NSObject
@property(nonatomic,retain)NSString *dish_Thumbnail;
@property(nonatomic,retain)NSString *dish_Name;
@property(nonatomic,retain)NSString *dish_Discription;
@property(nonatomic,retain)NSString *dish_Price;
@property(nonatomic,retain)NSString *rating;

@end
