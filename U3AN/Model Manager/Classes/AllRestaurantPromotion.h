//
//  AllRestaurantPromotion.h
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllRestaurantPromotion : NSObject
@property(nonatomic,retain)NSMutableArray *promotion_Array;
@property(nonatomic,retain)NSString *restaurant_Id;
@property(nonatomic,retain)NSString *restaurant_Logo;
@property(nonatomic,retain)NSString *restaurant_Name;
@property(nonatomic,retain)NSString *restaurant_Status;

@end
