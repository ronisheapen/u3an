//
//  CartInformation.h
//  U3AN
//
//  Created by Vipin on 30/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartInformation : NSObject
@property(nonatomic,retain)NSString *deliveryCharges_All;
@property(nonatomic,retain)NSString *discount_All;
@property(nonatomic,retain)NSString *grandTotal_All;
@property(nonatomic,retain)NSString *payByCash_All;
@property(nonatomic,retain)NSString *subtotal_All;
@property(nonatomic,retain)NSMutableArray *cart_InfoList_Array;
@end
