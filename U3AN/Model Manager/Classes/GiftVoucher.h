//
//  GiftVoucher.h
//  U3AN
//
//  Created by Vipin on 19/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftVoucher : NSObject

@property(nonatomic,retain)NSString *voucher_Amount;
@property(nonatomic,retain)NSString *voucher_Description;
@property(nonatomic,retain)NSString *voucher_Title;
@property(nonatomic,retain)NSString *voucher_Id;
@property(nonatomic,retain)NSString *voucher_Image;

@end
