//
//  ObjectMapper.m
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "ObjectMapper.h"

@implementation ObjectMapper

-(Banner *)getBannerObjectFromDictionary:(NSMutableDictionary *)bannerDictionary
{
    Banner *bannerObj = [[Banner alloc] init];
    
    if(![ApplicationDelegate isValid:[bannerDictionary objectForKey:@"BannerImage"]] )
    {
        bannerObj.bannerImageUrl=@"";
    }
    else
    {
        bannerObj.bannerImageUrl=[NSString stringWithFormat:@"%@",[bannerDictionary objectForKey:@"BannerImage"]];
    }
    
    if(![ApplicationDelegate isValid:[bannerDictionary objectForKey:@"RetaurantId"]] )
    {
        bannerObj.restaurantID=@"";
    }
    else
    {
        bannerObj.restaurantID=[NSString stringWithFormat:@"%@",[bannerDictionary objectForKey:@"RetaurantId"]];
    }
    
    return bannerObj;
}

-(Restaurant *)getRestaurantListFromDictionary:(NSMutableDictionary *)restaurantDictionary
{
        Restaurant *restaurantObj = [[Restaurant alloc] init];
    
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Rating"]] )
        {
            restaurantObj.rating=@"";
        }
        else
        {
            restaurantObj.rating=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"Rating"]];
        }
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantId"]] )
        {
            restaurantObj.restaurant_Id=@"";
        }
        else
        {
            restaurantObj.restaurant_Id=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"RestaurantId"]];
        }
        
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantLogo"]] )
        {
            restaurantObj.restaurant_Logo=@"";
        }
        else
        {
            restaurantObj.restaurant_Logo=[restaurantDictionary objectForKey:@"RestaurantLogo"];
        }
        
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantName"]] )
        {
            restaurantObj.restaurant_Name=@"";
        }
        else
        {
            restaurantObj.restaurant_Name=[restaurantDictionary objectForKey:@"RestaurantName"];
        }
        
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantStatus"]] )
        {
        restaurantObj.restaurant_Status=@"";
        }
        else
        {
        restaurantObj.restaurant_Status=[restaurantDictionary objectForKey:@"RestaurantStatus"];
        }
    
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Summary"]] )
        {
        restaurantObj.summary=@"";
        }
        else
        {
        restaurantObj.summary=[restaurantDictionary objectForKey:@"Summary"];
        }
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"PositionPrice"]] )
        {
            restaurantObj.positionPrice=@"0";
        }
        else
        {
            restaurantObj.positionPrice=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"PositionPrice"]];
        }
        if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"SortOrder"]] )
        {
            restaurantObj.SortOrder=@"0";
        }
        else
        {
            restaurantObj.SortOrder=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"SortOrder"]];
        }
        return restaurantObj;
}
-(Dishes *)getMostSellingDishesListFromDictionary:(NSMutableDictionary *)dishesDictionary
{
    Dishes  *dishesObj = [[Dishes alloc] init];
    
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"DishName"]] )
    {
        dishesObj.dish_Name=@"";
    }
    else
    {
        dishesObj.dish_Name=[NSString stringWithFormat:@"%@",[dishesDictionary objectForKey:@"DishName"]];
    }
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"Id"]] )
    {
        dishesObj.dish_Id=@"";
    }
    else
    {
        dishesObj.dish_Id=[dishesDictionary objectForKey:@"Id"];
    }
    
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"Price"]] )
    {
        dishesObj.dish_Price=@"";
    }
    else
    {
        dishesObj.dish_Price=[NSString stringWithFormat:@"%@",[dishesDictionary objectForKey:@"Price"]];
    }
    
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"RestaurantName"]] )
    {
        dishesObj.restaurant_Name=@"";
    }
    else
    {
        dishesObj.restaurant_Name=[dishesDictionary objectForKey:@"RestaurantName"];
    }
    
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"RestaurentStatus"]] )
    {
        dishesObj.restaurent_Status=@"";
    }
    else
    {
        dishesObj.restaurent_Status=[dishesDictionary objectForKey:@"RestaurentStatus"];
    }
    
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"Thumbnail"]] )
    {
        dishesObj.dish_Thumbnail=@"";
    }
    else
    {
       // dishesObj.dish_Thumbnail=[dishesDictionary objectForKey:@"Thumbnail"];
        
        dishesObj.dish_Thumbnail=[[dishesDictionary objectForKey:@"Thumbnail"]stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
    }
    
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"RestaurantId"]] )
    {
        dishesObj.restaurant_Id=@"";
    }
    else
    {
        dishesObj.restaurant_Id=[dishesDictionary objectForKey:@"RestaurantId"];
    }
    
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"RestaurantLogo"]] )
    {
        dishesObj.restaurant_Thumbnail=@"";
    }
    else
    {
        dishesObj.restaurant_Thumbnail=[dishesDictionary objectForKey:@"RestaurantLogo"];
    }
    if(![ApplicationDelegate isValid:[dishesDictionary objectForKey:@"Description"]] )
    {
        dishesObj.dish_Description=@"";
    }
    else
    {
        dishesObj.dish_Description=[dishesDictionary objectForKey:@"Description"];
    }

    return dishesObj;
    
}

-(Cuisine *)getMostSellingCusineListFromDictionary:(NSMutableDictionary *)cusineDictionary
{
    Cuisine *cusineObj = [[Cuisine alloc] init];
    
    if(![ApplicationDelegate isValid:[cusineDictionary objectForKey:@"CuisineName"]] )
    {
        cusineObj.cuisine_Name=@"";
    }
    else
    {
        cusineObj.cuisine_Name=[NSString stringWithFormat:@"%@",[cusineDictionary objectForKey:@"CuisineName"]];
    }
    if(![ApplicationDelegate isValid:[cusineDictionary objectForKey:@"CuisineId"]] )
    {
        cusineObj.cuisine_Id=@"";
    }
    else
    {
        cusineObj.cuisine_Id=[cusineDictionary objectForKey:@"CuisineId"];
    }
    return cusineObj;
}

-(AreasByRestaurant *)getAreaByRestaurantListFromDictionary:(NSMutableDictionary *)areaDictionary
{
    AreasByRestaurant *areaObj = [[AreasByRestaurant alloc] init];
    
    if(![ApplicationDelegate isValid:[areaDictionary objectForKey:@"AreaName"]] )
    {
        areaObj.area_Name=@"";
    }
    else
    {
        areaObj.area_Name=[areaDictionary objectForKey:@"AreaName"];
    }
    if(![ApplicationDelegate isValid:[areaDictionary objectForKey:@"AreaId"]] )
    {
        areaObj.area_Id=@"";
    }
    else
    {
        areaObj.area_Id=[areaDictionary objectForKey:@"AreaId"];
    }
    if(![ApplicationDelegate isValid:[areaDictionary objectForKey:@"CityName"]] )
    {
        areaObj.city_Name=@"";
    }
    else
    {
        areaObj.city_Name=[areaDictionary objectForKey:@"CityName"];
    }
    if(![ApplicationDelegate isValid:[areaDictionary objectForKey:@"CityId"]] )
    {
        areaObj.city_Id=@"";
    }
    else
    {
        areaObj.city_Id=[areaDictionary objectForKey:@"CityId"];
    }

    return areaObj;

}



-(Areas *)getAreaListFromDictionary:(NSMutableDictionary *)areaDictionary
{
    Areas *areaObj = [[Areas alloc] init];
    
    if(![ApplicationDelegate isValid:[areaDictionary objectForKey:@"CityName"]] )
    {
        areaObj.city_Name=@"";
    }
    else
    {
        areaObj.city_Name=[areaDictionary objectForKey:@"CityName"];
    }
    if(![ApplicationDelegate isValid:[areaDictionary objectForKey:@"CityId"]] )
    {
        areaObj.city_Id=@"";
    }
    else
    {
        areaObj.city_Id=[areaDictionary objectForKey:@"CityId"];
    }
    if(![ApplicationDelegate isValid:[areaDictionary objectForKey:@"ListAllAreas"]] )
    {
        areaObj.area_List=[[NSMutableArray alloc] init];
    }
    else
    {
        areaObj.area_List=[areaDictionary objectForKey:@"ListAllAreas"];
    }
    return areaObj;
    
}
-(RestaurantDetail *)getRestaurantDetailsFromDictionary:(NSMutableDictionary *)restaurantDictionary
{
    RestaurantDetail *restaurantDetailObj = [[RestaurantDetail alloc] init];
    
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"AcceptsCC"]] )
    {
        restaurantDetailObj.accepts_CC=@"";
    }
    else
    {
        restaurantDetailObj.accepts_CC=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"AcceptsCC"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"AcceptsCash"]] )
    {
        restaurantDetailObj.accepts_Cash=@"";
    }
    else
    {
        restaurantDetailObj.accepts_Cash=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"AcceptsCash"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"AcceptsKNET"]] )
    {
        restaurantDetailObj.accepts_KNET=@"";
    }
    else
    {
        restaurantDetailObj.accepts_KNET=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"AcceptsKNET"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"DeliveryTime"]] )
    {
        restaurantDetailObj.delivery_Time=@"";
    }
    else
    {
        restaurantDetailObj.delivery_Time=[restaurantDictionary objectForKey:@"DeliveryTime"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"MinimumAmount"]] )
    {
        restaurantDetailObj.min_Amount=@"";
    }
    else
    {
        restaurantDetailObj.min_Amount=[restaurantDictionary objectForKey:@"MinimumAmount"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantId"]] )
    {
        restaurantDetailObj.restaurant_Id=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Id=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"RestaurantId"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantLogo"]] )
    {
        restaurantDetailObj.restaurant_Logo=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Logo=[restaurantDictionary objectForKey:@"RestaurantLogo"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantName"]] )
    {
        restaurantDetailObj.restaurant_Name=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Name=[restaurantDictionary objectForKey:@"RestaurantName"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Summary"]] )
    {
        restaurantDetailObj.summary=@"";
    }
    else
    {
        restaurantDetailObj.summary=[restaurantDictionary objectForKey:@"Summary"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Rating"]] )
    {
        restaurantDetailObj.rating=@"";
    }
    else
    {
        restaurantDetailObj.rating=[restaurantDictionary objectForKey:@"Rating"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Workinghour"]] )
    {
        restaurantDetailObj.working_Hours=@"";
    }
    else
    {
        restaurantDetailObj.working_Hours=[restaurantDictionary objectForKey:@"Workinghour"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantStatus"]] )
    {
        restaurantDetailObj.restaurant_Status=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Status=[restaurantDictionary objectForKey:@"RestaurantStatus"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"DeliveryCharges"]] )
    {
        restaurantDetailObj.delivery_Charges=@"";
    }
    else
    {
        restaurantDetailObj.delivery_Charges=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"DeliveryCharges"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"u3anCharge"]] )
    {
        restaurantDetailObj.u3anCharge=@"";
    }
    else
    {
        restaurantDetailObj.u3anCharge=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"u3anCharge"]];
    }
    
//    if(![ApplicationDelegate isValid:[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"OpeningTime"]] )
//    {
//        restaurantDetailObj.opening_Time=@"";
//    }
//    else
//    {
//        restaurantDetailObj.opening_Time=[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"OpeningTime"];
//    }
//    if(![ApplicationDelegate isValid:[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"ClosingTime"]] )
//    {
//        restaurantDetailObj.closing_Time=@"";
//    }
//    else
//    {
//        restaurantDetailObj.closing_Time=[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"ClosingTime"];
//    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]] )
    {
        restaurantDetailObj.coverageArea_TimingArray=Nil;
    }
    else
    {
        restaurantDetailObj.coverageArea_TimingArray=[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Cuisine"]] )
    {
        restaurantDetailObj.cuisine=@"";
    }
    else
    {
        restaurantDetailObj.cuisine=[restaurantDictionary objectForKey:@"Cuisine"];
    }

    return restaurantDetailObj;
}
-(RestaurantDetail *)getRestaurantDetailsForORDER_DISHES_FromDictionary:(NSMutableDictionary *)restaurantDictionary
{
    RestaurantDetail *restaurantDetailObj = [[RestaurantDetail alloc] init];
    
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"AcceptsCC"]] )
    {
        restaurantDetailObj.accepts_CC=@"";
    }
    else
    {
        restaurantDetailObj.accepts_CC=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"AcceptsCC"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"AcceptsCash"]] )
    {
        restaurantDetailObj.accepts_Cash=@"";
    }
    else
    {
        restaurantDetailObj.accepts_Cash=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"AcceptsCash"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"AcceptsKNET"]] )
    {
        restaurantDetailObj.accepts_KNET=@"";
    }
    else
    {
        restaurantDetailObj.accepts_KNET=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"AcceptsKNET"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"DeliveryTime"]] )
    {
        restaurantDetailObj.delivery_Time=@"";
    }
    else
    {
        restaurantDetailObj.delivery_Time=[restaurantDictionary objectForKey:@"DeliveryTime"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"MinAmount"]] )
    {
        restaurantDetailObj.min_Amount=@"";
    }
    else
    {
        restaurantDetailObj.min_Amount=[restaurantDictionary objectForKey:@"MinAmount"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantId"]] )
    {
        restaurantDetailObj.restaurant_Id=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Id=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"RestaurantId"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantLogo"]] )
    {
        restaurantDetailObj.restaurant_Logo=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Logo=[restaurantDictionary objectForKey:@"RestaurantLogo"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantName"]] )
    {
        restaurantDetailObj.restaurant_Name=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Name=[restaurantDictionary objectForKey:@"RestaurantName"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Summary"]] )
    {
        restaurantDetailObj.summary=@"";
    }
    else
    {
        restaurantDetailObj.summary=[restaurantDictionary objectForKey:@"Summary"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Rating"]] )
    {
        restaurantDetailObj.rating=@"";
    }
    else
    {
        restaurantDetailObj.rating=[restaurantDictionary objectForKey:@"Rating"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Workinghour"]] )
    {
        restaurantDetailObj.working_Hours=@"";
    }
    else
    {
        restaurantDetailObj.working_Hours=[restaurantDictionary objectForKey:@"Workinghour"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"RestaurantStatus"]] )
    {
        restaurantDetailObj.restaurant_Status=@"";
    }
    else
    {
        restaurantDetailObj.restaurant_Status=[restaurantDictionary objectForKey:@"RestaurantStatus"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"DeliveryCharges"]] )
    {
        restaurantDetailObj.delivery_Charges=@"";
    }
    else
    {
        restaurantDetailObj.delivery_Charges=[NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"DeliveryCharges"]];
    }
    //    if(![ApplicationDelegate isValid:[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"OpeningTime"]] )
    //    {
    //        restaurantDetailObj.opening_Time=@"";
    //    }
    //    else
    //    {
    //        restaurantDetailObj.opening_Time=[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"OpeningTime"];
    //    }
    //    if(![ApplicationDelegate isValid:[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"ClosingTime"]] )
    //    {
    //        restaurantDetailObj.closing_Time=@"";
    //    }
    //    else
    //    {
    //        restaurantDetailObj.closing_Time=[[[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]objectAtIndex:0] objectForKey:@"ClosingTime"];
    //    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"]] )
    {
        restaurantDetailObj.coverageArea_TimingArray=Nil;
    }
    else
    {
        restaurantDetailObj.coverageArea_TimingArray=[restaurantDictionary objectForKey:@"CoverageAreaTimingDetails"];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Cuisine"]] )
    {
        restaurantDetailObj.cuisine=@"";
    }
    else
    {
        restaurantDetailObj.cuisine=[restaurantDictionary objectForKey:@"Cuisine"];
    }
    
    return restaurantDetailObj;
}
-(RestaurantMenuSection *)getRestaurantMenuSectionObjectFromDictionary:(NSMutableDictionary *)restaurantDictionary
{
    RestaurantMenuSection *restaurantMenuSectionObj = [[RestaurantMenuSection alloc] init];
    
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"MenuSectionId"]] )
    {
        restaurantMenuSectionObj._id = @"";
    }
    else
    {
        restaurantMenuSectionObj._id = [NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"MenuSectionId"]];
    }
    if(![ApplicationDelegate isValid:[restaurantDictionary objectForKey:@"Name"]] )
    {
        restaurantMenuSectionObj._name = @"";
    }
    else
    {
        restaurantMenuSectionObj._name = [NSString stringWithFormat:@"%@",[restaurantDictionary objectForKey:@"Name"]];
    }
    
    return restaurantMenuSectionObj;
}
-(UserData *) getUserDataFromDictionary:(NSMutableDictionary *)userDictionary
{
    UserData  *userObj = [[UserData alloc] init];
    if(![ApplicationDelegate isValid:[userDictionary objectForKey:@"Authkey"]] )
    {
        userObj.authKey=@"";
    }
    else
    {
        userObj.authKey=[userDictionary objectForKey:@"Authkey"];
    }
    if(![ApplicationDelegate isValid:[userDictionary objectForKey:@"Status"]] )
    {
        userObj.loginStatus=@"";
    }
    else
    {
        userObj.loginStatus=[userDictionary objectForKey:@"Status"];
    }
    if(![ApplicationDelegate isValid:[userDictionary objectForKey:@"UserName"]] )
    {
        userObj.userName=@"";
    }
    else
    {
        userObj.userName=[userDictionary objectForKey:@"UserName"];
    }
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    if(![ApplicationDelegate isValid:[userDictionary objectForKey:@"newsletterStatus"]] )
    {
        userObj.newsletterStatus = NO;
    }
    else
    {
        userObj.newsletterStatus=[[userDictionary objectForKey:@"newsletterStatus"] boolValue];
    }
    return userObj;
}

-(Promotions *) getRestaurantsByPromotionsListFromDictionary:(NSMutableDictionary *)promotionsDictionary
{
    Promotions  *promotionObj = [[Promotions alloc] init];
    if(![ApplicationDelegate isValid:[promotionsDictionary objectForKey:@"Name"]] )
    {
        promotionObj.dish_Name=Nil;
    }
    else
    {
        promotionObj.dish_Name=[promotionsDictionary objectForKey:@"Name"];
    }
    if(![ApplicationDelegate isValid:[promotionsDictionary objectForKey:@"Description"]] )
    {
        promotionObj.dish_Discription=@"";
    }
    else
    {
        promotionObj.dish_Discription=[promotionsDictionary objectForKey:@"Description"];
    }
    if(![ApplicationDelegate isValid:[promotionsDictionary objectForKey:@"Price"]] )
    {
        promotionObj.dish_Price=Nil;
    }
    else
    {
        promotionObj.dish_Price=[promotionsDictionary objectForKey:@"Price"];
    }
    if(![ApplicationDelegate isValid:[promotionsDictionary objectForKey:@"Thumbnail"]] )
    {
        promotionObj.dish_Thumbnail=@"";
    }
    else
    {
        promotionObj.dish_Thumbnail=[promotionsDictionary objectForKey:@"Thumbnail"];
    }
    if(![ApplicationDelegate isValid:[promotionsDictionary objectForKey:@"Rating"]] )
    {
        promotionObj.rating=@"";
    }
    else
    {
        promotionObj.rating=[promotionsDictionary objectForKey:@"Rating"];
    }
    
    return promotionObj;

}

-(DishMenuDetails *) getDishMenuDetailFromDictionary:(NSMutableDictionary *)dishMenuDetailDictionary
{
    DishMenuDetails  *menuDetailObj = [[DishMenuDetails alloc] init];
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"Description"]] )
    {
        menuDetailObj.description=@"";
    }
    else
    {
        menuDetailObj.description=[dishMenuDetailDictionary objectForKey:@"Description"];
    }
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"Discount"]] )
    {
        menuDetailObj.discount=@"";
    }
    else
    {
        menuDetailObj.discount=[dishMenuDetailDictionary objectForKey:@"Discount"];
    }
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"MenuItemId"]] )
    {
        menuDetailObj.menuItemId=@"";
    }
    else
    {
        menuDetailObj.menuItemId=[dishMenuDetailDictionary objectForKey:@"MenuItemId"];
    }
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"Name"]] )
    {
        menuDetailObj.name=@"";
    }
    else
    {
        menuDetailObj.name=[dishMenuDetailDictionary objectForKey:@"Name"];
    }
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"Price"]] )
    {
        menuDetailObj.price=@"";
    }
    else
    {
        menuDetailObj.price=[dishMenuDetailDictionary objectForKey:@"Price"];
    }
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"Rating"]] )
    {
        menuDetailObj.rating=@"";
    }
    else
    {
        menuDetailObj.rating=[dishMenuDetailDictionary objectForKey:@"Rating"];
    }
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"Thumbnail"]] )
    {
        menuDetailObj.dish_Thumbnail=@"";
    }
    else
    {
        menuDetailObj.dish_Thumbnail=[dishMenuDetailDictionary objectForKey:@"Thumbnail"];
    }
    
    if(![ApplicationDelegate isValid:[dishMenuDetailDictionary objectForKey:@"MenuItemChoices"]] )
    {
        menuDetailObj.menuChoice_array=[[NSMutableArray alloc] init];
    }
    else
    {
        menuDetailObj.menuChoice_array=[dishMenuDetailDictionary objectForKey:@"MenuItemChoices"];
    }

    return menuDetailObj;
}

-(RestaurantReview *) getRestaurantReviewDetailFromDictionary:(NSMutableDictionary *)restaurantDetailDictionary
{
    RestaurantReview  *reviewObj = [[RestaurantReview alloc] init];
    if(![ApplicationDelegate isValid:[restaurantDetailDictionary objectForKey:@"review"]] )
    {
        reviewObj.review_Txt=@"";
    }
    else
    {
        reviewObj.review_Txt=[restaurantDetailDictionary objectForKey:@"review"];
    }
    if(![ApplicationDelegate isValid:[restaurantDetailDictionary objectForKey:@"CreatedOn"]] )
    {
        reviewObj.created_Time=@"";
    }
    else
    {
        reviewObj.created_Time=[restaurantDetailDictionary objectForKey:@"CreatedOn"];
    }
    if(![ApplicationDelegate isValid:[restaurantDetailDictionary objectForKey:@"username"]] )
    {
        reviewObj.name=Nil;
    }
    else
    {
        reviewObj.name=[restaurantDetailDictionary objectForKey:@"username"];
    }

    return reviewObj;

}

-(RestaurantCart *) getRestaurantCartListFromDictionary:(NSMutableDictionary *)restaurantCartDictionary
{
    RestaurantCart  *restaurantCartObj = [[RestaurantCart alloc] init];
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"MinimumAmount"]] )
    {
        restaurantCartObj.minimum_Amount=@"";
    }
    else
    {
        restaurantCartObj.minimum_Amount=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"MinimumAmount"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"RestaurantId"]] )
    {
        restaurantCartObj.restaurant_Id=@"";
    }
    else
    {
        restaurantCartObj.restaurant_Id=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"RestaurantId"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"RestaurantName"]] )
    {
        restaurantCartObj.restaurant_Name=@"";
    }
    else
    {
        restaurantCartObj.restaurant_Name=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"RestaurantName"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"AcceptsCC"]] )
    {
        restaurantCartObj.accepts_CC=@"";
    }
    else
    {
        restaurantCartObj.accepts_CC=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"AcceptsCC"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"AcceptsCash"]] )
    {
        restaurantCartObj.accepts_Cash=@"";
    }
    else
    {
        restaurantCartObj.accepts_Cash=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"AcceptsCash"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"AcceptsKNET"]] )
    {
        restaurantCartObj.accepts_KNET=@"";
    }
    else
    {
        restaurantCartObj.accepts_KNET=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"AcceptsKNET"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"DeliveryCharges"]] )
    {
        restaurantCartObj.delivery_Charges=@"";
    }
    else
    {
        restaurantCartObj.delivery_Charges=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"DeliveryCharges"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"Discount"]] )
    {
        restaurantCartObj.discount=@"";
    }
    else
    {
        restaurantCartObj.discount=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"Discount"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"RestaurantTotal"]] )
    {
        restaurantCartObj.restaurant_Total=@"";
    }
    else
    {
        restaurantCartObj.restaurant_Total=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"RestaurantTotal"]];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"Subtotal"]] )
    {
        restaurantCartObj.subtotal=@"";
    }
    else
    {
        restaurantCartObj.subtotal=[NSString stringWithFormat:@"%@",[restaurantCartDictionary objectForKey:@"Subtotal"]];
    }
    
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"Thumbnail"]] )
    {
        restaurantCartObj.restaurant_Thumbnail=@"";
    }
    else
    {
        restaurantCartObj.restaurant_Thumbnail=[restaurantCartDictionary objectForKey:@"Thumbnail"];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"CartInfo"]] )
    {
        restaurantCartObj.cart_InfoArray=[[NSMutableArray alloc] init];
    }
    else
    {
        restaurantCartObj.cart_InfoArray=[restaurantCartDictionary objectForKey:@"CartInfo"];
    }
    if(![ApplicationDelegate isValid:[restaurantCartDictionary objectForKey:@"DeliveryTiming"]] )
    {
        restaurantCartObj.delivery_TimingArray=[[NSMutableArray alloc] init];
    }
    else
    {
        restaurantCartObj.delivery_TimingArray=[restaurantCartDictionary objectForKey:@"DeliveryTiming"];
    }

    return restaurantCartObj;
}

-(CartInformation *) getCartInfoListFromDictionary:(NSMutableDictionary *)cartInfoDictionary
{
    
    CartInformation  *cartInfoObj = [[CartInformation alloc] init];
    
    if(![ApplicationDelegate isValid:[cartInfoDictionary objectForKey:@"DeliveryChargesAll"]] )
    {
        cartInfoObj.deliveryCharges_All=@"";
    }
    else
    {
        cartInfoObj.deliveryCharges_All=[NSString stringWithFormat:@"%@",[cartInfoDictionary objectForKey:@"DeliveryChargesAll"]];
    }
    if(![ApplicationDelegate isValid:[cartInfoDictionary objectForKey:@"DiscountAll"]] )
    {
        cartInfoObj.discount_All=@"";
    }
    else
    {
        cartInfoObj.discount_All=[NSString stringWithFormat:@"%@",[cartInfoDictionary objectForKey:@"DiscountAll"]];
    }
    
    if(![ApplicationDelegate isValid:[cartInfoDictionary objectForKey:@"GrandTotalAll"]] )
    {
        cartInfoObj.grandTotal_All=@"";
    }
    else
    {
        cartInfoObj.grandTotal_All=[NSString stringWithFormat:@"%@",[cartInfoDictionary objectForKey:@"GrandTotalAll"]];
    }
    
    if(![ApplicationDelegate isValid:[cartInfoDictionary objectForKey:@"PayByCashAll"]] )
    {
        cartInfoObj.payByCash_All=@"";
    }
    else
    {
        cartInfoObj.payByCash_All=[NSString stringWithFormat:@"%@",[cartInfoDictionary objectForKey:@"PayByCashAll"]];
    }
    
    if(![ApplicationDelegate isValid:[cartInfoDictionary objectForKey:@"SubtotalAll"]] )
    {
        cartInfoObj.subtotal_All=@"";
    }
    else
    {
        cartInfoObj.subtotal_All=[NSString stringWithFormat:@"%@",[cartInfoDictionary objectForKey:@"SubtotalAll"]];
    }
    
    if(![ApplicationDelegate isValid:[cartInfoDictionary objectForKey:@"CartInfoList"]] )
    {
        cartInfoObj.cart_InfoList_Array=[[NSMutableArray alloc] init];
    }
    else
    {
        cartInfoObj.cart_InfoList_Array=[cartInfoDictionary objectForKey:@"CartInfoList"];
    }
    return cartInfoObj;
}

-(CartDetails *) getCartDetailFromDictionary:(NSMutableDictionary *)cartDetailDictionary
{
    CartDetails  *cartObj = [[CartDetails alloc] init];
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"AreaId"]] )
    {
        cartObj.area_Id=@"";
    }
    else
    {
        cartObj.area_Id=[NSString stringWithFormat:@"%@",[cartDetailDictionary objectForKey:@"AreaId"]];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"ItemChoiceIds"]] )
    {
        cartObj.item_ChoiceId=@"";
    }
    else
    {
        cartObj.item_ChoiceId=[cartDetailDictionary objectForKey:@"ItemChoiceIds"];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"ItemId"]] )
    {
        cartObj.item_Id=@"";
    }
    else
    {
        cartObj.item_Id=[NSString stringWithFormat:@"%@",[cartDetailDictionary objectForKey:@"ItemId"]];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"ItemName"]] )
    {
        cartObj.item_Name=@"";
    }
    else
    {
        cartObj.item_Name=[cartDetailDictionary objectForKey:@"ItemName"];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"ItemPrice"]] )
    {
        cartObj.item_Price=@"";
    }
    else
    {
        cartObj.item_Price=[NSString stringWithFormat:@"%@",[cartDetailDictionary objectForKey:@"ItemPrice"]];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"OrderDate"]] )
    {
        cartObj.order_Date=@"";
    }
    else
    {
        cartObj.order_Date=[cartDetailDictionary objectForKey:@"OrderDate"];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"Quantity"]] )
    {
        cartObj.quantity=@"";
    }
    else
    {
        cartObj.quantity=[NSString stringWithFormat:@"%@",[cartDetailDictionary objectForKey:@"Quantity"]];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"RestaurantId"]] )
    {
        cartObj.restaurant_Id=@"";
    }
    else
    {
        cartObj.restaurant_Id=[NSString stringWithFormat:@"%@",[cartDetailDictionary objectForKey:@"RestaurantId"]];
    }

    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"ShoppingCartId"]] )
    {
        cartObj.shopping_CartId=@"";
    }
    else
    {
        cartObj.shopping_CartId=[cartDetailDictionary objectForKey:@"ShoppingCartId"];
    }
    if(![ApplicationDelegate isValid:[cartDetailDictionary objectForKey:@"ItemThumbnail"]] )
    {
        cartObj.item_Thumbnail=@"";
    }
    else
    {
        cartObj.item_Thumbnail=[cartDetailDictionary objectForKey:@"ItemThumbnail"];
    }
    return cartObj;
}

-(PageDetails *) getPageDetailFromDictionary:(NSMutableDictionary *)pageDetailDictionary
{
    PageDetails  *pageObj = [[PageDetails alloc] init];
    if(![ApplicationDelegate isValid:[pageDetailDictionary objectForKey:@"Data"]] )
    {
        pageObj.page_Data=@"";
    }
    else
    {
        pageObj.page_Data=[pageDetailDictionary objectForKey:@"Data"];
    }
    if(![ApplicationDelegate isValid:[pageDetailDictionary objectForKey:@"Status"]] )
    {
        pageObj.status=@"";
    }
    else
    {
        pageObj.status=[pageDetailDictionary objectForKey:@"Status"];
    }

    return pageObj;
}

-(AllPromotionDetail *) getPromotionDetailFromDictionary:(NSMutableDictionary *)promotionDetailDictionary
{
    
    AllPromotionDetail  *promoObj = [[AllPromotionDetail alloc] init];
    
    if(![ApplicationDelegate isValid:[promotionDetailDictionary objectForKey:@"Description"]] )
    {
        promoObj.promo_Description=@"";
    }
    else
    {
        promoObj.promo_Description=[promotionDetailDictionary objectForKey:@"Description"];
    }
    if(![ApplicationDelegate isValid:[promotionDetailDictionary objectForKey:@"Name"]] )
    {
        promoObj.name=@"";
    }
    else
    {
        promoObj.name=[promotionDetailDictionary objectForKey:@"Name"];
    }
    if(![ApplicationDelegate isValid:[promotionDetailDictionary objectForKey:@"Price"]] )
    {
        promoObj.price=@"";
    }
    else
    {
        promoObj.price=[promotionDetailDictionary objectForKey:@"Price"];
    }
    if(![ApplicationDelegate isValid:[promotionDetailDictionary objectForKey:@"Rating"]] )
    {
        promoObj.rating=@"";
    }
    else
    {
        promoObj.rating=[promotionDetailDictionary objectForKey:@"Rating"];
    }
    if(![ApplicationDelegate isValid:[promotionDetailDictionary objectForKey:@"Thumbnail"]] )
    {
        promoObj.thumbnail=@"";
    }
    else
    {
        promoObj.thumbnail=[promotionDetailDictionary objectForKey:@"Thumbnail"];
    }
    return promoObj;
}


-(AllRestaurantPromotion *) getAllRestaurantPromotionFromDictionary:(NSMutableDictionary *)allRestaurantDictionary
{
    AllRestaurantPromotion  *restaurantPromoObj = [[AllRestaurantPromotion alloc] init];
    
    if(![ApplicationDelegate isValid:[allRestaurantDictionary objectForKey:@"PromotionItemsDetails"]] )
    {
        restaurantPromoObj.promotion_Array=[[NSMutableArray alloc] init];
    }
    else
    {
        restaurantPromoObj.promotion_Array=[allRestaurantDictionary objectForKey:@"PromotionItemsDetails"];
    }

    if(![ApplicationDelegate isValid:[allRestaurantDictionary objectForKey:@"RestaurantId"]] )
    {
        restaurantPromoObj.restaurant_Id=@"";
    }
    else
    {
        restaurantPromoObj.restaurant_Id=[NSString stringWithFormat:@"%@",[allRestaurantDictionary objectForKey:@"RestaurantId"]];
    }
    
    if(![ApplicationDelegate isValid:[allRestaurantDictionary objectForKey:@"RestaurantLogo"]] )
    {
        restaurantPromoObj.restaurant_Logo=@"";
    }
    else
    {
        restaurantPromoObj.restaurant_Logo=[allRestaurantDictionary objectForKey:@"RestaurantLogo"];
    }
    if(![ApplicationDelegate isValid:[allRestaurantDictionary objectForKey:@"RestaurantName"]] )
    {
        restaurantPromoObj.restaurant_Name=@"";
    }
    else
    {
        restaurantPromoObj.restaurant_Name=[allRestaurantDictionary objectForKey:@"RestaurantName"];
    }
    if(![ApplicationDelegate isValid:[allRestaurantDictionary objectForKey:@"RestaurantStatus"]] )
    {
        restaurantPromoObj.restaurant_Status=@"";
    }
    else
    {
        restaurantPromoObj.restaurant_Status=[allRestaurantDictionary objectForKey:@"RestaurantStatus"];
    }

    return restaurantPromoObj;
}

-(GiftVoucher *) getGiftVoucherDetailsFromDictionary:(NSMutableDictionary *)giftVoucherDictionary
{
    
    GiftVoucher  *voucherObj = [[GiftVoucher alloc] init];
    
    if(![ApplicationDelegate isValid:[giftVoucherDictionary objectForKey:@"GiftVoucherAmount"]] )
    {
        voucherObj.voucher_Amount=@"";
    }
    else
    {
        voucherObj.voucher_Amount=[giftVoucherDictionary objectForKey:@"GiftVoucherAmount"];
    }
    if(![ApplicationDelegate isValid:[giftVoucherDictionary objectForKey:@"GiftVoucherDescription"]] )
    {
        voucherObj.voucher_Description=@"";
    }
    else
    {
        voucherObj.voucher_Description=[giftVoucherDictionary objectForKey:@"GiftVoucherDescription"];
    }
    if(![ApplicationDelegate isValid:[giftVoucherDictionary objectForKey:@"GiftVoucherTitle"]] )
    {
        voucherObj.voucher_Title=@"";
    }
    else
    {
        voucherObj.voucher_Title=[giftVoucherDictionary objectForKey:@"GiftVoucherTitle"];
    }
    if(![ApplicationDelegate isValid:[giftVoucherDictionary objectForKey:@"GiftVoucher_Id"]] )
    {
        voucherObj.voucher_Id=@"";
    }
    else
    {
        voucherObj.voucher_Id=[giftVoucherDictionary objectForKey:@"GiftVoucher_Id"];
    }
    if(![ApplicationDelegate isValid:[giftVoucherDictionary objectForKey:@"GiftVoucher_Image"]] )
    {
        voucherObj.voucher_Image=@"";
    }
    else
    {
        voucherObj.voucher_Image=[giftVoucherDictionary objectForKey:@"GiftVoucher_Image"];
    }
    return voucherObj;
}

-(CustomerAddressDetails *) getCustAddressDetailsFromDictionary:(NSMutableDictionary *)custAddressDictionary
{
    CustomerAddressDetails  *custDetailObj = [[CustomerAddressDetails alloc] init];
    
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"AreaId"]] )
    {
        custDetailObj.areaID=@"";
    }
    else
    {
        custDetailObj.areaID=[custAddressDictionary objectForKey:@"AreaId"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"AreaName"]] )
    {
        custDetailObj.areaName=@"";
    }
    else
    {
        custDetailObj.areaName=[custAddressDictionary objectForKey:@"AreaName"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Birthday"]] )
    {
        custDetailObj.birthday=@"";
    }
    else
    {
        custDetailObj.birthday=[NSString stringWithFormat:@"%@",[custAddressDictionary objectForKey:@"Birthday"]];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Block"]] )
    {
        custDetailObj.block=@"";
    }
    else
    {
        custDetailObj.block=[custAddressDictionary objectForKey:@"Block"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"BuildingNo"]] )
    {
        custDetailObj.buildingNo=@"";
    }
    else
    {
        custDetailObj.buildingNo=[custAddressDictionary objectForKey:@"BuildingNo"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"ExtraDirections"]] )
    {
        custDetailObj.extraDirections=@"";
    }
    else
    {
        custDetailObj.extraDirections=[custAddressDictionary objectForKey:@"ExtraDirections"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"FirstName"]] )
    {
        custDetailObj.firstName=@"";
    }
    else
    {
        custDetailObj.firstName=[custAddressDictionary objectForKey:@"FirstName"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Floor"]] )
    {
        custDetailObj.floor=@"";
    }
    else
    {
        custDetailObj.floor=[custAddressDictionary objectForKey:@"Floor"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Gender"]] )
    {
        custDetailObj.gender=@"";
    }
    else
    {
        custDetailObj.gender=[custAddressDictionary objectForKey:@"Gender"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"HousePhone"]] )
    {
        custDetailObj.housePhone=@"";
    }
    else
    {
        custDetailObj.housePhone=[custAddressDictionary objectForKey:@"HousePhone"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Id"]] )
    {
        custDetailObj.addressID=@"";
    }
    else
    {
        custDetailObj.addressID=[custAddressDictionary objectForKey:@"Id"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"IsPrimary"]] )
    {
        custDetailObj.isPrimary=@"";
    }
    else
    {
        custDetailObj.isPrimary=[custAddressDictionary objectForKey:@"IsPrimary"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Judda"]] )
    {
        custDetailObj.judda=@"";
    }
    else
    {
        custDetailObj.judda=[custAddressDictionary objectForKey:@"Judda"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"LastName"]] )
    {
        custDetailObj.lastName=@"";
    }
    else
    {
        custDetailObj.lastName=[custAddressDictionary objectForKey:@"LastName"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Mobile"]] )
    {
        custDetailObj.mobile=@"";
    }
    else
    {
        custDetailObj.mobile=[custAddressDictionary objectForKey:@"Mobile"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"ProfileName"]] )
    {
        custDetailObj.profileName=@"";
    }
    else
    {
        custDetailObj.profileName=[custAddressDictionary objectForKey:@"ProfileName"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Street"]] )
    {
        custDetailObj.street=@"";
    }
    else
    {
        custDetailObj.street=[custAddressDictionary objectForKey:@"Street"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Suite"]] )
    {
        custDetailObj.suit=@"";
    }
    else
    {
        custDetailObj.suit=[custAddressDictionary objectForKey:@"Suite"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"Type"]] )
    {
        custDetailObj.type=@"";
    }
    else
    {
        custDetailObj.type=[custAddressDictionary objectForKey:@"Type"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"WorkPhone"]] )
    {
        custDetailObj.workPhone=@"";
    }
    else
    {
        custDetailObj.workPhone=[custAddressDictionary objectForKey:@"WorkPhone"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"company"]] )
    {
        custDetailObj.company=@"";
    }
    else
    {
        custDetailObj.company=[custAddressDictionary objectForKey:@"company"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"email"]] )
    {
        custDetailObj.email=@"";
    }
    else
    {
        custDetailObj.email=[custAddressDictionary objectForKey:@"email"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"occupation"]] )
    {
        custDetailObj.occupation=@"";
    }
    else
    {
        custDetailObj.occupation=[custAddressDictionary objectForKey:@"occupation"];
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"subscribed_to_newsletter"]] )
    {
        custDetailObj.subscribed_to_news=NO;
    }
    else
    {
        NSNumber* attendingObject = [custAddressDictionary objectForKey:@"subscribed_to_newsletter"];

        custDetailObj.subscribed_to_news=[attendingObject boolValue];
  
    }
    if(![ApplicationDelegate isValid:[custAddressDictionary objectForKey:@"subscribed_to_sms"]] )
    {
        custDetailObj.subscribed_to_sms=NO;
    }
    else
    {
        NSNumber* attendingObject = [custAddressDictionary objectForKey:@"subscribed_to_sms"];
        
        custDetailObj.subscribed_to_sms=[attendingObject boolValue];
    }
    
    return custDetailObj;
}

-(FavouriteDetails *) getFavouriteDetailsFromDictionary:(NSMutableDictionary *)favouriteDictionary
{
    
    FavouriteDetails  *favObj = [[FavouriteDetails alloc] init];
    
    if(![ApplicationDelegate isValid:[favouriteDictionary objectForKey:@"Description"]] )
    {
        favObj.fav_Description=@"";
    }
    else
    {
        favObj.fav_Description=[favouriteDictionary objectForKey:@"Description"];
    }
    if(![ApplicationDelegate isValid:[favouriteDictionary objectForKey:@"ItemId"]] )
    {
        favObj.itemID=@"";
    }
    else
    {
        favObj.itemID=[favouriteDictionary objectForKey:@"ItemId"];
    }
    if(![ApplicationDelegate isValid:[favouriteDictionary objectForKey:@"Name"]] )
    {
        favObj.name=@"";
    }
    else
    {
        favObj.name=[favouriteDictionary objectForKey:@"Name"];
    }
    if(![ApplicationDelegate isValid:[favouriteDictionary objectForKey:@"Price"]] )
    {
        favObj.price=@"";
    }
    else
    {
        favObj.price=[favouriteDictionary objectForKey:@"Price"];
    }
    if(![ApplicationDelegate isValid:[favouriteDictionary objectForKey:@"Status"]] )
    {
        favObj.status=@"";
    }
    else
    {
        favObj.status=[favouriteDictionary objectForKey:@"Status"];
    }
    if(![ApplicationDelegate isValid:[favouriteDictionary objectForKey:@"Thumbnail"]] )
    {
        favObj.thumbnail=@"";
    }
    else
    {
        favObj.thumbnail=[favouriteDictionary objectForKey:@"Thumbnail"];
    }
    return favObj;
}

-(MyOrderDeliveryDetail *)getOrderDeliveryDetailsFromDictionary:(NSMutableDictionary *)orderDeliveryDictionary
{
    MyOrderDeliveryDetail  *deliveryDetailObj = [[MyOrderDeliveryDetail alloc] init];
    
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"Date"]] )
    {
        deliveryDetailObj.order_Date=@"";
    }
    else
    {
        deliveryDetailObj.order_Date=[orderDeliveryDictionary objectForKey:@"Date"];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryAddressDirections"]] )
    {
        deliveryDetailObj.delivery_AddressDirection=@"";
    }
    else
    {
        deliveryDetailObj.delivery_AddressDirection=[orderDeliveryDictionary objectForKey:@"DeliveryAddressDirections"];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryAddressType"]] )
    {
        deliveryDetailObj.delivery_AddressType=@"";
    }
    else
    {
        deliveryDetailObj.delivery_AddressType=[orderDeliveryDictionary objectForKey:@"DeliveryAddressType"];
    }

    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryArea"]] )
    {
        deliveryDetailObj.delivery_Area=@"";
    }
    else
    {
        deliveryDetailObj.delivery_Area=[orderDeliveryDictionary objectForKey:@"DeliveryArea"];
    }
    
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryBlock"]] )
    {
        deliveryDetailObj.delivery_Block=@"";
    }
    else
    {
        deliveryDetailObj.delivery_Block=[orderDeliveryDictionary objectForKey:@"DeliveryBlock"];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryBuildingNo"]] )
    {
        deliveryDetailObj.delivery_BuildingNo=@"";
    }
    else
    {
        deliveryDetailObj.delivery_BuildingNo=[orderDeliveryDictionary objectForKey:@"DeliveryBuildingNo"];
    }
    
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryFloor"]] )
    {
        deliveryDetailObj.delivery_Floor=@"";
    }
    else
    {
        deliveryDetailObj.delivery_Floor=[orderDeliveryDictionary objectForKey:@"DeliveryFloor"];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryJudda"]] )
    {
        deliveryDetailObj.delivery_Judda=@"";
    }
    else
    {
        deliveryDetailObj.delivery_Judda=[orderDeliveryDictionary objectForKey:@"DeliveryJudda"];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryMobile"]] )
    {
        deliveryDetailObj.delivery_Mobile=@"";
    }
    else
    {
        deliveryDetailObj.delivery_Mobile=[orderDeliveryDictionary objectForKey:@"DeliveryMobile"];
    }
    
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliveryStreet"]] )
    {
        deliveryDetailObj.delivery_Street=@"";
    }
    else
    {
        deliveryDetailObj.delivery_Street=[orderDeliveryDictionary objectForKey:@"DeliveryStreet"];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"DeliverySuite"]] )
    {
        deliveryDetailObj.delivery_Suit=@"";
    }
    else
    {
        deliveryDetailObj.delivery_Suit=[orderDeliveryDictionary objectForKey:@"DeliverySuite"];
    }
    
    
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"PaidByCC"]] )
    {
        deliveryDetailObj.paid_ByCC=@"";
    }
    else
    {
        deliveryDetailObj.paid_ByCC=[NSString stringWithFormat:@"%@",[orderDeliveryDictionary objectForKey:@"PaidByCC"]];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"PaidByCash"]] )
    {
        deliveryDetailObj.paid_ByCash=@"";
    }
    else
    {
        deliveryDetailObj.paid_ByCash=[NSString stringWithFormat:@"%@",[orderDeliveryDictionary objectForKey:@"PaidByCash"]];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"PaidByKNET"]] )
    {
        deliveryDetailObj.paid_ByKNET=@"";
    }
    else
    {
        deliveryDetailObj.paid_ByKNET=[NSString stringWithFormat:@"%@",[orderDeliveryDictionary objectForKey:@"PaidByKNET"]];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"PaidByu3an"]] )
    {
        deliveryDetailObj.paid_Byu3an=@"";
    }
    else
    {
        deliveryDetailObj.paid_Byu3an=[NSString stringWithFormat:@"%@",[orderDeliveryDictionary objectForKey:@"PaidByu3an"]];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"Total"]] )
    {
        deliveryDetailObj.total=@"";
    }
    else
    {
        deliveryDetailObj.total=[NSString stringWithFormat:@"%@",[orderDeliveryDictionary objectForKey:@"Total"]];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"TransactionId"]] )
    {
        deliveryDetailObj.transactionID=@"";
    }
    else
    {
        deliveryDetailObj.transactionID=[NSString stringWithFormat:@"%@",[orderDeliveryDictionary objectForKey:@"TransactionId"]];
    }
    
    
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"UserIP"]] )
    {
        deliveryDetailObj.userIP=@"";
    }
    else
    {
        deliveryDetailObj.userIP=[orderDeliveryDictionary objectForKey:@"UserIP"];
    }
    if(![ApplicationDelegate isValid:[orderDeliveryDictionary objectForKey:@"Orders"]] )
    {
        deliveryDetailObj.orders_Array=[[NSMutableArray alloc] init];
    }
    else
    {
        deliveryDetailObj.orders_Array=[orderDeliveryDictionary objectForKey:@"Orders"];
    }
    return deliveryDetailObj;
}

-(MyOrderOrdersDetail *)getOrderDetailsFromDictionary:(NSMutableDictionary *)orderDetailDictionary
{
    MyOrderOrdersDetail  *orderDetailObj = [[MyOrderOrdersDetail alloc] init];
    
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"ChargesForU3an"]] )
    {
        orderDetailObj.charges_ForU3an=@"";
    }
    else
    {
        orderDetailObj.charges_ForU3an=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"ChargesForU3an"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"DeliveryCharges"]] )
    {
        orderDetailObj.delivery_Charges=@"";
    }
    else
    {
        orderDetailObj.delivery_Charges=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"DeliveryCharges"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"DeliveryTime"]] )
    {
        orderDetailObj.delivery_Time=@"";
    }
    else
    {
        orderDetailObj.delivery_Time=[orderDetailDictionary objectForKey:@"DeliveryTime"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"Discount"]] )
    {
        orderDetailObj.discount=@"";
    }
    else
    {
        orderDetailObj.discount=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"Discount"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"GeneralRequest"]] )
    {
        orderDetailObj.general_Request=@"";
    }
    else
    {
        orderDetailObj.general_Request=[orderDetailDictionary objectForKey:@"GeneralRequest"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"OrderId"]] )
    {
        orderDetailObj.order_ID=@"";
    }
    else
    {
        orderDetailObj.order_ID=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"OrderId"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"OrderMode"]] )
    {
        orderDetailObj.order_Mode=@"";
    }
    else
    {
        orderDetailObj.order_Mode=[orderDetailDictionary objectForKey:@"OrderMode"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"OrderSource"]] )
    {
        orderDetailObj.order_Source=@"";
    }
    else
    {
        orderDetailObj.order_Source=[orderDetailDictionary objectForKey:@"OrderSource"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"OrderStatus"]] )
    {
        orderDetailObj.order_Status=@"";
    }
    else
    {
        orderDetailObj.order_Status=[orderDetailDictionary objectForKey:@"OrderStatus"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"OrderType"]] )
    {
        orderDetailObj.order_Type=@"";
    }
    else
    {
        orderDetailObj.order_Type=[orderDetailDictionary objectForKey:@"OrderType"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"PaymentMethod"]] )
    {
        orderDetailObj.payment_Method=@"";
    }
    else
    {
        orderDetailObj.payment_Method=[orderDetailDictionary objectForKey:@"PaymentMethod"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"Rating"]] )
    {
        orderDetailObj.rating=@"";
    }
    else
    {
        orderDetailObj.rating=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"Rating"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"Restaurant"]] )
    {
        orderDetailObj.restaurant=@"";
    }
    else
    {
        orderDetailObj.restaurant=[orderDetailDictionary objectForKey:@"Restaurant"];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"RestaurantId"]] )
    {
        orderDetailObj.restaurant_ID=@"";
    }
    else
    {
        orderDetailObj.restaurant_ID=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"RestaurantId"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"SubTotal"]] )
    {
        orderDetailObj.subTotal=@"";
    }
    else
    {
        orderDetailObj.subTotal=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"SubTotal"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"Total"]] )
    {
        orderDetailObj.total=@"";
    }
    else
    {
        orderDetailObj.total=[NSString stringWithFormat:@"%@",[orderDetailDictionary objectForKey:@"Total"]];
    }
    if(![ApplicationDelegate isValid:[orderDetailDictionary objectForKey:@"Orders"]] )
    {
        orderDetailObj.orderItems_Array=[[NSMutableArray alloc] init];
    }
    else
    {
        orderDetailObj.orderItems_Array=[orderDetailDictionary objectForKey:@"Orders"];
    }
    return orderDetailObj;
}


-(MyOrderItemDetail *)getOrderItemDetailsFromDictionary:(NSMutableDictionary *)orderItemDetailDictionary
{
    MyOrderItemDetail  *orderItemDetailObj = [[MyOrderItemDetail alloc] init];
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"ItemId"]] )
    {
        orderItemDetailObj.item_ID=@"";
    }
    else
    {
        orderItemDetailObj.item_ID=[NSString stringWithFormat:@"%@",[orderItemDetailDictionary objectForKey:@"ItemId"]];
    }
    
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"ItemName"]] )
    {
        orderItemDetailObj.item_Name=@"";
    }
    else
    {
        orderItemDetailObj.item_Name=[orderItemDetailDictionary objectForKey:@"ItemName"];
    }
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"ItemPrice"]] )
    {
        orderItemDetailObj.item_Price=@"";
    }
    else
    {
        orderItemDetailObj.item_Price=[orderItemDetailDictionary objectForKey:@"ItemPrice"];
    }
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"ItemQuantity"]] )
    {
        orderItemDetailObj.item_Quantity=@"";
    }
    else
    {
        orderItemDetailObj.item_Quantity=[orderItemDetailDictionary objectForKey:@"ItemQuantity"];
    }
    
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"Rating"]] )
    {
        orderItemDetailObj.rating=@"";
    }
    else
    {
        orderItemDetailObj.rating=[NSString stringWithFormat:@"%@",[orderItemDetailDictionary objectForKey:@"Rating"]];
    }
    
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"Status"]] )
    {
        orderItemDetailObj.status=@"";
    }
    else
    {
        orderItemDetailObj.status=[orderItemDetailDictionary objectForKey:@"Status"];
    }
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"SpecialRequest"]] )
    {
        orderItemDetailObj.special_Request=@"";
    }
    else
    {
        orderItemDetailObj.special_Request=[orderItemDetailDictionary objectForKey:@"SpecialRequest"];
    }
    
    if(![ApplicationDelegate isValid:[orderItemDetailDictionary objectForKey:@"OrderItemChoices"]] )
    {
        orderItemDetailObj.itemChoice_Array=[[NSMutableArray alloc] init];
    }
    else
    {
        orderItemDetailObj.itemChoice_Array=[orderItemDetailDictionary objectForKey:@"OrderItemChoices"];
    }
    return orderItemDetailObj;

}

@end
