//
//  U3ANCreditViewController.h
//  U3AN
//
//  Created by Vipin on 20/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "BuyU3ANCreditVC.h"

@interface U3ANCreditViewController : UIViewController<ListSelectionProtocol>

@property (strong, nonatomic) DropDownView *creditDropDownObj;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleDescription;
@property (strong, nonatomic) IBOutlet UILabel *creditValue_Label;
@property (strong, nonatomic) IBOutlet UIButton *creditValueSelBttn;
@property (strong, nonatomic) IBOutlet UITextField *creditValueTxtBox;
@property (strong, nonatomic) IBOutlet UILabel *subDescLabel;
@property (strong, nonatomic) IBOutlet UILabel *subscribe2Label;
@property (strong, nonatomic) IBOutlet UIButton *subscribeTickBttn;
@property (strong, nonatomic) IBOutlet UIButton *submittBttn;
@property (strong, nonatomic) IBOutlet UIScrollView *u3anCreditScrollView;
@property (weak, nonatomic) IBOutlet UIButton *subscribeTCBtn;
- (IBAction)closeBtnActn:(UIButton *)sender;

- (IBAction)subscribeTickBttnAction:(UIButton *)sender;
- (IBAction)submitBttnAction:(UIButton *)sender;
- (IBAction)creditValueSelectBttnAction:(UIButton *)sender;
- (IBAction)TermsAndConditionAction:(UIButton *)sender;
- (IBAction)subscribeTCAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIWebView *TermsWebView;
@property (weak, nonatomic) IBOutlet UIButton *TermsandConditionbuttonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end
