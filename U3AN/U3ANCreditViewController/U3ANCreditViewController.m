//
//  U3ANCreditViewController.m
//  U3AN
//
//  Created by Vipin on 20/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "U3ANCreditViewController.h"

@interface U3ANCreditViewController (){
    NSMutableArray *creditIntervalListArray,*creditIntervalDropdownListArray;
    NSString *creditValue;
}

@end

@implementation U3ANCreditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    self.TermsWebView.hidden = YES;
    self.closeBtn.hidden = YES;
    self.subscribeTCBtn.selected = NO;
    [self.submittBttn setEnabled:NO];
    // Do any additional setup after loading the view from its nib.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self setupUI];
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.u3anCreditScrollView.layer.cornerRadius = 3.0;
    self.u3anCreditScrollView.layer.masksToBounds = YES;
    self.titleLabel.font = [UIFont fontWithName:@"BREESERIF-REGULAR" size:17.0f];
    self.titleDescription.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    self.creditValue_Label.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    self.subDescLabel.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
    self.subscribe2Label.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
   
    [self.subscribeTickBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.subscribeTickBttn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    [self.subscribeTCBtn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Unselected.png"] forState:UIControlStateNormal];
    [self.subscribeTCBtn setBackgroundImage:[UIImage imageNamed:@"Tickbox_Selected.png"] forState:UIControlStateSelected];
    self.TermsandConditionbuttonOutlet.titleLabel.font = [UIFont fontWithName:@"Tahoma" size:12.0f];
    //self.submittBttn.enabled=NO;
    self.u3anCreditScrollView.contentSize = CGSizeMake(self.u3anCreditScrollView.frame.size.width,  self.submittBttn.frame.origin.y+self.submittBttn.frame.size.height+30);
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    
    self.subscribeTickBttn.selected = ApplicationDelegate.newsLetterStatusFlag;
    
    [self getU3anCreditIntervals];
    
    
}
- (IBAction)closeBtnActn:(UIButton *)sender {
    self.TermsWebView.hidden = YES;
    self.closeBtn.hidden = YES;
}

- (IBAction)subscribeTickBttnAction:(UIButton *)sender
{
    self.subscribeTickBttn.selected = !self.subscribeTickBttn.selected;
    
    //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
    
    ApplicationDelegate.newsLetterStatusFlag = self.subscribeTickBttn.selected;
    [ApplicationDelegate saveNewsLetterStatusWithStatus:ApplicationDelegate.newsLetterStatusFlag];
    
    /*
    if (self.subscribeTickBttn.selected==YES) {
        self.submittBttn.enabled=YES;
    }
    else
    {
        self.submittBttn.enabled=NO;
    }
     */
}

- (IBAction)submitBttnAction:(UIButton *)sender
{
    if (creditValue.length>0)
    {
        [self buyU3ANCredit];
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Please select a credit value." title:@""];
    }
    
}

- (IBAction)creditValueSelectBttnAction:(UIButton *)sender {
    
    if (creditIntervalDropdownListArray.count==0)
    {
        [ApplicationDelegate showAlertWithMessage:@"No credit value available." title:@""];
        self.creditValueSelBttn.selected = NO;
    }
    else
    {
        self.creditValueSelBttn.selected = !self.creditValueSelBttn.selected;
        if (self.creditValueSelBttn.selected)
        {
            if (self.creditDropDownObj.view.superview) {
                [self.creditDropDownObj.view removeFromSuperview];
            }
            
            self.creditDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
            
            //////////// Customize the dropDown view Frame as your requirement
            
            self.creditDropDownObj.cellHeight = 35.0f;
            
            //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
            //            {
            self.creditDropDownObj.view.frame = CGRectMake((self.creditValueSelBttn.superview.frame.origin.x),(self.creditValueSelBttn.superview.frame.origin.y+self.creditValueSelBttn.superview.frame.size.height), (self.creditValueSelBttn.superview.frame.size.width), 0);
            
            self.creditDropDownObj.dropDownDelegate = self;
            self.creditDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:creditIntervalDropdownListArray];
            
            self.creditDropDownObj.view.layer.borderWidth = 0.1;
            self.creditDropDownObj.view.layer.shadowOpacity = 1.0;
            self.creditDropDownObj.view.layer.shadowRadius = 5.0;
            self.creditDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
            
            ///////Disabling Other part of the view except the DropDown View 	Container////////
            //        for (UIView *vw in self.restaurantDetailsContainerScroll.subviews) {
            //            if (vw!=self.self.cuisineSelectionButton) {
            //                [vw setUserInteractionEnabled:NO];
            //            }
            //        }
            //////////////
            self.creditDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
            self.creditDropDownObj.textLabelColor = [UIColor blackColor];
            self.creditDropDownObj.view.backgroundColor = [UIColor whiteColor];
            
            if (creditIntervalDropdownListArray.count>0)
            {
                [self.creditValueSelBttn.superview.superview addSubview:self.creditDropDownObj.view];
            }
            else
            {
                self.creditValueSelBttn.selected=NO;
            }
            
            /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
            CGFloat tableHT = 120.0f;
            
            if ((self.creditDropDownObj.cellHeight*self.creditDropDownObj.dataArray.count)<tableHT)
            {
                tableHT = (self.creditDropDownObj.cellHeight*self.creditDropDownObj.dataArray.count)+18.0f;
            }
            //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
            
            [UIView animateWithDuration:0.09f animations:^{
                self.creditDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
                self.creditDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
                self.creditDropDownObj.view.frame =
                CGRectMake(self.creditDropDownObj.view.frame.origin.x+2,
                           self.creditDropDownObj.view.frame.origin.y,
                           self.creditDropDownObj.view.frame.size.width-4,
                           tableHT);
            }];
        }
        else
        {
            self.creditDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
            self.creditDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
            
            ////////// ReEnabling the UserInteraction of all SubViews/////////
            for (UIView *vw in self.view.subviews) {
                [vw setUserInteractionEnabled:YES];
            }
            ///////////////////
            
            [UIView animateWithDuration:0.4f animations:^{
                self.creditDropDownObj.view.frame =
                CGRectMake(self.creditDropDownObj.view.frame.origin.x,
                           self.creditDropDownObj.view.frame.origin.y,
                           self.creditDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.creditDropDownObj.view removeFromSuperview];
                }
                
            }];
        }
    }
    
}

- (IBAction)TermsAndConditionAction:(UIButton *)sender{
    [self loadPageDetails];
    self.TermsWebView.hidden = NO;
    self.closeBtn.hidden = NO;
}

- (IBAction)subscribeTCAction:(UIButton *)sender {
    self.subscribeTCBtn.selected = !self.subscribeTCBtn.selected;
    if ([self.subscribeTCBtn isSelected]) {
        [self.submittBttn setEnabled:YES];
    }
    else{
        [self.submittBttn setEnabled:NO];
    }
    //self.subscribeTickBttn.selected = !self.subscribeTickBttn.selected;
}


-(void)getU3anCreditIntervals
{
    creditIntervalListArray=[[NSMutableArray alloc] init];
    creditIntervalDropdownListArray=[[NSMutableArray alloc] init];
    creditValue = @"";
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getU3ANCreditIntervalsWithDataDictionary:postData CompletionHandler:^(NSMutableArray *responseArray)
         {
             
             if ([ApplicationDelegate isValid:responseArray])
             {
                 if (responseArray.count>0)
                 {
                     for (int i=0; i<responseArray.count; i++)
                     {
                         if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                         {
                             if ([ApplicationDelegate isValid:[responseArray objectAtIndex:i]])
                             {
                                 if ([ApplicationDelegate isValid:[[responseArray objectAtIndex:i]objectForKey:@"Interval"]])
                                 {
                            [creditIntervalDropdownListArray addObject:[NSString stringWithFormat:@"%@KD",[[responseArray objectAtIndex:i]objectForKey:@"Interval"]]];
                                     
                                     [creditIntervalListArray addObject:[[responseArray objectAtIndex:i]objectForKey:@"Interval"]];
                                 }
                             }}
                     }
                     
                 }
             }
             if (creditIntervalListArray.count==0)
             {
                 [ApplicationDelegate showAlertWithMessage:kEMPTY_DATA_ERROR_MSG title:nil];
                 creditValue = @"";
             }
             
             else
             {
                 self.creditValueTxtBox.text =[creditIntervalDropdownListArray objectAtIndex:0];
                  creditValue=[creditIntervalListArray objectAtIndex:0];
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    return postDic;
}


-(void)buyU3ANCredit
{
    NSMutableDictionary *creditPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getCreditPostData]];
    if (creditPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine buyU3ANCreditWithDataDictionary:creditPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"status"]])
                 {
                     if ([[responseDictionary objectForKey:@"status"] isEqualToString:@"Success"])
                     {
                         
                         BuyU3ANCreditVC *buyCreditVC = [[BuyU3ANCreditVC alloc] initWithNibName:@"BuyU3ANCreditVC" bundle:nil]
                         ;
                         buyCreditVC.view.backgroundColor = [UIColor clearColor];
                         buyCreditVC.urlString=[responseDictionary objectForKey:@"Url"];
                         
                         if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[buyCreditVC class]])
                         {
                             [ApplicationDelegate.subHomeNav pushViewController:buyCreditVC animated:NO];
                         }
                         
                     }
                     else
                     {
                         [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}


-(NSMutableDictionary *)getCreditPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    if (creditValue.length>0)
    {
        [postDic setObject:creditValue forKey:@"amount"];
        //[postDic setObject:kLocal forKey:@"locale"];
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [postDic setObject:k_AR_Local forKey:@"locale"];
        }
        else
        {
            [postDic setObject:kLocal forKey:@"locale"];
        }
        [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    }
    
    return postDic;
}

#pragma mark - DROP-DOWN List Delegate Method



-(void)selectList:(int)selectedIndex
{
    [UIView animateWithDuration:0.4f animations:^{
        self.creditDropDownObj.view.frame =
        CGRectMake(self.creditDropDownObj.view.frame.origin.x,
                   self.creditDropDownObj.view.frame.origin.y,
                   self.creditDropDownObj.view.frame.size.width,
                   0);
    } completion:^(BOOL finished) {
        if (finished) {
            [self.creditDropDownObj.view removeFromSuperview];
        }
        
    }];
    
    self.creditValueSelBttn.selected = NO;
    self.creditValueTxtBox.text =[creditIntervalDropdownListArray objectAtIndex:selectedIndex];
    creditValue=[creditIntervalListArray objectAtIndex:selectedIndex];
}

-(void)loadPageDetails
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostDatas]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getPageDetailsWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     
                     
                     PageDetails *pageItem = [ApplicationDelegate.mapper getPageDetailFromDictionary:responseDictionary];
                     NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.TermsWebView.frame.size.width-10))];
                     
                     
                     NSString *formattedHTMLText = [NSString stringWithFormat:@"<html> \n"
                                                    "<head> \n"
                                                    "<style type=\"text/css\"> \n"
                                                    "body {font-family: \"%@\"; font-size: %@;} b {font-weight:bolder; font-size:%@; }\n"
                                                    "</style> \n"
                                                    "</head> \n"
                                                    "<body><div style=\"width: %@; word-wrap: break-word\">%@</div></body> \n"
                                                    "</html>", @"BREESERIF-REGULAR", [NSNumber numberWithInt:11],[NSNumber numberWithInt:11],widthVal, pageItem.page_Data];
                     [self.TermsWebView loadHTMLString:[NSString stringWithFormat:@"<div style=\"width: %@; word-wrap: break-word; color: #ffffff;\">%@</div>",widthVal,formattedHTMLText] baseURL:nil];
                     
                     NSLog(@"%@",formattedHTMLText);
                     
                     
                     //[self.termsWebView loadHTMLString:hTMLText baseURL:nil];
                 }
             }
             [ApplicationDelegate removeProgressHUD];
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
    
}



-(NSMutableDictionary *)getPostDatas
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:@"terms" forKey:@"PageName"];
    
    
    return postDic;
}

-(void)xibLoading
{
    NSString *nibName = @"U3ANCreditViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
